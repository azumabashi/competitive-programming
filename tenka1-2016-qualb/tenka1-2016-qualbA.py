def f(n):
    return (n ** 2 + 4) // 8


if __name__ == '__main__':
    print(f(f(f(20))))

