def main():
    length, repeat = map(int, input().split())
    sequence = list(map(int, input().split()))
    mod = 10 ** 9 + 7
    base_inversion = 0
    inter_inversion = 0
    count = [0] * 2001
    for i in range(length):
        count[sequence[i]] += 1
        for j in range(i + 1, length):
            if sequence[i] > sequence[j]:
                base_inversion += 1
    for i in range(length):
        inter_inversion += sum(count[:sequence[i]])
    print((base_inversion * repeat + inter_inversion * repeat * (repeat - 1) // 2) % mod)


if __name__ == '__main__':
    main()

