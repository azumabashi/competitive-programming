"""
aising2020-Dで，途中からは操作を再現して良いことの確認．
[1, max_n]の範囲で操作を行っても高々4回ということが確認できる．
"""


def main():
    max_n = 2 * 10 ** 5
    a_cnt = {}
    for i in range(1, max_n + 1):
        now = i
        cnt = 0
        while now:
            now %= bin(now).count("1")
            cnt += 1
        if cnt in a_cnt:
            a_cnt[cnt] += 1
        else:
            a_cnt[cnt] = 1
    print(a_cnt)


if __name__ == '__main__':
    main()

