def main():
    n = int(input())
    f = [0 for _ in range(200 ** 2 * 6)]
    for x in range(1, 111):
        for y in range(1, 111):
            for z in range(1, 111):
                f[x * x + y * y + z * z + x * y + y * z + z * x] += 1
    for i in range(1, n + 1):
        print(f[i])


if __name__ == '__main__':
    main()

