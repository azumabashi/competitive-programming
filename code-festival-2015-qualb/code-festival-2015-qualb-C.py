def main():
    rooms, reservations = map(int, input().split())
    capacity = sorted(list(map(int, input().split())))
    guest = sorted(list(map(int, input().split())))
    capacity_index = 0
    is_possible = True
    for i in range(reservations):
        while capacity_index < rooms and capacity[capacity_index] < guest[i]:
            capacity_index += 1
        if rooms <= capacity_index:
            is_possible = False
            break
        else:
            capacity_index += 1
    print("YES" if is_possible else "NO")


if __name__ == '__main__':
    main()

