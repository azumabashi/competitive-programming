def main():
    examinees, questions = map(int, input().split())
    answer = input().split()
    count = {}
    for a in answer:
        if a in count:
            count[a] += 1
        else:
            count[a] = 1
    count = sorted(count.items(), key=lambda x: x[1])
    print(count[-1][0] if count[-1][1] > examinees // 2 else "?")


if __name__ == '__main__':
    main()

