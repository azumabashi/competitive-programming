def main():
    # pypy3でAC．
    row, col = map(int, input().split())
    cookie = [list(map(int, input().split())) for _ in range(row)]
    answer = 0
    for i in range(pow(2, row)):
        count = [0 for _ in range(col)]
        now_ans = 0
        for j in range(row):
            if (i >> j) & 1:
                for k in range(col):
                    count[k] += 1 if cookie[j][k] == 0 else 0
            else:
                for k in range(col):
                    count[k] += cookie[j][k]
        for j in range(col):
            now_ans += max(count[j], row - count[j])
        answer = max(now_ans, answer)
    print(answer)


if __name__ == '__main__':
    main()

