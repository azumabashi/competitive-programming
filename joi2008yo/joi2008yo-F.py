# ワーシャルフロイド法解．TLEする．


def main():
    islands, query = map(int, input().split())
    max_val = 10 ** 9 + 7
    fare = [[max_val for _ in range(islands)] for _ in range(islands)]
    for i in range(islands):
        fare[i][i] = 0
    answer = []
    for _ in range(query):
        q = list(map(int, input().split()))
        q[1] -= 1
        q[2] -= 1
        if q[0]:
            fare[q[1]][q[2]] = min(q[3], fare[q[1]][q[2]])
            fare[q[2]][q[1]] = min(q[3], fare[q[1]][q[2]])
        else:
            for k in range(islands):
                for i in range(islands):
                    for j in range(islands):
                        fare[i][j] = min(fare[i][j], fare[i][k] + fare[k][j])
            if fare[q[1]][q[2]] == max_val:
                answer.append("-1")
            else:
                answer.append(str(fare[q[1]][q[2]]))
    print("\n".join(answer))


if __name__ == '__main__':
    main()

