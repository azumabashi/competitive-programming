def main():
    constellation_stars = int(input())
    constellation_x = []
    constellation_y = []
    for _ in range(constellation_stars):
        x, y = map(int, input().split())
        constellation_x.append(x)
        constellation_y.append(y)
    all_stars = int(input())
    all_x = []
    all_y = []
    for _ in range(all_stars):
        x, y = map(int, input().split())
        all_x.append(x)
        all_y.append(y)
    set_all_x = set(all_x)
    set_all_y = set(all_y)
    for i in range(all_stars):
        dx = all_x[i] - constellation_x[0]
        dy = all_y[i] - constellation_y[0]
        if all((constellation_x[j] + dx) in set_all_x for j in range(1, constellation_stars)):
            if all((constellation_y[j] + dy) in set_all_y for j in range(1, constellation_stars)):
                print(dx, dy)
                break


if __name__ == '__main__':
    main()
