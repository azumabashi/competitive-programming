#include<bits/stdc++.h>
using namespace std;

int main(){
    int N, K;
    cin >> N >> K;
    int type = 0;
    int a, b, c, d, e;
    int max_val = 1000000007;
    vector<vector<int>> fare(N, vector<int>(N, max_val));
    vector<int> ans(0);
    for (int i = 0; i < K; i++) {
        cin >> type;
        if (type == 1) {
            cin >> c >> d >> e;
            c--;
            d--;
            fare[c][d] = min(fare[c][d], e);
            fare[d][c] = min(fare[d][c], e);
        } else {
            cin >> a >> b;
            a--;
            b--;
            for (int l = 0; l < N; l++) {
                for (int j = 0; j < N; j++) {
                    for (int k = 0; k < N; k++) {
                        fare[j][k] = min(fare[j][k], fare[j][l] + fare[l][k]);
                    }
                }
            }
            if (fare[a][b] == max_val) {
                ans.push_back(-1);
            } else {
                ans.push_back(fare[a][b]);
            }
        }
    }
    for (auto a: ans) {
        cout << a << endl;
    }
    return 0;
}