def main():
    n = 1000 - int(input())
    answer = 0
    coin = [500, 100, 50, 10, 5, 1]
    for c in coin:
        answer += n // c
        n %= c
    print(answer)


if __name__ == '__main__':
    main()

