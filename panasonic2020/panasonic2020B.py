from math import ceil


def main():
    h, w = map(int, input().split())
    print(w * (h // 2) + ceil(w / 2) * (h % 2) if h > 1 and w > 1 else 1)


if __name__ == '__main__':
    main()

