def check(s):
    okyo_ind = s.find("okyo")
    ech_ind = s.rfind("ech")
    return 0 <= okyo_ind < ech_ind


def main():
    n = int(input())
    for _ in range(n):
        print("Yes" if check(input()) else "No")


if __name__ == '__main__':
    main()
