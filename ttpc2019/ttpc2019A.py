def main():
    a, b, t = map(int, input().split())
    interval = b - a
    ans = t
    while True:
        if (ans - b) % interval == 0:
            print(ans)
            break
        ans += 1


if __name__ == '__main__':
    main()
