def main():
    literal = input()
    cnt_left_brace = 0
    is_dict = False
    if literal == "{}":
        is_dict = True
    for l in literal:
        if l == "{":
            cnt_left_brace += 1
        elif l == "}":
            cnt_left_brace -= 1
        if cnt_left_brace == 1 and l == ":":
            is_dict = True
            break
    print("dict" if is_dict else "set")


if __name__ == '__main__':
    main()

