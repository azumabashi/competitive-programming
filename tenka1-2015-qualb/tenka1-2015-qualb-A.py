def main():
    answer = [0 for _ in range(20)]
    answer[0] = 100
    answer[1] = 100
    answer[2] = 200
    for i in range(3, 20):
        answer[i] = sum(answer[i - 3:i])
    print(answer[-1])


if __name__ == '__main__':
    main()

