def main():
    pencils = int(input())
    length = [float("inf")] + list(map(int, input().split())) + [float("inf")]
    answer = [1 for _ in range(pencils)]
    for i in range(pencils):
        answer[i] = min(length[i + 1], length[i])
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

