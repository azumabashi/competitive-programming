def main():
    cards = input()
    target_num = ['10', 'J', 'Q', 'K', 'A']
    target_mark = ["S", "H", "D", "C"]
    target_card = []
    for m in target_mark:
        now = []
        for n in target_num:
            now.append(m + n)
        target_card.append(now)
    left_limit = float("inf")
    left_mark = 0
    for i, target in enumerate(target_card):
        if all(t in cards for t in target):
            now_left = max(cards.index(t) for t in target)
            if now_left <= left_limit:
                left_limit = now_left
                left_mark = i
    answer = cards[:left_limit]
    for target in target_card[left_mark]:
        answer = answer.replace(target, "")
    print(answer if answer else "0")


if __name__ == '__main__':
    main()

