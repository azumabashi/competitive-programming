from bisect import bisect_left, bisect_right


def main():
    n = int(input())
    max_score = 10 ** 6
    students = [0 for _ in range(max_score + 1)]
    now_max = 0
    for _ in range(n):
        score = int(input())
        students[score] += 1
        now_max = max(now_max, score)
    for i in range(1, max_score + 1):
        students[i] += students[i - 1]
    query = int(input())
    for _ in range(query):
        capacity = int(input())
        if n - students[0] <= capacity or students[0] == n:
            print(0)
        elif capacity == 0:
            print(now_max + 1)
        else:
            print(bisect_left(students, n - capacity) + 1)


if __name__ == '__main__':
    main()

