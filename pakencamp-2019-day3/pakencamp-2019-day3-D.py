def main():
    n = int(input())
    grid = [input() for _ in range(5)]
    cnt = [{s: 0 for s in ["R", "B", "W", "#"]} for _ in range(n)]
    for j in range(5):
        for i in range(n):
            cnt[i][grid[j][i]] += 1
    dp = [[float("inf") for _ in range(3)] for _ in range(1100)]
    idx = {0: "R", 1: "B", 2: "W"}
    for i in range(3):
        dp[0][i] = 0
    for i in range(n):
        for j in range(3):
            for k in range(3):
                if j == k:
                    continue
                dp[i + 1][j] = min(dp[i][k] + 5 - cnt[i][idx[k]], dp[i + 1][j])
    print(min(dp[n]))


if __name__ == '__main__':
    main()
