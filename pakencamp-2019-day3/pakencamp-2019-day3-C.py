from itertools import product


def main():
    students, songs = map(int, input().split())
    score = [list(map(int, input().split())) for _ in range(students)]
    answer = 0
    all_songs = [i for i in range(songs)]
    for song in product(all_songs, repeat=2):
        now_answer = 0
        for i in range(students):
            now_answer += max(score[i][song[0] - 1], score[i][song[1] - 1])
        answer = max(answer, now_answer)
    print(answer)


if __name__ == '__main__':
    main()

