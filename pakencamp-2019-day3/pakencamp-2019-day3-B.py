def main():
    black = 0
    white = 0
    n = int(input())
    for _ in range(n):
        if input() == "black":
            black += 1
        else:
            white += 1
    print("black" if black > white else "white")


if __name__ == '__main__':
    main()

