def main():
    begin, end = map(int, input().split())
    print(end - begin + 1)


if __name__ == '__main__':
    main()

