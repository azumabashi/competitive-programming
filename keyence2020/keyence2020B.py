def main():
    robot = int(input())
    right_lim = 0
    answer = 1
    robot_info = []
    for _ in range(robot):
        coordinate, arm_length = map(int, input().split())
        robot_info.append([coordinate - arm_length, coordinate + arm_length])
    robot_info = sorted(robot_info, key=lambda x: x[1])
    for i in range(robot):
        if i == 0:
            right_lim = robot_info[i][1]
        elif right_lim <= robot_info[i][0]:
            right_lim = robot_info[i][1]
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

