from collections import deque


def main():
    queries, max_size = map(int, input().split())
    stack = deque([])
    stack_size = 0
    can_continue = True
    answer = []
    for _ in range(queries):
        q = input().split()
        if can_continue:
            if q[0] == "Push":
                if max_size < stack_size + int(q[1]):
                    answer.append("FULL")
                    can_continue = False
                elif len(stack) and stack[-1][0] == int(q[2]):
                    stack[-1][1] += int(q[1])
                else:
                    stack.append([int(q[2]), int(q[1])])
                stack_size += int(q[1])
            elif q[0] == "Pop":
                q[1] = int(q[1])
                stack_size -= q[1]
                if stack_size < 0:
                    answer.append("EMPTY")
                    can_continue = False
                else:
                    while q[1]:
                        now_delta = stack[-1][1]
                        stack[-1][1] -= min(stack[-1][1], q[1])
                        if stack[-1][1] == 0:
                            _ = stack.pop()
                        q[1] = max(0, q[1] - now_delta)
            elif q[0] == "Top":
                if len(stack) > 0:
                    answer.append(str(stack[-1][0]))
                else:
                    answer.append("EMPTY")
                    can_continue = False
            else:
                answer.append(str(stack_size))
    print("\n".join(answer))
    if can_continue:
        print("SAFE")


if __name__ == '__main__':
    main()

