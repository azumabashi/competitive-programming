def main():
    n = int(input())
    a = list(map(int, input().split()))
    small = [a[0]]
    diff = 1  # 1: increasing, 0: decreasing
    for i in range(1, n):
        if (a[i] < a[i - 1] and diff == 1) or (a[i] > a[i - 1] and diff == 0):
            small.append(a[i - 1])
            diff += 1
            diff %= 2
    small.append(a[-1])
    ans = 1000
    stock = 0
    for i, s in enumerate(small):
        if i % 2 == 0:
            stock += ans // s
            ans %= s
        else:
            ans += s * stock
            stock = 0
    if stock > 0:
        ans += stock * small[-1]
    print(max(1000, ans))


if __name__ == '__main__':
    main()

