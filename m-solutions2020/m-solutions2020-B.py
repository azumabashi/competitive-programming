def check(r, g, b):
    return g > r and b > g


def main():
    r, g, b = map(int, input().split())
    k = int(input())
    for i in range(k + 1):
        if check(r, g * pow(2, i), b * pow(2, k - i)):
            print("Yes")
            break
    else:
        print("No")

if __name__ == '__main__':
    main()

