def main():
    pillars = int(input())
    coordinates = [tuple(map(int, input().split())) for _ in range(pillars)]
    set_coordinates = set(coordinates)
    answer = 0
    for i in range(pillars):
        for j in range(i + 1, pillars):
            dx = coordinates[i][0] - coordinates[j][0]
            dy = coordinates[i][1] - coordinates[j][1]
            if (coordinates[i][0] + dy, coordinates[i][1] - dx) in set_coordinates and\
                    (coordinates[j][0] + dy, coordinates[j][1] - dx) in set_coordinates:
                answer = max(answer, dx ** 2 + dy ** 2)
    print(answer)


if __name__ == '__main__':
    main()

