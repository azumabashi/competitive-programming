def main():
    order = int(input())
    base = [i for i in range(1, 21)]
    answer = []
    for i in range(10):
        answer += base + base[::-1]
    print(answer[order - 1])


if __name__ == '__main__':
    main()

