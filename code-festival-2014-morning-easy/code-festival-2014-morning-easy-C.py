from heapq import heappush, heappop, heapify


def dijkstra(s, g):
    n = len(g)
    dist = [float("inf")] * n
    dist[s] = 0
    q = [(s, 0)]
    visited = [False] * n
    heapify(q)
    while q:
        now_v, now_d = heappop(q)
        if visited[now_v]:
            continue
        for next_v, next_l in g[now_v]:
            next_dist = dist[now_v] + next_l
            if dist[next_v] <= next_dist:
                continue
            dist[next_v] = next_dist
            heappush(q, (next_v, dist[next_v]))
    return dist


def main():
    n, m = map(int, input().split())
    s, t = map(lambda x: int(x) - 1, input().split())
    g = [[] for _ in range(n)]
    for _ in range(m):
        x, y, d = map(lambda x: int(x) - 1, input().split())
        d += 1
        g[x].append((y, d))
        g[y].append((x, d))
    dist_from_s = dijkstra(s, g)
    dist_from_t = dijkstra(t, g)
    for i in range(n):
        if i in [s, t]:
            continue
        if dist_from_s[i] == dist_from_t[i] and dist_from_s[i] != float("inf"):
            print(i + 1)
            break
    else:
        print(-1)


if __name__ == '__main__':
    main()
