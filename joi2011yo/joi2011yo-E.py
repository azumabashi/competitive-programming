from collections import deque


def main():
    height, width, cheese = map(int, input().split())
    target = [[] for _ in range(cheese + 1)]
    grid = []
    for i in range(height):
        now_row = list(input())
        for j in range(width):
            ind = -1
            if now_row[j] == "S":
                ind = 0
            elif now_row[j] not in ["X", "."]:
                ind = int(now_row[j])
            if ind != -1:
                target[ind] = [i, j]
        grid.append(now_row)
    delta = [[0, 1], [1, 0], [0, -1], [-1, 0]]

    def search(start, goal):
        dist = [[-1 for _ in range(width)] for _ in range(height)]
        dist[start[0]][start[1]] = 0
        q = deque([start])
        while q:
            y, x = q.popleft()
            for dy, dx in delta:
                new_y = y + dy
                new_x = x + dx
                if 0 <= new_y < height and 0 <= new_x < width and dist[new_y][new_x] < 0 and grid[new_y][new_x] != "X":
                    dist[new_y][new_x] = dist[y][x] + 1
                    if [new_y, new_x] != goal:
                        q.append([new_y, new_x])
        return dist[goal[0]][goal[1]]

    answer = 0
    for i in range(cheese):
        answer += search(target[i], target[i + 1])
    print(answer)


if __name__ == '__main__':
    main()

