from itertools import permutations


def main():
    n = int(input())
    k = int(input())
    card = [input() for _ in range(n)]
    all_num = set()
    for p in permutations(card, k):
        all_num.add("".join(p))
    print(len(all_num))


if __name__ == '__main__':
    main()

