def main():
    n = int(input())
    ans = 0
    for i in range(n + 1):
        ans += pow(5, i)
    print(ans)


if __name__ == '__main__':
    main()
