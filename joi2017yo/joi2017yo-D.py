def main():
    n, m = map(int, input().split())
    toy = [0] * n
    cnt = [[0 for _ in range(n + 10)] for _ in range(m)]
    for i in range(n):
        c = int(input()) - 1
        toy[i] = c
        cnt[c][i + 1] = 1
    for i in range(m):
        for j in range(1, n + 10):
            cnt[i][j] += cnt[i][j - 1]
    dp = [float("inf")] * (1 << m)
    dp[0] = 0
    ac = [0] * (1 << m)
    for i in range(1 << m):
        for j in range(m):
            if (i >> j) & 1:
                continue
            idx = i + (1 << j)
            ac[idx] = ac[i] + cnt[j][-1]
            dp[idx] = min(dp[idx], dp[i] + cnt[j][-1] - (cnt[j][ac[idx]] - cnt[j][ac[i]]))
    print(dp[(1 << m) - 1])


if __name__ == '__main__':
    main()
