def main():
    m, n = map(int, input().split())
    ans = 0
    for _ in range(m):
        ans = max(ans, sum(map(int, input().split())))
    print(ans)


if __name__ == '__main__':
    main()
