# from ABC067-C


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    for i in range(1, n):
        a[i] += a[i - 1]
    answer = float("inf")
    for i in range(n - 1):
        answer = min(answer, abs(a[-1] - 2 * a[i]))
    print(answer)


if __name__ == '__main__':
    main()

