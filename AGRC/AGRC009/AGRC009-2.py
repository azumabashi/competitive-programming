# from ARC086-C


def main():
    n, k = map(int, input().split())
    count = {}
    a = [int(x) for x in input().split()]
    for aa in a:
        if aa in count:
            count[aa] += 1
        else:
            count[aa] = 1
    count = sorted(count.items(), key=lambda x: x[1], reverse=True)
    print(sum(count[i][1] for i in range(k, len(count))))


if __name__ == '__main__':
    main()

