def main():
    h, w = map(int, input().split())
    n = int(input())
    a = [int(x) for x in input().split()]
    answer = [[0 for _ in range(w)] for _ in range(h)]
    dir = [[0, w, 1], [-1, -w - 1, -1]]
    count = 0
    color = 1
    for i in range(h):
        for j in range(dir[i % 2][0], dir[i % 2][1], dir[i % 2][2]):
            answer[i][j] = color
            count += 1
            if count == a[color - 1]:
                count = 0
                color += 1
    for ans in answer:
        print(" ".join(map(str, ans)))


if __name__ == '__main__':
    main()

