from collections import Counter


def main():
    n, p = map(int, input().split())
    f = []
    while p % 2 == 0:
        p //= 2
        f.append(2)
    tmp = 3
    while tmp * tmp <= p:
        if p % tmp == 0:
            p //= tmp
            f.append(tmp)
        else:
            tmp += 2
    if p != 1:
        f.append(p)
    f = Counter(f)
    f = sorted(f.items(), key=lambda x: x[1], reverse=True)
    answer = 1
    for ff, cnt in f:
        if cnt < n:
            break
        answer *= ff ** (cnt // n)
    print(answer)


if __name__ == '__main__':
    main()

