# from ARC069-C


def main():
    n, m = map(int, input().split())
    answer = min(n, m // 2)
    m -= 2 * answer
    answer += m // 4
    print(answer)


if __name__ == '__main__':
    main()

