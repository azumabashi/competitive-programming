# from ARC068-C


def main():
    x = int(input())
    answer = (x // 11) * 2
    if 1 <= x % 11 <= 6:
        answer += 1
    elif 7 <= x % 11:
        answer += 2
    print(answer)


if __name__ == '__main__':
    main()

