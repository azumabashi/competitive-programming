def main():
    word = input() + "  "
    answer = 0
    succession = 0
    i = 0
    while i < len(word) - 1:
        if word[i:i + 2] == "25":
            succession += 1
            i += 1
        elif succession > 0:
            answer += max(succession * (succession + 1) // 2, 1)
            succession = 0
        i += 1
    print(answer)


if __name__ == '__main__':
    main()

