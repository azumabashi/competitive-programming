def main():
    word = input()
    target = "keyence"
    target_length = len(target)
    answer = "NO"
    if word[:target_length] == target or word[-target_length:] == target:
        answer = "YES"
    else:
        for i in range(1, target_length):
            if word[:i] == target[:i] and word[-target_length + i:] == target[-target_length + i:]:
                answer = "YES"
                break
    print(answer)


if __name__ == '__main__':
    main()

