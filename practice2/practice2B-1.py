class BinaryIndexedTree:
    # 1-indexed
    def __init__(self, n):
        self.length = n
        self.tree = [0] * (n + 1)

    def add(self, idx, val):
        idx += 1
        while idx <= self.length:
            self.tree[idx] += val
            idx += idx & -idx

    def initialize(self, arr):
        for i in range(self.length):
            self.tree[i + 1] = arr[i]
        for i in range(1, self.length):
            idx = i + (i & -i)
            if idx > self.length:
                continue
            self.tree[idx] += self.tree[i]

    def sum_from_zero(self, idx):
        res = 0
        while idx > 0:
            res += self.tree[idx]
            idx -= idx & -idx
        return res

    def sum(self, left, right):
        return self.sum_from_zero(right) - self.sum_from_zero(left)


def main():
    n, q = map(int, input().split())
    a = list(map(int, input().split()))
    bit = BinaryIndexedTree(n)
    bit.initialize(a)
    for _ in range(q):
        t, x, y = map(int, input().split())
        if t:
            print(bit.sum(x, y))
        else:
            bit.add(x, y)


if __name__ == '__main__':
    main()
