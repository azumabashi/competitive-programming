from copy import deepcopy


def main():
    word = input()
    target = ["", "K", "I", "H", "", "B", "", "R", ""]
    index = [0, 4, 6, 8]
    for i in range(2 ** 4):
        now_target = deepcopy(target)
        bin_i = bin(i)[2:].zfill(4)
        for j in range(4):
            if bin_i[j] == "1":
                now_target[index[j]] = "A"
        if "".join(now_target) == word:
            print("YES")
            break
    else:
        print("NO")


if __name__ == '__main__':
    main()

