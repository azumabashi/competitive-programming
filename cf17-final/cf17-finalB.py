def main():
    word = input()
    count = {"a": 0, "b": 0, "c": 0}
    for w in word:
        count[w] += 1
    answer = list(count.values())
    answer.sort()
    print("YES" if (answer[0] == answer[1] and abs(answer[1] - answer[2]) <= 1) or
                   (answer[1] == answer[2] and abs(answer[0] - answer[1]) <= 1) else "NO")


if __name__ == '__main__':
    main()

