from collections import Counter
from copy import deepcopy


def solve(c_d):
    one = [24]
    all_d = []
    for i in range(13):
        if c_d[i] >= 3:
            return 0
        elif c_d[i] == 2:
            all_d.append(i)
            all_d.append(24 - i)
        elif (i == 0 or i == 12) and c_d[i] == 1:
            all_d.append(i)
        elif c_d[i] == 1:
            one.append(i)
    ans = 0
    for i in range(1 << len(one)):
        now = deepcopy(all_d)
        for j in range(len(one)):
            if (i >> j) & 1:
                now.append(one[j])
            else:
                now.append(24 - one[j])
        now_ans = float("inf")
        for j in range(len(now)):
            for k in range(j + 1, len(now)):
                now_ans = min(now_ans, abs(now[j] - now[k]))
        ans = max(ans, now_ans)
    return min(ans, 24 - ans)


def main():
    n = int(input())
    d = list(map(int, input().split()))
    d.append(0)
    print(solve(Counter(d)))


if __name__ == '__main__':
    main()
