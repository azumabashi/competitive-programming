def main():
    board_length = int(input())
    height = int(input())
    width = int(input())
    print((board_length - height + 1) * (board_length - width + 1))


if __name__ == '__main__':
    main()

