def main():
    length = int(input())
    lucky_num = input()
    answer = 0
    for i in range(1000):
        target = str(i).zfill(3)
        index = [-1, -1, -1]
        index[0] = lucky_num.find(target[0])
        if index[0] >= 0:
            index[1] = lucky_num.find(target[1], index[0] + 1)
            if index[1] > index[0]:
                index[2] = lucky_num.find(target[2], index[1] + 1)
                if index[2] > index[1]:
                    answer += 1
    print(answer)


if __name__ == '__main__':
    main()

