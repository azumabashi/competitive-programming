def main():
    target = int(input())
    answer = False
    for i in range(1, 1001):
        if i * 100 <= target <= i * 105:
            answer = True
            break
    print(1 if answer else 0)


if __name__ == '__main__':
    main()

