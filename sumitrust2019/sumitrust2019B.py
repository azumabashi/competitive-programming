from math import floor


def main():
    cost = int(input())
    for i in range(1, 50001):
        if floor(i * 1.08) == cost:
            print(i)
            break
    else:
        print(":(")


if __name__ == '__main__':
    main()

