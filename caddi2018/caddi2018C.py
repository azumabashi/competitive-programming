from collections import Counter


def main():
    number, product = map(int, input().split())
    prime_factors = []
    while product % 2 == 0:
        prime_factors.append(2)
        product //= 2
    i = 3
    while i * i <= product:
        if product % i == 0:
            product //= i
            prime_factors.append(i)
        else:
            i += 2
    if product != 1:
        prime_factors.append(product)
    prime_factors = Counter(prime_factors)
    answer = 1
    for p, n in prime_factors.items():
        if number <= n:
            answer *= pow(p, n // number)
    print(answer)


if __name__ == '__main__':
    main()

