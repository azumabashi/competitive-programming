def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    B = list(map(int, input().split(" ")))
    before = sum(A)
    for i in range(N):
        if A[i] >= B[i]:
            A[i] -= B[i]
            B[i] = 0
        else:
            B[i] -= A[i]
            A[i] = 0
            if A[i + 1] >= B[i]:
                A[i + 1] -= B[i]
                B[i] = 0
            else:
                B[i] -= A[i + 1]
                A[i + 1] = 0
    print(before - sum(A))


if __name__ == '__main__':
    main()