def main():
    N = int(input())
    P = list(map(int, input().split(" ")))
    order = 0
    for i in range(N):
        if P[i] != i + 1:
            order += 1
    if order == 2 or order == 0:
        print("YES")
    else:
        print("NO")


if __name__ == '__main__':
    main()