def main():
    n = int(input())
    initials = ["M", "A", "R", "C", "H"]
    person_name = {}
    for initial in initials:
        person_name[initial] = 0
    for _ in range(n):
        name = input()
        if name[0] in person_name:
            person_name[name[0]] += 1
    answer = 0
    for i in range(5):
        for j in range(i + 1, 5):
            for k in range(j + 1, 5):
                answer += person_name[initials[i]] * person_name[initials[j]] * person_name[initials[k]]
    print(answer)


if __name__ == '__main__':
    main()