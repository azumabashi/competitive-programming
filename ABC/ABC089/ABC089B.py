def main():
    num = int(input())
    colors = list(input().split(" "))
    for color in colors:
        if color == "Y":
            print("Four")
            break
    else:
        print("Three")


if __name__ == '__main__':
    main()