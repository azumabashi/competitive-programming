class UnionFind:
    def __init__(self, node):
        self.parent = [-1 for _ in range(node)]
        self.n = node

    def find(self, target):
        if self.parent[target] < 0:
            return target
        else:
            self.parent[target] = self.find(self.parent[target])
            return self.parent[target]

    def is_same(self, x, y):
        return self.find(x) == self.find(y)

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x == root_y:
            return
        if self.parent[root_x] > self.parent[root_y]:
            root_x, root_y = root_y, root_x
        self.parent[root_x] += self.parent[root_y]
        self.parent[root_y] = root_x

    def get_size(self, x):
        return -self.parent[self.find(x)]

    def get_root(self):
        return [i for i, root in enumerate(self.parent) if root < 0]

    def all_group_member(self):
        from collections import defaultdict
        group_member = defaultdict(list)
        for i in range(self.n):
            group_member[self.find(i)].append(i)
        return group_member


def solve():
    from collections import deque
    n, m = map(int, input().split())
    uf = UnionFind(n)
    g = [{} for _ in range(n)]
    if m == 0:
        return True
    elif n * (n - 1) // 2 < m:
        return False
    for _ in range(m):
        l, r, d = map(int, input().split())
        l -= 1
        r -= 1
        uf.union(l, r)
        g[r][l] = d
        g[l][r] = -d
    member = uf.all_group_member()
    seen = [False] * n
    idx = [0] * n
    for r, group in member.items():
        q = deque()
        q.append(r)
        while q:
            now = q.pop()
            seen[now] = True
            for nv, dist in g[now].items():
                if seen[nv]:
                    if idx[nv] - idx[now] != dist:
                        return False
                else:
                    idx[nv] = idx[now] + dist
                    q.append(nv)
    return True


def main():
    print("Yes" if solve() else "No")


if __name__ == '__main__':
    main()
