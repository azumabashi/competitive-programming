def main():
    width = int(input())
    candies = [list(map(int, input().split(" "))) for _ in range(2)]
    candies_sum = []
    for c in candies:
        tmp = [0] * width
        for i in range(width):
            if i == 0:
                tmp[0] = c[0]
            else:
                tmp[i] = tmp[i - 1] + c[i]
        candies_sum.append(tmp)
    answer = candies_sum[0][0] + candies_sum[1][-1]
    for i in range(1, width):
        answer = max(answer, candies_sum[0][i] + candies_sum[1][-1] - candies_sum[1][i - 1])
    print(answer)


if __name__ == '__main__':
    main()