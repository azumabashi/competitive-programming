def main():
    num = int(input())
    i = 2
    answer = len(str(num))
    while i * i <= num:
        if not num % i:
            answer = max(len(str(i)), len(str(num // i)))
        i += 1
    print(answer)


if __name__ == '__main__':
    main()

