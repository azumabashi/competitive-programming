def main():
    student_num, point_num = map(int, input().split())
    student_location = [tuple(map(int, input().split())) for _ in range(student_num)]
    point_location = [tuple(map(int, input().split())) for _ in range(point_num)]
    answer = [0] * student_num
    for i in range(student_num):
        min_distance = float("inf")
        for j in range(point_num):
            distance = abs(student_location[i][0] - point_location[j][0]) + abs(student_location[i][1] - point_location[j][1])
            if min_distance > distance:
                answer[i] = j + 1
                min_distance = distance
    for ans in answer:
        print(ans)


if __name__ == '__main__':
    main()

