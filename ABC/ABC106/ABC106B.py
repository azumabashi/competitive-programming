def main():
    limit = int(input())
    result = 0
    for i in range(1, limit + 1, 2):
        divisor = 2
        j = 2
        while j * j <= i:
            if i % j == 0:
                if i * i == j:
                    divisor += 1
                else:
                    divisor += 2
            j += 1
        if divisor == 8:
            result += 1
    print(result)


if __name__ == '__main__':
    main()