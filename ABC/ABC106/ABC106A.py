def main():
    height, width = map(int, input().split(" "))
    print((height - 1) * (width - 1))


if __name__ == '__main__':
    main()