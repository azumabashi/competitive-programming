def main():
    score = [int(input()) for _ in range(3)]
    sorted_score = sorted(score, reverse=True)
    for s in score:
        print(sorted_score.index(s) + 1)


if __name__ == '__main__':
    main()

