def main():
    string = input()
    operation_num = int(input())
    for _ in range(operation_num):
        left_limit, right_limit = map(int, input().split())
        string = string[:left_limit - 1] + string[right_limit - 1:left_limit - 2 if left_limit > 1 else None:-1] + string[right_limit:]
    print(string)


if __name__ == '__main__':
    main()

