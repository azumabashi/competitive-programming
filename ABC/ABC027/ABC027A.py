def main():
    length = sorted(list(map(int, input().split())))
    if length[0] == length[1]:
        print(length[2])
    else:
        print(length[0])


if __name__ == '__main__':
    main()

