def main():
    islands = int(input())
    population = list(map(int, input().split()))
    population_sum = sum(population)
    answer = 0
    if not population_sum % islands:
        target = population_sum // islands
        last_index = -1
        for i in range(islands):
            if sum(population[last_index + 1:i + 1]) == target * (i - last_index):
                answer += i - last_index - 1
                last_index = i
    else:
        answer = -1
    print(answer)


if __name__ == '__main__':
    main()

