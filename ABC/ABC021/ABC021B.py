def main():
    town_num = int(input())
    starting, ending = map(int, input().split())
    via_num = int(input())
    via = list(map(int, input().split()))
    via.append(starting)
    via.append(ending)
    print("YES" if via_num + 2 == len(set(via)) else "NO")


if __name__ == '__main__':
    main()

