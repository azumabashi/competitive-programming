# 最短経路は何通りか？のverifyに．
# DPして戻さなくてもリストrouteを持っておけば十分．
from collections import deque


def main():
    n = int(input())
    a, b = map(lambda x: int(x) - 1, input().split())
    graph = [[] for _ in range(n)]
    m = int(input())
    for _ in range(m):
        x, y = map(lambda x: int(x) - 1, input().split())
        graph[x].append(y)
        graph[y].append(x)
    q = deque([a])
    dist = [float("inf") for _ in range(n)]
    route = [0 for _ in range(n)]
    dist[a] = 0
    route[a] = 1
    while q:
        now = q.popleft()
        for next_v in graph[now]:
            if dist[next_v] == dist[now] + 1:
                route[next_v] += route[now]
            elif dist[next_v] > dist[now] + 1:
                dist[next_v] = dist[now] + 1
                route[next_v] += route[now]
                q.append(next_v)
    print(route[b] % (10 ** 9 + 7))


if __name__ == '__main__':
    main()

