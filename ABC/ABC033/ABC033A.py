def main():
    number = list(input())
    print("SAME" if len(set(number)) == 1 else "DIFFERENT")


if __name__ == '__main__':
    main()

