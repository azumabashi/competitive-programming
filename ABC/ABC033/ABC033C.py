def main():
    formula = input().split("+")
    answer = 0
    for f in formula:
        if "*" in f and "0" not in f:
            answer += 1
        elif "*" not in f and not f == "0":
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

