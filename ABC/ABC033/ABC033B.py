def main():
    town_num = int(input())
    name = []
    population = []
    for _ in range(town_num):
        n, p = input().split()
        name.append(n)
        population.append(int(p))
    new_population = sum(population)
    answer = "atcoder"
    for i in range(town_num):
        if population[i] * 2 > new_population:
            answer = name[i]
            break
    print(answer)


if __name__ == '__main__':
    main()

