def main():
    problem_num = int(input())
    solve_time = list(map(int, input().split()))
    drink_variation = int(input())
    drinks = [tuple(map(int, input().split())) for _ in range(drink_variation)]
    max_solve_time = sum(solve_time)
    for drink in drinks:
        print(max_solve_time - solve_time[drink[0] - 1] + drink[1])


if __name__ == '__main__':
    main()

