def main():
    a, b = input().split()
    print(int(a + b) * 2)


if __name__ == '__main__':
    main()

