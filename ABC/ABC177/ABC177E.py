from math import gcd
from functools import reduce


def pairwise_coprime(a):
    check = [set() for _ in range(10 ** 6 + 10)]
    for i, aa in enumerate(a):
        now = aa
        while now % 2 == 0:
            if len(check[2]) > 0 and i not in check[2]:
                return False
            now //= 2
            check[2].add(i)
        f = 3
        while f * f <= now:
            if now % f == 0:
                if len(check[f]) > 0 and i not in check[f]:
                    return False
                now //= f
                check[f].add(i)
            else:
                f += 2
        if now != 1:
            if len(check[now]) > 0 and i not in check[now]:
                return False
            check[now].add(i)
    return True


def main():
    n = int(input())
    a = list(map(int, input().split()))
    if pairwise_coprime(a):
        print("pairwise coprime")
    elif reduce(gcd, a) == 1:
        print("setwise coprime")
    else:
        print("not coprime")


if __name__ == '__main__':
    main()
