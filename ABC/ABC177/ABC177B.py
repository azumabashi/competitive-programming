def main():
    s = input()
    t = input()
    len_s = len(s)
    len_t = len(t)
    ans = float("inf")
    for i in range(len_s - len_t + 1):
        now = 0
        for j in range(len_t):
            if s[i + j] != t[j]:
                now += 1
        ans = min(ans, now)
    print(ans)


if __name__ == '__main__':
    main()
