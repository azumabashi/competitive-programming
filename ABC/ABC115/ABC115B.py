def main():
    N = int(input())
    prices = []
    for _ in range(N):
        prices.append(int(input()))
    prices.sort()
    cost = 0
    for i in range(N - 1):
        cost += prices[i]
    cost += prices[-1] // 2
    print(cost)


if __name__ == '__main__':
    main()