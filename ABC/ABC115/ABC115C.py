def main():
    N, K = map(int, input().split(" "))
    tree_height = []
    for _ in range(N):
        tree_height.append(int(input()))
    tree_height.sort()
    result = 10 ** 10
    for i in range(N - K + 1):
        result = min(result, tree_height[i + K - 1] - tree_height[i])
    print(result)


if __name__ == '__main__':
    main()