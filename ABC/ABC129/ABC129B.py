def main():
    N = int(input())
    W = list(map(int, input().split(" ")))
    diff = 10000000
    for T in range(1, N):
        a = W[:T]
        b = W[T:]
        diff = min(diff, abs(sum(a) - sum(b)))
    print(diff)


if __name__ == '__main__':
    main()