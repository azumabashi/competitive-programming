def main():
    N, M = map(int, input().split(" "))
    A = [-1]
    maximum = 0
    way = [1, 2]
    mod = 1000000007
    result = 1
    for i in range(M):
        a = int(input())
        if A[-1] + 1 == a:
            return 0
        else:
            maximum = max(maximum, a - A[-1])
            A.append(a)
    for i in range(2, maximum):
        way.append(way[i - 2] + way[i - 1])
    for j in range(1, M):
        result = result * (way[j] - way[j - 1] + 1) % mod
    return result


if __name__ == '__main__':
    print(main())