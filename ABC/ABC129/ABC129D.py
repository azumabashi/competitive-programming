from copy import deepcopy
# editorialをそのまま実装．TLE．


def set_value(l, i, j, di, dj, is_edge):
    if is_edge:
        return 1
    return l[i + di][j + dj] + 1


def main():
    height, width = map(int, input().split())
    grid = [input() for _ in range(height)]
    left = [[0 for _ in range(width)] for _ in range(height)]
    right = deepcopy(left)
    down = deepcopy(left)
    upper = deepcopy(left)
    for i in range(height):
        for j in range(width):
            if grid[i][j] == ".":
                upper[i][j] = set_value(upper, i, j, -1, 0, i == 0)
                left[i][j] = set_value(left, i, j, 0, -1, j == 0)
            if grid[-i - 1][-j - 1] == ".":
                down[-i - 1][-j - 1] = set_value(down, - i - 1,  - j - 1, 1, 0, i == -height + 1)
                right[-i - 1][-j - 1] = set_value(right, - i - 1,  - j - 1, 0, 1, j == -width + 1)
    answer = 0
    for i in range(height):
        for j in range(width):
            answer = max(answer, upper[i][j] + left[i][j] + down[i][j] + right[i][j] - 3)
    print(answer)


if __name__ == '__main__':
    main()

