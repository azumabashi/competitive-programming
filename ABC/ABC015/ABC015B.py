from math import ceil


def main():
    software = int(input())
    bugs = list(map(int, input().split()))
    software_with_bug = 0
    bug_sum = 0
    for bug in bugs:
        if bug > 0:
            bug_sum += bug
            software_with_bug += 1
    print(ceil(bug_sum / software_with_bug))


if __name__ == '__main__':
    main()

