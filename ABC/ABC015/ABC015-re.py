def main():
    # 再帰関数で解いてみる
    question, choice = map(int, input().split())
    value = [list(map(int, input().split())) for _ in range(question)]

    def search(index, result):
        if index == question:
            return result
        else:
            for j in range(choice):
                if not search(index + 1, result ^ value[index][j]):
                    return 0
        return 1

    print("Found" if not search(0, 0) else "Nothing")


if __name__ == '__main__':
    main()

