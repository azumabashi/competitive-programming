from itertools import product
# bit全探索風にやってみたかったけどTLE & WA -> DFSを使うようだ


def main():
    questions, choices = map(int, input().split())
    scores = [list(map(int, input().split())) for _ in range(questions)]
    answer = True
    c = [i for i in range(choices)]
    q = [i for i in range(questions)]
    for i in range(choices ** questions):
        score = 0
        for choice in list(product(c, repeat=choices)):
            for j, k in zip(c, q):
                score = score ^ scores[k][int(choice[j])]
        if score == 0:
            answer = False
    print("Nothing" if answer else "Found")


if __name__ == '__main__':
    main()

