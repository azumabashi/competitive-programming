def main():
    w = int(input())
    n, max_pic = map(int, input().split())
    width = [0 for _ in range(n)]
    value = [0 for _ in range(n)]
    for i in range(n):
        width[i], value[i] = map(int, input().split())
    dp = [[[0 for _ in range(w + 1)] for _ in range(max_pic + 1)] for _ in range(n + 1)]
    # dp[i][j][k] := i枚目までから, ちょうとj枚使ったとき, 幅がkになるような組の重要度の最大値.
    for i in range(n):
        for j in range(max_pic + 1):
            for k in range(w + 1):
                if k >= width[i] and j >= 1:
                    dp[i + 1][j][k] = max(dp[i][j][k], dp[i][j - 1][k - width[i]] + value[i])
                else:
                    dp[i + 1][j][k] = dp[i][j][k]
    print(dp[n][max_pic][w])


if __name__ == '__main__':
    main()
