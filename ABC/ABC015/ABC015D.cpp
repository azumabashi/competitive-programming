#include<bits/stdc++.h>
using namespace std;

int main() {
  int W, N, K;
  cin >> W >> N >> K;
  vector<int> width(N);
  vector<int> value(N);
  for (int i = 0; i < N; i++) {
    cin >> width[i] >> value[i];
  }
  vector<vector<vector<int>>> dp(N + 1, vector<vector<int>>(K + 1, vector<int>(W + 1)));
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < K + 1; j++) {
      for (int k = 0; k < W + 1; k++) {
	if (k >= width[i] and j >= 1) {
	  dp[i + 1][j][k] = max(dp[i][j][k], dp[i][j - 1][k - width[i]] + value[i]);
	} else {
	  dp[i + 1][j][k] = dp[i][j][k];
	}
      }
    }
  }
  cout << dp[N][K][W] << endl;
}
