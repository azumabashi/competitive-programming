def main():
    strings = [input() for _ in range(2)]
    print(strings[0] if len(strings[0]) > len(strings[1]) else strings[1])


if __name__ == '__main__':
    main()

