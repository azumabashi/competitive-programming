def main():
    lower_limit, upper_limit, divisible_by = map(int, input().split())
    lower_limit -= 1
    print(upper_limit // divisible_by - lower_limit // divisible_by)


if __name__ == '__main__':
    main()

