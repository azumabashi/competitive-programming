def main():
    box, limit = map(int, input().split())
    candies = list(map(int, input().split()))
    answer = 0
    for i in range(1, box):
        candy_sum = candies[i - 1] + candies[i]
        if candy_sum > limit:
            candies[i] -= min(candy_sum - limit, candies[i])
            answer += candy_sum - limit
    print(answer)


if __name__ == '__main__':
    main()

