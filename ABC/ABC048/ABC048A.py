def main():
    words = input().split()
    answer = ""
    for word in words:
        answer += word[0]
    print(answer)


if __name__ == '__main__':
    main()

