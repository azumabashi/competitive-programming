def main():
    exercising = int(input())
    answer = 1
    mod = 10 ** 9 + 7
    for i in range(2, exercising + 1):
        answer = (answer * i) % mod
    print(answer)


if __name__ == '__main__':
    main()

