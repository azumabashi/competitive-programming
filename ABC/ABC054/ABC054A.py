def main():
    alice, bob = map(int, input().split())
    answer = ""
    if alice == bob:
        answer = "Draw"
    elif alice == 1 or (alice > bob and not bob == 1):
        answer = "Alice"
    else:
        answer = "Bob"
    print(answer)


if __name__ == '__main__':
    main()

