def main():
    N = int(input())
    restaurants = []
    for i in range(N):
        s, p = input().split(" ")
        restaurants.append((s, -1 * int(p), i + 1))
    restaurants.sort()
    for r in restaurants:
        print(r[2])


if __name__ == '__main__':
    main()