def main():
    N, M = map(int, input().split(" "))
    S = []
    for _ in range(M):
        tmp = list(map(int, input().split(" ")))[1:]
        binary_s = 0
        for t in tmp:
            binary_s += 2 ** (t - 1)
        S.append(binary_s)
    P = list(map(int, input().split(" ")))
    result = 0
    for i in range(2 ** N):
        for s, p in zip(S, P):
            if not bin(s & i).count("1") % 2 == p:
                break
        else:
            result += 1
    print(result)


if __name__ == '__main__':
    main()