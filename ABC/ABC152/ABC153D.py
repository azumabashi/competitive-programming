def top_number(num):
    top = 0
    while num > 0:
        top = num
        num //= 10
    return top


def main():
    N = int(input())
    group = [[0] * 10 for _ in range(10)]
    result = 0
    for i in range(1, N + 1):
        group[top_number(i)][i % 10] += 1
    for i in range(10):
        for j in range(10):
            result += group[i][j] * group[j][i]
    print(result)


if __name__ == '__main__':
    main()
