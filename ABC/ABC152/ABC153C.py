def main():
    N = int(input())
    P = list(map(int, input().split(" ")))
    minimum = 10 ** 6
    result = 0
    for p in P:
        if minimum > p:
            result += 1
            minimum = p
    print(result)


if __name__ == '__main__':
    main()