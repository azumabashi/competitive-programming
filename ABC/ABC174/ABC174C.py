def main():
    k = int(input())
    n = 7
    if k % 2 == 0:
        print(-1)
    else:
        for i in range(10 ** 7):
            if n % k == 0:
                print(i + 1)
                break
            n = (10 * n + 7) % k
        else:
            print(-1)


if __name__ == '__main__':
    main()

