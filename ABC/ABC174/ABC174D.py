def main():
    n = int(input())
    c = input()
    w = c.count("W")
    ans = 0
    r = n - w
    for i in range(n):
        target = "R" if i < r else "W"
        if c[i] != target:
            ans += 1
    print(min(ans // 2, w, r) if 0 < w < n else 0)


if __name__ == '__main__':
    main()

