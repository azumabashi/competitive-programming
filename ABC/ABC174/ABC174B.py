def main():
    n, d = map(int, input().split())
    d *= d
    ans = 0
    for _ in range(n):
        x, y = map(int, input().split())
        if x ** 2 + y ** 2 <= d:
            ans += 1
    print(ans)


if __name__ == '__main__':
    main()

