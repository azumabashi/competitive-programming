# めぐる式二分探索が使えるように！！！


from math import ceil


def main():
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    a.sort()
    ng = 0
    ok = a[-1]
    while ok - ng > 1:
        mid = (ok + ng) // 2
        cnt = 0
        for i in range(n):
            cnt += (a[i] - 1) // mid
        if cnt <= k:
            ok = mid
        else:
            ng = mid
    print(ceil(ok) if k > 0 else a[-1])


if __name__ == '__main__':
    main()

