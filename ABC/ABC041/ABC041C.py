def make_list(string):
    return [int(string), 0]


def main():
    student_num = int(input())
    students = list(map(make_list, input().split()))
    for i in range(student_num):
        students[i][1] = i + 1
    students.sort(key=lambda x: x[0], reverse=True)
    for student in students:
        print(student[1])


if __name__ == '__main__':
    main()

