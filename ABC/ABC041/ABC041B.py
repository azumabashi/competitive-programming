def main():
    width, height, depth = map(int, input().split())
    mod = 10 ** 9 + 7
    print((width * height * depth) % mod)


if __name__ == '__main__':
    main()

