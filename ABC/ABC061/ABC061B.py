def main():
    operation, target = map(int, input().split())
    count = [0] * (10 ** 5 + 1)
    for _ in range(operation):
        a, b = map(int, input().split())
        count[a] += b
    for i in range(1, 10 ** 5 + 2):
        if target <= count[i]:
            print(i)
            break
        else:
            target -= count[i]


if __name__ == '__main__':
    main()

