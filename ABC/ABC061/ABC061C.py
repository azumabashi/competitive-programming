def main():
    cities, roads = map(int, input().split())
    count = [0] * cities
    for _ in range(roads):
        begin, end = map(int, input().split())
        count[begin - 1] += 1
        count[end - 1] += 1
    for i in range(cities):
        print(count[i])


if __name__ == '__main__':
    main()

