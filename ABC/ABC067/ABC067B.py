def main():
    sticks, join_num = map(int, input().split())
    length = list(map(int, input().split()))
    length.sort(reverse=True)
    print(sum(length[:join_num]))


if __name__ == '__main__':
    main()

