def main():
    card_num = int(input())
    cards = list(map(int, input().split()))
    answer = float("inf")
    num_sum = [cards[0]] * card_num
    for i in range(1, card_num):
        num_sum[i] = num_sum[i - 1] + cards[i]
    for i in range(card_num - 1):
        answer = min(answer, abs(2 * num_sum[i] - num_sum[-1]))
    print(answer)


if __name__ == '__main__':
    main()

