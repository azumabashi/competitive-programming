from collections import deque


def main():
    n = int(input())
    graph = [[] for _ in range(n)]
    for _ in range(n - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        graph[b].append(a)
        graph[a].append(b)

    def dfs(x):
        dist = [float("inf")] * n
        dist[x] = 0
        q = deque([x])
        while q:
            now = q.pop()
            for nv in graph[now]:
                if dist[nv] > dist[now] + 1:
                    dist[nv] = dist[now] + 1
                    q.append(nv)
        return dist

    dist_f = dfs(0)
    dist_s = dfs(n - 1)
    cnt = 0
    for i in range(n):
        if dist_f[i] <= dist_s[i]:
            cnt += 1
        else:
            cnt -= 1
    print("Fennec" if cnt > 0 else "Snuke")


if __name__ == '__main__':
    main()
