def main():
    number = int(input())
    ng = sorted([int(input()) for _ in range(3)])
    minus = [3, 2, 1]
    answer = True
    if number in ng:
        answer = False
    else:
        for _ in range(100):
            for m in minus:
                if number - m not in ng:
                    number -= m
                    break
            else:
                answer = False
                break
        if number > 0:
            answer = False
    print("YES" if answer else "NO")


if __name__ == '__main__':
    main()

