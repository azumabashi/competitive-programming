def main():
    month = int(input())
    print(month + 1 if month < 12 else month + 1 - 12)


if __name__ == '__main__':
    main()

