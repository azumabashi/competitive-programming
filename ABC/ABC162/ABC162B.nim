import strutils

var answer: int = 0
var n: int = readLine(stdin).parseInt
for i in 1..n:
    if i mod 3 != 0 and i mod 5 != 0:
        answer = answer + i
echo answer