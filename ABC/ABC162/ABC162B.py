def main():
    n = int(input())
    answer = 0
    for i in range(1, n + 1):
        if i % 3 and i % 5:
            answer += i
    print(answer)


if __name__ == '__main__':
    main()

