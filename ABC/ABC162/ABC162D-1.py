# editorialの解法


def main():
    n = int(input())
    s = input()
    answer = s.count("R") * s.count("G") * s.count("B")
    for i in range(n):
        for j in range(i + 1, n):
            k = 2 * j - i
            if n <= k:
                continue
            elif s[i] != s[j] and s[i] != s[k] and s[j] != s[k]:
                answer -= 1
    print(answer)


if __name__ == '__main__':
    main()

