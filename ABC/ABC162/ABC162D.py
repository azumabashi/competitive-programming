# コンテスト中のsubmission.
from bisect import bisect_right, bisect_left
from itertools import permutations


def main():
    n = int(input())
    s = input()
    ind = [[] for _ in range(3)]
    for i in range(n):
        if s[i] == "R":
            ind[0].append(i)
        elif s[i] == "G":
            ind[1].append(i)
        else:
            ind[2].append(i)
    length = [len(ind[i]) for i in range(3)]
    ind_set = [set(ind[i]) for i in range(3)]
    answer = 0
    for p1, p2, p3 in permutations([0, 1, 2]):
        for j in ind[p2]:
            i = bisect_right(ind[p1], j)
            k = bisect_left(ind[p3], j)
            if length[p1] < i or length[p3] <= k:
                continue
            answer += i * (length[p3] - k)
            for ii in ind[p1]:
                target = 2 * j - ii
                t_ind = bisect_left(ind[p3], target)
                if target in ind_set[p3] and k <= t_ind < length[p3]:
                    answer -= 1
    print(answer)


if __name__ == '__main__':
    main()

