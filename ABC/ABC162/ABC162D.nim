import strutils

var N: int = readLine(stdin).parseInt
var S: string = readLine(stdin)
var answer: int = S.count("R") * S.count("G") * S.count("B")
for i in 0..<N:
    for j in i + 1..<N:
        var k: int = 2 * j - i
        if N <= k:
            break
        elif S[i] != S[j] and S[j] != S[k] and S[k] != S[i]:
            answer = answer - 1
echo answer