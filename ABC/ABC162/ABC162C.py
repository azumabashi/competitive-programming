from math import gcd
from functools import reduce


def main():
    l = int(input())
    answer = 0
    for i in range(1, l + 1):
        for j in range(1, l + 1):
            for k in range(1, l + 1):
                answer += reduce(gcd, [i, j, k])
    print(answer)


if __name__ == '__main__':
    main()

