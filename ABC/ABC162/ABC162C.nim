import strutils, math

let k: int = readLine(stdin).parseInt
var answer: int = 0
for a in 1..k:
    for b in 1..k:
        for c in 1..k:
            answer = answer + gcd(gcd(a, b), c)
echo answer