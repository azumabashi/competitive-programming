def main():
    n, m = map(int, input().split())
    a = [int(x) for x in input().split()]
    answer = n - sum(a)
    print(answer if answer >= 0 else -1)


if __name__ == '__main__':
    main()

