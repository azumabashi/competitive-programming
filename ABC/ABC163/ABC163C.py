def main():
    n = int(input())
    a = [int(x) - 1 for x in input().split()]
    answer = {i: 0 for i in range(n)}
    for aa in a:
        answer[aa] += 1
    for i in range(n):
        print(answer[i])


if __name__ == '__main__':
    main()

