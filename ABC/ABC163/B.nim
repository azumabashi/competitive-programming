import sequtils,strutils

var N, M: int
(N, M) = map(readLine(stdin).split(" "), parseInt)
var A = map(readLine(stdin).split(" "), parseInt)
var require: int
for i in 0..<M:
    require = require + A[i]
if require <= N:
    echo N - require
else:
    echo -1