import strutils, sequtils, math

var
    N, K: int
    ans: int = 0
const MOD: int = 10 ^ 9 + 7
(N, K) = map(readLine(stdin).split(), parseInt)

proc sumFrom1toX(x: int): int = 
    return x * (x + 1) div 2

for i in K..N+1:
    ans += sumFrom1toX(N) - sumFrom1toX(N - i) - sumFrom1toX(i - 1) + 1
    ans = ans mod MOD
echo ans