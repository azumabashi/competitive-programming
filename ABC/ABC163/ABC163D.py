def sum_from_1_to_n(n):
    return n * (n + 1) // 2


def main():
    n, k = map(int, input().split())
    mod = 10 ** 9 + 7
    answer = 1  # N = Kのとき
    max_num = sum_from_1_to_n(n)
    for i in range(k, n + 1):
        answer += max_num - sum_from_1_to_n(n - i) - sum_from_1_to_n(i - 1) + 1
        answer %= mod
    print(answer)


if __name__ == '__main__':
    main()

