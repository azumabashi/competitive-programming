#include<bits/stdc++.h>
using namespace std;

int main(){
    int N, M;
    cin >> N >> M;
    vector<int> A(M);
    for (int i = 0; i < M; i++) cin >> A[i];
    cout << max(N - accumulate(A.begin(), A.end(), 0), -1) << endl;
    return 0;
}