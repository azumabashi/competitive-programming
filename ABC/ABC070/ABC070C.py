from functools import reduce
from math import gcd


def lcm(a, b):
    return a * b // gcd(a, b)


def main():
    clocks_num = int(input())
    time = [int(input()) for _ in range(clocks_num)]
    print(reduce(lcm, time))


if __name__ == '__main__':
    main()

