def main():
    n, kk = map(int, input().split())
    x = [0] * n
    y = [0] * n
    for i in range(n):
        x[i], y[i] = map(int, input().split())
    ans = 1 << 100
    for i in range(n):
        for j in range(n):
            for k in range(n):
                if i == k:
                    continue
                for l in range(n):
                    if j == l:
                        continue
                    cnt = 0
                    for m in range(n):
                        if x[i] <= x[m] <= x[k] and y[j] <= y[m] <= y[l]:
                            cnt += 1
                    if cnt >= kk:
                        ans = min(ans, abs(x[i] - x[k]) * abs(y[j] - y[l]))
    print(ans)


if __name__ == '__main__':
    main()

