def main():
    grid = []
    height, width = map(int, input().split())
    grid.append(["."] * (width + 2))
    search_directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]
    for _ in range(height):
        grid.append(["."] + list(input()) + ["."])
    grid.append(["."] * (width + 2))
    for i in range(1, height + 1):
        for j in range(1, width + 1):
            if grid[i][j] == ".":
                count_bomb = 0
                for dir in search_directions:
                    if grid[i + dir[0]][j + dir[1]] == "#":
                        count_bomb += 1
                grid[i][j] = str(count_bomb)
    for i in range(1, height + 1):
        print("".join(grid[i][1:-1]))


if __name__ == '__main__':
    main()

