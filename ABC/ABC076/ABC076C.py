def main():
    key_str = input()
    substring = input()
    answer = "z" * len(key_str)
    for i in range(len(key_str) - len(substring) + 1):
        if key_str[i] == substring[0] or key_str[i] == "?":
            for j in range(len(substring)):
                if not key_str[i + j] == substring[j] and not key_str[i + j] == "?":
                    break
            else:
                answer = min((key_str[:i] + substring + key_str[i + len(substring):]).replace("?", "a"), answer)
    print(answer if not answer == "z" * len(key_str) else "UNRESTORABLE")


if __name__ == '__main__':
    main()

