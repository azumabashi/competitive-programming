def main():
    lower_limit, upper_limit = map(int, input().split(" "))
    answer = 0
    for i in range(lower_limit, upper_limit + 1):
        num = str(i)
        if num[0] == num[-1] and num[1] == num[-2] and num[2] == num[-3]:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()