def main():
    N, T = map(int, input().split(" "))
    cost = 10000
    tle = 0
    for _ in range(N):
        c, t = map(int, input().split(" "))
        if t > T:
            tle += 1
            continue
        else:
            cost = min(cost, c)
    if tle == N:
        print("TLE")
    else:
        print(cost)


if __name__ == '__main__':
    main()