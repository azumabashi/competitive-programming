def main():
    N = int(input())
    X = []
    Y = []
    H = []
    for _ in range(N):
        x, y, h = map(int, input().split(" "))
        X.append(x)
        Y.append(y)
        H.append(h)
    result = 0
    for cx in range(101):
        for cy in range(101):
            for i in range(N):
                h = 0
                # wakaran
