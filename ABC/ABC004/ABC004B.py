def main():
    grid = [input().split() for _ in range(4)]
    answer = [[""] * 4 for _ in range(4)]
    for i in range(4):
        for j in range(4):
            answer[3-i][3-j] = grid[i][j]
    for ans in answer:
        print(" ".join(ans))


if __name__ == '__main__':
    main()

