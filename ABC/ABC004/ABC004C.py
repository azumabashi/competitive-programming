def main():
    index = int(input()) % 30
    numbers = [str(i) for i in range(1, 7)]
    for i in range(index):
        tmp = numbers[i % 5]
        numbers[i % 5] = numbers[i % 5 + 1]
        numbers[i % 5 + 1] = tmp
    print("".join(numbers))


if __name__ == '__main__':
    main()

