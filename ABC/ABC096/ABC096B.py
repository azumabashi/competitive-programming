def main():
    numbers = list(map(int, input().split(" ")))
    double_time = int(input())
    numbers.sort()
    for i in range(double_time):
        numbers[-1] *= 2
    print(sum(numbers))


if __name__ == '__main__':
    main()