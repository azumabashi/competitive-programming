def main():
    height, width = map(int, input().split(" "))
    grids = ["." * (width + 2)]
    for _ in range(height):
        grids.append("." + input() + ".")
    grids.append("." * (width + 2))
    is_possible = True
    for i in range(1, height + 1):
        for j in range(1, width + 1):
            if grids[i][j] == "#":
                if not (grids[i + 1][j] == "#" or grids[i - 1][j] == "#" or grids[i][j + 1] == "#" or grids[i][j - 1] == "#"):
                    is_possible = False
    if is_possible:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()