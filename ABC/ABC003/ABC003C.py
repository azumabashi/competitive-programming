def main():
    lectures, limit = map(int, input().split())
    lecturer_rating = sorted(list(map(int, input().split())))[lectures - limit:]
    answer = 0
    for i in range(limit):
        answer = (answer + lecturer_rating[i]) / 2
    print(answer)


if __name__ == '__main__':
    main()

