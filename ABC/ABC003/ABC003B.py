def main():
    target = input()
    cards = input()
    answer = True
    at_cards = ["a", "t", "c", "o", "d", "e", "r"]
    for i in range(len(target)):
        if cards[i] == target[i] or (cards[i] == "@" and target[i] in at_cards) or\
                (target[i] == "@" and cards[i] in at_cards):
            continue
        else:
            answer = False
    print("You can win" if answer else "You will lose")


if __name__ == '__main__':
    main()

