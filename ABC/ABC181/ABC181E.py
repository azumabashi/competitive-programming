# 要反省！！！！！


from bisect import bisect_right


def main():
    n, m = map(int, input().split())
    h = [int(k) for k in input().split()]
    h.sort()
    even_sum = [h[i + 1] - h[i] for i in range(n - 1)]
    odd_sum = even_sum[1::2] + [0]
    even_sum = [0] + even_sum[::2]
    for i in range(len(even_sum) - 1):
        even_sum[i + 1] += even_sum[i]
    for i in range(len(odd_sum) - 1, 0, -1):
        odd_sum[i - 1] += odd_sum[i]
    w = [int(k) for k in input().split()]
    ans = 1 <<100
    for ww in w:
        idx = bisect_right(h, ww)
        if idx % 2:
            idx -= 1
        ans = min(ans, even_sum[idx // 2] + odd_sum[idx // 2] + abs(h[idx] - ww))
    print(ans)


if __name__ == '__main__':
    main()
