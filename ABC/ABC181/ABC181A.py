def main():
    n = int(input())
    print("White" if (n + 1) % 2 else "Black")


if __name__ == '__main__':
    main()
