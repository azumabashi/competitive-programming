def main():
    n = int(input())
    ans = 0

    def f(n):
        return n * (n + 1) // 2

    for _ in range(n):
        a, b = map(int, input().split())
        ans += f(b) - f(a - 1)
    print(ans)


if __name__ == '__main__':
    main()
