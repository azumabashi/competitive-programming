from collections import Counter


def solve():
    x = list(input())
    if len(x) == 1:
        return not int(x[0]) % 8
    elif len(x) == 2:
        return not (int(x[0] + x[1]) % 8) or not(int(x[1] + x[0]) % 8)
    s = Counter(x)
    for i in range(100, 1000):
        if i % 8:
            continue
        t = Counter(list(str(i)))
        if all(s[y] >= z for y, z in t.items()):
            return True
    return False


def main():
    print("Yes" if solve() else "No")


if __name__ == '__main__':
    main()
