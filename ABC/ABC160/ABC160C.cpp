#include<bits/stdc++.h>
using namespace std;

int main() {
    int K, N;
    cin >> K >> N;
    vector<int> A(N);
    for (int i = 0; i < N ; i++) {
        cin >> A[i];
    }
    A.push_back(K + A[0]);
    int d = 0;
    for (int i = 0; i < N; i++) {
        d = max(d, A[i + 1] - A[i]);
    }
    cout << K - d << endl;
}