#include<bits/stdc++.h>
using namespace std;

int main() {
    int N, X, Y;
    cin >> N >> X >> Y;
    vector<int> count(N);
    for (int i = 1; i < N + 1; i++) {
        for (int j = i + 1; j < N + 1; j++) {
            int dist = min(j - i, abs(Y - j) + abs(X - i) + 1);
            count[dist]++;
        }
    }
    for (int i = 1; i < N; i++) {
        cout << count[i] << endl;
    }
    return 0;
}