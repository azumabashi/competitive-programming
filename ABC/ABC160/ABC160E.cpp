#include<bits/stdc++.h>
using namespace std;

int main() {
    int A, B, C, X, Y;
    cin >> X >> Y >> A >> B >> C;
    vector<int> p(A), q(B), r(C);
    for (int i = 0; i < A; i++) {
        cin >> p[i];
    }
    for (int i = 0; i < B; i++) {
        cin >> q[i];
    }
    for (int i = 0; i < C; i++) {
        cin >> r[i];
    }
    sort(p.rbegin(), p.rend());
    sort(q.rbegin(), q.rend());
    for (int i = 0; i < X; i++) {
        r.push_back(p[i]);
    }
    for (int i = 0; i < Y; i++) {
        r.push_back(q[i]);
    }
    sort(r.rbegin(), r.rend());
    long long answer = 0;
    for (int i = 0; i < X + Y; i++) {
        answer += r[i];
    }
    cout << answer << endl;
    return 0;
}