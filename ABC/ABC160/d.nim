import sequtils, strutils

var N, X, Y: int
(N, X, Y) = map(readLine(stdin).split(), parseInt)
var count_dist = newSeq[int](N)
var dist: int = 0
for i in 1..N:
    for j in i + 1..N:
        dist = min(j - i, abs(Y - j) + abs(X - i) + 1)
        count_dist[dist] += 1
for i in 1..<N:
    echo count_dist[i]