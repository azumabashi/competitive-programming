def main():
    n, x, y = map(int, input().split())
    count = {}
    for i in range(1, n):
        count[i] = 0
    for i in range(1, n + 1):
        for j in range(i + 1, n + 1):
            dist = min(j - i, 1 + abs(y - j) + abs(x - i))
            count[dist] += 1
    for i in range(1, n):
        print(count[i])


if __name__ == '__main__':
    main()

