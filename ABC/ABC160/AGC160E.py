def main():
    x, y, a, b, c = map(int, input().split())
    p = list(map(int, input().split()))
    q = list(map(int, input().split()))
    r = list(map(int, input().split()))
    p.sort()
    q.sort()
    for i in range(-1, -x - 1, -1):
        r.append(p[i])
    for i in range(-1, -y - 1, -1):
        r.append(q[i])
    r.sort(reverse=True)
    print(sum(r[:x + y]))


if __name__ == '__main__':
    main()

