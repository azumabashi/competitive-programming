def main():
    multiple_base, divisor, remainder = map(int, input().split())
    for i in range(multiple_base):
        if not (divisor * i + remainder) % multiple_base:
            print("YES")
            break
    else:
        print("NO")


if __name__ == '__main__':
    main()


