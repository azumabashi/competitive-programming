def main():
    A, B, C = input().split()
    print("YES" if A[-1] == B[0] and B[-1] == C[0] else "NO")


if __name__ == '__main__':
    main()

