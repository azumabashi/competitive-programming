def main():
    people_num, emit_time = map(int, input().split())
    times = list(map(int, input().split()))
    answer = 0
    for i in range(1, people_num):
        if times[i] - times[i - 1] > emit_time:
            answer += emit_time
        else:
            answer += times[i] - times[i - 1]
    answer += emit_time
    print(answer)


if __name__ == '__main__':
    main()

