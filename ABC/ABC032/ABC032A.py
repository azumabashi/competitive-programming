from math import gcd


def main():
    numbers = [int(input()) for _ in range(3)]
    answer = numbers[0] * numbers[1] // gcd(numbers[0], numbers[1])
    i = 1
    while answer * i < numbers[2]:
        i += 1
    print(answer * i)


if __name__ == '__main__':
    main()

