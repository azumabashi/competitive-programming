def main():
    string = input()
    length = int(input())
    password = []
    answer = 0
    if len(string) >= length:
        for i in range(len(string) - length + 1):
            if string[i:i + length] not in password:
                password.append(string[i:i + length])
        answer = len(password)
    print(answer)


if __name__ == '__main__':
    main()

