def main():
    length, product_lower_limit = map(int, input().split())
    sequence = [[1, 1]]
    is_zero_in_seq = False
    for i in range(length):
        num = int(input())
        if num == 0:
            is_zero_in_seq = True
        if sequence[-1][0] == 1 and num == 1:
            sequence[-1][1] += 1
        else:
            sequence.append([num, 1])
    answer = 0
    length = len(sequence)
    if length == 1:
        now_product = sequence[0][0]
        for i in range(1, sequence[0][1]):
            if product_lower_limit < now_product:
                break
            now_product *= sequence[0][0]
            answer += 1
    else:
        for i in range(length):
            j = i + 1
            now_product = sequence[i][0]
            add = 0 if sequence[i][0] > 1 else sequence[i][1] - 1
            if now_product <= product_lower_limit:
                while j < length:
                    if now_product * sequence[j][0] > product_lower_limit:
                        break
                    if sequence[j][0] == 1:
                        add += sequence[j][1] - 1
                    now_product *= sequence[j][0]
                    j += 1
                top = 0 if i > 0 else 1
                answer = max(answer, j - i + add - top)
    print(sum(sequence[i][1] for i in range(length)) - 1 if is_zero_in_seq else answer)


if __name__ == '__main__':
    main()

