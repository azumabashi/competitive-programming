def div_by_2(n):
    n = int(n)
    while n % 2 == 0:
        n //= 2
    return n


def main():
    n = int(input())
    a = list(map(div_by_2, input().split()))
    print(len(set(a)))


if __name__ == '__main__':
    main()

