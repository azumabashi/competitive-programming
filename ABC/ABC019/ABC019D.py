import sys


def main():
    n = int(input())

    def query(s):
        max_dist = 0
        res = 0
        for i in range(1, n + 1):
            if i == s:
                continue
            print("? {} {}".format(s, i))
            sys.stdout.flush()
            dist = int(input())
            if max_dist < dist:
                max_dist = dist
                res = i
        return res, max_dist

    v, _ = query(1)
    u, ans = query(v)
    print("!", ans)
    sys.exit()


if __name__ == '__main__':
    main()
