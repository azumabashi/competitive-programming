def main():
    string = input()
    now = string[0]
    count = 0
    answer = ""
    for s in string:
        if now == s:
            count += 1
        else:
            answer += now + str(count)
            count = 1
            now = s
    answer += now + str(count)
    print(answer)


if __name__ == '__main__':
    main()

