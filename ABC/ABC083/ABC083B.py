def get_digits_sum(num):
    result = 0
    while num > 0:
        result += num % 10
        num //= 10
    return result


def main():
    limit, digit_sum_lower, digit_sum_upper = map(int, input().split())
    answer = 0
    for i in range(1, limit + 1):
        if digit_sum_lower <= get_digits_sum(i) <= digit_sum_upper:
            answer += i
    print(answer)


if __name__ == '__main__':
    main()