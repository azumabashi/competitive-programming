def main():
    lower_limit, upper_limit = map(int, input().split())
    answer = 0
    while lower_limit <= upper_limit:
        lower_limit *= 2
        answer += 1
    print(answer)


if __name__ == '__main__':
    main()