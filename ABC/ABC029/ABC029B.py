def main():
    strings = [input() for _ in range(12)]
    answer = 0
    for s in strings:
        if "r" in s:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

