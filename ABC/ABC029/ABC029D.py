def main():
    n = int(input())
    ans = 0
    for i in range(9):
        ans += (n // pow(10, i + 1)) * pow(10, i)
        r = n % pow(10, i + 1)
        if pow(10, i) <= r < pow(10, i) * 2:
            ans += r - pow(10, i) + 1
        elif pow(10, i) * 2 <= r:
            ans += pow(10, i)
    print(ans)


if __name__ == '__main__':
    main()
