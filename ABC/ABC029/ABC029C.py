from itertools import product


def main():
    length = int(input())
    for password in list(product(["a", "b", "c"], repeat=length)):
        print("".join(password))


if __name__ == '__main__':
    main()

