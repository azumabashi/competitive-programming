def main():
    bus_stop, lines = map(int, input().split())
    max_val = 10 ** 10
    required_time = [[max_val for _ in range(bus_stop)] for _ in range(bus_stop)]
    for _ in range(lines):
        start, terminal, time = map(int, input().split())
        start -= 1
        terminal -= 1
        required_time[start][terminal] = time
        required_time[terminal][start] = time
    for k in range(bus_stop):
        for i in range(bus_stop):
            for j in range(bus_stop):
                required_time[i][j] = min(required_time[i][j], required_time[i][k] + required_time[k][j])
    answer = max_val
    for i in range(bus_stop):
        now_max = 0
        for j in range(bus_stop):
            if required_time[i][j] == max_val or i == j:
                continue
            else:
                now_max = max(now_max, required_time[i][j])
        answer = min(answer, now_max)
    print(answer)


if __name__ == '__main__':
    main()

