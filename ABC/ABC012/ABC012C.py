def main():
    number = int(input())
    all_sum = 0
    for i in range(1, 10):
        for j in range(1, 10):
            all_sum += i * j
    number = all_sum - number
    for i in range(1, 10):
        if not number % i and 1 <= number // i <= 9:
            print(i, "x", number // i)


if __name__ == '__main__':
    main()

