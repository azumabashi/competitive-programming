def main():
    seconds = int(input())
    answer = ["0", "0", "0"]
    for i in range(1, 4):
        answer[-i] = str(seconds % 60).zfill(2)
        seconds //= 60
    print(":".join(answer))


if __name__ == '__main__':
    main()

