def main():
    N = int(input())
    words = []
    success = True
    for _ in range(N):
        word = input()
        if word in words:
            success = False
        if not words == []:
            if not words[-1][-1] == word[0]:
                success = False
        words.append(word)
    if success:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()