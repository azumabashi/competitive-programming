def main():
    height, width = map(int, input().split(" "))
    coins = []
    change_history = []
    for _ in range(height):
        l = list(map(int, input().split(" ")))
        coins.append(l)
    coins.append([0] * width)
    for j in range(width):
        for i in range(height):
            if coins[i][j] % 2:
                coins[i][j] -= 1
                if not j == width - 2 and not i == height - 2:
                    coins[i + 1][0] += 1
                    change_history.append([i + 1, j + 1, i + 2, 1])
                else:
                    coins[i][j + 1] += 1
                    change_history.append([i + 1, j + 1, i + 1, j + 2])
    print(len(change_history))
    for change in change_history:
        print(change[0], change[1], change[2], change[3])


if __name__ == '__main__':
    main()