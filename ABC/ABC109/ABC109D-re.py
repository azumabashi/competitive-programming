def main():
    height, width = map(int, input().split())
    grid = [list(map(int, input().split())) for _ in range(height)]
    answer = []
    for i in range(height - 1):
        for j in range(width):
            if grid[i][j] % 2:
                grid[i + 1][j] += 1
                grid[i][j] -= 1
                answer.append([i + 1, j + 1, i + 2, j + 1])
    for j in range(width - 1):
        if grid[-1][j] % 2:
            grid[-1][j + 1] += 1
            grid[-1][j] -= 1
            answer.append([height, j + 1, height, j + 2])
    print(len(answer))
    for ans in answer:
        print(" ".join(map(str, ans)))


if __name__ == '__main__':
    main()

