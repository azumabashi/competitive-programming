from functools import reduce
from math import gcd


def main():
    N, X = map(int, input().split(" "))
    x = list(map(int, input().split(" ")))
    abs_x = []
    for i in x:
        abs_x.append(abs(X - i))
    print(reduce(gcd, abs_x))


if __name__ == '__main__':
    main()