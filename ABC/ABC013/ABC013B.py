def main():
    now_int = int(input())
    target = int(input())
    print(min(abs(now_int - target), abs(10 + min(now_int, target) - max(now_int, target))))


if __name__ == '__main__':
    main()

