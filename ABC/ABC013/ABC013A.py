def main():
    string = input()
    print(["A", "B", "C", "D", "E"].index(string) + 1)


if __name__ == '__main__':
    main()

