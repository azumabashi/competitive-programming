def main():
    N = int(input())
    v = list(map(int, input().split(" ")))
    while len(v) > 1:
        v.sort(reverse=True)
        a = v[-1]
        b = v[-2]
        v = v[:-2]
        v.append((a + b) / 2)
    print(v[0])


if __name__ == '__main__':
    main()