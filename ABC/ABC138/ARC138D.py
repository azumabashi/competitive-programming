import sys
from collections import deque


def main():
    vertex_num, operation_num = map(int, sys.stdin.buffer.readline().split())
    tree = [[] for _ in range(vertex_num)]
    for _ in range(vertex_num - 1):
        a, b = map(lambda x: int(x) - 1, sys.stdin.buffer.readline().split())
        tree[a].append(b)
        tree[b].append(a)
    answer = [0 for _ in range(vertex_num)]
    checked = [True for _ in range(vertex_num)]
    checked[0] = False
    for _ in range(operation_num):
        p, x = map(int, sys.stdin.buffer.readline().split())
        answer[p - 1] += x
    q = deque([0])
    empty_deque = deque()
    while not q == empty_deque:
        now = q.popleft()
        for next_vertex in tree[now]:
            if checked[next_vertex]:
                checked[next_vertex] = False
                answer[next_vertex] += answer[now]
                q.append(next_vertex)
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

