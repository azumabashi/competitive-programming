def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    result = 0
    for a in A:
        result += 1 / a
    print(1 / result)


if __name__ == '__main__':
    main()