def main():
    staying_days, first_night, first_cost, latter_cost = [int(input()) for _ in range(4)]
    print(min(staying_days, first_night) * first_cost + max(0, staying_days - first_night) * latter_cost)


if __name__ == '__main__':
    main()

