from string import ascii_lowercase


def main():
    string = input()
    count = {}
    for lowercase in ascii_lowercase:
        count[lowercase] = 0
    for s in string:
        count[s] += 1
    answer = True
    for c in count.values():
        if c % 2:
            answer = False
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()

