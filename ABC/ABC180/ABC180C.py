def main():
    n = int(input())
    ans = []
    i = 1
    while i * i <= n:
        if n % i == 0:
            ans.append(i)
            if i * i < n:
                ans.append(n // i)
        i += 1
    ans.sort()
    for a in ans:
        print(a)


if __name__ == '__main__':
    main()
