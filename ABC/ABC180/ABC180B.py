from decimal import Decimal


def main():
    n = int(input())
    x = [int(k) for k in input().split()]
    print(sum([abs(k) for k in x]))
    print(Decimal(sum(k ** 2 for k in x)) ** Decimal("0.5"))
    print(max([abs(k) for k in x]))


if __name__ == '__main__':
    main()
