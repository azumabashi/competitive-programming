def main():
    n = int(input())
    info = [[int(t) for t in input().split()] for _ in range(n)]
    dist = [[0 for _ in range(n)] for _ in range(n)]
    # dist[i][j] := 街iから街jまでの距離
    for i in range(n):
        for j in range(n):
            dist[i][j] = abs(info[i][0] - info[j][0]) + abs(info[i][1] - info[j][1]) + max(0, info[j][2] - info[i][2])
    dp = [[float("inf") for _ in range(n)] for _ in range(1 << n)]
    dp[0][0] = 0
    for i in range(1 << n):
        for j in range(n):
            for k in range(n):
                if i & (1 << k):
                    continue
                dp[i + (1 << k)][k] = min(dp[i][j] + dist[j][k], dp[i + (1 << k)][k])
    print(dp[(1 << n) - 1][0])


if __name__ == '__main__':
    main()
