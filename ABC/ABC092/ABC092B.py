def main():
    members = int(input())
    days, lest = map(int, input().split(" "))
    eaten_chocolate = 0
    for _ in range(members):
        eaten_chocolate += (1 + (days - 1) // int(input()))
    print(eaten_chocolate + lest)


if __name__ == '__main__':
    main()