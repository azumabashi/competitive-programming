def main():
    w, b = map(lambda x: int(x) - 1, input().split())
    ans = []
    for _ in range(50):
        ans.append(["#"] * 100)
    for _ in range(50):
        ans.append(["."] * 100)
    for i in range(0, 49, 2):
        if not w:
            break
        for j in range(0, 100, 2):
            ans[i][j] = "."
            w -= 1
            if not w:
                break
    for i in range(51, 100, 2):
        if not b:
            break
        for j in range(0, 100, 2):
            ans[i][j] = "#"
            b -= 1
            if not b:
                break
    print(100, 100)
    for a in ans:
        print("".join(a))


if __name__ == '__main__':
    main()
