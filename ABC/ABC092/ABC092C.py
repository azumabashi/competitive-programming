def main():
    spots_number = int(input())
    spots = [0] + list(map(int, input().split(" "))) + [0]
    visit_time = [0] * (spots_number + 2)
    for i in range(1, spots_number + 1):
        visit_time[i] = visit_time[i - 1] + abs(spots[i] - spots[i - 1])
    visit_time[-1] = visit_time[-2] + abs(spots[-2])
    for i in range(1, spots_number + 1):
        print(visit_time[i - 1] + visit_time[-1] - visit_time[i + 1] + abs(spots[i + 1] - spots[i - 1]))


if __name__ == '__main__':
    main()