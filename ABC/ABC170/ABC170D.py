def main():
    n = int(input())
    a = list(map(int, input().split()))
    a.sort()
    answer = n
    chk = [0 for _ in range(a[-1] + 1)]
    for aa in a:
        chk[aa] += 1
    if a[0] == 1:
        answer = 0 if chk[1] - 1 else 1
    else:
        for i in range(a[-1] + 1):
            if chk[i]:
                if chk[i] - 1:
                    answer -= chk[i]
                for j in range(2 * i, a[-1] + 1, i):
                    answer -= chk[j]
                    chk[j] = 0
        answer = max(answer, 0)
    print(answer)


if __name__ == '__main__':
    main()

