#include<bits/stdc++.h>
using namespace std;
#define rep(i, n) for (int i = 0; i < (int)(n); i++)

int main(){
  int n, q;
  cin >> n >> q;
  int max_num = 200000;
  vector<int> A(n), B(n);
  rep(i, n){
    cin >> A[i] >> B[i];
    B[i]--;
  }
  vector<multiset<int>> each_rating(max_num);
  multiset<int> res;
  rep(i, n) each_rating[B[i]].insert(A[i]);
  rep(i, max_num) if(each_rating[i].size()) res.insert(*each_rating[i].rbegin());
  int c, d;
  rep(i, q) {
    cin >> c >> d;
    c--;
    d--;
    int before = B[c];
    B[c] = d;

    // delete c from before
    if (each_rating[before].size()) res.erase(res.find(*each_rating[before].rbegin()));
    each_rating[before].erase(each_rating[before].find(A[c]));
    if (each_rating[before].size()) res.insert(*each_rating[before].rbegin());

    // add c in d
    if (each_rating[d].size()) res.erase(res.find(*each_rating[d].rbegin()));
    each_rating[d].insert(A[c]);
    if (each_rating[d].size()) res.insert(*each_rating[d].rbegin());

    int ans = *res.begin();
    cout << ans << endl;
  }
  return 0;
}
