def main():
    x, n = map(int, input().split())
    p = set(map(int, input().split()))
    diff = float("inf")
    answer = 0
    for i in range(102):
        if i not in p:
            if abs(i - x) < diff:
                diff = abs(i - x)
                answer = i
    print(answer)


if __name__ == '__main__':
    main()

