import heapq


class HeapDict:
    def __init__(self):
        self.h = []
        self.d = {}

    def insert(self, x):
        heapq.heappush(self.h, x)
        if x not in self.d:
            self.d[x] = 1
        else:
            self.d[x] += 1

    def erase(self, x):
        self.d[x] -= 1
        while len(self.h) != 0:
            val = heapq.heappop(self.h)
            if self.d[val]:
                heapq.heappush(self.h, val)
                break

    def is_exist(self, x):
        return x in self.d and self.d[x] != 0

    def get_min(self):
        return self.h[0]

    def size(self):
        return len(self.h)


def main():
    n, q = map(int, input().split())
    a = [0] * n
    b = [0] * n
    inf = 2 * 10 ** 5
    for i in range(n):
        a[i], b[i] = map(int, input().split())
        b[i] -= 1
    each_rating = [HeapDict() for _ in range(inf)]
    res = HeapDict()
    for i in range(n):
        each_rating[b[i]].insert(-a[i])
    for i in range(inf):
        if each_rating[i].size():
            res.insert(-each_rating[i].get_min())
    for _ in range(q):
        c, d = map(int, input().split())
        c -= 1
        d -= 1
        before = b[c]
        b[c] = d

        # delete c from before
        res.erase(-each_rating[before].get_min())
        each_rating[before].erase(-a[c])
        if each_rating[before].size():
            res.insert(-each_rating[before].get_min())

        # add c into d
        if each_rating[d].size():
            res.erase(-each_rating[d].get_min())
        each_rating[d].insert(-a[c])
        res.insert(-each_rating[d].get_min())

        print(res.get_min())


if __name__ == '__main__':
    main()
