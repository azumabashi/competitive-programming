def main():
    squares, tolls_number, start = map(int, input().split(" "))
    tolls = list(map(int, input().split(" ")))
    first_toll_index = 0
    for i in range(tolls_number):
        if tolls[i] > start:
            first_toll_index = i
            break
    print(min(len(tolls[:first_toll_index]), len(tolls[first_toll_index:])))


if __name__ == '__main__':
    main()