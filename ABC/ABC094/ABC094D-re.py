def main():
    n = int(input())
    numbers = list(map(int, input().split(" ")))
    numbers.sort()
    if numbers[-1] % 2:
        target = (numbers[-1] + 1) // 2
    else:
        target = numbers[-1] // 2
    answer = 0
    print(target)
    for i in range(n):
        if target <= numbers[i]:
            print(numbers[i - 1], numbers[i])
            if i == 0:
                answer = numbers[0]
                break
            elif abs(target - numbers[i]) < abs(target - numbers[i - 1]):
                answer = numbers[i]
                break
            else:
                answer = numbers[i - 1]
                break
    print(numbers[-1], answer)


if __name__ == '__main__':
    main()