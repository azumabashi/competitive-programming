def main():
    N = int(input())
    numbers = list(map(int, input().split(" ")))
    sorted_numbers = sorted(numbers)
    medians = [sorted_numbers[N // 2], sorted_numbers[N // 2 - 1]]
    # numbers.sort()
    for number in numbers:
        if number <= medians[1]:
            print(medians[0])
        else:
            print(medians[1])


if __name__ == '__main__':
    main()