def main():
    n = int(input())
    numbers = list(map(int, input().split(" ")))
    numbers.sort()
    if n > 2:
        # 二分探索試したけどsortして順にfor文で見てみてもO(n)だから間に合いそう?
        if numbers[-1] % 2:
            target = (numbers[-1] - 1) // 2
        else:
            target = numbers[-1] // 2
        upper = n - 1
        lower = 0
        print(numbers)
        while lower + 1 < upper:
            print(lower, upper, numbers[(lower + upper) // 2])
            if numbers[(lower + upper) // 2] < target:
                lower = (lower + upper) // 2
            else:
                upper = (lower + upper) // 2
        print(numbers[-1], numbers[upper])
    else:
        print(numbers[1], numbers[0])


if __name__ == '__main__':
    main()
