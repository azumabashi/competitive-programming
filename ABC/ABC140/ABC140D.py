def main():
    length, operation = map(int, input().split())
    direction = list(input())
    before = ""
    inverse = {"L": "R", "R": "L"}
    change = 0
    section = 0
    answer = 0
    for i in range(length):
        if not before == direction[i]:
            before = direction[i]
            section += 1
            if section % 2:
                change += 1
        if section % 2 == 0 and change <= operation:
            direction[i] = inverse[direction[i]]
    for i in range(1, length):
        if direction[i] == direction[i - 1]:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

