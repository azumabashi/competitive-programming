from math import floor


def main():
    N, M = map(int, input().split(" "))
    A = list(map(int, input().split(" ")))
    A.sort(reverse=True)
    while M > 0:
        for i in range(0, N):
            if A[i] > 2 and M > 0:
                A[i] = floor(A[i] / 2)
                M -= 1
    print(sum(A))


if __name__ == '__main__':
    main()