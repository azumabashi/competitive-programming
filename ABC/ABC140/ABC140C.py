def main():
    A = int(input())
    B = list(map(int, input().split(" ")))
    result = B[0]
    if A > 3:
        for i in range(0, A - 2):
            result += min(B[i], B[i + 1])
    elif A == 3:
        result += B[0]
    result += B[-1]
    print(result)


if __name__ == '__main__':
    main()