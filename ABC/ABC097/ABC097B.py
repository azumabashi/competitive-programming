def main():
    limit = int(input())
    answer = 1
    for i in range(2, limit):
        j = 2
        while i ** j <= limit:
            if answer <= i ** j:
                answer = i ** j
            j += 1
    print(answer)


if __name__ == '__main__':
    main()