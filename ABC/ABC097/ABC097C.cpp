#include<bits/stdc++.h>
using namespace std;

int main(){
    string s;
    cin >> s;
    int k;
    cin >> k;
    int n = s.size();
    set<string> all_substr;
    for (int i = 0; i < n; i++) {
        for (int j = 1; j <= k; j++) {
            all_substr.insert(s.substr(i, j));
        }
    }
    auto itr = all_substr.begin();
    for (int i = 1; i < k; i++) {
        itr++;
    }
    cout << *itr << endl;
    return 0;
}