def main():
    keyboard = input()
    octave = "WBWBWWBWBWBW" * 5
    key = "0_1_23_4_5_6" * 5  # 音階とanswerのindexに対応
    answer = ["Do", "Re", "Mi", "Fa", "So", "La", "Si"]
    index = 0
    i = 0
    while True:
        if octave[index:index + len(keyboard)] == keyboard:
            print(answer[int(key[index])])
            break
        else:
            index += 1


if __name__ == '__main__':
    main()

