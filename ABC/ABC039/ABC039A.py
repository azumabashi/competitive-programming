def main():
    height, width, depth = map(int, input().split())
    print(2 * (height * width + width * depth + depth * height))


if __name__ == '__main__':
    main()

