def main():
    height, width = map(int, input().split())
    grid = [list(input()) for _ in range(height)]
    answer = [["#" for _ in range(width)] for _ in range(height)]
    dx = [0, -1, 1]
    dy = [0, -1, 1]
    for i in range(height):
        for j in range(width):
            if grid[i][j] == ".":
                for x in dx:
                    for y in dy:
                        if 0 <= j + x < width and 0 <= i + y < height:
                            answer[i + y][j + x] = "."
    restore = [["." for _ in range(width)] for _ in range(height)]
    for i in range(height):
        for j in range(width):
            if answer[i][j] == "#":
                for x in dx:
                    for y in dy:
                        if 0 <= j + x < width and 0 <= i + y < height:
                            restore[i + y][j + x] = "#"
    if restore == grid:
        print("possible")
        for ans in answer:
            print("".join(ans))
    else:
        print("impossible")


if __name__ == '__main__':
    main()

