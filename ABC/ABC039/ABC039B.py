def main():
    target = int(input())
    i = 0
    while i ** 4 < target:
        i += 1
    print(i)


if __name__ == '__main__':
    main()

