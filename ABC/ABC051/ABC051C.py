def main():
    coordinates = list(map(int, input().split()))
    dx = coordinates[2] - coordinates[0]
    dy = coordinates[3] - coordinates[1]
    answer = "R" * dx + "U" * dy + "L" * dx + "D" * dy
    dx += 1
    dy += 1
    answer += "L" + "U" * dy + "R" * dx + "D" + "R" + "D" * dy + "L" * dx + "U"
    print(answer)


if __name__ == '__main__':
    main()

