def main():
    domain, num_sum = map(int, input().split())
    answer = 0
    for i in range(domain + 1):
        for j in range(domain + 1):
            if 0 <= num_sum - i - j <= domain:
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

