#include<bits/stdc++.h>
using namespace std;

int main() {
    long long N, A, B;
    cin >> N >> A >> B;
    long long base = A + B;
    long long answer = (N / base) * A;
    N -= (N / base) * base;
    answer += min(N, A);
    cout << answer << endl;
    return 0;
}