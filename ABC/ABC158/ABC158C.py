from math import floor
# あくまでもa, bは消費税分！！


def main():
    a, b = map(int, input().split())
    tmp = True
    for n in range(1, 1251):
        if floor(n * 0.08) == a and floor(n * 0.1) == b:
            print(n)
            tmp = False
            break
    if tmp:
        print(-1)


if __name__ == '__main__':
    main()

