#include<bits/stdc++.h>
using namespace std;

int main() {
    string S, top;
    cin >> S;
    int Q;
    cin >> Q;
    for (int i = 0; i < Q; i++) {
        int type;
        cin >> type;
        if (type == 1) {
            swap(top, S);
        } else {
            int f;
            char c;
            cin >> f >> c;
            if (f == 1) {
                top += c;
            } else {
                S += c;
            }
        }
    }
    reverse(top.begin(), top.end());
    string answer = top + S;
    cout << answer << endl;
    return 0;
}