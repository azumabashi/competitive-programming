def main():
    n, a, b = map(int, input().split())
    answer = 0
    if a > 0:
        answer = a * (n // (a + b))
        n %= a + b
        answer += min(n, a)
    print(answer)


if __name__ == '__main__':
    main()

