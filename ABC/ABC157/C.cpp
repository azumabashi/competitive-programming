#include<bits/stdc++.h>
using namespace std;

int main() {
    int N, M;
    cin >> N >> M;
    vector<int> answer(N, -1);
    bool is_exist = true;
    for (int i = 0; i < M; i++ ) {
        int s, c;
        cin >> s >> c;
        if (N < s) {
            is_exist = false;
        }
        if (0 <= answer[s - 1] and answer[s - 1] != c) {
            is_exist = false;
        } else {
            answer[s - 1] = c;
        }
    }
    if (1 < N and answer[0] == 0) is_exist = false;
    if (is_exist) {
        for (int i = 0; i < N; i++) {
            if (answer[i] == -1) {
                if (N == 1) answer[i] = 0;
                else if (i == 0) answer[i] = 1;
                else answer[i] = 0;
            }
            cout << answer[i];
        }
        cout << endl;
    } else {
        cout << -1 << endl;
    }
    return 0;
}