def main():
    n, m = map(int, input().split())
    sc = [tuple(map(int, input().split())) for _ in range(m)]
    answer = -1
    lower_lim = 10 ** (n - 1)
    if n == 1:
        lower_lim = 0
    for i in range(lower_lim, 10 ** n):
        tmp = 0
        if not answer == -1:
            break
        for j in range(m):
            if str(i)[sc[j][0] - 1] == str(sc[j][1]):
                tmp += 1
        if tmp == m:
            answer = i
            break
    print(answer)


if __name__ == '__main__':
    main()

