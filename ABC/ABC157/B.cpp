#include<bits/stdc++.h>
using namespace std;

int main() {
    vector<vector<int>> A(3, vector<int>(3));
    vector<vector<int>> check(3, vector<int>(3, -1));
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cin >> A[i][j];
        }
    }
    int N;
    cin >> N;
    for (int i = 0; i < N; i++) {
        int b;
        cin >> b;
        for (int j = 0; j < 3; j++) {
            if (b < 0) break;
            for (int k = 0; k < 3; k++) {
                if (A[j][k] == b) {
                    check[j][k] = 0;
                    b = -1;
                    break;
                }
            }
        }
    }
    bool answer = false;
    for (int i = 0; i < 3; i++) {
        if ((check[i][0] + check[i][1] + check[i][2] == 0) or (check[0][i] + check[1][i] + check[2][i] == 0)) {
            answer = true;
            break;
        }
    }
    if (not answer) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            if (check[i][i] == 0) count++;
        }
        if (count == 3) answer = true;
    }
    if (not answer) {
        if (check[0][2] + check[1][1] + check[2][0] == 0) answer = true;
    }
    if (answer) {
        cout << "Yes" << endl;
    } else {
        cout << "No" << endl;
    }
    return 0;
}