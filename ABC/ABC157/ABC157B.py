def main():
    a = [list(map(int, input().split())) for _ in range(3)]
    n = int(input())
    b = [int(input()) for _ in range(n)]
    for i in range(3):
        for j in range(3):
            if a[i][j] in b:
                a[i][j] = -1
    answer = 0
    for i in range(3):
        if a[i] == [-1, -1, -1]:
            answer += 1
    for i in range(3):
        if a[0][i] == a[1][i] == a[2][i] == -1:
            answer += 1
    if a[0][0] == a[1][1] == a[2][2] == -1:
        answer += 1
    if a[0][2] == a[1][1] == a[2][0] == -1:
        answer += 1
    print("Yes" if answer > 0 else "No")


if __name__ == '__main__':
    main()

