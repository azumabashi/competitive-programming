from math import ceil


def main():
    print(ceil(int(input()) / 2))


if __name__ == '__main__':
    main()

