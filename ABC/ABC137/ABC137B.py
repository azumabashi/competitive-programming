def main():
    K, X = map(int, input().split(" "))
    lower = max(X - K + 1, -1000000)
    upper = min(X + K, 1000000)
    ans = ""
    for i in range(lower, upper):
        ans = ans + str(i) + " "
    print(ans[:-1])


if __name__ == '__main__':
    main()