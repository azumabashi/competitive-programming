def main():
    list = []
    result = 0
    N = int(input())
    for i in range(0, N):
        ss = input()
        s = "".join(sorted(ss))
        if i == 0:
            list.append(s)
        else:
            for j in list:
                if j == s:
                    result += 1
                    break
                list.append(s)
    print(result)


if __name__ == '__main__':
    main()