def main():
    d, g = map(int, input().split())
    info = []
    for i in range(d):
        p, c = map(int, input().split())
        info.append([100 * (i + 1), p, c])
    ans = float("inf")
    for i in range(1, 1 << d):
        now_info = []
        for j in range(d):
            if (i >> j) & 1:
                now_info.append(info[j])
        now_info.sort(key=lambda x: x[0])
        for j in range(2):
            target = g
            now_ans = 0
            for per, cnt, bonus in now_info:
                if target <= 0:
                    break
                solve = min(target // per, cnt)
                target -= solve * per
                if solve == cnt:
                    target -= bonus
                now_ans += solve
            if target <= 0:
                ans = min(ans, now_ans)
            now_info = now_info[::-1]
    print(ans)


if __name__ == '__main__':
    main()
