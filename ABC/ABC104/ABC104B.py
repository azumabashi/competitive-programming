def main():
    string = input()
    if string[0] == "A" and string[2:-1].count("C") == 1 and string.replace("A", "").replace("C", "").islower():
        print("AC")
    else:
        print("WA")


if __name__ == '__main__':
    main()