def main():
    D, N = map(int, input().split(" "))
    answer = 100 ** D * N
    i = 1
    flag = True
    while flag:
        if not answer % 100 ** (D + i):
            answer += 100 ** D
        else:
            flag = False
    print(answer)


if __name__ == '__main__':
    main()