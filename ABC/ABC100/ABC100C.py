def main():
    N = int(input())
    numbers = list(map(int, input().split(" ")))
    answer = 0
    odds = 0
    while odds < N:
        odds = 0
        for i in range(N):
            if not numbers[i] % 2:
                numbers[i] //= 2
                answer += 1
            else:
                odds += 1
    print(answer)


if __name__ == '__main__':
    main()