import numpy as np


def main():
    length = int(input())
    numbers = np.array(list(map(int, input().split())))
    mod = 10 ** 9 + 7
    answer = 0
    for d in range(60):
        count_one = np.count_nonzero((numbers >> d) & 1)
        answer = (answer + pow(2, d) * count_one * (length - count_one)) % mod
    print(answer)


if __name__ == '__main__':
    main()

