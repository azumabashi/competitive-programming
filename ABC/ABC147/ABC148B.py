from math import floor


def main():
    S = input()
    result = 0
    for i in range(floor(len(S) / 2)):
        if S[i] != S[len(S) - i - 1]:
            result += 1

    print(result)


if __name__ == '__main__':
    main()