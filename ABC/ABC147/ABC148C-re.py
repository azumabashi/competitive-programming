from itertools import product


def main():
    N = int(input())
    testimonies = []
    for _ in range(N):
        a = int(input())
        xy = []
        for _ in range(a):
            xy.append(list(map(int, input().split(" "))))
        testimonies.append(xy)

    assumptions = product([0, 1], repeat=N)
    assumptions = sorted(assumptions, key=lambda x: x.count(0))
    for assumption in assumptions:
        for i in range(N):
            if assumption[i]:
                for testimony in testimonies[i]:
                    if not assumption[testimony[0] - 1] == testimony[1]:
                        break
                else:
                    continue
                break
        else:
            print(assumption.count(1))
            break
        continue


if __name__ == '__main__':
    main()