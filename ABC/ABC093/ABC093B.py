def main():
    lower_limit, upper_limit, ranges = map(int, input().split(" "))
    for i in range(lower_limit, min(lower_limit + ranges, upper_limit + 1)):
        print(i)
    for i in range(max(upper_limit - ranges + 1, lower_limit + ranges), upper_limit + 1):
        print(i)


if __name__ == '__main__':
    main()