def main():
    numbers = list(map(int, input().split(" ")))
    numbers.sort()
    diff = 3 * numbers[2] - sum(numbers)
    if diff % 2:
        answer = (diff + 3) // 2
    else:
        answer = diff // 2
    print(answer)


if __name__ == '__main__':
    main()