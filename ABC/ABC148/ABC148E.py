def main():
    result = 0
    N = int(input())
    if N % 2 == 0:
        N //= 2
        while N > 0:
            N //= 5
            result += N
        print(result)
    else:
        print(0)


if __name__ == '__main__':
    main()

