def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    i = 1
    flag = True
    for a in A:
        if a == i:
            flag = False
            i += 1
    if flag:
        print(-1)
    else:
        print(len(A) - i + 1)


if __name__ == "__main__":
    main()
