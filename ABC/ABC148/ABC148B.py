def main():
    N = int(input())
    strings = input().split(" ")
    result = ""
    for i in range(0, N):
        result = result + strings[0][i] + strings[1][i]
    print(result)


if __name__ == '__main__':
    main()