from heapq import heapify, heappop, heappush


def solve():
    n, u, v = map(int, input().split())
    u -= 1
    v -= 1
    inf = 1 << 30
    g = [[] for _ in range(n)]
    g1 = [[] for _ in range(n)]
    for _ in range(n - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        g[a].append((b, 1))
        g[b].append((a, 1))
        g1[a].append(b)
        g1[b].append(a)

    if g1[u] == [v]:
        return 0

    def dijkstra(root):
        # graph[s] := (t, dist(s,t))
        dist = [inf for _ in range(len(g))]
        dist[root] = 0
        q = []
        heapify(q)
        heappush(q, (0, root))
        while q:
            now_d, now_v = heappop(q)
            for next_v, length in g[now_v]:
                now_dist = dist[now_v] + length
                if dist[next_v] > now_dist:
                    dist[next_v] = now_dist
                    heappush(q, (dist[next_v], next_v))
        return dist

    dist_from_u = dijkstra(u)
    dist_from_v = dijkstra(v)
    res = 0
    for i in range(n):
        if dist_from_u[i] <= dist_from_v[i] and len(g1[i]) == 1:
            res = max(res, dist_from_v[i])
    return res - 1


def main():
    print(solve())


if __name__ == '__main__':
    main()
