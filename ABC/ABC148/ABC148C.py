import math


def euclid(A, B):
    n = max(A, B)
    m = min(A, B)
    while m > 0:
        k = n
        n = m
        m = k % m
    return n


def main():
    numbers = input().split(" ")
    A = int(numbers[0])
    B = int(numbers[1])
    print(round(A * B / euclid(A, B)))


if __name__ == '__main__':
    main()
