def main():
    limit = int(input())
    answer = 2
    while answer <= limit:
        answer *= 2
    print(answer // 2)


if __name__ == '__main__':
    main()

