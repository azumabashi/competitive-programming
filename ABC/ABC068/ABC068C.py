def main():
    islands, regular_boat = map(int, input().split())
    departure_from_island1 = []
    arrive_to_islandN = []
    for _ in range(regular_boat):
        start, final = map(int, input().split())
        if start == 1 and final == islands:
            continue
        elif start == 1:
            departure_from_island1.append(final)
        elif final == islands:
            arrive_to_islandN.append(start)
    print("POSSIBLE" if len(set(departure_from_island1) & set(arrive_to_islandN)) > 0 else "IMPOSSIBLE")


if __name__ == '__main__':
    main()

