def main():
    n = int(input())
    numbers = []
    order = set()
    for _ in range(n):
        num = int(input())
        numbers.append(num)
        order.add(num)
    count = {}
    for i, o in enumerate(sorted(order)):
        count[o] = i
    for number in numbers:
        print(count[number])


if __name__ == '__main__':
    main()

