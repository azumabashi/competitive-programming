def main():
    length = int(input())
    grid = [input() for _ in range(length)]
    answer = [[""] * length for _ in range(length)]
    for i in range(length):
        for j in range(length):
            answer[j][length - i - 1] = grid[i][j]
    for ans in answer:
        print("".join(ans))


if __name__ == '__main__':
    main()

