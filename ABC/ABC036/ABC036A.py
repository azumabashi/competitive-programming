from math import ceil


def main():
    bottle_per_box, required_bottle = map(int, input().split())
    print(ceil(required_bottle / bottle_per_box))


if __name__ == '__main__':
    main()

