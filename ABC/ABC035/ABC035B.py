def main():
    command = input()
    max_or_min = input()
    drone = [0, 0]
    count = 0
    for c in command:
        if c == "?":
            count += 1
        elif c == "L":
            drone[0] -= 1
        elif c == "R":
            drone[0] += 1
        elif c == "U":
            drone[1] += 1
        else:
            drone[1] -= 1
    answer = abs(drone[0]) + abs(drone[1])
    if max_or_min == "1":
        answer += count
    elif answer >= count:
        answer -= count
    else:
        answer = (count - answer) % 2
    print(answer)


if __name__ == '__main__':
    main()

