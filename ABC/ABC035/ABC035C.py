from itertools import accumulate


def main():
    reversi_num, query = map(int, input().split())
    reversi = [0] * (reversi_num + 1)
    for _ in range(query):
        l, r = map(int, input().split())
        reversi[l - 1] += 1
        reversi[r] -= 1
    reversi = list(accumulate(reversi))
    print("".join(map(lambda x: str(x % 2), reversi[:-1])))


if __name__ == '__main__':
    main()

