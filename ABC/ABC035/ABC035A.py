def main():
    width, height = map(int, input().split())
    print("4:3" if width // 4 * 3 == height else "16:9")


if __name__ == '__main__':
    main()

