def main():
    N = int(input())
    strings = {}
    for _ in range(N):
        s = input()
        if s in strings.keys():
            strings[s] += 1
        else:
            strings[s] = 1
    strings = sorted(strings.items(), key=lambda x: x[1], reverse=True)
    maximum = strings[0][1]
    answer = []
    for s in strings:
        if s[1] == maximum:
            answer.append(s[0])
    answer.sort()
    for ans in answer:
        print(ans)


if __name__ == '__main__':
    main()