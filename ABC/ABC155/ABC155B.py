def main():
    N = int(input())
    A = list(map(int, input().split()))
    answer = True
    for a in A:
        if not a % 2:
            if a % 3 and a % 5:
                answer = False
                break
    print("APPROVED" if answer else "DENIED")


if __name__ == '__main__':
    main()