from math import log, ceil


def main():
    N, K = map(int, input().split())
    answer = log(N, K)
    if answer.is_integer():
        print(int(answer) + 1)
    else:
        print(ceil(answer))


if __name__ == '__main__':
    main()

