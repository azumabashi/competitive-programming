def main():
    N, R = map(int, input().split())
    print(R + 100 * max(0, 10 - N))


if __name__ == '__main__':
    main()

