#include<bits/stdc++.h>
using namespace std;

int main() {
    int N, K;
    cin >> N >> K;
    cout << floor(log(N) / log(K)) + 1 << endl;
    return 0;
}