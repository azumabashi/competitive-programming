#include<bits/stdc++.h>
using namespace std;

int main() {
    int N;
    cin >> N;
    vector<int> X(N);
    for (int i = 0; i < N; i++) cin >> X[i];
    int answer = pow(10, 9);
    sort(X.begin(), X.end());
    for (int i = X[0]; i <= X[N - 1]; i++) {
        int now = 0;
        for (int j = 0; j < N; j++) {
            now += pow(i - X[j], 2);
        }
        answer = min(answer, now);
    }
    cout << answer << endl;
    return 0;
}