def main():
    N = int(input())
    X = list(map(int, input().split()))
    P = [sum(X) // N]
    P.append(P[0] + 1)
    answer = [0, 0]
    for x in X:
        for i in range(2):
            answer[i] += (x - P[i]) ** 2
    print(min(answer))


if __name__ == '__main__':
    main()