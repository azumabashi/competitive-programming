def nck(n, k, mod):
    result = 1
    for i in range(n - k + 1, n + 1):
        result = result * i % mod
    tmp = 1
    for i in range(1, k + 1):
        tmp = tmp * i % mod
    return result * pow(tmp, mod - 2, mod) % mod


def main():
    n, a, b = map(int, input().split())
    mod = 10 ** 9 + 7
    print((pow(2, n, mod) - nck(n, a, mod) - nck(n, b, mod) - 1) % mod)


if __name__ == '__main__':
    main()