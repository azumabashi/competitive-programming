def main():
    A, B = map(int, input().split())
    postcode = input()
    answer = False
    if postcode.count("-") == 1:
        if postcode[A] == "-" and postcode[:A].isdecimal() and postcode[A + 1:].isdecimal():
            answer = True
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()