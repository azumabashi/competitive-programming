from math import ceil


def get_required_time(trains, station_num, station):
    arrival = [0] * station_num
    for i in range(station, station_num):
        if arrival[i - 1] < trains[i - 1][1]:
            arrival[i] = trains[i - 1][0] + trains[i - 1][1]
        else:
            arrival[i] = trains[i - 1][0] + trains[i - 1][2] * ceil(arrival[i - 1] / trains[i - 1][2])
    return arrival


def main():
    station_num = int(input())
    trains = [list(map(int, input().split())) for _ in range(station_num - 1)]
    for i in range(1, station_num):
        print(get_required_time(trains, station_num, i)[-1])
    print(0)


if __name__ == '__main__':
    main()