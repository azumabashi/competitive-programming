from math import ceil
'''
    累積和っぽく解けるかなぁと思ったけどダメらしい
    -> 全パターンやってみないとダメらしい 500^2=250000より間に合う
'''


def main():
    station_num = int(input())
    trains = [list(map(int, input().split())) for _ in range(station_num - 1)]
    arrival = [0] * station_num
    for i in range(1, station_num):
        if arrival[i - 1] < trains[i - 1][1]:
            arrival[i] = trains[i - 1][0] + trains[i - 1][1]
        else:
            arrival[i] = trains[i - 1][0] + trains[i - 1][2] * ceil(arrival[i - 1] / trains[i - 1][2])
        # これはダメ：
        # arrival[i] = trains[i - 1][0] + max(arrival[i - 1] + abs(arrival[i - 1] % (-trains[i - 1][2])), trains[i - 1][1])
    print(arrival)
    for i in range(station_num):
        if i == 0:
            answer = arrival[-1]
        elif i == station_num - 1:
            answer = 0
        else:
            answer = min(arrival[-1] - arrival[i] + trains[i][1], arrival[-1])
            if i < station_num - 2:
                # 駅Nへの所要時間は単調非減少
                next_i = min(arrival[-1] - arrival[i + 1] + trains[i + 1][1], arrival[-1])
                if answer < next_i:
                    answer = next_i
        print(answer)


if __name__ == '__main__':
    main()