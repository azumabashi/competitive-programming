def main():
    numbers = [0] * (10 ** 5 + 1)
    prime = [0] * (10 ** 5 + 1)
    numbers[2] = 1
    prime[2] = 1
    for i in range(3, 10 ** 5 + 1):
        if i % 2:
            is_prime = True
            j = 1
            while j * j <= i and is_prime:
                if (not i % j) and j > 1:
                    is_prime = False
                j += 1
            if is_prime:
                prime[i] = 1
            if is_prime and prime[(i + 1) // 2] and (((i + 1) // 2) % 2 or i == 3):
                numbers[i] = numbers[i - 1] + 1
            else:
                numbers[i] = numbers[i - 1]
        else:
            numbers[i] = numbers[i - 1]
    query = int(input())
    for _ in range(query):
        left, right = map(int, input().split())
        tmp = 0
        if left <= 2 <= right:
            tmp = -1
        print(numbers[right] - numbers[left - 1] + tmp)


if __name__ == '__main__':
    main()