def solve(n, d):
    for i in range(n - 2):
        if all(len(d[i + k]) == 1 for k in range(3)):
            return True
    return False


def main():
    n = int(input())
    d = [set(map(int, input().split())) for _ in range(n)]
    print("Yes" if solve(n, d) else "No")


if __name__ == '__main__':
    main()
