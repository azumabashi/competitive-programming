def main():
    string = input()
    print("YES" if string.replace("ch", "").replace("o", "").replace("k", "").replace("u", "") == "" else "NO")


if __name__ == '__main__':
    main()

