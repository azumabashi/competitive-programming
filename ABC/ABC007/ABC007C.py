def main():
    height, width = map(int, input().split())
    start = list(map(int, input().split()))
    goal = list(map(int, input().split()))
    maze = [list(map(lambda x: -1 if x == "." else float("inf"), input())) for _ in range(height)]

    def bfs(y, x):
        q = []
        maze[y][x] = 0
        q.append((y, x))
        while not q == []:
            point = q.pop(0)
            for delta in [(0, 1), (1, 0), (-1, 0), (0, -1)]:
                if maze[point[0] + delta[0]][point[1] + delta[1]] == -1:
                    maze[point[0] + delta[0]][point[1] + delta[1]] = 1 + maze[point[0]][point[1]]
                    q.append((point[0] + delta[0], point[1] + delta[1]))
        return maze[goal[0] - 1][goal[1] - 1]

    print(bfs(start[0] - 1, start[1] - 1))


if __name__ == '__main__':
    main()

