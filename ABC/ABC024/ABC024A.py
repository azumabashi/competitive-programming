def main():
    admission_per_child, admission_per_adult, discount_per_person, discount_lim = map(int, input().split())
    child, adult = map(int, input().split())
    discount = 0
    if child + adult >= discount_lim:
        discount = discount_per_person * (child + adult)
    print(admission_per_adult * adult + admission_per_child * child - discount)


if __name__ == '__main__':
    main()

