def main():
    town_num, days, tribe_num = map(int, input().split())
    transfer_limit = [list(map(int, input().split())) for _ in range(days)]
    starting_and_ending = []
    location = []
    for _ in range(tribe_num):
        starting, ending = map(int, input().split())
        direction = 1
        if starting > ending:
            direction = -1
        starting_and_ending.append((ending, direction))
        location.append(starting)
    answer = [0] * tribe_num
    for i in range(days):
        l, r = transfer_limit[i][0:2]
        for j in range(tribe_num):
            if location[j] >= 0 and l <= location[j] <= r:
                if l <= starting_and_ending[j][0] <= r:
                    answer[j] = i + 1
                    location[j] = -1
                else:
                    if starting_and_ending[j][1] > 0:
                        location[j] = r
                    else:
                        location[j] = l
                    if l <= starting_and_ending[j][0] <= r:
                        answer[j] = i + 1
                        location[j] = -1
    for ans in answer:
        print(ans)


if __name__ == '__main__':
    main()

