def main():
    customer_num, open_time = map(int, input().split())
    entered_time = [int(input()) for _ in range(customer_num)]
    answer = open_time
    for i in range(customer_num - 1):
        answer += min(open_time, entered_time[i + 1] - entered_time[i])
    print(answer)


if __name__ == '__main__':
    main()

