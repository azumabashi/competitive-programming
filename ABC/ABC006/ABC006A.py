def main():
    num = input()
    print("YES" if "3" in num or not int(num) % 3 else "NO")


if __name__ == '__main__':
    main()

