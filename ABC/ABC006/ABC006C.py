def main():
    people, foot = map(int, input().split())
    answer = [0, 0, 0]
    if foot % 2 == 1:
        foot -= 3
        answer[1] = 1
    foot //= 2
    if foot % 2 == 1:
        answer[0] = 1
        foot -= 1
    answer[2] = foot // 2
    for i in range(answer[2] + 1):
        if sum(answer) + i == people:
            answer[0] += 2 * i
            answer[2] -= i
            break
    else:
        answer = [-1, -1, -1]
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

