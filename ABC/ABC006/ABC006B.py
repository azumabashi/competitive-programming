def main():
    index = int(input())
    tribonacci = [0] * (index + 1)
    if index >= 3:
        tribonacci[3] = 1
        mod = 10007
        for i in range(4, index + 1):
            tribonacci[i] = (tribonacci[i - 1] + tribonacci[i - 2] + tribonacci[i - 3]) % mod
    print(tribonacci[-1])


if __name__ == '__main__':
    main()

