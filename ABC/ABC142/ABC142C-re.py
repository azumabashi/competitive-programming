def main():
    N = int(input())
    A = map(int, input().split(" "))
    list = [""] * (N + 1)
    for i, a in enumerate(A, 1):
        list[a] = str(i)
    print(" ".join(list[1:]))


if __name__ == '__main__':
    main()