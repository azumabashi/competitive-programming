from math import gcd


def main():
    A, B = map(int, input().split(" "))
    g = gcd(A, B)
    answer = [1]
    while not g % 2:
        g //= 2
        answer.append(2)
    i = 3
    while i * i <= g:
        if not g % i:
            g //= i
            answer.append(i)
        else:
            i += 2
    if not g == 1:
        answer.append(g)
    print(len(set(answer)))


if __name__ == '__main__':
    main()

