def main():
    N, K = map(int, input().split(" "))
    h = input().split(" ")
    ans = 0
    for i in h:
        if K <= int(i):
            ans += 1
    print(ans)


if __name__ == '__main__':
    main()