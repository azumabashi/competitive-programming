def main():
    N = int(input())
    if N % 2 == 0:
        print(0.5000000000)
    else:
        a = (N // 2) + 1
        print(a / N)


if __name__ == '__main__':
    main()