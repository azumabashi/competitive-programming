def main():
    N = int(input())
    a = input().split(" ")
    L = []
    for i in range(N):
        L.append("")
    for i in range(N):
        L[int(a[i]) - 1] = str(i + 1)
    ans = ""
    for l in L:
        ans = ans + l + " "
    print(ans[:-1])


if __name__ == '__main__':
    main()