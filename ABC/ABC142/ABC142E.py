def solve():
    n, m = map(int, input().split())
    cost = [0] * m
    box = [0] * m
    can_open = [False] * n
    for i in range(m):
        cost[i], _ = map(int, input().split())
        b = list(map(lambda x: int(x) - 1, input().split()))
        for bb in b:
            can_open[bb] = True
            box[i] += 1 << bb
    if not all(can_open):
        return -1
    length = 1 << n
    max_val = 10 ** 9 + 7
    dp = [[max_val for _ in range(5000)] for _ in range(5000)]
    dp[0][0] = 0
    for i in range(m):
        for j in range(length):
            dp[i + 1][j] = min(dp[i + 1][j], dp[i][j])
            idx = j | box[i]
            dp[i + 1][idx] = min(dp[i + 1][idx], dp[i][j] + cost[i])
    return dp[m][length - 1]


def main():
    print(solve())


if __name__ == '__main__':
    main()
