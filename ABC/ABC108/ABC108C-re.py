def main():
    # 整数問題で処理していく解法
    limit, divisor = map(int, input().split(" "))
    if divisor % 2:
        print((limit // divisor) ** 3)
    else:
        print((limit // divisor) ** 3 + ((limit + divisor // 2) // divisor) ** 3)


if __name__ == '__main__':
    main()