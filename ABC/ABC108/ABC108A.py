def main():
    K = int(input())
    even = 0
    odd = 0
    if K % 2 == 0:
        even = K // 2
        odd = K // 2
    else:
        even = (K - 1) // 2
        odd = K - even
    print(even * odd)


if __name__ == '__main__':
    main()