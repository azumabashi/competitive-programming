def main():
    N, K = map(int, input().split(" "))
    if not K % 2:
        K //= 2
    print((N // K) ** 3)


if __name__ == '__main__':
    main()