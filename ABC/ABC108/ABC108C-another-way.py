def main():
    # 全探索する解法
    limit, divisor = map(int, input().split(" "))
    reminders = [0] * divisor
    answer = 0
    for i in range(1, limit + 1):
        reminders[i % divisor] += 1
    # 余りにより分類 -> 余り別に総計
    for i in range(0, divisor):
        others_reminder = (divisor - i) % divisor
        if not (2 * others_reminder) % divisor:
            answer += reminders[i % divisor] * (reminders[others_reminder]) ** 2
    print(answer)


if __name__ == '__main__':
    main()