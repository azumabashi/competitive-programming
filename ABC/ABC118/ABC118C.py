# 解説読んでやり方理解した
from fractions import gcd
from functools import reduce


def get_gcd(numbers):
    return reduce(gcd, numbers)


def main():
    N = int(input())
    monsters = list(map(int, input().split(" ")))
    print(get_gcd(monsters))


if __name__ == '__main__':
    main()