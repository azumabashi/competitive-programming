def main():
    N, M = map(int, input().split(" "))
    foods = [0] * (M + 1)
    result = 0
    for _ in range(N):
        tmp = list(map(int, input().split(" ")))
        for t in tmp[1:]:
            foods[t] += 1
    for food in foods:
        if food == N:
            result += 1
    print(result)


if __name__ == '__main__':
    main()