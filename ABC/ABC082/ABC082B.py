def main():
    s = "".join(sorted(input()))
    t = "".join(reversed(sorted(input())))
    print("Yes" if s < t else "No")


if __name__ == '__main__':
    main()


'''
    教訓：極端な場合を比較せよ
    sは小さい順，tは大きい順(ともに辞書順で)で並んでいれば確実にs<t．
'''