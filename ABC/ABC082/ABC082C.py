def main():
    n = int(input())
    numbers = sorted(list(map(int, input().split())))
    now = numbers[0]
    count = 0
    answer = 0
    print(numbers)
    for i in range(n):
        if now == numbers[i] and not i == n - 1:
            count += 1
        elif now < numbers[i] or i == n - 1:
            if i == n - 1:
                count += 1
            if count > now:
                answer += count - now
            elif now > count:
                answer += count
            now = numbers[i]
            count = 1
    print(answer)


if __name__ == '__main__':
    main()