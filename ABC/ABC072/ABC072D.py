def main():
    N = int(input())
    permutation = list(map(int, input().split()))
    answer = 0
    for i in range(N):
        if permutation[i] == i + 1:
            """
            # 元々考えていたもの．こんなことしなくても操作を素直にシミュレートすれば解ける．
            condition[i] = 0
            condition[i + 1] = 0
            """
            if permutation[i] == N:
                # もしNが右から2番目にいたときにswapしてしまうと，Nが最右端に来てしまい条件を満たさない．
                # そこで場合分けを要する．
                permutation[i], permutation[i - 1] = permutation[i - 1], permutation[i]
            else:
                permutation[i], permutation[i + 1] = permutation[i + 1], permutation[i]
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()