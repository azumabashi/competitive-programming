"""
    中央値が使えないのであれば，全パターン試すしかない．
    for文でXをiとして決めている．
    i >= 10 ** 5 - 3のときは，len(count[i:i+3])が小さくなるので問題ない．
    (大きい方の数3つで最大値をとるにしても，(3つの和)>=(2つの和)なのでmaxの前では意味がない)
"""


def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    count = [0] * 10 ** 5
    for num in numbers:
        count[num] += 1
    answer = 0
    for i in range(10 ** 5):
        answer = max(answer, sum(count[i:i + 3]))
    print(answer)


if __name__ == '__main__':
    main()

