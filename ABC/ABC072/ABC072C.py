"""
    真ん中の方の値を持ってくればうまくいきそうな感じがするが実は正しくない
    -> 与えられた数値の個数をカウントし，総当たりで調べる．
"""


def main():
    N = int(input())
    numbers = sorted(list(map(int, input().split())))
    target = [numbers[N // 2 + i] for i in range(-1, -1 + min(N, 3))]
    answer = [0, 0, 0]
    for num in numbers:
        for i in range(min(N, 3)):
            if target[i] == num or target[i] == num - 1 or target[i] == num + 1:
                answer[i] += 1
    print(max(answer))


if __name__ == '__main__':
    main()

