def main():
    dam = int(input())
    water = list(map(int, input().split()))
    answer = [0] * dam
    answer[0] = sum(water) - 2 * sum(water[1::2])
    for i in range(1, dam):
        answer[i] = 2 * water[i - 1] - answer[i - 1]
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

