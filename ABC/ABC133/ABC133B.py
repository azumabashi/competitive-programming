def main():
    N, D = map(int, input().split(" "))
    X = []
    result = 0
    for n in range(N):
        X.append(list(map(int, input().split(" "))))
    for i in range(N):
        for j in range(i + 1, N):
            dist = 0
            for d in range(D):
                dist += (X[i][d] - X[j][d]) ** 2
            if float.is_integer(dist ** 0.5):
                result += 1
    print(result)


if __name__ == '__main__':
    main()