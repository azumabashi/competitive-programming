def main():
    L, R = map(int, input().split(" "))
    if R - L >= 2019:
        # 部屋割り論法でn%2019=0なるnが存在
        print(0)
    else:
        l = []
        for i in range(L, R + 1):
            l.append(i % 2019)
        l.sort()
        minimum = 2018
        for j in range(len(l)):
            for k in range(j + 1, len(l)):
                minimum = min(minimum, l[j] * l[k] % 2019)
        print(minimum)


if __name__ == '__main__':
    main()