def main():
    N, A, M = map(int, input().split(" "))
    print(min(N * A, M))


if __name__ == '__main__':
    main()