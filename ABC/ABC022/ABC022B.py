def main():
    flower = int(input())
    flower_variation = [int(input()) for _ in range(flower)]
    count = {}
    answer = 0
    for f in flower_variation:
        if f in count:
            answer += 1
            count[f] += 1
        else:
            count[f] = 1
    print(answer)


if __name__ == '__main__':
    main()

