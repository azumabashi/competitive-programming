def main():
    days, lower_limit, upper_limit = map(int, input().split())
    weight = int(input())
    increment = [int(input()) for _ in range(days - 1)]
    answer = 0
    if lower_limit <= weight <= upper_limit:
        answer = 1
    for i in increment:
        weight += i
        if lower_limit <= weight <= upper_limit:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

