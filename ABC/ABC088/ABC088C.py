from itertools import permutations


def main():
    grid = []
    num_sum = 0
    for _ in range(3):
        tmp = list(map(int, input().split(" ")))
        num_sum += sum(tmp)
        grid.append(tmp)
    num_sum //= 3
    index_list = permutations([0, 1, 2])
    answer = "Yes"
    for index in index_list:
        if not grid[0][index[0]] + grid[1][index[1]] + grid[2][index[2]] == num_sum:
            answer = "No"
            break
    print(answer)


if __name__ == '__main__':
    main()