def main():
    height, width = map(int, input().split())
    grid = []
    initial_white = 0
    for _ in range(height):
        g = input()
        initial_white += g.count(".")
        grid.append(list(g))
    count = [[0 for _ in range(width)] for _ in range(height)]
    next_search = [(0, 0)]
    grid[0][0] = "#"
    while not next_search == []:
        now = next_search.pop(0)
        for d in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
            next_index = (now[0] + d[0], now[1] + d[1])
            if next_index[0] < 0 or height <= next_index[0] or next_index[1] < 0 or width <= next_index[1]:
                continue
            elif grid[next_index[0]][next_index[1]] == ".":
                grid[next_index[0]][next_index[1]] = "#"
                count[next_index[0]][next_index[1]] = count[now[0]][now[1]] + 1
                next_search.append(next_index)
    print(initial_white - count[-1][-1] - 1 if count[-1][-1] > 0 else -1)


if __name__ == '__main__':
    main()

