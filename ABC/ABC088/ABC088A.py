def main():
    cost = int(input())
    one_yen = int(input())
    if (cost - cost // 500 * 500) <= one_yen:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()