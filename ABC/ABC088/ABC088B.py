def main():
    card_num = int(input())
    cards = list(map(int, input().split(" ")))
    cards.sort(reverse=True)
    answer = 0
    for i in range(card_num):
        if i % 2:
            answer -= cards[i]
        else:
            answer += cards[i]
    print(answer)


if __name__ == '__main__':
    main()