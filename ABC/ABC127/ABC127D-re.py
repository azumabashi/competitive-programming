def main():
    n, m = map(int, input().split())
    a = sorted(list(map(int, input().split())))
    change = []
    change_n = 0
    for _ in range(m):
        b, c = map(int, input().split())
        change.append((b, c))
        change_n += b
    change.sort(key=lambda x: x[1], reverse=True)
    answer = 0
    added = 0
    index = 0
    for i in range(n):
        if i < change_n:
            if added + change[index][0] == i:
                added = i
                index += 1
            answer += max(a[i], change[index][1])
        else:
            answer += a[i]
    print(answer)


if __name__ == '__main__':
    main()

