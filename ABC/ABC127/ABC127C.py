def main():
    N, M = map(int, input().split(" "))
    left = 0
    right = 10**5
    for i in range(M):
        a, b = map(int, input().split(" "))
        left = max(left, a)
        right = min(right, b)
    if not left == 1:
        print(right - left + 1)
    else:
        print(right)


if __name__ == '__main__':
    main()