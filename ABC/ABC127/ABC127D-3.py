from collections import Counter


def main():
    # 「混ぜてソートする」の練習がてら解き直す．
    card_num, operation = map(int, input().split())
    card = Counter(list(map(int, input().split())))
    for _ in range(operation):
        target_cards, after_change = map(int, input().split())
        if after_change in card:
            card[after_change] += target_cards
        else:
            card[after_change] = target_cards
    card = sorted(dict(card).items(), key=lambda x: x[0], reverse=True)
    answer = 0
    for c, n in card:
        answer += c * min(card_num, n)
        card_num -= n
        if card_num <= 0:
            break
    print(answer)


if __name__ == '__main__':
    main()

