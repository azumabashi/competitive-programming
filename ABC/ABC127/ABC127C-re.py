def main():
    N, M = map(int, input().split(" "))
    left = 0
    right = N
    for _ in range(M):
        l, r = map(int, input().split(" "))
        left = max(left, l)
        right = min(right, r)
    if right - left < 0:
        print(0)
    else:
        print(right - left + 1)


if __name__ == '__main__':
    main()