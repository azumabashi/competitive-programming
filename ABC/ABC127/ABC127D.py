from heapq import heapify, heappop, heappush


def main():
    N, M = map(int, input().split(" "))
    A = list(map(int, input().split(" ")))
    heapify(A)
    for i in range(M):
        b, c = map(int, input().split(" "))
        for j in range(b):
            x = heappop(A)
            if x < b:
                heappush(A, c)
            else:
                heappush(A, x)
    print(sum(list(A)))


if __name__ == '__main__':
    main()