def main():
    paint_cans = list(map(int, input().split()))
    print(len(set(paint_cans)))


if __name__ == '__main__':
    main()

