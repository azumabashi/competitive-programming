def main():
    s = input()
    score = 0
    diff = 0
    for i in range(len(s)):
        if s[i] == "g":
            if diff == 0:
                diff += 1
            else:
                diff -= 1
                score += 1
        else:
            if diff == 0:
                diff = 1
                score -= 1
            else:
                diff -= 1
    print(max(score, 0))


if __name__ == '__main__':
    main()

