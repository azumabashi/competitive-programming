def main():
    string = input()
    begin_index = -1
    end_index = 0
    for i in range(len(string)):
        if string[i] == "A" and begin_index < 0:
            begin_index = i
        elif string[i] == "Z":
            end_index = i
    print(end_index - begin_index + 1)


if __name__ == '__main__':
    main()

