def main():
    limit = int(input())
    square_num = 0
    i = 1
    while True:
        if i * i <= limit:
            square_num = i * i
            i += 1
        else:
            break
    print(square_num)


if __name__ == '__main__':
    main()