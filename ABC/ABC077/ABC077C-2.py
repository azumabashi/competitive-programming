"""
【二分探索と添字のまとめ】

A = [a_1, a_2, ..., a_N]において，値val
  * 未満の値がいくつあるか --> bisect_left(A, val)
  * より大きい値がいくつあるか --> N - bisect_right(A, val) or
                              B = [-a_1, -a_2, ..., -a_N]においてbisect_left(B, -val)
いずれの場合もsortされていることは表記上省略

bisect_left:  xがすでにaに含まれている場合、挿入点は既存のどのエントリーよりも前(左)になります。
bisect_right: bisect_left()と似ていますが、aに含まれるxのうち、どのエントリーよりも後ろ(右)にくるような挿入点を返します。
from https://docs.python.org/ja/3/library/bisect.html
"""


from bisect import bisect_right, bisect_left


def main():
    n = int(input())
    a = [int(k) for k in input().split()]
    b = [int(k) for k in input().split()]
    c = [-int(k) for k in input().split()]
    a.sort()
    c.sort()
    ans = 0
    for bb in b:
        upper = bisect_left(a, bb)
        lower = bisect_left(c, -bb)
        ans += max(0, upper) * max(0, lower)
    print(ans)


if __name__ == '__main__':
    main()
