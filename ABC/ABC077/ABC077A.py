def main():
    grids = [input() for _ in range(2)]
    for i in range(3):
        if not grids[0][i] == grids[1][2 - i]:
            print("NO")
            break
    else:
        print("YES")


if __name__ == '__main__':
    main()