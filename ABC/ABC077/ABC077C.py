from bisect import bisect_left, bisect_right
'''
    通常の手続: a->b->c or c->b->a
    しかしこれはO(N^2)解法のため間に合わない
    -> bを決めれば二分探索で a, cの取りうる範囲が決まる
    -> 左にxが来るか右に来るかで区間の取り方が変わるので注意 https://kujira16.hateblo.jp/entry/2015/09/26/234614
'''


def binary_search(list, base, length):
    lower_limit = 0
    upper_limit = length - 1
    while lower_limit + 1 < upper_limit:
        if list[(lower_limit + upper_limit) // 2] > base:
            upper_limit = (lower_limit + upper_limit) // 2
        else:
            lower_limit = (lower_limit + upper_limit) // 2
    return lower_limit


def main():
    N = int(input())
    parts = [sorted(list(map(int, input().split()))) for _ in range(3)]
    answer = 0
    print(parts)
    for b in parts[1]:
        # a = binary_search(parts[0], b, N)
        # c = binary_search(parts[2], b, N)
        a = bisect_left(parts[0], b)
        c = bisect_right(parts[2], b)
        answer += a * (N - c)
    print(answer)


if __name__ == '__main__':
    main()