def main():
    participants = int(input())
    ratings = list(map(int, input().split()))
    color = [0] * 9
    for rating in ratings:
        color[min(rating // 400, 8)] += 1
    answer = 0
    for i in range(8):
        if color[i] > 0:
            answer += 1
    print(max(1, answer), answer + color[-1])


if __name__ == '__main__':
    main()

