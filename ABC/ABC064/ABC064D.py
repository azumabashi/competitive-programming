def main():
    length = int(input())
    string = input()
    count = [0] * length
    min_value = 0
    for i in range(length):
        if i == 0:
            if string[i] == "(":
                count[i] = 1
            else:
                count[i] = -1
        elif string[i] == "(":
            count[i] = count[i - 1] + 1
        else:
            count[i] = count[i - 1] - 1
        min_value = min(min_value, count[i])
    print("(" * (-min_value) + string + ")" * (count[-1] - min_value))


if __name__ == '__main__':
    main()

