def main():
    print("NO" if int(input().replace(" ", "")) % 4 else "YES")


if __name__ == '__main__':
    main()

