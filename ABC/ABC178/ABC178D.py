def comb(n, k, mod=10 ** 9 + 7):
    denominator = 1
    numerator = 1
    for i in range(1, k + 1):
        denominator = denominator * i % mod
    for i in range(n - k + 1, n + 1):
        numerator = numerator * i % mod
    return numerator * pow(denominator, mod - 2, mod) % mod


def main():
    n = int(input())
    mod = 10 ** 9 + 7
    ans = 0
    for i in range(1, n):
        if n - 3 * i < 0:
            break
        ans += comb(n - 2 * i - 1, n - 3 * i, mod)
        ans %= mod
    print(ans)


if __name__ == '__main__':
    main()
