def main():
    n = int(input())
    d1 = []
    d2 = []
    for _ in range(n):
        x, y = map(int, input().split())
        d1.append(x + y)
        d2.append(x - y)
    d1.sort()
    d2.sort()
    print(max(d1[-1] - d1[0], d2[-1] - d2[0]))


if __name__ == '__main__':
    main()
