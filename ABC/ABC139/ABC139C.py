def main():
    N = int(input())
    H = list(map(int, input().split(" ")))
    tmp = 0
    result = 0
    before = H[0]
    for i in range(1, N):
        if before >= H[i]:
            tmp += 1
        else:
            result = max(result, tmp)
            tmp = 0
        before = H[i]
    print(max(result, tmp))


if __name__ == '__main__':
    main()