from math import ceil


def main():
    A, B = map(int, input().split(" "))
    A += 1
    print(ceil((1 + B - A) / (A - 2)) + 1)


if __name__ == '__main__':
    main()