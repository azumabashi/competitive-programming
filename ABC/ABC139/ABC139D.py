def main():
    '''
    N = aq + r (0 <= r <= a-1)よりΣrを最大にするのはr = a-1のとき
    '''
    N = int(input())
    print(N * (N - 1) // 2)


if __name__ == '__main__':
    main()