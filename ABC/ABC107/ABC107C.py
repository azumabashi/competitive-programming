def binary_search(list):
    lower = 0
    upper = len(list)
    while upper - lower > 1:
        if list[(lower + upper) // 2] < 0:
            lower = (lower + upper) // 2
        else:
            upper = (lower + upper) // 2
    return lower


def main():
    N, K = map(int, input().split(" "))
    coodinates = list(map(int, input().split(" ")))
    result = 10 ** 9 + 7
    if N > K:
        for i in range(0, N - K + 1):
            if coodinates[i] < 0 and coodinates[i + K - 1] > 0:
                start = binary_search(coodinates[i:i + K])
                # print(coodinates[i:i + K], start)
                round_steps = min(0 - coodinates[i], coodinates[i + K - 1])
                # print(round_steps, coodinates[i+K-1], coodinates[i])
                result = min(result, coodinates[i + K - 1] - coodinates[i] + abs(round_steps))
            else:
                result = min(result, coodinates[i + K - 1])
            # print(result)
    else:
        result = abs(coodinates[0]) + coodinates[-1] - coodinates[0]
    print(result)


if __name__ == '__main__':
    main()