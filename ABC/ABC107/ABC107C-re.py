def main():
    # editorial読んだ．
    N, K = map(int, input().split(" "))
    coordinates = list(map(int, input().split(" ")))
    result = 10 ** 9 + 7
    for i in range(N - K + 1):
        result = min(result, abs(coordinates[i + K - 1] - coordinates[i]) + abs(coordinates[i]),
                     abs(coordinates[i + K - 1] - coordinates[i]) + abs(coordinates[i + K - 1]))
    print(result)


if __name__ == '__main__':
    main()