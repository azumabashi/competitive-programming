from collections import deque


def main():
    h, w = map(int, input().split())
    c = list(map(lambda x: int(x) - 1, input().split()))
    d = list(map(lambda x: int(x) - 1, input().split()))
    grid = [list(input()) for _ in range(h)]
    q = deque([c])
    max_val = 10 ** 10
    dist = [[max_val for _ in range(w)] for _ in range(h)]
    dist[c[0]][c[1]] = 0
    next_d = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    while q:
        now_x, now_y = q.popleft()
        for dx, dy in next_d:
            nx = now_x + dx
            ny = now_y + dy
            if not (0 <= nx < h and 0 <= ny < w):
                continue
            elif grid[nx][ny] == "#":
                continue
            if dist[nx][ny] > dist[now_x][now_y]:
                dist[nx][ny] = dist[now_x][now_y]
                if [nx, ny] == d:
                    continue
                q.appendleft([nx, ny])
        for dx, dy in [[0, 2], [1, 2], [1, 1], [2, 2], [2, 0], [2, 1]]:
            for cx, cy in [[1, 1], [1, -1], [-1, -1], [-1, 1]]:
                nx = now_x + dx * cx
                ny = now_y + dy * cy
                if not (0 <= nx < h and 0 <= ny < w):
                    continue
                elif grid[nx][ny] == "#":
                    continue
                if dist[nx][ny] > dist[now_x][now_y] + 1:
                    dist[nx][ny] = dist[now_x][now_y] + 1
                    if [nx, ny] == d:
                        continue
                    q.append([nx, ny])
    print(dist[d[0]][d[1]] if dist[d[0]][d[1]] != max_val else -1)


if __name__ == '__main__':
    main()
