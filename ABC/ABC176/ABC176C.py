def main():
    n = int(input())
    a = list(map(int, input().split()))
    ans = 0
    now = a[0]
    for i in range(1, n):
        if now > a[i]:
            ans += now - a[i]
        now = max(a[i], now)
    print(ans)


if __name__ == '__main__':
    main()
