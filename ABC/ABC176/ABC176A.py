from math import ceil


def main():
    n, x, t = map(int, input().split())
    print(t * ceil(n / x))


if __name__ == '__main__':
    main()
