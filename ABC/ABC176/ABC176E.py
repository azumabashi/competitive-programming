def max_bomb_set(l):
    l.sort(key=lambda x: x[0], reverse=True)
    res = []
    for cnt, tag in l:
        if cnt == l[0][0]:
            res.append(tag)
        else:
            break
    return res, l[0][0]


def main():
    h, w, m = map(int, input().split())
    height = [[0, i] for i in range(h)]
    width = [[0, i] for i in range(w)]
    target = set()
    for _ in range(m):
        x, y = map(lambda x: int(x) - 1, input().split())
        target.add((x, y))
        height[x][0] += 1
        width[y][0] += 1
    max_height, max_h_val = max_bomb_set(height)
    max_width, max_w_val = max_bomb_set(width)
    ans = max_h_val + max_w_val
    for hh in max_height:
        for ww in max_width:
            if (hh, ww) not in target:
                return ans
    return ans - 1


if __name__ == '__main__':
    print(main())
