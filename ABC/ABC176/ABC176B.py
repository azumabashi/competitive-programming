def main():
    n = list(input())
    n = list(map(int, n))
    print("Yes" if sum(n) % 9 == 0 else "No")


if __name__ == '__main__':
    main()
