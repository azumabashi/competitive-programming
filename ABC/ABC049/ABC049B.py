def main():
    height, width = map(int, input().split())
    image = [input() for _ in range(height)]
    for img in image:
        for _ in range(2):
            print(img)


if __name__ == '__main__':
    main()

