def main():
    vowel = ["a", "i", "u", "e", "o"]
    print("vowel" if input() in vowel else "consonant")


if __name__ == '__main__':
    main()

