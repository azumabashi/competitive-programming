def main():
    target = input()
    string = ["eraser", "dreamer", "erase", "dream"]
    for i in range(len(target)):
        for s in string:
            if target[-len(s):] == s:
                target = target[:-len(s)]
        if target == "":
            break
    print("YES" if target == "" else "NO")


if __name__ == '__main__':
    main()

