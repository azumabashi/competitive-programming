#include<bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<vector<int>> d(n, vector<int>(n));
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> d[i][j];
        }
    }
    vector<vector<int>> d_sum(n + 1, vector<int>(n + 1));
    for (int i = 1; i < n + 1; i++) {
        for (int j = 1; j < n + 1; j++) {
            d_sum[i][j] = d_sum[i - 1][j] + d_sum[i][j - 1] - d_sum[i - 1][j - 1] + d[i - 1][j - 1];
        }
    }
    int q;
    cin >> q;
    int p = 0;
    int ans;
    for (int qq = 0; qq < q; qq++) {
        cin >> p;
        ans = 0;
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < n + 1; j++) {
                for (int k = i + 1; k < n + 1; k++) {
                    for (int l = j + 1; l < n + 1; l++) {
                        if ((k - i) * (l - j) <= p) {
                            ans = max(ans, d_sum[k][l] - d_sum[i][l] - d_sum[k][j] + d_sum[i][j]);
                        }
                    }
                }
            }
        }
        cout << ans << endl;
    }
    return 0;
}