def main():
    n = int(input())
    d = []
    for _ in range(n):
        d.append(list(map(int, input().split())))
    d_sum = [[0 for _ in range(n + 1)] for _ in range(n + 1)]
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            d_sum[i][j] = d_sum[i - 1][j] + d_sum[i][j - 1] - d_sum[i - 1][j - 1] + d[i - 1][j - 1]
    q = int(input())
    for _ in range(q):
        p = int(input())
        tmp = 0
        for i in range(n + 1):
            for j in range(n + 1):
                for k in range(i, n + 1):
                    for l in range(j, n + 1):
                        if (k - i) * (l - j) <= p:
                            tmp = max(tmp, d_sum[k][l] - d_sum[i][l] - d_sum[k][j] + d_sum[i][j])
        print(tmp)


if __name__ == '__main__':
    main()

