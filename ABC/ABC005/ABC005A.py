def main():
    per_wheat, weight = map(int, input().split())
    print(weight // per_wheat)


if __name__ == '__main__':
    main()

