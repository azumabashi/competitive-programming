from math import pi


def main():
    circles = int(input())
    radius = sorted([int(input()) for _ in range(circles)], reverse=True)
    is_red = True
    answer = 0
    for r in radius:
        if is_red:
            answer += r ** 2
            is_red = False
        else:
            answer -= r ** 2
            is_red = True
    print(answer * pi)


if __name__ == '__main__':
    main()

