def main():
    total = int(input())
    answer = 0
    for i in range(1, total + 1):
        answer = max(answer, i * (total - i))
    print(answer)


if __name__ == '__main__':
    main()

