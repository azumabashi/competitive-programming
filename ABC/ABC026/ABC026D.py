from math import sin, pi


def main():
    a, b, c = map(int, input().split())

    def f(t):
        return a * t + b * sin(c * t * pi) - 100

    ng = 0
    ok = 100

    for _ in range(10 ** 6):
        mid = (ok + ng) / 2
        if f(mid) > 0:
            ok = mid
        else:
            ng = mid

    print(ok)


if __name__ == '__main__':
    main()
