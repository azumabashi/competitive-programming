def main():
    employee = int(input())
    assistants = [[0] for _ in range(employee)]
    for i in range(employee - 1):
        assistants[int(input()) - 1].append(i + 1)
    pay = [0 for _ in range(employee)]
    for i in range(employee - 1, -1, -1):
        if assistants[i] == [0]:
            pay[i] = 1
        else:
            assistant_pay = sorted([pay[a] for a in assistants[i][1:]])
            pay[i] = assistant_pay[0] + assistant_pay[-1] + 1
    print(pay[0])


if __name__ == '__main__':
    main()

