def main():
    people_num = int(input())
    print(people_num + 1 if people_num % 2 else people_num - 1)


if __name__ == '__main__':
    main()

