def main():
    w, h = map(lambda x: int(x) - 1, input().split())
    num = 1
    den = 1
    mod = 10 ** 9 + 7
    for i in range(1, w + h + 1):
        num *= i
        num %= mod
    for _ in range(2):
        for i in range(1, w + 1):
            den *= i
            den %= mod
        w, h = h, w
    print(num * pow(den, mod - 2, mod) % mod)


if __name__ == '__main__':
    main()
