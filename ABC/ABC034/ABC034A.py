def main():
    score = list(map(int, input().split()))
    print("Better" if score[0] < score[1] else "Worse")


if __name__ == '__main__':
    main()

