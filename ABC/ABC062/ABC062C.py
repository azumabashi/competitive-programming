from math import floor, ceil


def result_dividing_by_a_direction(x, y, is_ceil):
    new_x = floor(x / 3)
    if is_ceil:
        new_x = ceil(x / 3)
    return abs(new_x - (x - 2 * new_x)) * y


def result_dividing_2plus1(x, y, d):
    area1 = x * d
    area2 = (y - d) * (x // 2)
    area3 = (y - d) * (x - (x // 2))
    return max(area1, area2, area3) - min(area1, area2, area3)


def main():
    h, w = map(int, input().split())
    ans = h * w
    for i in range(2):
        ans = result_dividing_by_a_direction(h, w, i)
        ans = min(result_dividing_by_a_direction(w, h, i), ans)
    for d in range(1, w):
        ans = min(result_dividing_2plus1(h, w, d), ans)
    for d in range(1, h):
        ans = min(result_dividing_2plus1(w, h, d), ans)
    print(ans)


if __name__ == '__main__':
    main()

