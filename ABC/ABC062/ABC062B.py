def main():
    height, width = map(int, input().split())
    image = ["#" * (width+ 2)] * (height + 2)
    for i in range(1, height + 1):
        image[i] = "#" + input() + "#"
    for i in range(height + 2):
        print(image[i])


if __name__ == '__main__':
    main()

