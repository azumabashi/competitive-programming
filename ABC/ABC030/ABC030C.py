from bisect import bisect_left


def main():
    airplanes = list(map(int, input().split()))
    required_time = list(map(int, input().split()))
    departure_time = [list(map(int, input().split())) for _ in range(2)]
    departure_from = 0
    arriving_time = departure_time[departure_from][0] + required_time[departure_from]
    answer = 1
    while True:
        if departure_from == 0:
            departure_from = 1
        else:
            departure_from = 0
        next_index = bisect_left(departure_time[departure_from], arriving_time)
        if next_index > airplanes[departure_from] - 1:
            break
        else:
            arriving_time = departure_time[departure_from][next_index] + required_time[departure_from]
            answer += 1
    print(answer // 2)


if __name__ == '__main__':
    main()

