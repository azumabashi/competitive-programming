def main():
    results = list(map(int, input().split()))
    winning_average = [results[1] / results[0], results[3] / results[2]]
    if winning_average[0] > winning_average[1]:
        print("TAKAHASHI")
    elif winning_average[0] == winning_average[1]:
        print("DRAW")
    else:
        print("AOKI")


if __name__ == '__main__':
    main()

