def main():
    hour, minute = map(int, input().split())
    if hour >= 12:
        hour -= 12
    degree = abs((60 * hour + minute) * 0.5 - minute * 6)
    print(min(degree, 360 - degree))


if __name__ == '__main__':
    main()

