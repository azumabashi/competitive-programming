from fractions import gcd
from functools import reduce


def main():
    n = int(input())
    numbers_gcd = reduce(gcd, list(map(int, input().split())))
    answer = 0
    while numbers_gcd > 0:
        if numbers_gcd % 2:
            break
        else:
            numbers_gcd //= 2
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()