def main():
    n, target = map(int, input().split())
    count = {}
    numbers = list(map(int, input().split()))
    for num in numbers:
        if num in count:
            count[num] += 1
        else:
            count[num] = 1
    count = sorted(count.values())
    print(sum(count[:-target]) if len(count) > target else 0)


if __name__ == '__main__':
    main()