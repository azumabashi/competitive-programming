def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    even_num = 0
    odd_num = 0
    multiple_of_4 = 0
    for num in numbers:
        if not num % 4:
            multiple_of_4 += 1
        elif not num % 2:
            even_num += 1
        else:
            odd_num += 1
    if multiple_of_4 >= odd_num or (multiple_of_4 + 1 >= odd_num and even_num == 0):
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()

