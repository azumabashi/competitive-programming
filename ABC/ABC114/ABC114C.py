from itertools import product


def main():
    N = int(input())
    result = 0
    numbers = ["3", "5", "7"]
    for i in range(3, len(str(N)) + 1):
        for p in product(numbers, repeat=i):
            flag = True
            tmp = "".join(p)
            for n in numbers:
                if n in tmp:
                    continue
                else:
                    flag = False
            if flag:
                if int(tmp) <= N:
                    print(int(tmp), flag)
                    result += 1
    print(result)


if __name__ == '__main__':
    main()