def main():
    S = input()
    result = 800
    for i in range(len(S) - 2):
        result = min(abs(int(S[i:i + 3]) - 753), result)
    print(result)


if __name__ == '__main__':
    main()