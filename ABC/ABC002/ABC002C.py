def main():
    coordinates = list(map(int, input().split()))
    for i in range(2, 6):
        coordinates[i] -= coordinates[i % 2]
    print(abs(coordinates[2] * coordinates[5] - coordinates[3] * coordinates[4]) / 2)


if __name__ == '__main__':
    main()

