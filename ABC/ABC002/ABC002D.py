def main():
    diet_members, relation_num = map(int, input().split())
    relations = [set([i]) for i in range(diet_members)]
    answer = 1
    for _ in range(relation_num):
        x, y = map(lambda i: int(i) - 1, input().split())
        relations[x].add(y)
        relations[y].add(x)
    for i in range(1, pow(2, diet_members)):
        bin_i = bin(i)[2:].zfill(diet_members)
        now_relation = set()
        for j in range(diet_members):
            if bin_i[j] == "1":
                now_relation.add(j)
        if all(now_relation <= relations[j] for j in now_relation):
            answer = max(answer, len(now_relation))
    print(answer)


if __name__ == '__main__':
    main()

