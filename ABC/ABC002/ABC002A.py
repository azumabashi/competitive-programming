def main():
    money = list(map(int, input().split()))
    print(money[0] if money[0] >= money[1] else money[1])


if __name__ == '__main__':
    main()

