def signed_area(ax, ay, bx, by, cx, cy):
    # (ax, ay), (bx, by), (cx, cy)から成る三角形の符号付き面積
    return ((bx - ax) * (cy - ay) - (cx - ax) * (by - ay)) / 2


def is_crossing(ax, ay, bx, by, cx, cy, dx, dy):
    # 線分(ax, ay) -- (bx, by)と線分(cx, cy) -- (dx, dy)が交差するかの判定
    if (signed_area(ax, ay, bx, by, cx, cy) * signed_area(ax, ay, bx, by, dx, dy) < 0) and\
            (signed_area(cx, cy, dx, dy, ax, ay) * signed_area(cx, cy, dx, dy, bx, by) < 0):
        return 1
    else:
        return 0


def main():
    ax, ay, bx, by = map(int, input().split())
    n = int(input())
    c = [list(map(int, input().split())) for _ in range(n)]
    c.append(c[0])
    answer = 0
    for i in range(n):
        answer += is_crossing(ax, ay, bx, by, c[i][0], c[i][1], c[i + 1][0], c[i + 1][1])
    print(answer // 2 + 1)


if __name__ == '__main__':
    main()

