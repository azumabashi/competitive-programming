def main():
    a, b, result = map(int, input().split())
    if not a - b == result and not a + b == result:
        print("!")
    elif a - b == result and a + b == result:
        print("?")
    elif a - b == result:
        print("-")
    else:
        print("+")


if __name__ == '__main__':
    main()

