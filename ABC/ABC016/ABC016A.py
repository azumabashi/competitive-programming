def main():
    month, day = map(int, input().split())
    print("YES" if not month % day else "NO")


if __name__ == '__main__':
    main()

