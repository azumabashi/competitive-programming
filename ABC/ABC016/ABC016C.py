def main():
    users, friend = map(int, input().split())
    friend_index = [[0] for _ in range(users + 1)]
    for _ in range(friend):
        a, b = map(int, input().split())
        friend_index[a].append(b)
        friend_index[b].append(a)
    for i in range(1, users + 1):
        friend_friend = set()
        for f in friend_index[i]:
            friend_friend = friend_friend.union(set(friend_index[f])) - set(friend_index[i]) - set([i])
        print(len(friend_friend))


if __name__ == '__main__':
    main()

