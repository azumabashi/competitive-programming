def main():
    N = int(input())
    height = list(map(int, input().split(" ")))
    target = [0] * N
    result = 0
    print(target)
    while not height == target:
        print(height)
        end = False
        result += 1
        for i in range(N):
            if not end:
                if height[i] > 0:
                    end = True
                    height[i] -= 1
            else:
                if height[i] > 0:
                    height[i] -= 1
                else:
                    break
    print(result)


if __name__ == '__main__':
    main()
