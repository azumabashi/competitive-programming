def main():
    s = int(input())
    numbers = [s]
    while True:
        next_number = 0
        if numbers[-1] % 2 == 0:
            next_number = numbers[-1] // 2
        else:
            next_number = numbers[-1] * 3 + 1
        if next_number in numbers:
            print(len(numbers) + 1)
            break
        else:
            numbers.append(next_number)


if __name__ == '__main__':
    main()