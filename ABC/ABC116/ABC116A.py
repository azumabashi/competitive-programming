def main():
    side_length = list(map(int, input().split(" ")))
    even_side = 0
    for i in range(3):
        if side_length[i] % 2 == 0:
            even_side = side_length.pop(i)
            break
    if even_side ** 2 + side_length[0] ** 2 == side_length[1] ** 2:
        print(even_side * side_length[0] // 2)
    else:
        print(even_side * side_length[1] // 2)


if __name__ == '__main__':
    main()