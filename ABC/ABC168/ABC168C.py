from math import sqrt, cos, pi


def main():
    a, b, h, m = map(int, input().split())
    theta = abs(6 * m - 0.5 * (60 * h + m))
    theta = min(theta, 360 - theta)
    if theta == 0:
        print(abs(b - a))
    elif theta == 180:
        print(a + b)
    else:
        print(sqrt(a ** 2 + b ** 2 - 2 * a * b * cos(theta * pi / 180)))


if __name__ == '__main__':
    main()

