def main():
    tasks = list(map(int, input().split(" ")))
    tasks.sort()
    print(abs(tasks[0] - tasks[1]) + abs(tasks[1] - tasks[2]))


if __name__ == '__main__':
    main()