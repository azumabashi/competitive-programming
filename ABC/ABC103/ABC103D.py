def main():
    islands, request_num = map(int, input().split())
    demolition = []
    requests = []
    for _ in range(request_num):
        requests.append(list(map(int, input().split())))
    requests.sort(key=lambda x: x[0], reverse=True)
    for begin, end in requests:
        if 0 < len(demolition) and begin <= demolition[-1] < end:
            continue
        else:
            demolition.append(begin)
    print(len(demolition))


if __name__ == '__main__':
    main()

