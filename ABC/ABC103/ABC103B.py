def main():
    string = input()
    target = input()
    result = False
    for _ in range(len(target)):
        if string == target:
            result = True
            break
        string = string[-1] + string[:-1]
    if result:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()