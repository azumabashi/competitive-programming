from functools import reduce
from math import gcd


def find_lcm(a, b):
    return a * b // gcd(a, b)


def main():
    number = int(input())
    mods = list(map(int, input().split(" ")))
    be_divided = reduce(find_lcm, mods) - 1
    answer = 0
    for mod in mods:
        answer += be_divided % mod
    print(answer)


if __name__ == '__main__':
    main()