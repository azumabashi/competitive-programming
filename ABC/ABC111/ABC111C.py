def main():
    n = int(input())
    v = input().split(" ")
    even = {}
    odd = {}
    for i in range(n):
        if i % 2 == 0:
            even.setdefault(v[i], 0)
            even[v[i]] += 1
        else:
            odd.setdefault(v[i], 0)
            odd[v[i]] += 1
    even = sorted(even.items(), key=lambda x: x[1], reverse=True)
    odd = sorted(odd.items(), key=lambda x: x[1], reverse=True)
    # print(even,odd)
    if even[0][0] == odd[0][0]:
        if len(even) > 1 and len(odd) > 1:
            print(n - int(even[0][1]) - max(even[1][1], odd[1][1]))
        elif len(even) > 1:
            print(n - odd[0][1] - even[1][1])
        elif len(odd) > 1:
            print(n - even[0][1] - odd[1][1])
        else:
            print(n // 2)
    else:
        print(n - int(even[0][1]) - int(odd[0][1]))


if __name__ == '__main__':
    main()