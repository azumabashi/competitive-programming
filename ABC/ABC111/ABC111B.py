def main():
    N = int(input())
    next_ABCs = [111, 222, 333, 444, 555, 666, 777, 888, 999]
    for next in next_ABCs:
        if N <= next:
            print(next)
            break


if __name__ == '__main__':
    main()