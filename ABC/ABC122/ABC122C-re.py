def main():
    N, Q = map(int, input().split(" "))
    S = input()
    result = [0] * (N + 1)
    for i in range(1, N + 1):
        if S[i-1:i+1] == "AC":
            result[i] = result[i - 1] + 1
        else:
            result[i] = result[i - 1]
    for _ in range(Q):
        l, r = map(int, input().split(" "))
        print(result[r - 1] - result[l - 1])


if __name__ == '__main__':
    main()