def main():
    result = []
    strings = ["A", "T", "G", "C"]
    S = input()
    tmp = 0
    for s in S:
        if s in strings:
            tmp += 1
        else:
            result.append(tmp)
            tmp = 0
    result.append(tmp)
    print(max(result))


if __name__ == '__main__':
    main()