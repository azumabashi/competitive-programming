def main():
    b = input()
    answer = {"A": "T", "T": "A", "G": "C", "C": "G"}
    print(answer[b])


if __name__ == '__main__':
    main()