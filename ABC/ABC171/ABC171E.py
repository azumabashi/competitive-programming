def main():
    n = int(input())
    all_xor = 0
    a = list(map(int, input().split()))
    for i in range(n):
        all_xor ^= a[i]
    answer = []
    for i in range(n):
        answer.append(all_xor ^ a[i])
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

