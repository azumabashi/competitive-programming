from string import ascii_lowercase


def main():
    n = int(input())
    answer = []
    while n > 0:
        answer.append(ascii_lowercase[n % 26 - 1])
        if n % 26:
            n //= 26
        else:
            n = (n - 1) // 26
    print("".join(answer[::-1]))


if __name__ == '__main__':
    main()

