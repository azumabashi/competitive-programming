from collections import Counter


def main():
    n = int(input())
    a = list(map(int, input().split()))
    d_a = Counter(a)
    q = int(input())
    answer = sum(a)
    for _ in range(q):
        b, c = map(int, input().split())
        if b in d_a:
            answer += (c - b) * d_a[b]
            if c in d_a:
                d_a[c] += d_a[b]
            else:
                d_a[c] = d_a[b]
            d_a[b] = 0
        print(answer)


if __name__ == '__main__':
    main()

