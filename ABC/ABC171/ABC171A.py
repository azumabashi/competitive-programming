from string import ascii_lowercase


def main():
    a = input()
    if a in ascii_lowercase:
        print("a")
    else:
        print("A")


if __name__ == '__main__':
    main()

