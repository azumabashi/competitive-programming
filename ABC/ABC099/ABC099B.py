def main():
    west_tower_height, east_tower_height = map(int, input().split(" "))
    east_tower_num = east_tower_height - west_tower_height
    print(east_tower_num * (east_tower_num + 1) // 2 - east_tower_height)


if __name__ == '__main__':
    main()