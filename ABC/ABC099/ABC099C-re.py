# 貪欲的だが「6^i円で払う分」と「9^i円で払う分」にNを分割してN+1通り全探索．


def how_many_times_withdraw(withdraw, base, limit):
    result = 0
    while withdraw >= base:
        result += withdraw // (base ** limit)
        withdraw = withdraw % (base ** limit)
        limit -= 1
    return result + withdraw


def main():
    N = int(input())
    limit = []
    answer = float("inf")
    for yen in [6, 9]:
        i = 1
        while yen ** i <= N:
            i += 1
        limit.append(i - 1)
    for i in range(N + 1):
        answer = min(answer,
                     how_many_times_withdraw(i, 6, limit[0]) + how_many_times_withdraw(N - i, 9, limit[1]))
    print(answer)


if __name__ == '__main__':
    main()