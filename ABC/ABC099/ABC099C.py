from heapq import heappop, heapify
# 貪欲的な解法．N以下の最大の引き出し金額でできるだけ引き出せば最小になりそう．
# 少なくともN=44852のとき解が一致しない．


def main():
    N = int(input())
    withdraw = []
    answer = 0
    for yen in [6, 9]:
        i = 1
        while yen ** i <= N:
            withdraw.append(-1 * yen ** i)
            i += 1
    heapify(withdraw)
    print(withdraw)
    while N > 6:
        max_withdraw = heappop(withdraw) * -1
        answer += N // max_withdraw
        N = N % max_withdraw
        print(N, max_withdraw, answer)
    answer += N
    print(answer, N)


if __name__ == '__main__':
    main()