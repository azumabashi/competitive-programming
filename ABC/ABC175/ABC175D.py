class UnionFind:
    def __init__(self, node):
        self.parent = [-1 for _ in range(node)]

    def find(self, target):
        if self.parent[target] < 0:
            return target
        else:
            self.parent[target] = self.find(self.parent[target])
            return self.parent[target]

    def is_same(self, x, y):
        return self.find(x) == self.find(y)

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x == root_y:
            return
        if self.parent[root_x] > self.parent[root_y]:
            root_x, root_y = root_y, root_x
        self.parent[root_x] += self.parent[root_y]
        self.parent[root_y] = root_x

    def get_size(self, x):
        return -self.parent[self.find(x)]

    def get_root(self):
        return [i for i, root in enumerate(self.parent) if root < 0]


def main():
    n, k = map(int, input().split())
    p = [int(k) - 1 for k in input().split()]
    c = [int(k) for k in input().split()]
    inf = 1 << 70
    dist_from_root = [inf for _ in range(n)]
    path_to_root = [0 for _ in range(n)]
    uf = UnionFind(n)
    for start in range(n):
        uf.union(start, p[start])
    loop = {}
    for root in uf.get_root():
        dist_from_root[root] = 0
        nv = p[root]
        before = root
        while root != nv:
            dist_from_root[nv] = dist_from_root[before] + c[p[before]]
            path_to_root[nv] = path_to_root[before] + 1
            nv, before = p[nv], nv
        loop[root] = dist_from_root[before] + c[p[before]]
    ans = -inf
    all_root = [uf.find(i) for i in range(n)]
    all_size = [-uf.parent[all_root[i]] for i in range(n)]
    for start in range(n):
        for goal in range(n):
            if all_root[start] != all_root[goal]:
                continue
            size = all_size[start]
            if start == goal and size > k:
                continue
            root = all_root[start]
            l_long = loop[root]
            r = k

            # start -> root -> loop -> root -> goal
            res = (loop[root] - dist_from_root[start] if root != start else 0) + dist_from_root[goal]
            moved = path_to_root[goal] + (size - path_to_root[start] if start != root else 0)
            r -= moved
            if l_long >= 0:
                res += l_long * (r // size)
                moved += (r // size) * size
            if moved > 0:
                ans = max(ans, res)

            # start -> goal (without loop)
            if path_to_root[goal] > path_to_root[start] and path_to_root[goal] - path_to_root[start] <= k:
                ans = max(ans, dist_from_root[goal] - dist_from_root[start])
    print(ans)


if __name__ == '__main__':
    main()
