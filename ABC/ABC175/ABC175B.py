def is_exist_triangle(a, b, c):
    l = [a, b, c]
    l.sort()
    return l[0] + l[1] > l[2] if len(set(l)) == 3 else False


def main():
    n = int(input())
    length = list(map(int, input().split()))
    ans = 0
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                ans += 1 if is_exist_triangle(length[i], length[j], length[k]) else 0
    print(ans)


if __name__ == '__main__':
    main()
