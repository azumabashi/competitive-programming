from math import factorial


def main():
    dog, monkey = map(int, input().split())
    answer = 0
    mod = 10 ** 9 + 7
    if dog == monkey or dog + 1 == monkey or dog - 1 == monkey:
       answer = (factorial(dog) % mod) * (factorial(monkey) % mod) % mod
       if dog == monkey:
            answer = answer * 2 % mod
    print(answer)


if __name__ == '__main__':
    main()

