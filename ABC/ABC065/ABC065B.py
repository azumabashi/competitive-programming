def main():
    buttons = int(input())
    next_lighten_up = [int(input()) for _ in range(buttons)]
    searched = [False] * (buttons + 1)
    answer = 1
    index = 1
    while True:
        if next_lighten_up[index - 1] == 2:
            break
        elif searched[next_lighten_up[index - 1]]:
            answer = -1
            break
        else:
            searched[next_lighten_up[index - 1]] = True
            answer += 1
            index = next_lighten_up[index - 1]
    print(answer)


if __name__ == '__main__':
    main()

