def main():
    print("YES" if input()[-1] == "T" else "NO")


if __name__ == '__main__':
    main()

