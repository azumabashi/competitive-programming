def main():
    length = int(input())
    numbers = list(map(int, input().split())) + [0]
    before_num = numbers[0]
    answer = 0
    count = 0
    for i in range(1, length + 1):
        if before_num < numbers[i]:
            count += 1
        else:
            answer += count * (count + 1) // 2
            count = 0
        before_num = numbers[i]
    print(answer + length)


if __name__ == '__main__':
    main()

