def main():
    display_size = [list(map(int, input().split())) for _ in range(2)]
    if display_size[0][0] == display_size[1][0] or display_size[0][1] == display_size[1][1] \
            or display_size[0][0] == display_size[1][1] or display_size[0][1] == display_size[1][0]:
        print("YES")
    else:
        print("NO")


if __name__ == '__main__':
    main()

