def main():
    h, w, k = map(int, input().split())
    grid = [list(input()) for _ in range(h)]
    ans = now = 0
    for now_h in range(pow(2, h)):
        for now_w in range(pow(2, w)):
            now = 0
            for i in range(h):
                for j in range(w):
                    if (now_h >> i) & 1 or (now_w >> j) & 1:
                        continue
                    elif grid[i][j] == "#":
                        now += 1
            if now == k:
                ans += 1
    print(ans)


if __name__ == '__main__':
    main()

