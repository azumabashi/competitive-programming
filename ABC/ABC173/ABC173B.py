def main():
    n = int(input())
    l = ["AC", "WA", "TLE", "RE"]
    cnt = {ll: 0 for ll in l}
    for _ in range(n):
        cnt[input()] += 1
    for ll in l:
        print(ll, "x", cnt[ll])


if __name__ == '__main__':
    main()

