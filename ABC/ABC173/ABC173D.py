def main():
    n = int(input())
    a = list(map(int, input().split()))
    ans = 0
    a.sort()
    for i in range(1, n):
        ans += a[n - i // 2 - 1]
    print(ans)


if __name__ == '__main__':
    main()

