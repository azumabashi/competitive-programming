def main():
    flowers = int(input())
    petals = list(map(int, input().split()))
    answer = 0
    for petal in petals:
        pull_off = 0
        while True:
            if (petal - pull_off) % 2 == 1 and not (petal - pull_off) % 3 == 2:
                answer += pull_off
                break
            else:
                pull_off += 1
    print(answer)


if __name__ == '__main__':
    main()

