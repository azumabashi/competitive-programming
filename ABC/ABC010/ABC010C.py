from math import sqrt


def main():
    xa, ya, xb, yb, time, speed = map(int, input().split())
    girls = int(input())
    girls_homes = [tuple(map(int, input().split())) for _ in range(girls)]
    answer = "NO"
    for home in girls_homes:
        distance = sqrt((xa - home[0]) ** 2 + (ya - home[1]) ** 2) + sqrt((xb - home[0]) ** 2 + (yb - home[1]) ** 2)
        if distance <= time * speed:
            answer = "YES"
            break
    print(answer)


if __name__ == '__main__':
    main()

