def main():
    length, operation = map(int, input().split())
    sequence = [0] * length
    for _ in range(operation):
        lower_limit, upper_limit, new_value = map(int, input().split())
        sequence[lower_limit - 1:upper_limit] = [new_value] * (upper_limit - lower_limit + 1)
    for seq in sequence:
        print(seq)


if __name__ == '__main__':
    main()

