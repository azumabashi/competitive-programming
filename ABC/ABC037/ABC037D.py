import sys


def main():
    h, w = map(int, input().split())
    grid = [list(map(int, input().split())) for _ in range(h)]
    mod = 10 ** 9 + 7
    sys.setrecursionlimit(mod)
    dp = [[-1 for _ in range(w)] for _ in range(h)]

    def search(i, j):
        res = 1
        if dp[i][j] > 0:
            return dp[i][j]
        for di, dj in [[0, 1], [1, 0], [0, -1], [-1, 0]]:
            new_i = i + di
            new_j = j + dj
            if 0 <= new_i < h and 0 <= new_j < w and grid[new_i][new_j] > grid[i][j]:
                res += search(new_i, new_j)
            res %= mod
        dp[i][j] = res
        return res

    ans = 0
    for i in range(h):
        for j in range(w):
            search(i, j)

    for i in range(h):
        for j in range(w):
            ans += dp[i][j]
            ans %= mod

    print(ans)


if __name__ == '__main__':
    main()
