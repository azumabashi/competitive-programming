def main():
    length, width = map(int, input().split())
    sequence = list(map(int, input().split()))
    cumulative_sum = [0] * (length + 1)
    cumulative_sum[1] = sequence[0]
    for i in range(2, length + 1):
        cumulative_sum[i] = cumulative_sum[i - 1] + sequence[i - 1]
    answer = 0
    for i in range(0, length - width + 1):
        answer += cumulative_sum[i + width] - cumulative_sum[i]
    print(answer)


if __name__ == '__main__':
    main()

