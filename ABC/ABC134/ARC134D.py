def main():
    box = int(input())
    target = [0] + list(map(int, input().split()))
    balls = [0] * (box + 1)
    put = []
    for i in range(box, 1, -1):
        if not sum(balls[i::i]) % 2 == target[i]:
            balls[i] = 1
            put.append(str(i))
    all_balls = sum(balls)
    if not all_balls % 2 == target[1]:
        put.append("1")
        all_balls += 1
    print(all_balls)
    print(" ".join(put))


if __name__ == '__main__':
    main()

