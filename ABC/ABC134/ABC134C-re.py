def main():
    N = int(input())
    maximum = 0
    next_maximum = 0
    A = []
    for _ in range(N):
        a = int(input())
        A.append(a)
        if a > maximum:
            maximum = a
        elif a > next_maximum:
            next_maximum = a
    for b in A:
        if b == maximum:
            print(next_maximum)
        else:
            print(maximum)


if __name__ == '__main__':
    main()