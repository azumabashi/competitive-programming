#include<bits/stdc++.h>
using namespace std;

int N, M, Q;
vector<int> A, B, C, D;
long long answer = 0;

void dfs(vector<int> seq) {
    if (seq.size() == N + 1) {
        long long now = 0;
        for (int i = 0; i < Q; i++) {
            if (seq[B[i]] - seq[A[i]] == C[i]) now += D[i];
        }
        answer = max(answer, now);
        return;
    }
    seq.push_back(seq.back());
    for (int i = seq.back(); i <= M; i++) {
        dfs(seq);
        seq.back()++;
    }
}

int main() {
    cin >> N >> M >> Q;
    A = B = C = D = vector<int>(Q);
    for (int i = 0; i < Q; i++) {
        cin >> A[i] >> B[i] >> C[i] >> D[i];
    }
    dfs({1});
    cout << answer << endl;
    return 0;
}