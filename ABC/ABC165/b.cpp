#include<bits/stdc++.h>
using namespace std;

int main() {
    long long X;
    cin >> X;
    long long now = 100;
    int answer = 0;
    while (now < X) {
        answer++;
        now += now / 100;
    }
    cout << answer << endl;
    return 0;
}