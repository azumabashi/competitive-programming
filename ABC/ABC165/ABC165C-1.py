# solve with DFS.

answer = 0


def main():
    n, m, q = map(int, input().split())
    a = []
    b = []
    c = []
    d = []
    for i in range(q):
        aa, bb, cc, dd = map(int, input().split())
        a.append(aa)
        b.append(bb)
        c.append(cc)
        d.append(dd)

    global answer

    def dfs(seq):
        if len(seq) == n + 1:
            global answer
            now = 0
            for i in range(q):
                if seq[b[i]] - seq[a[i]] == c[i]:
                    now += d[i]
            answer = max(answer, now)
            return
        else:
            for i in range(seq[-1], m + 1):
                dfs(seq + [i])

    dfs([1])
    global answer
    print(answer)


if __name__ == '__main__':
    main()

