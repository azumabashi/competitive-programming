from itertools import combinations_with_replacement


def main():
    n, m, q = map(int, input().split())
    a = []
    b = []
    c = []
    d = []
    answer = 0
    for i in range(q):
        aa, bb, cc, dd = map(int, input().split())
        a.append(aa - 1)
        b.append(bb - 1)
        c.append(cc)
        d.append(dd)
    numbers = [i for i in range(1, m + 1)]
    for seq in combinations_with_replacement(numbers, n):
        now = 0
        for i in range(q):
            if seq[b[i]] - seq[a[i]] == c[i]:
                now += d[i]
        answer = max(answer, now)
    print(answer)


if __name__ == '__main__':
    main()

