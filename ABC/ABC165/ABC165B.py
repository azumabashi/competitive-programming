from math import floor


def main():
    x = int(input())
    a = 100
    i = 0
    while a < x:
        a = floor(a * 1.01)
        i += 1
    print(i)


if __name__ == '__main__':
    main()

