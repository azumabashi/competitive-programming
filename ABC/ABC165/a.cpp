#include<bits/stdc++.h>
using namespace std;

int main() {
    int K, A, B;
    cin >> K >> A >> B;
    int answer = -1;
    for (int i = A; i <= B; i++) {
        if (i % K == 0) {
            answer = i;
            break;
        }
    }
    if (0 <= answer) {
        cout << "OK" << endl;
    } else {
        cout << "NG" << endl;
    }
    return 0;
}