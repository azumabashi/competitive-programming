from math import floor


def main():
    a, b, n = map(int, input().split())

    def f(x):
        return floor((x * a) / b) - a * floor(x / b)

    print(f(min(n, b - 1)))


if __name__ == '__main__':
    main()

