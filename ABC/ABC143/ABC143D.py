def triangle(a, b, c):
    return (a < b + c) and (b < a + c) and (c < a + b)


def main():
    N = int(input())
    ll = input().split(" ")
    ll.sort()
    ll.reverse()
    L = []
    M = []
    tmp = ''
    for l in ll:
        if tmp != l:
            L.append(int(l))
            tmp = l
        else:
            M.append(int(l))
    result = 0
    # 3辺相異なるとき
    for i in range(len(L)):
        for j in range(i + 1, len(L)):
            a = L[i]
            b = L[j]
            for k in range(j + 1, len(L)):
                c = L[k]
                if triangle(a, b, c):
                    result += 1

    # 二等辺(正三角形含む)
    tmp = False
    for i in range(len(M)):
        a = M[i]
        for j in range(i + 1, len(M)):
            b = j[i]
            if a == b and tmp:
                tmp = True
                result += 1
            elif a == b and not tmp:
                continue
            elif triangle(a, a, b):
                tmp = False
                result += 1
            else:
                continue

    print(result)


if __name__ == '__main__':
    main()