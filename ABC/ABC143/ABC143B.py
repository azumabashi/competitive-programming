def main():
    N = int(input())
    ll = input().split(" ")
    d = []
    result = 0
    for l in ll:
        d.append(int(l))
    for i in range(N):
        for j in range(i + 1, N):
            result += d[i] * d[j]
    print(result)


if __name__ == '__main__':
    main()