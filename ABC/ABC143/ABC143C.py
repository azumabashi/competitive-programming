def main():
    N = int(input())
    S = input()
    result = 0
    slime = ''
    for s in S:
        if slime != s:
            slime = s
            result += 1
        else:
            continue
    print(result)


if __name__ == '__main__':
    main()