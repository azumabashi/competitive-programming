from bisect import bisect_left


def main():
    N = int(input())
    L = list(map(int, input().split(" ")))
    L.sort()
    result = 0
    for i in range(N):
        for j in range(i + 1, N):
            k = bisect_left(L, L[i] + L[j])
            result += k - j - 1
    print(result)


if __name__ == '__main__':
    main()