def main():
    cost, dislike = map(int, input().split())
    dislike_numbers = list(input().split())
    answer = cost
    while True:
        if not set(str(answer)) & set(dislike_numbers):
            print(answer)
            break
        else:
            answer += 1


if __name__ == '__main__':
    main()

