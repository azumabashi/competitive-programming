def main():
    string_num, length = map(int, input().split())
    strings = [input() for _ in range(string_num)]
    strings.sort()
    print("".join(strings))


if __name__ == '__main__':
    main()

