def main():
    numbers = input()
    answer = 0
    for i in range(pow(2, len(numbers) - 1)):
        sign = bin(i)[2:].zfill(len(numbers) - 1).replace("1", "+").replace("0", "x")
        formula = numbers[0]
        for i in range(1, len(numbers)):
            formula += sign[i - 1] + numbers[i]
        answer += eval(formula.replace("x", ""))
    print(answer)


if __name__ == '__main__':
    main()

