def main():
    cards = [input() for _ in range(3)]
    next_turn = {"a": 0, "b": 1, "c": 2}
    answer = {}
    for person, index in next_turn.items():
        answer[index] = person.upper()
    now_turn = 0
    while True:
        if cards[now_turn] == "":
            print(answer[now_turn])
            break
        else:
            next = cards[now_turn][0]
            cards[now_turn] = cards[now_turn][1:]
            now_turn = next_turn[next]


if __name__ == '__main__':
    main()

