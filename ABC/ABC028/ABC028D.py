def main():
    upper_limit, target = map(int, input().split())
    pattern = (target - 1) * (upper_limit - target) * 6 + (upper_limit - 1) * 3 + 1
    print(pattern / (upper_limit ** 3))


if __name__ == '__main__':
    main()

