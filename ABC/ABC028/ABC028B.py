def main():
    string = input()
    component = ["A", "B", "C", "D", "E", "F"]
    answer = [0] * len(component)
    for i in range(len(component)):
        answer[i] = string.count(component[i])
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

