from itertools import combinations


def main():
    numbers = list(map(int, input().split()))
    answer = []
    for num in list(combinations(numbers, 3)):
        answer.append(sum(num))
    answer.sort()
    print(answer[-3])


if __name__ == '__main__':
    main()

