def main():
    score = int(input())
    answer = "Bad"
    if score == 100:
        answer = "Perfect"
    elif score >= 90:
        answer = "Great"
    elif score >= 60:
        answer = "Good"
    print(answer)


if __name__ == '__main__':
    main()

