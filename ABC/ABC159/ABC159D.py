def main():
    n = int(input())
    a = list(map(int, input().split()))
    count = {}
    for aa in a:
        if aa in count:
            count[aa] += 1
        else:
            count[aa] = 1
    balls = list(count.items())
    all = 0
    for b in balls:
        all += b[1] * (b[1] - 1) // 2
    for aa in a:
        print(all - count[aa] + 1)


if __name__ == '__main__':
    main()

