def is_kaibun(word):
    result = True
    for i in range(len(word) // 2):
        if word[i] != word[len(word) - i - 1]:
            result = False
            break
    return result


def main():
    s = input()
    l = len(s)
    answer = is_kaibun(s) and is_kaibun(s[:(l - 1) // 2]) and is_kaibun(s[(l + 3) // 2 - 1:])
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()

