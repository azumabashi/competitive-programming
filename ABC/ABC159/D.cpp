#include<bits/stdc++.h>
using namespace std;

long long choose(long long n) {
    return n * (n - 1) / 2;
}

int main() {
    int N;
    cin >> N;
    vector<int> A(N), count(N);
    for (int i = 0; i < N; i++) {
        cin >> A[i];
    }
    for (int i = 0; i < N; i++) {
        A[i]--;
    }
    for (int i = 0; i < N; i++) {
        count[A[i]]++;
    }
    long long all = 0;
    for (int i = 0; i < N; i++) {
        all += choose(count[i]);
    }
    for (int i = 0; i < N; i++) {
        long long answer = all;
        answer -= choose(count[A[i]]);
        answer += choose(count[A[i]] - 1);
        cout << answer << endl;
    }
    return 0;
}