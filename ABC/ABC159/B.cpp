#include<bits/stdc++.h>
using namespace std;

bool is_palindrome(string s) {
    bool result = true;
    int length = s.size();
    for (int i = 0; i < length; i++) {
        if (s[i] != s[length - i - 1]) {
            result = false;
            break;
        }
    }
    return result;
}

int main() {
    string S;
    cin >> S;
    int length = S.size();
    if (is_palindrome(S) and is_palindrome(S.substr((length + 1) / 2)) and is_palindrome(S.substr(0, (length - 1) / 2))) {
        cout << "Yes" << endl;
    } else
    {
        cout << "No" << endl;
    }
    return 0;
}