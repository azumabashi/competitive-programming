def main():
    dates = int(input())
    numbers = list(map(int, input().split()))
    count = {i: 0 for i in range(1, dates + 1)}
    for n in numbers:
        count[n] += 1
    answer = 0
    for c in count.values():
        answer += c * (c - 1) // 2
    for n in numbers:
        print(answer - count[n] + 1)


if __name__ == '__main__':
    main()

