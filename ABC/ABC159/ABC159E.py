def solve():
    from math import ceil
    h, w, kk = map(int, input().split())
    s = [[int(t) for t in input()] for _ in range(h)]
    cnt = [[0 for _ in range(w + 1)] for _ in range(h + 1)]
    for i in range(h):
        for j in range(w):
            cnt[i + 1][j + 1] = cnt[i][j + 1] + cnt[i + 1][j] - cnt[i][j] + s[i][j]
    if h == 1:
        return ceil(cnt[-1][-1] / kk)
    elif cnt[h][w] <= kk:
        return 0
    ans = h + w - 2
    for i in range(1, 1 << (h - 1)):
        cut = [0]
        for j in range(h - 1):
            if (i >> j) & 1:
                cut.append(j + 1)
        cut.append(h)
        res = len(cut) - 2
        divide = res + 1
        seen = [0] * divide
        now_cut = [0]
        for j in range(1, w + 1):
            for k in range(divide):
                if cnt[cut[k + 1]][j] - cnt[cut[k + 1]][0] - cnt[cut[k]][j] + cnt[cut[k]][0] - seen[k] > kk:
                    res += 1
                    now_cut.append(j - 1)
                    for jj in range(divide):
                        seen[jj] = cnt[cut[jj + 1]][j - 1] - cnt[cut[jj + 1]][0] - cnt[cut[jj]][j - 1] + cnt[cut[jj]][0]
                    break
        now_cut.append(w)
        if all(cnt[cut[j + 1]][now_cut[k + 1]] - cnt[cut[j + 1]][now_cut[k]] - cnt[cut[j]][now_cut[k + 1]] +
               cnt[cut[j]][now_cut[k]] <= kk for k in range(len(now_cut) - 1) for j in range(divide)):
            ans = min(ans, res)
    return ans


def main():
    print(solve())


if __name__ == '__main__':
    main()
