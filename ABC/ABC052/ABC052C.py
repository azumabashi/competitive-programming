from collections import Counter
from math import factorial


def main():
    number = factorial(int(input()))
    mod = 10 ** 9 + 7
    prime_factors = []
    while not number % 2:
        prime_factors.append(2)
        number //= 2
    i = 3
    while i <= number:
        if not number % i:
            number //= i
            prime_factors.append(i)
        else:
            i += 2
    answer = 1
    for p in list(Counter(prime_factors).values()):
        answer = answer * (p + 1) % mod
    print(answer)


if __name__ == '__main__':
    main()

