def main():
    length = int(input())
    string = input()
    answer = 0
    integer = 0
    for s in string:
        if s == "I":
            integer += 1
        else:
            integer -= 1
        answer = max(answer, integer)
    print(answer)


if __name__ == '__main__':
    main()

