def main():
    N = int(input())
    diameter = set([int(input()) for _ in range(N)])
    print(len(diameter))


if __name__ == '__main__':
    main()