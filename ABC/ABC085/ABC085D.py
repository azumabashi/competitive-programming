from math import ceil
'''
    考える上では貪欲でよい．攻撃順はb_iが後ろに来るように整列しなおす．
'''


def main():
    number, target = map(int, input().split())
    attacks = []
    for _ in range(number):
        attack = list(map(int, input().split()))
        attacks.append((attack[0], 1))  # 振る
        attacks.append((attack[1], 0))  # 投げつける
    attacks.sort(key=lambda x: x[0], reverse=True)
    answer = 0
    index = 0
    while target > 0:
        if attacks[index][1]:
            answer += ceil(target / attacks[index][0])
            target = 0
        else:
            target -= attacks[index][0]
            index += 1
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()