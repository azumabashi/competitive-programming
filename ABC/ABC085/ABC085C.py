def main():
    number, new_year_gift = map(int, input().split(" "))
    answer = True
    for i in range(number, -1, -1):
        # for文は範囲に注意!! range(number, 0, -1)ではiは1で終わってしまう!!
        if not answer:
            break
        for j in range(number + 1):
            if number < i + j:
                continue
            elif 10000 * i + 5000 * j + 1000 * (number - i - j) == new_year_gift and answer:
                print(i, j, number - i - j)
                answer = False
                break
    if answer:
        print(-1, -1, -1)


if __name__ == '__main__':
    main()
