def main():
    height = [int(input()) for _ in range(2)]
    print(height[0] - height[1])


if __name__ == '__main__':
    main()

