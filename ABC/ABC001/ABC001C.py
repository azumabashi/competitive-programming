from decimal import Decimal, ROUND_HALF_UP


def main():
    degree, wind_speed = map(Decimal, input().split())
    direction = ""
    wind_power = 0
    wind_speed = Decimal(wind_speed / Decimal(60)).quantize(Decimal("0.1"), rounding=ROUND_HALF_UP)
    degree *= 10
    degree_range = [0, 1125]
    for _ in range(16):
        degree_range.append(degree_range[-1] + 2250)
    all_direction = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S",
                     "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"]
    for i in range(17):
        if degree_range[i] <= degree < degree_range[i + 1]:
            direction = all_direction[i]
    wind_power_range_raw = [Decimal(0), Decimal(0.3), Decimal(1.6), Decimal(3.4), Decimal(5.5), Decimal(8.0),
                            Decimal(10.8), Decimal(13.9), Decimal(17.2), Decimal(20.8), Decimal(24.5), Decimal(28.5),
                            Decimal(32.7), Decimal(10000000)]
    wind_power_range = []
    for raw in wind_power_range_raw:
        wind_power_range.append(Decimal(raw).quantize(Decimal("0.1"), rounding=ROUND_HALF_UP))
    decrease = Decimal(0.1)
    for i in range(13):
        upper = Decimal(wind_power_range[i + 1] - decrease).quantize(Decimal("0.1"), rounding=ROUND_HALF_UP)
        if wind_power_range[i] <= wind_speed <= upper:
            wind_power = i
    if wind_power == 0:
        direction = "C"
    print(direction, wind_power)


if __name__ == '__main__':
    main()

