def main():
    distance = int(input()) / 1000
    answer = "00"
    if distance > 70:
        answer = "89"
    elif distance >= 35:
        answer = str(int((distance - 30) // 5 + 80))
    elif distance >= 6:
        answer = str(int(distance + 50))
    elif distance >= 0.1:
        answer = str(int(distance * 10))
    print(answer.zfill(2))


if __name__ == '__main__':
    main()

