def main():
    yen_per_btc = 380000.0
    result = 0
    N = int(input())
    for _ in range(N):
        x, u = input().split(" ")
        if u == "JPY":
            result += float(x)
        else:
            result += (float(x) * yen_per_btc)
    print(result)


if __name__ == '__main__':
    main()