def main():
    S = list(map(int, input().split("/")))
    result = ""
    if S[0] < 2019:
        result = "Heisei"
    elif S[0] == 2019 and S[1] <= 4 and S[2] <= 30:
        result = "Heisei"
    else:
        result = "TBD"

    print(result)


if __name__ == '__main__':
    main()