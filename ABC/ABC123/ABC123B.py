from itertools import permutations


def main():
    result = 700
    cooking_time = []
    for _ in range(5):
        cooking_time.append(int(input()))
    for permutation in permutations(cooking_time, 5):
        tmp = 0
        for i in range(4):
            if not permutation[i] % 10 == 0:
                tmp += (permutation[i] // 10 + 1) * 10
            else:
                tmp += permutation[i]
        result = min(result, tmp + permutation[-1])
    print(result)


if __name__ == '__main__':
    main()
