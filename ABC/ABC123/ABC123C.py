from math import ceil


def main():
    N = int(input())
    capacity = []
    for _ in range(5):
        capacity.append(int(input()))
    min_capacity = min(capacity)
    if N <= min_capacity:
        print(5)
    else:
        print(4 + ceil(N / min_capacity))


if __name__ == '__main__':
    main()