def main():
    antenna = []
    for _ in range(5):
        antenna.append(int(input()))
    k = int(input())
    result = True
    for i in range(5):
        for j in range(i + 1, 5):
            if antenna[j] - antenna[i] > k:
                result = False
    if result:
        print("Yay!")
    else:
        print(":(")


if __name__ == '__main__':
    main()