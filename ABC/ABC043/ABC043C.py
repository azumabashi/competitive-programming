from decimal import Decimal, ROUND_HALF_UP


def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    target = Decimal(str(sum(numbers) / length)).quantize(Decimal('0'), rounding=ROUND_HALF_UP)
    answer = 0
    for num in numbers:
        answer += (num - target) ** 2
    print(answer)


if __name__ == '__main__':
    main()

