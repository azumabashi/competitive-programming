def main():
    children_num = int(input())
    print(children_num * (children_num + 1) // 2)


if __name__ == '__main__':
    main()

