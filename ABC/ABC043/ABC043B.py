def main():
    operations = input()
    answer = ""
    for operation in operations:
        if operation == "B":
            if not answer == "":
                answer = answer[:-1]
        else:
            answer += operation
    print(answer)


if __name__ == '__main__':
    main()

