def main():
    N = int(input())
    S = input()
    result = 0
    for i in range(N - 2):
        if S[i:i+3] == "ABC":
            result += 1
    print(result)


if __name__ == '__main__':
    main()