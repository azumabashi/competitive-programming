from itertools import permutations as p


def main():
    N = int(input())
    P = list(map(int, input().split(" ")))
    Q = list(map(int, input().split(" ")))
    l = []
    for i in range(1, N + 1):
        l.append(i)
    ints = []
    for k in p(l, N):
        ints.append(list(k))
    print(abs(ints.index(P) - ints.index(Q)))


if __name__ == '__main__':
    main()