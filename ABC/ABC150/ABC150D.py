from math import gcd


def main():
    N, M = map(int, input().split(" "))
    A = list(map(int, input().split(" ")))
    A = list(set(A))
    max = A[-1]
    i = 0.5
    result = []
    while i * max <= M:
        tmp = i * max
        print(tmp)
        for b in A[:-1]:
            if float.is_integer(tmp / b - 0.5):
                result.append(tmp)
                break
        i += 1

    print(len(result))


if __name__ == '__main__':
    main()