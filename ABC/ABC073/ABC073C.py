def main():
    repeat = int(input())
    announced_num = [int(input()) for _ in range(repeat)]
    count = {}
    for number in announced_num:
        if number in count:
            if count[number] % 2:
                count[number] = 0
            else:
                count[number] = 1
        else:
            count[number] = 1
    answer = 0
    for i in count.values():
        answer += i
    print(answer)


if __name__ == '__main__':
    main()

