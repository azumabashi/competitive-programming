def main():
    length = int(input())
    sequence = list(map(int, input().split()))
    answer = -float("inf")
    for i in range(length):
        aoki_score = []
        for j in range(length):
            if i == j:
                continue
            else:
                aoki_score.append(sum(sequence[min(i, j) + 1:max(i, j) + 1:2]))
        aoki_index = aoki_score.index(max(aoki_score))
        answer = max(answer, sum(sequence[min(i, aoki_index):max(i, aoki_index) + 1:2]))
    print(answer)


if __name__ == '__main__':
    main()

