def main():
    attack_power, defence_power = map(int, input().split())
    if (attack_power + 1) * defence_power >= (attack_power * (defence_power + 1)):
        print((attack_power + 1) * defence_power)
    else:
        print(attack_power * (defence_power + 1))


if __name__ == '__main__':
    main()

