def main():
    lower_limit, upper_limit = map(int, input().split())
    week = int(input())
    exercise_time = [int(input()) for _ in range(week)]
    for e in exercise_time:
        if e < lower_limit:
            print(lower_limit - e)
        elif lower_limit <= e <= upper_limit:
            print(0)
        else:
            print(-1)


if __name__ == '__main__':
    main()

