def main():
    SHOP_NUM = int(input())
    open_info = [input().split() for _ in range(SHOP_NUM)]
    profit = [list(map(int, input().split())) for _ in range(SHOP_NUM)]
    answer = -float("inf")
    for i in range(1, 1024):
        open = str(format(i, '10b'))
        now_profit = 0
        for shop in range(SHOP_NUM):
            tmp = 0
            for j in range(10):
                if open[j] == "1" and open_info[shop][j] == "1":
                    tmp += 1
            now_profit += profit[shop][tmp]
        answer = max(answer, now_profit)
    print(answer)


if __name__ == '__main__':
    main()