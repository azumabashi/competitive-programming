def get_digits_sum(num):
    result = 0
    while num > 0:
        result += num % 10
        num //= 10
    return result


def main():
    NUMBER = int(input())
    digits_sum = get_digits_sum(NUMBER)
    print("Yes" if not NUMBER % digits_sum else "No")


if __name__ == '__main__':
    main()