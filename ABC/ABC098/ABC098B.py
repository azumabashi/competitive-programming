from collections import Counter


def main():
    N = int(input())
    S = input()
    answer = 0
    for i in range(1, N):
        first_half = Counter(S[:i])
        second_hald = Counter(S[i:])
        answer = max(answer, len(first_half & second_hald))
    print(answer)


if __name__ == '__main__':
    main()