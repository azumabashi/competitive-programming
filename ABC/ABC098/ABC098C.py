def main():
    people_num = int(input())
    directions = input()
    east_direction = [0]
    for i in range(people_num):
        if directions[i] == "E":
            east_direction.append(east_direction[i] + 1)
        else:
            east_direction.append(east_direction[i])
    answer = float("inf")
    print(east_direction)
    for i in range(1, people_num + 1):
        change_to_east = i - 1 - east_direction[i - 1]
        change_to_west = east_direction[-1] - east_direction[i]
        answer = min(answer, change_to_east + change_to_west)
    print(answer)


if __name__ == '__main__':
    main()