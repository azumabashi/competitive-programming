def main():
    dish_num = int(input())
    price = sorted(set([int(input()) for _ in range(dish_num)]))
    print(price[-2])


if __name__ == '__main__':
    main()

