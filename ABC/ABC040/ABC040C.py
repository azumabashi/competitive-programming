def main():
    pillar_num = int(input())
    pillar_height = list(map(int, input().split()))
    min_cost = [0] * pillar_num
    min_cost[1] = abs(pillar_height[1] - pillar_height[0])
    for i in range(2, pillar_num):
        min_cost[i] = min(min_cost[i - 1] + abs(pillar_height[i] - pillar_height[i - 1]),
                          min_cost[i - 2] + abs(pillar_height[i] - pillar_height[i - 2]))
    print(min_cost[-1])


if __name__ == '__main__':
    main()

