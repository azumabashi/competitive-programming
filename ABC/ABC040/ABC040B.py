def main():
    tile_num = int(input())
    answer = float("inf")
    for i in range(1, tile_num + 1):
        for j in range(i, tile_num + 1):
            if i * j <= tile_num:
                answer = min(answer, abs(i - j) + tile_num - i * j)
            else:
                break
    print(answer)


if __name__ == '__main__':
    main()

