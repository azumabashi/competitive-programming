def main():
    block_num, blue_index = map(int, input().split())
    print(min(blue_index - 1, block_num - blue_index))


if __name__ == '__main__':
    main()

