def main():
    list_length = int(input())
    num_list = list(map(int, input().split(" ")))
    answer = -float("inf")
    for i in range(list_length):
        for j in range(i, list_length):
            answer = max(answer, abs(num_list[i] - num_list[j]))
    print(answer)


if __name__ == '__main__':
    main()