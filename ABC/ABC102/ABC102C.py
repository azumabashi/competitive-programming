def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    B = [0] * N
    for i in range(N):
        B[i] = A[i] - i - 1
    B.sort()
    answer = 0
    if N % 2:
        median = B[(N - 1) // 2]
        for b in B:
            answer += abs(b - median)
    else:
        median = [B[N // 2], B[N // 2 - 1]]
        for b in B:
            answer += abs(b - median[0])
        tmp = 0
        for b in B:
            tmp += abs(b - median[1])
        answer = min(answer, tmp)
    print(answer)


if __name__ == '__main__':
    main()