def main():
    number = int(input())
    if number % 2:
        print(number * 2)
    else:
        print(number)


if __name__ == '__main__':
    main()