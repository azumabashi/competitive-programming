from math import floor
from decimal import Decimal


def main():
    a, b = map(Decimal, input().split())
    print(floor(a * b))


if __name__ == '__main__':
    main()

