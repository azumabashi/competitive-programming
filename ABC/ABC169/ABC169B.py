# 全部積撮ってから比較は間に合わない．途中で打ち切る必要がある．


def main():
    n = int(input())
    a = list(map(int, input().split()))
    max_val = pow(10, 18)
    if 0 in a:
        print(0)
    else:
        answer = 1
        for aa in a:
            answer *= aa
            if max_val < answer:
                print(-1)
                break
        else:
            print(answer)


if __name__ == '__main__':
    main()

