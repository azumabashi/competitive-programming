from statistics import median
# 全部A_iにしたときの解xと全部B_iとしたときの解yについて，解全体の集合は{x, x+d, ..., y}とかけることを利用する


def main():
    n = int(input())
    a = [0] * n
    b = [0] * n
    for i in range(n):
        a[i], b[i] = map(int, input().split())
    ans = median(b) - median(a)
    if n % 2 == 0:
        ans *= 2
    ans = int(ans)
    ans += 1
    print(ans)


if __name__ == '__main__':
    main()
