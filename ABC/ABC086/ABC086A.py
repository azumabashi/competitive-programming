def main():
    integers = list(map(int, input().split(" ")))
    if integers[0] * integers[1] % 2:
        print("Odd")
    else:
        print("Even")


if __name__ == '__main__':
    main()