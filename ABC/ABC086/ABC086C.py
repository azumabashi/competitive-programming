def main():
    N = int(input())
    plan = [[0, 0, 0]]
    for _ in range(N):
        plan.append(list(map(int, input().split(" "))))
    answer = True
    for i in range(N):
        distance = abs(plan[i + 1][1] - plan[i][1]) + abs(plan[i + 1][2] - plan[i][2])
        dt = plan[i + 1][0] - plan[i][0]
        if distance > dt or (dt - distance) % 2:
            answer = False
            break
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()