def main():
    a, b = input().split(" ")
    num = int(a + b)
    i = 2
    answer = False
    while i * i <= num:
        if i * i == num:
            answer = True
        i += 1
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()