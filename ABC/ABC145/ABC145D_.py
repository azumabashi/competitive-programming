def main():
    mod = 10 ** 9 + 7
    X, Y = map(int, input().split(" "))
    if (2 * X - Y) % 3 == 0 and (2 * Y - X) % 3 == 0:
        s = (2 * X - Y) // 3
        t = (2 * Y - X) // 3
        if s < 0 or t < 0:
            print(0)
        else:
            numerator = 1
            denominator = 1
            for i in range(s + 1, s + t + 1):
                numerator = (numerator * i) % mod
            for i in range(1, t + 1):
                denominator = (denominator * i) % mod
            print(numerator * pow(denominator, mod - 2, mod) % mod)
            # math.factorialはTLEになりそう?
    else:
        print(0)


if __name__ == '__main__':
    main()