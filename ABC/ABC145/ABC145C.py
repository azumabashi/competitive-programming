from math import sqrt


def get_distance(x1, y1, x2, y2):
    return sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def main():
    x = []
    y = []
    N = int(input())
    distance = 0
    for i in range(N):
        list = input().split(" ")
        x.append(int(list[0]))
        y.append(int(list[1]))
    for j in range(N):
        for k in range(j + 1, N):
            distance += get_distance(x[j], y[j], x[k], y[k])
    print(distance * 2 / N)


if __name__ == '__main__':
    main()