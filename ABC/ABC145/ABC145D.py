from math import floor


def product(x, limit, mod):
    list = []
    for i in range(limit + 1, x + 1):
        list.append(i)
    ans = 1
    for l in list:
        ans *= l
        ans = ans % mod
    print("product:")
    print(ans)
    return ans


def factorial(x, mod):
    ans = 1
    for i in range(2, x + 1):
        ans *= i
        # ans %= mod
    print("factorial: ")
    print(ans)
    return ans


def main():
    X, Y = map(int, input().split(" "))
    mod = 10 ** 9 + 7
    if ((2 * X - Y) * (2 * Y - X)) % 9 == 0:
        n = (2 * X - Y) // 3
        m = (2 * Y - X) // 3
        minimal = floor(min(n, m))
        hoge = (product(n + m, minimal, mod) // pow(factorial(minimal, mod)), mod - 2, mod) % mod
        print(hoge)
        return hoge % mod
    else:
        return 0


if __name__ == '__main__':
    print(main())

