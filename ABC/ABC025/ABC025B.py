def main():
    movement, lower_limit, upper_limit = map(int, input().split())
    location = 0
    for _ in range(movement):
        direction, dist = input().split()
        distance = int(dist)
        if distance < lower_limit:
            distance = lower_limit
        elif distance > upper_limit:
            distance = upper_limit
        if direction == "East":
            location += distance
        else:
            location -= distance
    if location > 0:
        print("East", str(location))
    elif location < 0:
        print("West", str(-1 * location))
    else:
        print(0)


if __name__ == '__main__':
    main()

