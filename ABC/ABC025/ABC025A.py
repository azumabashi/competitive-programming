from itertools import product


def main():
    available_str = list(input())
    index = int(input()) - 1
    print("".join(list(product(available_str, repeat=2))[index]))


if __name__ == '__main__':
    main()

