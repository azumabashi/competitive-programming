"""
決め打ち二分探索のverify．
Xが答え = すべての風船がXまでに割れる = all((X - H_i) // S_i < X)であるmin X
T_i = (X - H_i) // S_i としてT_iをソート -> 二分探索で狭められる
"""


def main():
    balloons = int(input())
    info = [list(map(int, input().split())) for _ in range(balloons)]
    answer = 0
    left = 0
    right = 0
    for initial_height, delta_altitude in info:
        right = max(right, initial_height + delta_altitude * (balloons - 1))
    while right - left > 1:
        mid = (left + right) // 2
        now_time = [(mid - info[i][0]) // info[i][1] for i in range(balloons)]
        now_time.sort()
        if all(now_time[i] >= i for i in range(balloons)):
            right = mid
        else:
            left = mid
    answer = right
    print(answer)


if __name__ == '__main__':
    main()

