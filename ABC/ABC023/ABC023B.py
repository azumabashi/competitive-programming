def main():
    length = int(input())
    target = input()
    accessory = "b"
    answer = -1
    if not accessory == target:
        for i in range(1, 51):
            if i % 3 == 1:
                accessory = "a" + accessory + "c"
            elif i % 3 == 2:
                accessory = "c" + accessory + "a"
            else:
                accessory = "b" + accessory + "b"
            if accessory == target:
                answer = i
                break
    else:
        answer = 0
    print(answer)


if __name__ == '__main__':
    main()

