def main():
    integer = int(input())
    answer = 0
    while integer > 0:
        answer += integer % 10
        integer //= 10
    print(answer)


if __name__ == '__main__':
    main()

