from collections import defaultdict


def main():
    n, m = map(int, input().split())
    a = [0] + [int(k) for k in input().split()]
    for i in range(1, n + 1):
        a[i] += a[i - 1]
        a[i] %= m
    cnt = defaultdict(int)
    for aa in a:
        cnt[aa] += 1
    ans = 0
    for c in cnt.values():
        ans += c * (c - 1) // 2
    print(ans)


if __name__ == '__main__':
    main()
