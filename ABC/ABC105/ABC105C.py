def main():
    target_number = int(input())
    answer = ""
    if target_number == 0:
        answer = "0"
    else:
        while abs(target_number) > 0:
            if target_number % 2:
                answer = "1" + answer
                target_number = (target_number - 1) // -2
            else:
                answer = "0" + answer
                target_number = target_number // -2
    print(answer)


if __name__ == '__main__':
    main()