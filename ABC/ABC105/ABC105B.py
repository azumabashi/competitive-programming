def main():
    limit = int(input())
    result = False
    for i in range(26):
        for j in range(15):
            if 4 * i + 7 * j == limit:
                result = True
                break
    if result:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()