def main():
    width, height, point_num = map(int, input().split())
    points = [tuple(map(int, input().split())) for _ in range(point_num)]
    white_range = [[0, 0], [width, height]]
    for point in points:
        if point[2] == 1:
            white_range[0][0] = max(point[0], white_range[0][0])
        elif point[2] == 2:
            white_range[1][0] = min(point[0], white_range[1][0])
        elif point[2] == 3:
            white_range[0][1] = max(point[1], white_range[0][1])
        else:
            white_range[1][1] = min(point[1], white_range[1][1])
    if white_range[1][0] < white_range[0][0] or white_range[1][0] < white_range[0][0]:
        print(0)
    else:
        print((white_range[1][0] - white_range[0][0]) * (white_range[1][1] - white_range[0][1]))


if __name__ == '__main__':
    main()

