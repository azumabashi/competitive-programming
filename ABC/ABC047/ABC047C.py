def main():
    board = input()
    answer = 0
    series = board[0]
    for b in board:
        if not series == b:
            series = b
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

