def main():
    candies = list(map(int, input().split()))
    print("Yes" if candies[0] + candies[1] == candies[2] or candies[1] + candies[2] == candies[0] or
                   candies[0] + candies[2] == candies[1] else "No")


if __name__ == '__main__':
    main()

