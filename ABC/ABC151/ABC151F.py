def main():
    N = int(input())
    x = []
    y = []
    distance = 0
    for _ in range(N):
        z = input().split(" ")
        x.append(int(z[0]))
        y.append(int(z[1]))
    for i in range(N):
        for j in range(i + 1, N):
            distance = max(distance, (x[i] - x[j]) ** 2 + (y[i] - y[j]) ** 2)
    print(distance ** 0.5 / 2)


if __name__ == '__main__':
    main()