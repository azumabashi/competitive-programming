def main():
    N, K, M = map(int, input().split(" "))
    A = list(map(int, input().split(" ")))
    score = sum(A)
    last = N * M - score
    if last > K:
        print(-1)
    else:
        print(max(last, 0))


if __name__ == '__main__':
    main()