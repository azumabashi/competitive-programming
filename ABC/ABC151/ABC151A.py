from string import ascii_lowercase as lower


def main():
    C = input()
    for i in range(28):
        if C == lower[i]:
            print(lower[i+1])
            break


if __name__ == '__main__':
    main()