def main():
    N, M = map(int, input().split(" "))
    ac = 0
    wa = 0
    answer = ["C"] * N  # C: 適当な初期値
    for _ in range(M):
        ps = input().split(" ")
        p = int(ps[0]) - 1
        if not answer[p][-1] == "A":
            answer[p] = answer[p] + ps[1][0]
    if M > 0:
        for a in answer:
            if a[-1] == "A":
                ac += 1
                wa += (len(a) - 2)
            # いくらWAしたってACしない限りはWAに加算しない！
    print(ac, wa)


if __name__ == '__main__':
    main()