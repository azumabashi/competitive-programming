from math import ceil


def main():
    length, section_range = map(int, input().split(" "))
    numbers = list(map(int, input().split(" ")))
    print(ceil((length - 1) / (section_range - 1)))


if __name__ == '__main__':
    main()