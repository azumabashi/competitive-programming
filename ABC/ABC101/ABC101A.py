def main():
    symbols = input()
    answer = 0
    for symbol in symbols:
        if symbol == "+":
            answer += 1
        else:
            answer -= 1
    print(answer)


if __name__ == '__main__':
    main()