def get_digit_sum(integer):
    digit_sum = 0
    while integer > 0:
        digit_sum += integer % 10
        integer //= 10
    return digit_sum


def main():
    number = int(input())
    if not number % get_digit_sum(number):
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()