def main():
    N = int(input())
    V = list(map(int, input().split(" ")))
    C = list(map(int, input().split(" ")))
    L = []
    for v, c in zip(V, C):
        L.append(max(v - c, 0))
    print(sum(L))


if __name__ == '__main__':
    main()