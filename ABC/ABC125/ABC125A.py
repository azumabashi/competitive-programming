def main():
    A, B, T = map(int, input().split(" "))
    result = 0
    if T >= A:
        while T > A - 1:
            result += B
            T -= A
    print(result)


if __name__ == '__main__':
    main()