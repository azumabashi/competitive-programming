def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    negative = 0
    result = 0
    abs_minimum = 10**9
    for a in A:
        abs_minimum = min(abs_minimum, abs(a))
        if a < 0:
            negative += 1
    if negative % 2 == 0:
        for a in A:
            result += abs(a)
    else:
        for a in A:
            if abs(a) == abs_minimum:
                result += min(a, -a)
            else:
                result += abs(a)
    print(result)


if __name__ == '__main__':
    main()