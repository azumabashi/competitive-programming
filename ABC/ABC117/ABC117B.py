def main():
    N = int(input())
    L = list(map(int, input().split(" ")))
    L.sort()
    sum_except_max = sum(L[:-1])
    if sum_except_max > L[-1]:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()