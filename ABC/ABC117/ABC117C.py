def main():
    N, M = map(int, input().split(" "))
    X = list(map(int, input().split(" ")))
    X.sort()
    dist = []
    for i in range(M - 1):
        dist.append(X[i + 1] - X[i])
    dist.sort(reverse=True)
    print(X[-1] - X[0] - sum(dist[:N - 1]))


if __name__ == '__main__':
    main()