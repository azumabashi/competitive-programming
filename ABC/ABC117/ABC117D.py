def main():
    n, k = map(int, input().split())
    a = [int(t) for t in input().split()]
    dp = [[-1 for _ in range(2)] for _ in range(100)]
    dp[0][0] = 0
    for i in range(50):
        mask = 1 << (50 - i - 1)
        cnt_bit = len(list(filter(lambda x: x & mask, a)))
        cost_zero = mask * cnt_bit
        cost_one = mask * (n - cnt_bit)
        if dp[i][1] != -1:
            dp[i + 1][1] = max(dp[i + 1][1], dp[i][1] + max(cost_zero, cost_one))
        if dp[i][0] != -1 and k & mask:
            dp[i + 1][1] = max(dp[i + 1][1], dp[i][0] + cost_zero)
        if dp[i][0] != -1:
            if k & mask:
                dp[i + 1][0] = max(dp[i + 1][0], dp[i][0] + cost_one)
            else:
                dp[i + 1][0] = max(dp[i + 1][0], dp[i][0] + cost_zero)
    print(max(dp[50]))


if __name__ == '__main__':
    main()
