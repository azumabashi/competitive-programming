def main():
    H, N = map(int, input().split(" "))
    attack_sum = sum(list(map(int, input().split(" "))))
    if H > attack_sum:
        print("No")
    else:
        print("Yes")


if __name__ == '__main__':
    main()