def main():
    H = int(input())
    i = 2
    if H > 1:
        while True:
            if H <= 2 ** i - 1:
                break
            i += 1
        print(2 ** i - 1)
    else:
        print(1)


if __name__ == '__main__':
    main()