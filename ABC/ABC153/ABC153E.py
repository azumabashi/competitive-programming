def main():
    monster_health, spells = map(int, input().split())
    attack = []
    damage = []
    for _ in range(spells):
        a, b = map(int, input().split())
        attack.append(a)
        damage.append(b)
    dp = [[float("inf") for _ in range(monster_health + 1)] for _ in range(spells + 1)]
    dp[0][0] = 0
    for i in range(1, spells + 1):
        for j in range(monster_health + 1):
            ind = min(j + attack[i - 1], monster_health)
            dp[i][j] = min(dp[i][j], dp[i - 1][j])
            dp[i][ind] = min(dp[i][ind], dp[i][j] + damage[i - 1])
    print(dp[-1][-1])


if __name__ == '__main__':
    main()

