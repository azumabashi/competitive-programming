def main():
    N, M = map(int, input().split(" "))
    formed_year = []
    order = [0] * (N + 1)
    cities_order = [0] * (M + 1)
    for i in range(1, M + 1):
        p, y = map(int, input().split(" "))
        formed_year.append((i, p, y))
    formed_year_formated = sorted(formed_year, key=lambda x: x[2])
    print(formed_year_formated)  # できた年順 -> P_i内で何番目にできたか？
    for year in formed_year_formated:
        order[year[1]] += 1
        cities_order[year[0]] = order[year[1]]
    for i in range(M):
        print(str(formed_year[i][1]).zfill(6) + str(cities_order[formed_year[i][0]]).zfill(6))


if __name__ == '__main__':
    main()