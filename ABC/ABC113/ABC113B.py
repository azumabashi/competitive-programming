def main():
    N = int(input())
    T, A = map(int, input().split(" "))
    H = list(map(int, input().split(" ")))
    target = (T - A) / 0.006
    difference = 10 ** 6
    answer = 60
    for i in range(N):
        if abs(H[i] - target) < difference:
            difference = abs(H[i] - target)
            answer = i
    print(answer + 1)


if __name__ == '__main__':
    main()