def main():
    n, k = map(int, input().split())
    s = [0 for _ in range(n)]
    for _ in range(k):
        d = int(input())
        a = list(map(int, input().split()))
        for aa in a:
            s[aa - 1] += 1
    ans = 0
    for ss in s:
        if ss == 0:
            ans += 1
    print(ans)


if __name__ == '__main__':
    main()

