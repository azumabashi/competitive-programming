from collections import Counter


def main():
    n = int(input())
    a = list(map(int, input().split()))
    b = []
    c = []
    for i in range(n):
        b.append(a[i] + i + 1)
        c.append(i + 1 - a[i])
    b = list(Counter(b).items())
    c = Counter(c)
    answer = 0
    for i, n in b:
        if i in c:
            answer += n * c[i]
    print(answer)


if __name__ == '__main__':
    main()

