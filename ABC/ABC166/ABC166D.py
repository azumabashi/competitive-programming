"""
問題文に注意力系問題．問題文は
「与えられるXに対して、条件を満たす整数の組 (A,B) が存在することが保証されています」
であるから，「すべてのXが」問題で与えられたような形で表せるとは限らない．
だから，1e9を入れて表せなくても問題ない(テストケースにそのような値は含まれないため)．
適当な小さな値で全探索してそれを投げるだけで良い．
"""


def main():
    x = int(input())
    is_answered = False
    for a in range(-200, 201):
        if is_answered:
            break
        for b in range(-200, 201):
            if pow(a, 5) - pow(b, 5) == x:
                print(a, b)
                is_answered = True
                break


if __name__ == '__main__':
    main()

