#include<bits/stdc++.h>
using namespace std;

int main() {
    int N, K;
    cin >> N >> K;
    vector<int> A(N);
    int d;
    int x;
    for (int i = 0; i < K; i++) {
        cin >> d;
        for (int j = 0; j < d; j++) {
            cin >> x;
            x--;
            A[x]++;
        }
    }
    int ans = 0;
    for (int i = 0; i < N; i++) {
        if (A[i] == 0) ans++;
    }
    cout << ans << endl;
    return 0;
}