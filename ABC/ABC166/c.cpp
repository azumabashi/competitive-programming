#include<bits/stdc++.h>
using namespace std;

int main() {
    int N, M;
    cin >> N >> M;
    vector<int> H(N);
    for (int i = 0; i < N; i++) cin >> H[i];
    vector<vector<int>> next(N, vector<int>(0));
    for (int i = 0; i < M; i++) {
        int A, B;
        cin >> A >> B;
        A--;
        B--;
        next[A].push_back(B);
        next[B].push_back(A);
    }
    int ans = 0;
    for (int i = 0; i < N; i++) {
        int cnt = 0;
        for (auto j: next[i]) {
            if (H[j] < H[i]) cnt++;
        }
        if (cnt == next[i].size()) ans++;
    }
    cout << ans << endl;
    return 0;
}