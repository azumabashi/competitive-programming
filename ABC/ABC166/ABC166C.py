def main():
    n, m = map(int, input().split())
    h = list(map(int, input().split()))
    route = [[] for _ in range(n)]
    for _ in range(m):
        a, b = map(lambda x: int(x) - 1, input().split())
        route[a].append(b)
        route[b].append(a)
    answer = 0
    for i in range(n):
        if all(h[i] > h[j] for j in route[i]):
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

