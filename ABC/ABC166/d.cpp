#include<bits/stdc++.h>
using namespace std;

int main() {
    int X;
    cin >> X;
    bool is_answered = false;
    for (int i = -200; i < 201; i++) {
        if (is_answered) break;
        for (int j = -200; j < 201; j++) {
            if (pow(i, 5) - pow(j, 5) == X) {
                cout << i << " " << j << endl;
                is_answered = true;
                break;
            }
        }
    }
    return 0;
}