#include<bits/stdc++.h>
using namespace std;

int main() {
    int N;
    cin >> N;
    map<long long, long long> B, C;
    long long A;
    for (int i = 0; i < N; i++){
        cin >> A;
        B[A + i]++;
        C[i - A]++;
    }
    long long ans = 0;
    for (auto b: B) {
        int target = b.first;
        int pairs = b.second;
        ans += pairs * C[target];
    }
    cout << ans << endl;
    return 0;
}