import strutils, sequtils, math, algorithm, tables, sets, lists, intsets, critbits, future
var N = readLine(stdin).parseInt
var A = map(readLine(stdin).split(), parseInt)
var B, C = initTable[int, int]()
var answer: int = 0

for i in 0..<N:
    if A[i] + i in B:
        B[A[i] + i] += 1
    else:
        B[A[i] + i] = 1
    if i - A[i] in C:
        C[i - A[i]] += 1
    else:
        C[i - A[i]] = 1
for key, val in B:
    if key in C:
        answer += val * C[key]
echo answer