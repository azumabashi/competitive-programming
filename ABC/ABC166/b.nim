import strutils, sequtils
var N, K, D, answer: int
(N, K) = map(readLine(stdin).split(), parseInt)
var count = newSeq[int](N)
for i in 0..<K:
    D = readLine(stdin).parseInt
    var A = map(readLine(stdin).split(), parseInt)
    for j in 0..<D:
        count[A[j] - 1] += 1
for i in 0..<N:
    if count[i] == 0:
        answer += 1
echo answer