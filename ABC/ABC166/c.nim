import strutils, sequtils
var N, M, A, B: int
(N, M) = map(readLine(stdin).split(), parseInt)
var H = map(readLine(stdin).split(), parseInt)
var next = newSeq[newSeq[int](0)](N)
for i in 0..<M:
    (A, B) = map(readLine(stdin).split(), parseInt)
    next[A - 1].add(B - 1)
    next[B - 1].add(A - 1)
var answer, cnt: int
for i in 0..<N:
    cnt = 0
    for j in 0..<len(next[i]):
        if H[next[i][j]] < H[i]:
            cnt += 1
    if cnt == len(next[i]):
        answer += 1
echo answer