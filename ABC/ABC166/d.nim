import strutils, math

var X = readLine(stdin).parseInt
var f: bool = false
for a in -200..200:
    if f:
        break
    for b in -200..200:
        if a ^ 5 - b ^ 5 == X:
            echo a, " ", b
            f = true
            break