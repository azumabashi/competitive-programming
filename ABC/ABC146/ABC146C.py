from math import floor
# 1,1,11  1,1,102

def main():
    A, B, X = map(int, input().split(" "))
    digits = [19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    max = 10 ** 9
    N = 0

    for d in digits:
        N = floor((X - B * d) / A)
        print(N, d, len(str(N)), len(str(N)) == d)
        if len(str(N)) == d - 1 or len(str(N)) == d:
            print("Hi")
            break
        else:
            continue
    if N > max:
        N = max
    elif N < 0:
        N = 0

    print(N)


if __name__ == '__main__':
    main()
