from math import floor


def myFunc(A, B, X):
    digits = [19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    max = 10 ** 9
    N = 0

    for d in digits:
        N = floor((X - B * d) / A)
        if len(str(N)) == d:
            break
        else:
            continue
    if N > max:
        N = max
    elif N < 0:
        N = 0
    return N


def acceptedAns(a, b, x):
    ans = 0
    for d in range(1, 18):
        if b * d > x:
            continue
        lower = 10 ** (d - 1)
        upper = 10 ** d
        n = (x - (b * d)) // a
        if n < upper:
            ans = max(ans, n)
    ans = min(ans, 10 ** 9)
    return ans


def main():
    for aa in range(1, 10**9 + 1):
        for bb in range(1, 10 ** 9 + 1):
            for xx in range(1, 10 ** 18 + 1):
                my = myFunc(aa, bb, xx)
                ac = acceptedAns(aa, bb, xx)
                if my != ac:
                    print("Oops!: A=" + str(aa) + ", B=" + str(bb) + ", X=" + str(xx))
                    print("    myFunc=" + str(my) + "acceptedAns=" + str(ac))


if __name__ == '__main__':
    main()
    '''
    Oops!: A=1, B=1, X=11
    myFunc=10acceptedAns=9
Oops!: A=1, B=1, X=102
    myFunc=101acceptedAns=99
Oops!: A=1, B=1, X=1003
    myFunc=1002acceptedAns=999
Oops!: A=1, B=1, X=10004
    myFunc=10003acceptedAns=9999
Oops!: A=1, B=1, X=100005
    myFunc=100004acceptedAns=99999
Oops!: A=1, B=1, X=1000006
    myFunc=1000005acceptedAns=999999
    '''