def buy(A, B, N, X):
    return X >= (A * N + len(str(N)) * B)


def main():
    A, B, X = map(int, input().split(" "))
    result = 0
    if buy(A, B, 10 ** 9, X):
        print(1000000000)
    else:
        n = 10 ** 9 // 2
        lower = 0
        upper = 10 ** 9
        while upper - lower != 1:
            if not buy(A, B, n, X):
                upper = (lower + upper) // 2
                n = (lower + upper) // 2
                buy(A, B, n, X)
            else:
                lower = (lower + upper) // 2
                n = (lower + upper) // 2
                buy(A, B, n, X) 
        print(lower)



if __name__ == "__main__":
    main()
