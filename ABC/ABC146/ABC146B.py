from string import ascii_uppercase as alph


def main():
    N = int(input())
    S = input()
    result = ''
    alphabets = []
    for a in alph:
        alphabets.append(a)
    for s in S:
        result += alphabets[(alphabets.index(s) + N) % len(alphabets)]
    print(result)


if __name__ == '__main__':
    main()
