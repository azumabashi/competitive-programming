def main():
    string = list(input())
    print("yes" if len(string) == len(set(string)) else "no")


if __name__ == '__main__':
    main()

