def main():
    problem = int(input())
    score = sorted([int(input()) for _ in range(problem)])
    all_score = sum(score)
    if not all_score % 10:
        missed_score = all_score
        for s in score:
            if s % 10:
                missed_score = s
                break
        print(all_score - missed_score)
    else:
        print(all_score)


if __name__ == '__main__':
    main()

