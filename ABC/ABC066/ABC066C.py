"""
    素直に書いた　間に合わなかった
    O(N^2)なので制約からして厳しい
"""


def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    answer = []
    for num in numbers:
        answer.append(num)
        answer = list(reversed(answer))
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

