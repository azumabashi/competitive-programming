def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    answer = [0] * length
    if length % 2:
        answer[:length // 2 + 1] = numbers[::-2]
        answer[length // 2 + 1:] = numbers[1::2]
    else:
        answer[:length // 2] = numbers[::-2]
        answer[length // 2:] = numbers[::2]
    print(" ".join(list(map(str, answer))))


if __name__ == '__main__':
    main()

