def main():
    string = input()[:-1]
    while True:
        if len(string) % 2 == 0 and string[:len(string) // 2] == string[len(string) // 2:]:
            print(len(string))
            break
        else:
            string = string[:-1]


if __name__ == '__main__':
    main()

