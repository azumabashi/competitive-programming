from math import ceil, sqrt


def main():
    target = int(input())
    print(ceil((-1 + sqrt(1 + 8 * target)) / 2))


if __name__ == '__main__':
    main()

