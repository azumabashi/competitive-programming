from bisect import bisect_left


def main():
    N, L = map(int, input().split(" "))
    taste = []
    for i in range(N):
        taste.append(L + i)
    taste.sort()
    zero = bisect_left(taste, 0)
    if zero == 0:
        eat = taste[0]
    elif zero == N:
        eat = taste[-1]
    elif abs(taste[zero]) >= abs(taste[zero - 1]):
        eat = taste[zero - 1]
    else:
        eat = taste[zero]
    print(sum(taste) - eat)


if __name__ == '__main__':
    main()