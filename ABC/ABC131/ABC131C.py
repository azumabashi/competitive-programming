from math import gcd


def count(X, C, D, l):
    c = X // C
    d = X // D
    cd = X // l
    return c + d - cd


def main():
    A, B, C, D = map(int, input().split(" "))
    lcm = C * D // gcd(C, D)
    BorC = B - A + 1 - count(B, C, D, lcm) + count(A - 1, C, D, lcm)
    print(BorC)


if __name__ == '__main__':
    main()