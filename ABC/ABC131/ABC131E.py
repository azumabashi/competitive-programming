from collections import deque


def solve(n, k):
    g = [deque([j for j in range(i + 1, n)]) for i in range(n)]
    i = 0
    cnt = 0
    nk = k
    while True:
        if nk <= 0 or cnt == n:
            break
        if len(g[i]) > 1:
            g[i].popleft()
            nk -= 1
            cnt = 0
        else:
            cnt += 1
        i += 1
        i %= n
    res = []
    for i in range(n):
        for d in g[i]:
            res.append([i + 1, d + 1])
    dist = [[1 << 20 for _ in range(n)] for _ in range(n)]
    for i in range(n):
        dist[i][i] = 0
    for u, v in res:
        dist[u - 1][v - 1] = 1
        dist[v - 1][u - 1] = 1
    for kk in range(n):
        for i in range(n):
            for j in range(n):
                dist[i][j] = min(dist[i][kk] + dist[kk][j], dist[i][j])
    check = sum([1 if dist[i][j] == 2 else 0 for i in range(n) for j in range(i + 1, n)])
    return check == k, res


def main():
    n, k = map(int, input().split())
    chk, res = solve(n, k)
    if chk:
        print(len(res))
        for r in res:
            print(*r)
    else:
        print(-1)


if __name__ == '__main__':
    main()
