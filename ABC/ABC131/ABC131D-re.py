def main():
    N = int(input())
    flag = True
    tascs = []
    for _ in range(N):
        a, b = map(int, input().split(" "))
        tmp = [a, b]
        tascs.append(tuple(tmp))
    tascs.sort(key=lambda x: x[1])
    passed_time = 0
    for tasc in tascs:
        passed_time += tasc[0]
        if passed_time > tasc[1]:
            flag = False
            break
    if flag:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()