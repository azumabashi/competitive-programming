from math import ceil


def main():
    cake = int(input())
    children = int(input())
    print(ceil(cake / children) * children - cake)


if __name__ == '__main__':
    main()

