def main():
    n = int(input())
    length = 1000002
    count = [0 for _ in range(length)]
    for _ in range(n):
        a, b = map(int, input().split())
        count[a] += 1
        count[b + 1] -= 1
    for i in range(1, length):
        count[i] += count[i - 1]
    answer = 0
    for i in range(length):
        if answer < count[i]:
            answer = count[i]
    print(answer)


if __name__ == '__main__':
    main()

