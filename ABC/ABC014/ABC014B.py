def main():
    goods, subset = map(int, input().split())
    price = list(map(int, input().split()))
    subset = "".join(reversed(list(bin(subset)[2:].zfill(goods))))
    answer = 0
    for i in range(goods):
        if subset[i] == "1":
            answer += price[i]
    print(answer)


if __name__ == '__main__':
    main()

