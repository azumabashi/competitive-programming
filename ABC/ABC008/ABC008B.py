def main():
    vote_num = int(input())
    count = {}
    for _ in range(vote_num):
        vote = input()
        if vote in count:
            count[vote] += 1
        else:
            count[vote] = 1
    count = sorted(count.items(), key=lambda x: x[1])
    print(count[-1][0])


if __name__ == '__main__':
    main()

