def main():
    min_num, max_num = map(int, input().split())
    print(max_num - min_num + 1)


if __name__ == '__main__':
    main()

