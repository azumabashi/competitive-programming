def main():
    A, B, K = map(int, input().split(" "))
    common_divisor = [1]
    for i in range(2, min(A, B) + 1):
        if A % i == 0 and B % i == 0:
            common_divisor.append(i)
    print(common_divisor[-K])


if __name__ == '__main__':
    main()