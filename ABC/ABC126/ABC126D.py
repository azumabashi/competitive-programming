from collections import deque


def main():
    vertices = int(input())
    tree = [[] for _ in range(vertices)]
    for _ in range(vertices - 1):
        u, v, length = map(int, input().split())
        u -= 1
        v -= 1
        tree[u].append([v, length])
        tree[v].append([u, length])
    root_index = 0
    max_deg = 0
    for i in range(vertices):
        if max_deg < len(tree[i]):
            root_index = i
            max_deg = len(tree[i])
    queue = deque([[root_index, 0]])
    is_checked = [False for _ in range(vertices)]
    is_checked[root_index] = True
    answer = ["0" for _ in range(vertices)]
    while queue:
        now_v, dist_from_root = queue.pop()
        for next_v, length in tree[now_v]:
            if not is_checked[next_v]:
                is_checked[next_v] = True
                if (dist_from_root + length) % 2:
                    answer[next_v] = "1"
                queue.append([next_v, dist_from_root + length])
    print("\n".join(answer))


if __name__ == '__main__':
    main()

