def main():
    N, K = map(int, input().split(" "))
    result = 0
    for i in range(1, N + 1):
        point = i
        face = 0
        while point < K:
            point *= 2
            face += 1
        result += 0.5 ** face / N
    print(result)


if __name__ == '__main__':
    main()