def main():
    N, K = map(int, input().split(" "))
    S = input()
    if S[K-1] == 'A':
        result = S[:max(K - 1, 0)] + 'a' + S[K:]
    elif S[K-1] == 'B':
        result = S[:max(K - 1, 0)] + 'b' + S[K:]
    else:
        result = S[:max(K - 1, 0)] + 'c' + S[K:]
    print(result)


if __name__ == '__main__':
    main()