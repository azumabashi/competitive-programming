def main():
    S = input()
    if 0 < int(S[:2]) <= 12 and 0 < int(S[2:]) <= 12:
        return "AMBIGUOUS"
    elif (int(S[:2]) > 12 or int(S[:2]) == 0) and 12 >= int(S[2:]) > 0:
        return "YYMM"
    elif 0 < int(S[:2]) <= 12 and (12 < int(S[2:]) or int(S[2:]) == 0):
        return "MMYY"
    else:
        return "NA"


if __name__ == '__main__':
    print(main())
