def ans(a):
    m = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    if a[:2] in m and a[2:] in m:
      return "AMBIGUOUS"
    elif a[:2] not in m and a[2:] in m:
      return "YYMM"
    elif a[:2] in m and a[2:] not in m:
      return "MMYY"
    else:
      return "NA"

def my(S):
    if 0 < int(S[:2]) <= 12 and 0 < int(S[2:]) <= 12:
        return "AMBIGUOUS"
    elif (int(S[:2]) > 12 or int(S[:2]) == 0) and 12 >= int(S[2:]) > 0:
        return "YYMM"
    elif 0 < int(S[:2]) <= 12 and (12 < int(S[2:]) or int(S[2:]) == 0):
        return "MMYY"
    else:
        return "NA"


print("START.")
for i in range(10):
    for j in range(10):
        for k in range(10):
            for l in range(10):
                string = str(i) + str(j) + str(k) + str(l)
                b = ans(string)
                c = my(string)
                if b != c:
                    print(string + ", " + b + ", but got [" + c + "].")

print("END")
