def main():
    N, M, C = map(int, input().split(" "))
    B = list(map(int, input().split(" ")))
    result = 0
    for _ in range(N):
        A = list(map(int, input().split(" ")))
        tmp = C
        for i in range(M):
            tmp += A[i] * B[i]
        if tmp > 0:
            result += 1
    print(result)


if __name__ == '__main__':
    main()