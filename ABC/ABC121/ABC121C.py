def main():
    N, M = map(int, input().split(" "))
    prices = []
    for i in range(N):
        tmp = list(map(int, input().split(" ")))
        prices.append(tuple(tmp))
    prices.sort(key=lambda x: x[0])
    cost = 0
    for price in prices:
        if M < 0:
            break
        cost += price[0] * min(M, price[1])
        M = max(0, M - price[1])
    print(cost)


if __name__ == '__main__':
    main()