def main():
    N = int(input())
    i = 2
    ans = N + 1
    while i ** 2 <= N:
        if N % i == 0:
            ans = min(i + N / i, ans)
        i += 1
    print(int(ans - 2))


if __name__ == '__main__':
    main()