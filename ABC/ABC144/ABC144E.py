def solve():
    n, k = map(int, input().split())
    a = [int(x) for x in input().split()]
    f = [int(x) for x in input().split()]
    if sum(a) <= k:
        return 0
    a.sort(reverse=True)
    f.sort()
    ng = 0
    ok = 1 << 63
    while abs(ok - ng) > 1:
        mid = (ok + ng) // 2
        cnt = 0
        for i in range(n):
            cnt += max(0, a[i] - mid // f[i])
        if cnt <= k:
            ok = mid
        else:
            ng = mid
    return ok


def main():
    print(solve())


if __name__ == '__main__':
    main()
