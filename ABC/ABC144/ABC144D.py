from math import atan as arctan
from math import degrees


def main():
    a, b, x = map(int, input().split(" "))
    bottom_area = x / a
    if bottom_area >= a * b / 2:
        print(abs(degrees(arctan((2 * x / (a ** 3)) - (2 * b / a)))))
    else:
        print(90 - degrees(arctan(2 * x / a / (b ** 2))))


if __name__ == '__main__':
    main()