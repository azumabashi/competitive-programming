def main():
    a, b, c, d = map(int, input().split())
    answer = "Yes"
    while True:
        if a <= 0:
            answer = "No"
            break
        c -= b
        if c <= 0:
            answer = "Yes"
            break
        a -= d
    print(answer)


if __name__ == '__main__':
    main()

