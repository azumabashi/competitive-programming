#include<bits/stdc++.h>
using namespace std;

int main(){
    int N;
    map<string, int> count;
    cin >> N;
    string S;
    for (int i = 0; i < N; i++) {
        cin >> S;
        count[S]++;
    }
    cout << count.size() << endl;
    return 0;
}