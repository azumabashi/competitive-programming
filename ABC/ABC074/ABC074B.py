def main():
    ball_num = int(input())
    type_b_initial_y = int(input())
    x_coodinates = list(map(int, input().split()))
    answer = 0
    for i in range(ball_num):
        answer += min(x_coodinates[i] * 2, abs(x_coodinates[i] - type_b_initial_y) * 2)
    print(answer)


if __name__ == '__main__':
    main()

