def main():
    length = len(input())
    print("x" * length)


if __name__ == '__main__':
    main()