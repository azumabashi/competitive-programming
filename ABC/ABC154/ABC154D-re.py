def main():
    # 差分だけ微調整する解法
    N, K = map(int, input().split(" "))
    p = list(map(int, input().split(" ")))
    answer = sum(p[:K])
    tmp = sum(p[:K])
    for i in range(N - K):
        tmp = tmp + p[i+K] - p[i]
        answer = max(answer, tmp)
    print((answer + K) / 2)


if __name__ == '__main__':
    main()