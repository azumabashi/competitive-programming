def main():
    strings = input().split(" ")
    A, B = map(int, input().split(" "))
    U = input()
    if U == strings[0]:
        A -= 1
    else:
        B -= 1
    print(A, B)


if __name__ == '__main__':
    main()