def main():
    n = input()
    non_zero = int(input())
    length = len(n)
    dp = [[[0, 0] for _ in range(non_zero + 1)] for _ in range(length + 1)]
    # dp[i][j][k] :=左からi桁目まで見て，0でない桁がjあって，
    # 「そこまででN以下が確定」ならk = 1, 「そこまでがNと一致」ならk = 0
    # (iの小さいうちにN以下ならkは1，iが大きくなってからN以下ならkは0)
    dp[0][0][0] = 1
    for i in range(length):
        for j in range(non_zero + 1):  # j = 0, 1, ..., non_zero のnon_zero + 1通り
            for k in range(2):
                now_digit = int(n[i])  # i桁目に注目する
                for next_d in range(10):  # 変更後の値を全部試す
                    new_i = i + 1  # i桁目までの情報から，i+1桁目の情報がほしいので
                    new_j = j
                    new_k = k  # 以上2つは以下で更新する
                    if next_d > 0:
                        new_j += 1
                        # next_dが0なら，そこの桁で0を1つ消費するので，new_jは1増やす
                        # そうでないなら，まだ0はnew_j回使う必要がある
                    if new_j > non_zero:
                        continue
                        # 0の利用回数が上限オーバーなら最早どうしようもないので，これ以上考えない
                        # non_zero未満でも，後にnon_zeroに到達するかもしれないので，とりあえず置いておく
                    if k == 0:
                        # 左からi-1桁目までがNと一致しているときを考える
                        if next_d > now_digit:
                            continue
                            # 上の桁がNと一致しているのに，大きい数で更新することはできないので，continue
                            # ex. N=31415で314xyと来ているのにx=9は無理
                        elif next_d < now_digit:
                            new_k = 1
                            # 桁が小さくなっていれば，i桁目までですでにNより小さくなることは確定なので，
                            # 更新後のインデックスは1としてよい
                            # ex. N=31415で314xyと来ているとき，x=0ならその時点でN > 3140yだからnew_k = 1としてよい
                    # k = 1のとき，既にi-1桁目までで未満であることが保証されているので，
                    # 遷移後のnew_kもやはり1のまま．
                    # ex. N=31415で214xyと来ているとき，xyの組に関係なく未満であることが保証される

                    # 値の更新
                    dp[new_i][new_j][new_k] += dp[i][j][k]

    # 1の位でN未満であると確定する必要はない(途中のどこかでN未満になるように遷移すればよい)ので，
    # dp[length][non_zero][*]の総和をとればよい
    print(sum(dp[length][non_zero]))


if __name__ == '__main__':
    main()

