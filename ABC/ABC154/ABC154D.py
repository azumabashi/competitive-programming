def main():
    N, K = map(int, input().split(" "))
    p = list(map(int, input().split(" ")))
    exceptation_sum = [0] * N
    answer = 0
    for i in range(N):
        if not i == 0:
            exceptation_sum[i] = (p[i] + 1) / 2 + exceptation_sum[i - 1]
        else:
            exceptation_sum[i] = (p[i] + 1) / 2
    for i in range(N - K + 1):
        answer = max(answer, exceptation_sum[i + K - 1] - exceptation_sum[i - 1])
    print(answer)


if __name__ == '__main__':
    main()