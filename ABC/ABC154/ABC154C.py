def main():
    N = int(input())
    A = list(map(int, input().split(" ")))
    A.sort()
    answer = "YES"
    for i in range(N - 1):
        if A[i] == A[i + 1]:
            answer = "NO"
            break
    print(answer)


if __name__ == '__main__':
    main()