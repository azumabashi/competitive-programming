def get_operator(n):
    if n == -1:
        return "-"
    return "+"


def main():
    ticket_num = input()
    numbers = [int(ticket_num[i]) for i in range(len(ticket_num))]
    operator = [1, -1]
    flag = True
    for op1 in operator:
        for op2 in operator:
            for op3 in operator:
                if numbers[0] + op1 * numbers[1] + op2 * numbers[2] + op3 * numbers[3] == 7 and flag:
                    print(ticket_num[0] + get_operator(op1) + ticket_num[1] + get_operator(op2) + ticket_num[2] +
                          get_operator(op3) + ticket_num[3] + "=7")
                    flag = False


if __name__ == '__main__':
    main()