def main():
    NUMBER = int(input())
    lucas_number = [2, 1]
    while len(lucas_number) < NUMBER + 1:
        lucas_number.append(lucas_number[-1] + lucas_number[-2])
    print(lucas_number[-1])


if __name__ == '__main__':
    main()