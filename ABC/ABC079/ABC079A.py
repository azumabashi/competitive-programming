def main():
    NUMBER = input()
    answer = False
    if NUMBER[:3] == NUMBER[0] * 3 or NUMBER[1:] == NUMBER[1] * 3:
        answer = True
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()