def main():
    height, width = map(int, input().split())
    change_cost = [list(map(int, input().split())) for _ in range(10)]
    grid = [list(map(int, input().split())) for _ in range(height)]
    for k in range(10):
        for i in range(10):
            for j in range(10):
                change_cost[i][j] = min(change_cost[i][j], change_cost[i][k] + change_cost[k][j])
    answer = 0
    for g in grid:
        for n in g:
            if abs(n) != 1:
                answer += change_cost[n][1]
    print(answer)


if __name__ == '__main__':
    main()

