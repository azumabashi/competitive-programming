from copy import deepcopy
"""
    「正方形を含む」　これを忘れている！！
"""


def main():
    sticks = int(input())
    stick_length = list(map(int, input().split()))
    length_count = {}
    for length in stick_length:
        if length in length_count:
            length_count[length] += 1
        else:
            length_count[length] = 1
    for length, count in deepcopy(length_count).items():
        if count < 2:
            del length_count[length]
    answer = 0
    if len(length_count) >= 2:
        length_count = sorted(length_count.items(), key=lambda x: x[0])
        answer = length_count[-1][0] * length_count[-2][0]
    print(answer)


if __name__ == '__main__':
    main()

