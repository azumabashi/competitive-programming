from string import ascii_lowercase


def main():
    string = input()
    count = {}
    for lcase in ascii_lowercase:
        count[lcase] = 0
    for s in string:
        count[s] += 1
    count = sorted(count.items(), key=lambda x: x[0])
    for i, count in enumerate(count):
        if count[1] == 0:
            print(ascii_lowercase[i])
            break
    else:
        print("None")


if __name__ == '__main__':
    main()

