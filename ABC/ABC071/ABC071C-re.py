def main():
    sticks = int(input())
    stick_length = list(map(int, input().split()))
    length_count = {}
    for length in stick_length:
        if length in length_count:
            length_count[length] += 1
        else:
            length_count[length] = 1
    length_count = sorted(length_count.items(), key=lambda x: x[0], reverse=True)
    answer = 0
    for stick_info in length_count:
        if stick_info[1] < 2:
            continue
        elif stick_info[1] >= 4:
            if answer == 0:
                answer = stick_info[0] ** 2
                break
            else:
                answer *= stick_info[0]
                break
        else:
            if answer > 0:
                answer *= stick_info[0]
                break
            else:
                answer = stick_info[0]
    print(answer)


if __name__ == '__main__':
    main()

