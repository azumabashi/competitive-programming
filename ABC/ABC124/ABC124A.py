def main():
    A, B = map(int, input().split(" "))
    L = []
    L.append(A)
    L.append(B)
    L.append(A - 1)
    L.append(B - 1)
    L.sort(reverse=True)
    print(L[0] + L[1])


if __name__ == '__main__':
    main()