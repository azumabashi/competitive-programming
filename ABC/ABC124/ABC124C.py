def main():
    s = input()
    N = len(s)
    S = []
    for j in range(N):
        S.append(s[j])
    result = 0
    for i in range(N - 1):
        if S[i] == S[i + 1]:
            result += 1
            if S[i + 1] == "0":
                S[i + 1] = "1"
            else:
                S[i + 1] = "0"
    print(result)


if __name__ == '__main__':
    main()