def main():
    n, k = map(int, input().split())
    remainder = n % k
    print(min(remainder, k - remainder))


if __name__ == '__main__':
    main()

