import strutils, sequtils

var N, K: int
(N, K) = map(readLine(stdin).split(" "), parseInt)
var reminder: int = N mod K
echo min(reminder, K - reminder)