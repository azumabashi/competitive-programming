from collections import deque


def main():
    k = int(input())
    queue = deque([i for i in range(1, 10)])
    pop_time = 0
    now = 1
    while pop_time < k:
        now = queue.popleft()
        if now % 10 != 0:
            queue.append(10 * now + now % 10 - 1)
        queue.append(10 * now + now % 10)
        if now % 10 != 9:
            queue.append(10 * now + now % 10 + 1)
        pop_time += 1
    print(now)


if __name__ == '__main__':
    main()

