def main():
    n, m = map(int, input().split())
    a = list(map(int, input().split()))
    a.sort(reverse=True)
    all_a = sum(a)
    count = 0
    for i in range(n):
        if all_a / (4 * m) <= a[i]:
            count += 1
        else:
            break
    print("Yes" if m <= count else "No")


if __name__ == '__main__':
    main()

