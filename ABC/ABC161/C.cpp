#include<bits/stdc++.h>
using namespace std;

int main() {
    long long N, K;
    cin >> N >> K;
    long long remainder = N % K;
    cout << min(remainder, K - remainder) << endl;
    return 0;
}