def main():
    n, m, x = map(int, input().split())
    c = []
    a = []
    for _ in range(n):
        ac = list(map(int, input().split()))
        c.append(ac[0])
        a.append(ac[1:])
    answer = float("inf")
    for i in range(1, pow(2, n)):
        now = 0
        all_c = [0 for _ in range(m)]
        for j in range(n):
            if (i >> j) & 1:
                for k in range(m):
                    all_c[k] += a[j][k]
                now += c[j]
        if all(all_c[j] >= x for j in range(m)):
            answer = min(answer, now)
    print(answer if answer < float("inf") else -1)


if __name__ == '__main__':
    main()

