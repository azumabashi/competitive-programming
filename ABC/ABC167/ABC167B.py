def main():
    a, b, c, k = map(int, input().split())
    answer = min(a, k)
    k -= min(a, k)
    if k > 0:
        k -= min(b, k)
    if k > 0:
        answer -= min(k, c)
    print(answer)


if __name__ == '__main__':
    main()

