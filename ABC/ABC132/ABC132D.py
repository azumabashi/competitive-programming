def comb(n, r, mod):
    denominator = 1
    numerator = 1
    for i in range(1, r + 1):
        denominator *= i
        denominator %= mod
    for i in range(n - r + 1, n + 1):
        numerator *= i
        numerator %= mod
    return (numerator * pow(denominator, mod - 2, mod)) % mod


def main():
    all_balls, blue_balls = map(int, input().split())
    mod = 10 ** 9 + 7
    for i in range(1, blue_balls + 1):
        print((comb(all_balls - blue_balls + 1, i, mod) * comb(blue_balls - 1, i - 1, mod)) % mod)


if __name__ == '__main__':
    main()

