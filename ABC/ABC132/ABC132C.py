def main():
    N = int(input())
    d = list(map(int, input().split(" ")))
    d.sort()
    central = N // 2
    print(d[central] - d[central - 1])


if __name__ == '__main__':
    main()