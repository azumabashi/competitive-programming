def main():
    donut_num, material = map(int, input().split(" "))
    min_material = float("inf")
    sum_material = 0
    answer = 0
    for _ in range(donut_num):
        mass = int(input())
        min_material = min(min_material, mass)
        sum_material += mass
    material -= sum_material
    answer += donut_num
    answer += material // min_material
    print(answer)


if __name__ == '__main__':
    main()