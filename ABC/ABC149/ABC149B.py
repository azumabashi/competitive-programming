def main():
    numbers = input().split(" ")
    takahashi = int(numbers[0])
    aoki = int(numbers[1])
    repeat = int(numbers[2])
    if repeat == 0:
        pass
    elif takahashi >= repeat and takahashi > 0:
        takahashi -= repeat
    elif aoki == 0:
        takahashi -= min(takahashi, repeat)
    elif takahashi == 0 and aoki >= repeat:
        aoki -= repeat
    elif takahashi + aoki < repeat:
        aoki = 0
        takahashi = 0
    else:
        aoki += takahashi
        aoki -= repeat
        takahashi = 0

    print(str(takahashi) + " " + str(aoki))


if __name__ == '__main__':
    main()
