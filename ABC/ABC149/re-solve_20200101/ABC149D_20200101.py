def main():
    N, K = map(int, input().split(" "))
    R, S, P = map(int, input().split(" "))
    strings = input()
    T = []
    for s in strings:
        T.append(s)
    score = {'r': P, 's': R, 'p': S}
    total = 0

    for i in range(K):
        total += score[T[i]]

    for j in range(K, N):
        if T[j] != T[j - K]:
            total += score[T[j]]
        else:
            T[j] = ' '
            continue

    print(total)


if __name__ == '__main__':
    main()
