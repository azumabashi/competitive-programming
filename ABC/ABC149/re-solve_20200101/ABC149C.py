def main():
    X = int(input())
    i = 2
    while i ** 2 <= X:
        if X % i == 0:
            X += 1
            i = 2
        else:
            i += 1
    print(X)


if __name__ == '__main__':
    main()
