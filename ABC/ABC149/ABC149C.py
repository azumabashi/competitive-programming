from math import floor


def prime(LowerLimit, isPrime):
    print(isPrime,LowerLimit)
    max = floor(LowerLimit ** 0.5) + 3
    if LowerLimit > 2:
        while not isPrime:
            for i in range(2, max):
                print(i, max, LowerLimit)
                if LowerLimit % i == 0:
                    LowerLimit += 1
                    prime(LowerLimit, isPrime)
                elif i == max - 1:
                    isPrime = True
                    break
    return LowerLimit


def main():
    LowerLimit = int(input())
    print(prime(LowerLimit, False))


if __name__ == '__main__':
    main()
