def solve(a, sgn):
    n = len(a)
    a_sum = 0
    res = 0
    for i in range(n):
        sgn *= -1
        a_sum += a[i]
        if (sgn == 1 and a_sum <= 0) or (sgn == -1 and a_sum >= 0):
            res += abs(a_sum) + 1
            a_sum = sgn
    return res


def main():
    n = int(input())
    a = list(map(int, input().split()))
    print(min(solve(a, 1), solve(a, -1)))


if __name__ == '__main__':
    main()
