def main():
    strings = input().split()
    answer = ""
    for s in strings:
        answer += s[0].upper()
    print(answer)


if __name__ == '__main__':
    main()

