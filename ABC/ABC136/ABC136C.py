# なぜかWA

def main():
    N = int(input())
    H = list(map(int, input().split(" ")))
    flag = True
    for i in range(1, N):
        if H[i] - H[i - 1] < 0:
            H[i - 1] -= 1
    for j in range(N - 1):
        if H[j + 1] - H[j] < 0:
            flag = False
            break
    if flag:
        print("Yes")
    else:
        print("No")


if __name__ == '__main__':
    main()