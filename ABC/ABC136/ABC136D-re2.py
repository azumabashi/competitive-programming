def main():
    # wakaran
    S = input()
    answer = [0] * len(S)
    directions = S.split("LR")
    last_index = 0
    for i in range(len(directions)):
        if i == 0:
            direction = directions[i] + "L"
        elif i == len(directions) - 1:
            direction = "R" + directions[i]
        else:
            direction = "R" + directions[i] + "L"
        for j in range(len(direction)):
            if direction[j] == "L":
                answer[last_index + j - (j - 1) % 2] = len(direction) // 2
                answer[last_index + j - j % 2] = len(direction) - answer[last_index + j - (j - 1) % 2]
                break
        last_index += len(direction)
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()