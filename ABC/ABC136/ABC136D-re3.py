def main():
    string = "_" + input()
    last_index = 0
    gather_index = 0
    answer = [0] * (len(string) - 1)
    for i in range(1, len(string)):
        if i < len(string) - 1:
            if string[i] == "R" and string[i + 1] == "L":
                gather_index = i
            elif string[i] == "L" and string[i + 1] == "R":
                answer[gather_index] = (gather_index - last_index) // 2 + (i - gather_index + 1) // 2
                answer[gather_index - 1] = (i - last_index) - answer[gather_index]
                last_index = i
        else:
            answer[gather_index] = (gather_index - last_index) // 2 + (i - gather_index + 1) // 2
            answer[gather_index - 1] = (len(string) - last_index - 1) - answer[gather_index]
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

