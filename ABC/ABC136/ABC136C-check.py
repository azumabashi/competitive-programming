def me(N, H):
    flag = True
    for i in range(1, N):
        if H[i] - H[i - 1] < 0:
            H[i - 1] -= 1
        print(H)
    for j in range(N - 1):
        if H[j + 1] - H[j] < 0:
            flag = False
            break
    if flag:
        return "Yes"
    else:
        return "No"

def ans(H):
    pre = None
    for h in H:
        if pre is None:
            pre = h
        elif h <= pre:
            pre = h
        elif pre + 1 == h:
            continue
        else:
            # pre + 1 < h
            return "No"

    return "Yes"

def nans(h_list):
    pre_j = h_list[0]
    f = 0
    for j in h_list[1:]:
        if pre_j > j:
            f = 1
            break
        else:
            if pre_j < j:
                pre_j = j - 1
     
    if f == 1:
        return 'No'
    else:
        return 'Yes'

def main():
    N = 5
    pattern = 0
    non = 0
    print("START")
    for a in range(5, N + 1):
        for b in range(4, N + 1):
            for c in range(5, N + 1):
                for d in range(5, N + 1):
                    for e in range(4, N + 1):
                        pattern += 1
                        h = str(a) + " " + str(b) + " " + str(c) + " "+ str(d) + " " + str(e)
                        H = list(map(int, h.split(" ")))
                        m = me(N, H)
                        s = nans(H)
                        if not m == s:
                            print(H, m, s)
                            # print(H)
                            non += 1
    print("END")
    print(str(pattern) + " patterns, incollect:" + str(non))


if __name__ == '__main__':
    print(me(5, [5, 4, 5, 5, 4]))
    print(ans([5, 4, 5, 5, 4])) # ←嘘解法？
    print(nans([5, 4, 5, 5, 4]))
    # main()
