def main():
    S = input()
    N = len(S)
    S = S.split("LR")
    result = [0] * N
    number = 0
    R_index = 0
    for s in S:
        for i in range(len(s)):
            if s[i] == "R":
                R_index = i + number
                break
        for j in range(len(s)):
            if (j - R_index) % 2 == 0:
                result[R_index] += 1
            else:
                result[R_index + 1] += 1
        number += len(s)

    print(" ".join(map(str, result)))


if __name__ == '__main__':
    main()
