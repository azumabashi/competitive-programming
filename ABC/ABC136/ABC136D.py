from copy import deepcopy
# wakaran


def main():
    S = input()
    move = []
    person = [1] * len(S)
    new = [0] * len(S)
    for j in range(2):
        for i in range(len(S)):
            if str == 'R':
                new[i + 1] += person[i]
            else:
                new[i - 1] += person[i]
        move.append("".join(str(new)))
        person = deepcopy(new)
        new = []
    while move[0] != move[-1] or move[-2] != move[-1]:
        for i in range(len(S)):
            if str == 'R':
                new[i + 1] += person[i]
            else:
                new[i - 1] += person[i]
        move.append("".join(str(new)))
        person = deepcopy(new)
        new = []
    if move[0] == move[-1]:
        N = len(move)
        print(move[pow(10, 100, N)])
    elif move[-2] == move[-1]:
        print(move[-1])


if __name__ == '__main__':
    main()