def main():
    W, H, x, y = map(int, input().split(" "))
    S = W * H / 2
    ans = 0
    if 2 * x == W:
        ans += 1
    if 2 * y == H:
        ans += 1
    if x * H == y * W:
        ans += 1
    if ans > 1:
        ans = 1
    else:
        ans = 0
    print(str(S) + " " + str(ans))


if __name__ == '__main__':
    main()