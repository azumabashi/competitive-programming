def main():
    N, X = map(int, input().split(" "))
    L = list(map(int, input().split(" ")))
    d = 0
    result = 1
    for i in range(0, N):
        d += L[i]
        if d <= X:
            result += 1
        else:
            break
    print(result)


if __name__ == '__main__':
    main()