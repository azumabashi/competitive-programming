from bisect import bisect_left


def main():
    length, lower_limit = map(int, input().split())
    numbers = list(map(int, input().split()))
    cumulative_sum = [numbers[0]] * (length + 1)
    for i in range(2, length + 1):
        cumulative_sum[i] = numbers[i - 1] + cumulative_sum[i - 1]
    cumulative_sum[0] = 0
    answer = 0
    for i in range(length):
        bin_search = bisect_left(cumulative_sum, cumulative_sum[i] + lower_limit)
        answer += max(length - bin_search + 1, 0)
    print(answer)


if __name__ == '__main__':
    main()

