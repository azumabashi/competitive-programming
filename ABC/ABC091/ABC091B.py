# rand_4 -> WA


def get_info(diff):
    length = int(input())
    strings = {}
    for _ in range(length):
        tmp = input()
        if tmp in strings:
            strings[tmp] += diff
        else:
            strings[tmp] = diff
    return length, strings


def main():
    blue_card, blue_card_str = get_info(1)
    red_card, red_card_str = get_info(-1)
    answer = 0
    available_keys = (blue_card_str.keys() & red_card_str.keys()) | blue_card_str.keys()
    for key in available_keys:
        if key in red_card_str:
            diff = blue_card_str[key] + red_card_str[key]
        else:
            diff = blue_card_str[key]
        answer = max(answer, diff)
    print(answer)


if __name__ == '__main__':
    main()