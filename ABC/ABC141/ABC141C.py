def main():
    N, K, Q = map(int, input().split(" "))
    points = [K - Q] * N
    for i in range(Q):
        points[int(input()) - 1] += 1
    for j in points:
        if j > 0:
            print("Yes")
        else:
            print("No")


if __name__ == '__main__':
    main()