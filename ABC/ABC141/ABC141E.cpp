#include<bits/stdc++.h>
using namespace std;

int main() {
  string S;
  int N;
  cin >> N >> S;
  int ans = 0;
  for (int i = 0; i < N; i++){
    for (int j = N - 1; i < j; j--) {
      int max_len = min(j - i, N - j);
      if (max_len <= ans) continue;
      int res = 0;
      for (int k = 0; k < max_len; k++) {
	if (S[i+k] == S[j+k]) res++;
	else break;
      }
      ans = max(ans, res);
    }
  }
  cout << ans << endl;
}
