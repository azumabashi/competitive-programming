def main():
    S = input()
    result = 'Yes'
    for i in range(len(S)):
        if (i % 2 == 0 and S[i] == 'L') or (i % 2 == 1 and S[i] == 'R'):
            result = 'No'
            break
    print(result)


if __name__ == '__main__':
    main()