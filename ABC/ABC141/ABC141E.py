def main():
    n = int(input())
    s = input()
    ans = 0
    for i in range(n):
        for j in range(n - 1, i, -1):
            max_len = min(j - i, n - j)
            if max_len <= ans:
                continue
            res = 0
            for k in range(max_len):
                if s[i + k] == s[j + k]:
                    res += 1
                else:
                    break
            ans = max(ans, res)
    print(ans)


if __name__ == '__main__':
    main()
