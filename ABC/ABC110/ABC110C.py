def main():
    S = input()
    T = input()
    st = {}
    ts = {}
    # S -> TとT -> Sの両方を見ないとダメらしい
    for i in range(97, 97 + 26):
        st[chr(i)] = ""
        ts[chr(i)] = ""
    for s, t in zip(S, T):
        if st[s] == "":
            st[s] = t
        if ts[t] == "":
            ts[t] = s
        if not st[s] == t or not ts[t] == s:
            print("No")
            break
    else:
        print("Yes")


if __name__ == '__main__':
    main()