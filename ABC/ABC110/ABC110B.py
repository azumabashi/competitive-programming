def main():
    N, M, X, Y = map(int, input().split(" "))
    x = max(list(map(int, input().split(" "))))
    y = min(list(map(int, input().split(" "))))
    if X < x < y < Y:
        print("No War")
    else:
        print("War")


if __name__ == '__main__':
    main()