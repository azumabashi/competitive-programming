def main():
    odd_str = input()
    even_str = input()
    answer = [""] * (len(odd_str) + len(even_str))
    for i in range(len(odd_str)):
        answer[2 * i] = odd_str[i]
    for i in range(len(even_str)):
        answer[2 * i + 1] = even_str[i]
    print("".join(answer))


if __name__ == '__main__':
    main()

