# from ABC163-C


def main():
    n = int(input())
    score = [[], []]
    minus = float("inf")
    for _ in range(n):
        s = int(input())
        if s % 10 == 0:
            score[1].append(s)
        else:
            score[0].append(s)
            minus = min(minus, s)
    answer = sum(score[0])
    if answer % 10 == 0:
        answer -= minus
    answer += sum(score[1])
    print(max(answer, 0))


if __name__ == '__main__':
    main()

