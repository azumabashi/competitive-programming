# from ARC099-C
from math import ceil


def main():
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    target = float("inf")
    target_index = []
    for i in range(n):
        if a[i] < target:
            target = a[i]
            target_index.append(i)
    answer = float("inf")
    for t in target_index:
        answer = min(answer, ceil(t / (k - 1)) + ceil((n - t - 1) / (k - 1)))
    print(answer)


if __name__ == '__main__':
    main()

