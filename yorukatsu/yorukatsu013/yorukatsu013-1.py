# from ABC127-C


def main():
    n, m = map(int, input().split())
    gates = [0 for _ in range(n + 1)]
    for _ in range(m):
        l, r = map(lambda x: int(x) - 1, input().split())
        gates[l] += 1
        gates[r + 1] -= 1
    for i in range(1, n + 1):
        gates[i] += gates[i - 1]
    answer = 0
    for i in range(n):
        if gates[i] == m:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

