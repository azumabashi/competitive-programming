# from ABC114-C
from itertools import product


def main():
    n = input()
    numbers = ['3', '5', '7']
    length = len(n)
    answer = 0
    for i in range(3, length + 1):
        for new_num in product(numbers, repeat=i):
            if not all(x in new_num for x in numbers):
                continue
            elif int("".join(new_num)) <= int(n):
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

