# from ABC065-B


def main():
    n = int(input())
    a = [0] + [int(input()) for _ in range(n)]
    checked = [False for _ in range(n + 1)]
    answer = 0
    ind = 1
    while True:
        answer += 1
        checked[ind] = True
        ind = a[ind]
        if checked[ind]:
            answer = -1
            break
        elif ind == 2:
            break
    print(answer if answer > -1 else -1)


if __name__ == '__main__':
    main()

