# from ABC061-C


def main():
    n, k = map(int, input().split())
    seq = {}
    for _ in range(n):
        a, b = map(int, input().split())
        if a in seq:
            seq[a] += b
        else:
            seq[a] = b
    seq = sorted(seq.items(), key=lambda x: x[0])
    answer = 0
    for n, c in seq:
        k -= min(c, k)
        if k <= 0:
            answer = n
            break
    print(answer)


if __name__ == '__main__':
    main()

