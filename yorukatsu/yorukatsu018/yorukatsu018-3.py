# from ABC089-C
from itertools import permutations


def main():
    n = int(input())
    l = ["M", "A", "R", "C", "H"]
    count = {x: 0 for x in l}
    for _ in range(n):
        s = input()[0]
        if s in count:
            count[s] += 1
    answer = 0
    for p in permutations(l, 3):
        answer += count[p[0]] * count[p[1]] * count[p[2]]
    print(answer // 6)


if __name__ == '__main__':
    main()

