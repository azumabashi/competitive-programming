# from ARC073-D


def main():
    n, w_lim = map(int, input().split())
    group = {}
    min_w = 0
    for i in range(n):
        w, v = map(int, input().split())
        if i == 0:
            group = {x: [] for x in range(w, w + 4)}
            min_w = w
        group[w].append(v)
    length = [len(group[i]) + 1 for i in range(min_w, min_w + 4)]
    for i in range(min_w, min_w + 4):
        group[i].sort(reverse=True)
        for j in range(1, length[i - min_w] - 1):
            group[i][j] += group[i][j - 1]
        group[i] = [0] + group[i]
    answer = 0
    for i in range(length[0]):
        for j in range(length[1]):
            for k in range(length[2]):
                for l in range(length[3]):
                    if w_lim < min_w * i + (min_w + 1) * j + (min_w + 2) * k + (min_w + 3) * l:
                        break
                    answer = max(answer, group[min_w][i] + group[min_w + 1][j] + group[min_w + 2][k] + group[min_w + 3][l])
    print(answer)


if __name__ == '__main__':
    main()

