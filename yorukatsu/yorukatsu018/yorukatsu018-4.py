# from ABC075-C
from collections import deque


def main():
    n, m = map(int, input().split())
    g = [[] for _ in range(n)]
    edge = []
    answer = 0
    for _ in range(m):
        a, b = map(lambda x: int(x) - 1, input().split())
        g[a].append(b)
        g[b].append(a)
        edge.append([a, b])
    for ng in edge:
        ng.sort()
        visited = [0 for _ in range(n)]
        q = deque([0])
        while q:
            now = q.pop()
            if visited[now] == 0:
                visited[now] = 1
                for next_v in g[now]:
                    if ng != sorted([next_v, now]):
                        q.append(next_v)
        if sum(visited) < n:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

