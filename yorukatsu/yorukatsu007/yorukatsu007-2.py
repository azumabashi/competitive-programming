# from ABC103-C


from math import gcd
from functools import reduce


def lcm(a, b):
    return a * b // gcd(a, b)


def main():
    n = int(input())
    a = list(map(int, input().split()))
    m = reduce(lcm, a) - 1
    ans = 0
    for aa in a:
        ans += m % aa
    print(ans)


if __name__ == '__main__':
    main()

