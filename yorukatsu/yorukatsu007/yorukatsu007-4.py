# from ABC151-D


from collections import deque
from copy import deepcopy


def main():
    h, w = map(int, input().split())
    grid = [list(input()) for _ in range(h)]
    ans = 0
    d = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    for i in range(h):
        for j in range(w):
            if grid[i][j] == "#":
                continue
            now_grid = deepcopy(grid)
            answer = [[float("inf") for _ in range(w)] for _ in range(h)]
            next = deque([[i, j]])
            answer[i][j] = 0
            while next:
                y, x = next.popleft()
                if now_grid[y][x] == ".":
                    now_grid[y][x] = "#"
                    for dx, dy in d:
                        if 0 <= x + dx < w and 0 <= y + dy < h and now_grid[y + dy][x + dx] == ".":
                            answer[y + dy][x + dx] = min(answer[y][x] + 1, answer[y + dy][x + dx])
                            next.append([y + dy, x + dx])
            for k in range(h):
                for l in range(w):
                    if grid[k][l] == "#":
                        continue
                    ans = max(ans, answer[k][l])
    print(ans)


if __name__ == '__main__':
    main()

