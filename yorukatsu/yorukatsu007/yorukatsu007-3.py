# from ABC129-C


def main():
    n, m = map(int, input().split())
    a = set([int(input()) for _ in range(m)])
    if 1 < n:
        dp = [0 for _ in range(n + 1)]
        dp[1] = 0 if 1 in a else 1
        dp[2] = 0 if 2 in a else 1
        dp[2] += dp[1]
        for i in range(3, n + 1):
            if i - 1 not in a:
                dp[i] += dp[i - 1]
            if i - 2 not in a:
                dp[i] += dp[i - 2]
        print(dp[-1] % (10 ** 9 + 7))
    else:
        print(1 if 1 not in a else 0)


if __name__ == '__main__':
    main()

