# from ABC082-B


def main():
    s = list(input())
    t = list(input())
    s.sort()
    t.sort(reverse=True)
    print("Yes" if "".join(s) < "".join(t) else "No")


if __name__ == '__main__':
    main()

