# from ABC113-C


def main():
    n, m = map(int, input().split())
    group = {i: [] for i in range(1, n + 1)}
    for i in range(m):
        p, y = map(int, input().split())
        group[p].append([y, i])
    answer = ["" for _ in range(m)]
    for i in range(1, n + 1):
        group[i].sort(key=lambda x: x[0])
    for i in range(1, n + 1):
        for j, yi in enumerate(group[i]):
            y, k = yi
            answer[k] = str(i).zfill(6) + str(j + 1).zfill(6)
    print("\n".join(answer))


if __name__ == '__main__':
    main()

