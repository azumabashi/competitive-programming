# from ABC051-B


def main():
    k, s = map(int, input().split())
    answer = 0
    for i in range(k + 1):
        for j in range(k + 1):
            l = s - i - j
            if 0 <= l <= k:
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

