# from ABC072-C


def main():
    n = int(input())
    a = list(map(int, input().split()))
    count = {i: 0 for i in range(10 ** 5 + 1)}
    for i in range(n):
        count[a[i]] += 1
    answer = 0
    for i in range(1, 10 ** 5):
        answer = max(answer, count[i - 1] + count[i] + count[i + 1])
    print(answer)


if __name__ == '__main__':
    main()

