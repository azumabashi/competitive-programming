def main():
    n = int(input())
    card = {}
    for _ in range(n):
        s = input()
        if s in card:
            card[s] += 1
        else:
            card[s] = 1
    m = int(input())
    for _ in range(m):
        t = input()
        if t in card:
            card[t] -= 1
        else:
            card[t] = -1
    card = sorted(card.items(), key=lambda x: x[1], reverse=True)
    print(max(card[0][1], 0))


if __name__ == '__main__':
    main()

