def main():
    s = list(input())
    length = len(s)
    answer = 0
    for i in range(pow(2, length - 1)):
        op = ["" for _ in range(length - 1)]
        for j in range(length - 1):
            if (i >> j) & 1:
                op[j] = "+"
        formula = s[0]
        for j in range(length - 1):
            formula += op[j] + s[j + 1]
        answer += eval(formula)
    print(answer)


if __name__ == '__main__':
    main()

