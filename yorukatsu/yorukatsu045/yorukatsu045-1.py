# from ABC134-C
from copy import deepcopy


def main():
    n = int(input())
    a = [int(input()) for _ in range(n)]
    b = deepcopy(a)
    b.sort()
    for aa in a:
        if aa == b[-1]:
            print(b[-2])
        else:
            print(b[-1])


if __name__ == '__main__':
    main()

