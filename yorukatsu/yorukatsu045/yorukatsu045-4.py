# from ABC057-C


def main():
    n = int(input())
    base = n
    f = []
    i = 2
    while i * i <= n:
        if n % i == 0:
            f.append(i)
            if i * i != n:
                f.append(n // i)
        i += 1
    if n != 1:
        f.append(n)
    f.sort()
    answer = float("inf")
    for ff in f:
        answer = min(answer, max(len(str(ff)), len(str(base // ff))))
    if base == 1:
        answer = 1
    print(answer)


if __name__ == '__main__':
    main()

