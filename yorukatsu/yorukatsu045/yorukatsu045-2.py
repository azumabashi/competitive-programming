# from ABC144-C


def main():
    n = int(input())
    f = []
    i = 2
    while i * i <= n:
        if n % i == 0:
            f.append(i)
            if i * i != n:
                f.append(n // i)
        i += 1
    if n != 1:
        f.append(n)
    f.sort()
    answer = float("inf")
    for ff in f:
        answer = min(answer, ff + n // ff - 2)
    print(answer)


if __name__ == '__main__':
    main()

