# from ARC059-D

# アンバランスであるための条件は，
# 同じ文字が並ぶ(aaaaaaa)か，同じ文字に1文字異なる文字が挟まれているか(aba)かの
# いずれかでありさえすれば良い．


def main():
    s = input() + "#"
    group = [[s[0], 1, 1, 0]]
    for i in range(1, len(s)):
        if s[i] == group[-1][0]:
            group[-1][1] += 1
        else:
            group[-1][3] = i
            group.append([s[i], 1, i + 1, -1])
    group.sort(key=lambda x: x[1], reverse=True)
    if group[0][1] > 1:
        print(group[0][2], group[0][3])
    else:
        for i in range(len(s) - 4):
            if s[i] == s[i + 2] and s[i] != s[i + 1]:
                print(i + 1, i + 3)
                break
        else:
            print(-1, -1)


if __name__ == '__main__':
    main()

