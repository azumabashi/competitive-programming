# from ABC055-B
def main():
    n = int(input())
    mod = 10 ** 9 + 7
    answer = 1
    for i in range(1, n + 1):
        answer = answer * i % mod
    print(answer)


if __name__ == '__main__':
    main()

