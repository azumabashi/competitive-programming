# from ABC118-C
from math import gcd
from functools import reduce


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    print(reduce(gcd, a))


if __name__ == '__main__':
    main()

