# from ABC084-D


def is_prime(n):
    result = True
    i = 2
    while i * i <= n:
        if n % i == 0:
            result = False
            break
        i += 1
    if n == 1:
        result = False
    return result


def main():
    q = int(input())
    max_num = 10 ** 5
    count_num = [0 for _ in range(max_num + 1)]
    is_prime_num = [False for _ in range(max_num + 1)]
    count_num[3] = 1
    is_prime_num[3] = True
    for i in range(4, max_num + 1):
        count_num[i] = count_num[i - 1]
        if i % 2:
            if is_prime(i):
                is_prime_num[i] = True
                count_num[i] = count_num[i - 1]
                if is_prime_num[(i + 1) // 2]:
                    count_num[i] += 1
    for _ in range(q):
        l, r = map(int, input().split())
        print(count_num[r] - count_num[l - 1])


if __name__ == '__main__':
    main()

