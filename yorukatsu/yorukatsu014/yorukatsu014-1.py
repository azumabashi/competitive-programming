# from ABC120-B
from math import gcd


def main():
    a, b, k = map(int, input().split())
    d = []
    g = gcd(a, b)
    i = 1
    while i * i <= g:
        if g % i == 0:
            d.append(i)
            if i * i != g:
                d.append(g // i)
        i += 1
    d.sort()
    print(d[-k])


if __name__ == '__main__':
    main()

