# from ABC145-C
from itertools import permutations
from math import sqrt, factorial


def main():
    n = int(input())
    xy = []
    for _ in range(n):
        xy.append(list(map(int, input().split())))
    town = [i for i in range(n)]
    dist_sum = 0
    for p in permutations(town):
        for i in range(n - 1):
            dist_sum += sqrt((xy[p[i]][0] - xy[p[i + 1]][0]) ** 2 + (xy[p[i]][1] - xy[p[i + 1]][1]) ** 2)
    print(dist_sum / factorial(n))


if __name__ == '__main__':
    main()

