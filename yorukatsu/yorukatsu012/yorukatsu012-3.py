# from ABC154-D
from itertools import accumulate


def expectation(n):
    return (n + 1) / 2


def main():
    n, k = map(int, input().split())
    p = [0] + list(map(int, input().split()))
    n += 1
    for i in range(1, n):
        p[i] = expectation(p[i])
    p = list(accumulate(p))
    answer = 0
    for i in range(n - k + 1):
        answer = max(answer, p[i + k - 1] - p[i - 1])
    print(answer)


if __name__ == '__main__':
    main()

