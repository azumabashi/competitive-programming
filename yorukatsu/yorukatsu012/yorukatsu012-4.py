# from ABC138-D
from collections import deque


def main():
    n, q = map(int, input().split())
    tree = [[] for _ in range(n)]
    for _ in range(n - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        tree[a].append(b)
        tree[b].append(a)
    counter = [0 for _ in range(n)]
    for _ in range(q):
        p, x = map(int, input().split())
        p -= 1
        counter[p] += x
    q = deque([0])
    checked = [False for _ in range(n)]
    checked[0] = True
    while q:
        now = q.pop()
        for next_v in tree[now]:
            if not checked[next_v]:
                counter[next_v] += counter[now]
                checked[next_v] = True
                q.append(next_v)
    print(" ".join(map(str, counter)))


if __name__ == '__main__':
    main()

