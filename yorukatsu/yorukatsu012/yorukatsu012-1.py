# from ABC101-B


def main():
    n = input()
    digit_sum = sum(list(map(int, list(n))))
    n = int(n)
    print("Yes" if n % digit_sum == 0 else "No")


if __name__ == '__main__':
    main()

