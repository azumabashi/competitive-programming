# from ABC085-D
from math import ceil


def main():
    n, h = map(int, input().split())
    attack = []
    for _ in range(n):
        a, b = map(int, input().split())
        attack.append([a, 1])
        attack.append([b, 0])
    answer = 0
    attack.sort(key=lambda x: x[0], reverse=True)
    for a, t in attack:
        if h <= 0:
            break
        if t:
            answer += ceil(h / a)
            h = 0
        else:
            answer += 1
            h -= a
    print(answer)


if __name__ == '__main__':
    main()

