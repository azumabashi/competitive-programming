# ABC151-C


def main():
    n, m = map(int, input().split())
    questions = [[0, False] for _ in range(n)]
    for _ in range(m):
        p, s = input().split()
        p = int(p) - 1
        if not questions[p][1]:
            if s == "WA":
                questions[p][0] += 1
            else:
                questions[p][1] = True
    ac = 0
    wa = 0
    for wa_num, is_accepted in questions:
        if is_accepted:
            ac += 1
            wa += wa_num
    print(ac, wa)


if __name__ == '__main__':
    main()

