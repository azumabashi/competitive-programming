# from ABC047-B


def main():
    w, h, n = map(int, input().split())
    a = [0, 0]
    b = [w, h]
    for _ in range(n):
        x, y, t = map(int, input().split())
        if t == 1:
            a[0] = max(a[0], x)
        elif t == 2:
            b[0] = min(b[0], x)
        elif t == 3:
            a[1] = max(a[1], y)
        else:
            b[1] = min(b[1], y)
    print(max(b[0] - a[0], 0) * max(b[1] - a[1], 0))


if __name__ == '__main__':
    main()

