# from ABC136-D


def main():
    s = input()
    group = []
    last = 0
    for i in range(len(s) - 1):
        if s[i:i + 2] == "LR":
            group.append(s[last:i + 1])
            last = i + 1
    group.append(s[last:])
    answer = [0 for _ in range(len(s))]
    accumulate_index = 0
    for g in group:
        gather = g.index("RL")
        length = len(g)
        if gather % 2:
            answer[accumulate_index + gather] = length // 2
            answer[accumulate_index + gather + 1] = length - answer[accumulate_index + gather]
        else:
            answer[accumulate_index + gather + 1] = length // 2
            answer[accumulate_index + gather] = length - answer[accumulate_index + gather + 1]
        accumulate_index += length
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

