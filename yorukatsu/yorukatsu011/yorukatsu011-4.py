# from ABC079-D


def main():
    h, w = map(int, input().split())
    cost = [list(map(int, input().split())) for _ in range(10)]
    a = [list(map(int, input().split())) for _ in range(h)]
    for k in range(10):
        for i in range(10):
            for j in range(10):
                cost[i][j] = min(cost[i][j], cost[i][k] + cost[k][j])
    answer = 0
    for i in range(h):
        for j in range(w):
            if a[i][j] == -1:
                continue
            answer += cost[a[i][j]][1]
    print(answer)


if __name__ == '__main__':
    main()

