# from ABC093-C


def main():
    numbers = list(map(int, input().split()))
    numbers.sort()
    answer = 0
    diff = [numbers[2] - numbers[0], numbers[2] - numbers[1]]
    if diff[0] % 2 == diff[1] % 2:
        answer += diff[0] // 2 + diff[1] // 2
        if diff[0] % 2:
            answer += 1
    else:
        answer += (diff[0] + 1) // 2 + (diff[1] + 1) // 2
        answer += 1
    print(answer)


if __name__ == '__main__':
    main()

