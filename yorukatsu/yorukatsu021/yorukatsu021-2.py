# from ABC100-C


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    answer = 0
    for aa in a:
        while aa > 0 and aa % 2 == 0:
            aa //= 2
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

