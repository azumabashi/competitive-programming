# from ABC143-D
from bisect import bisect_left


def main():
    n = int(input())
    l = [int(x) for x in input().split()]
    l.sort()
    answer = 0
    for i in range(n):
        for j in range(i + 1, n):
            index = bisect_left(l, l[j] + l[i])
            answer += index - j - 1
    print(answer)


if __name__ == '__main__':
    main()

