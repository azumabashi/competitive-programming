# from ABC126-C


def main():
    n, k = map(int, input().split())
    answer = 0
    for i in range(1, n + 1):
        start = i
        coin_toss_time = 0
        while start < k:
            coin_toss_time += 1
            start *= 2
        answer += pow(2, -coin_toss_time) / n
    print(answer)


if __name__ == '__main__':
    main()

