# from ABC097-B


def main():
    x = int(input())
    answer = 1
    b = 2
    while b <= x:
        p = 2
        while pow(b, p) <= x:
            answer = max(answer, pow(b, p))
            p += 1
        b += 1
    print(answer)


if __name__ == '__main__':
    main()

