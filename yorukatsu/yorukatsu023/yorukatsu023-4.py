# from ABC134-D


def main():
    n = int(input())
    a = [0] + [int(x) for x in input().split()]
    balls = [0 for _ in range(n + 1)]
    answer = []
    for i in range(n, 0, -1):
        if sum(balls[i::i]) % 2 != a[i]:
            balls[i] += 1
            answer.append(i)
    print(sum(balls))
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

