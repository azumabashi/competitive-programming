# from ABC079-C
from itertools import combinations_with_replacement, permutations


def main():
    abcd = list(input())
    can_break = False
    for pp in combinations_with_replacement(["+", "-"], 3):
        if can_break:
            break
        for p in permutations(pp):
            now = [abcd[0]]
            for i in range(3):
                now.append(p[i])
                now.append(abcd[i + 1])
            now_f = "".join(now)
            if eval(now_f) == 7:
                print(now_f + "=7")
                can_break = True
                break


if __name__ == '__main__':
    main()

