# from ABC089-B


def main():
    n = int(input())
    s = input().split()
    print("Three" if len(set(s)) == 3 else "Four")


if __name__ == '__main__':
    main()

