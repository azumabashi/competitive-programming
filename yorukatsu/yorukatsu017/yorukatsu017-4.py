# from ABC051-C
def main():
    sx, sy, tx, ty = map(int, input().split())
    answer = "U" * (ty - sy) + "R" * (tx - sx) + "D" * (ty - sy) + "L" * (tx - sx)
    answer += "L" + "U" * (ty - sy + 1) + "R" * (tx - sx + 1) + "D"
    answer += "R" + "D" * (ty - sy + 1) + "L" * (tx - sx + 1) + "U"
    print(answer)


if __name__ == '__main__':
    main()

