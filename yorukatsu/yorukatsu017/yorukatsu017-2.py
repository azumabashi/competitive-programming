# from ABC075-B


def main():
    h, w = map(int, input().split())
    grid = [["." for _ in range(w + 2)]]
    for _ in range(h):
        grid.append(["."] + list(input()) + ["."])
    grid.append(grid[0])
    answer = [["0" for _ in range(w)] for _ in range(h)]
    for i in range(1, h + 1):
        for j in range(1, w + 1):
            now_ans = 0
            if grid[i][j] == ".":
                for k in range(-1, 2):
                    now_ans += ("".join(grid[i + k][j - 1:j + 2])).count("#")
                answer[i - 1][j - 1] = str(now_ans)
            else:
                answer[i - 1][j - 1] = "#"
    for ans in answer:
        print("".join(ans))


if __name__ == '__main__':
    main()

