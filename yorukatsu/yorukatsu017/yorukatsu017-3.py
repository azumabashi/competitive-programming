# from ARC059-C
from math import floor, ceil


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    average = sum(a) / n
    answer = [0, 0]
    for i in range(n):
        answer[0] += (a[i] - floor(average)) ** 2
        answer[1] += (a[i] - ceil(average)) ** 2
    print(min(answer))


if __name__ == '__main__':
    main()

