# from ABC128-B


def main():
    n = int(input())
    group = {}
    city = []
    for i in range(n):
        s, p = input().split()
        if s not in city:
            city.append(s)
        if s in group:
            group[s].append([int(p), i + 1])
        else:
            group[s] = [[int(p), i + 1]]
    city.sort()
    for c in city:
        group[c].sort(key=lambda x: x[0], reverse=True)
        for _, n in group[c]:
            print(n)


if __name__ == '__main__':
    main()

