# from ABC066-B


def main():
    s = input()
    for i in range(-1, -len(s) - 1, -1):
        now = s[:i]
        if len(now) % 2:
            continue
        else:
            if now[:len(now) // 2] == now[len(now) // 2:]:
                print(len(now))
                break


if __name__ == '__main__':
    main()

