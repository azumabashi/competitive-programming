# from ABC094-C
from copy import deepcopy


def main():
    n = int(input())
    x = list(map(int, input().split()))
    y = deepcopy(x)
    y.sort()
    median = y[n // 2]
    next_median = y[n // 2 - 1]
    for xx in x:
        if xx >= median:
            print(next_median)
        else:
            print(median)


if __name__ == '__main__':
    main()

