# from ABC148-E


def main():
    n = int(input())
    if n % 2:
        print(0)
    else:
        n //= 2
        i = 5
        answer = 0
        while i <= n:
            answer += n // i
            i *= 5
        print(answer)


if __name__ == '__main__':
    main()

