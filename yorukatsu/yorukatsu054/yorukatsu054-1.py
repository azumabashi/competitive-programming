# from ABC068-A


def main():
    n = int(input())
    answer = 1
    while answer <= n:
        answer *= 2
    print(answer // 2)


if __name__ == '__main__':
    main()

