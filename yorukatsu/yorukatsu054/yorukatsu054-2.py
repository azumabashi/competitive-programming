# from ABC093-C


def main():
    num = list(map(int, input().split()))
    num.sort()
    if (num[2] - num[1]) % 2 == (num[2] - num[0]) % 2:
        print((2 * num[2] - num[1] - num[0]) // 2)
    else:
        print((2 * num[2] - num[1] - num[0] + 3) // 2)


if __name__ == '__main__':
    main()

