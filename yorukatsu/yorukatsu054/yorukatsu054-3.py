# from ABC149-D


def main():
    n, k = map(int, input().split())
    r, s, p = map(int, input().split())
    t = list(input())
    score = {"r": p, "s": r, "p": s}
    answer = 0
    for i in range(n):
        if i < k:
            answer += score[t[i]]
            # i + k < nをしっかりと入れること！！！
            # IndexError防止
            if i + k < n and t[i] == t[i + k]:
                t[i + k] = t[i + k] + "*"
        elif i + k < n:
            if "*" not in t[i]:
                answer += score[t[i][0]]
                if t[i][0] == t[i + k][0]:
                    t[i + k] = t[i + k] + "*"
        else:
            if "*" not in t[i]:
                answer += score[t[i][0]]
    print(answer)


if __name__ == '__main__':
    main()

