# from ABC137-C


def main():
    n = int(input())
    answer = 0
    count = {}
    for _ in range(n):
        s = "".join(sorted(input()))
        if s in count:
            answer += count[s]
            count[s] += 1
        else:
            count[s] = 1
    print(answer)


if __name__ == '__main__':
    main()

