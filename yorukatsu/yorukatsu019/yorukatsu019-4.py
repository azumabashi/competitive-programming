# from ABC130-D
from bisect import bisect_left


def main():
    n, k = map(int, input().split())
    a = [int(x) for x in input().split()]
    for i in range(1, n):
        a[i] += a[i - 1]
    answer = 0
    a = [0] + a
    for i in range(1, n + 1):
        index = bisect_left(a, a[i - 1] + k)
        if index <= n:
            answer += n - index + 1
    print(answer)


if __name__ == '__main__':
    main()

