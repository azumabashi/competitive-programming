# from ABC117-B


def main():
    n = int(input())
    l = [int(x) for x in input().split()]
    all_l = sum(l)
    l.sort()
    print("Yes" if all(l[-1] < all_l - l[-1] for i in range(n)) else "No")


if __name__ == '__main__':
    main()

