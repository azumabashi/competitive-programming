# from ABC121-C


def main():
    n, m = map(int, input().split())
    answer = 0
    cost = [list(map(int, input().split())) for _ in range(n)]
    cost.sort(key=lambda x: x[0])
    for c, num in cost:
        if m < 0:
            break
        answer += c * min(m, num)
        m -= min(m, num)
    print(answer)


if __name__ == '__main__':
    main()

