# from ABC107-C


def main():
    n, k = map(int, input().split())
    x = [int(i) for i in input().split()]
    x.sort()
    answer = float("inf")
    if n == k:
        if n == 1:
            answer = abs(x[0])
        if 0 <= x[0]:
            answer = x[-1]
        elif x[-1] <= 0:
            answer = abs(x[0])
        else:
            answer = x[-1] - x[0] + min(abs(x[-1]), abs(x[0]))
    else:
        for i in range(n - k + 1):
            left = x[i]
            right = x[i + k - 1]
            if left * right >= 0:
                answer = min(answer, max(abs(left), abs(right)))
            else:
                answer = min(answer, right - left + min(abs(left), abs(right)))
    print(answer)


if __name__ == '__main__':
    main()

