# from ABC142-A


def main():
    n = int(input())
    print(0.5 if n % 2 == 0 else (1 + n // 2) / n)


if __name__ == '__main__':
    main()

