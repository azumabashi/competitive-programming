# from ABC159-D


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    count = {}
    for aa in a:
        if aa in count:
            count[aa] += 1
        else:
            count[aa] = 1
    all_way = 0
    for c in count.values():
        all_way += c * (c - 1) // 2
    for i in range(n):
        if a[i] in count:
            print(all_way - count[a[i]] + 1)
        else:
            print(all_way)


if __name__ == '__main__':
    main()

