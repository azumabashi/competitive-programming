# from ABC115-C


def main():
    n, k = map(int, input().split())
    h = [int(input()) for _ in range(n)]
    h.sort()
    answer = float("inf")
    for i in range(n - k + 1):
        answer = min(answer, h[i + k - 1] - h[i])
    print(answer)


if __name__ == '__main__':
    main()

