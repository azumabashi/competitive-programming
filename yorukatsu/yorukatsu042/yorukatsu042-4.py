def main():
    all_s = input()
    k = int(input())
    substr = set()
    for i in range(len(all_s)):
        for j in range(1, k + 1):
            substr.add(all_s[i:i + j])
    substr = list(substr)
    substr.sort()
    print(substr[k - 1])


if __name__ == '__main__':
    main()

