# from ABC073-C


def main():
    n = int(input())
    numbers = {}
    for _ in range(n):
        a = int(input())
        if a in numbers:
            numbers[a] += 1
        else:
            numbers[a] = 1
    answer = 0
    for c in numbers.values():
        if c % 2 == 1:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

