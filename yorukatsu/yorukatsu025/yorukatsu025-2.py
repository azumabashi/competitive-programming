# from ABC064-C


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    colors = [0] * 9
    for aa in a:
        index = -1
        for i in range(8):
            if 400 * i <= aa < 400 * (i + 1):
                index = i
        colors[index] += 1
    answer = 0
    for i in range(8):
        if colors[i] > 0:
            answer += 1
    print(max(answer, 1), answer + colors[-1])


if __name__ == '__main__':
    main()

