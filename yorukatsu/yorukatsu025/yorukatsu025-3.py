# from ABC056-C
from math import ceil, sqrt


def main():
    x = int(input())
    print(ceil((-1 + sqrt(1 + 8 * x)) / 2))


if __name__ == '__main__':
    main()

