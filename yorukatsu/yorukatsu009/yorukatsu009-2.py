# from ABC042-B


def main():
    n, l = map(int, input().split())
    s = [input() for _ in range(n)]
    s.sort()
    print("".join(s))


if __name__ == '__main__':
    main()

