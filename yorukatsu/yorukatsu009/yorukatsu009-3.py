# from ABC157-C


def main():
    n, m = map(int, input().split())
    answer = [-1 for _ in range(n)]
    is_possible = True
    for _ in range(m):
        s, c = map(int, input().split())
        s -= 1
        if is_possible:
            if n <= s:
                is_possible = False
            if answer[s] == -1:
                answer[s] = c
            elif answer[s] != c:
                is_possible = False
    if answer[0] == 0 and 1 < n:
        is_possible = False
    for i in range(n):
        if answer[i] == -1:
            answer[i] = 0 if (0 < i or n < 1) or n == 1 else 1
    print("".join(map(str, answer)) if is_possible else -1)


if __name__ == '__main__':
    main()

