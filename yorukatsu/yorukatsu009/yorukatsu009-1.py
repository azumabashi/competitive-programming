# from ABC083-B


def main():
    n, a, b = map(int, input().split())
    answer = 0
    for i in range(1, n + 1):
        digit_sum = 0
        now = i
        while 0 < now:
            digit_sum += now % 10
            now //= 10
        if a <= digit_sum <= b:
            answer += i
    print(answer)


if __name__ == '__main__':
    main()

