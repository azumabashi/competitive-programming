# ABC079-C


def main():
    abcd = list(input())
    can_break = False
    for i in range(8):
        if can_break:
            break
        p = [""] * 3
        for j in range(3):
            if (i >> j) & 1:
                p[j] = "+"
            else:
                p[j] = "-"
        now = abcd[0] + p[0] + abcd[1] + p[1] + abcd[2] + p[2] + abcd[3]
        if eval(now) == 7:
            print(now + "=7")
            can_break = True
            break


if __name__ == '__main__':
    main()

