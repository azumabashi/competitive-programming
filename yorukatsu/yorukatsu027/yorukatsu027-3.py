# from ARC098-C


def main():
    n = int(input())
    s = input()
    west = [0 for _ in range(n)]
    west[0] = 1 if s[0] == "W" else 0
    for i in range(1, n):
        if s[i] == "W":
            west[i] = west[i - 1] + 1
        else:
            west[i] = west[i - 1]
    answer = n - west[-1]
    if s[0] == "E":
        answer -= 1
    for i in range(1, n):
        answer = min(answer, west[i - 1] + n - i - west[-1] + west[i - 1])
    tmp = 0 if s[-1] == "E" else 1
    answer = min(answer, west[-1] - tmp)
    print(answer)


if __name__ == '__main__':
    main()

