# from ABC096-C


def main():
    h, w = map(int, input().split())
    s = [["" for _ in range(w + 2)]]
    for _ in range(h):
        s.append([""] + list(input()) + [""])
    s.append(s[0])
    f = False
    for i in range(1, h + 1):
        if f:
            break
        for j in range(1, w + 1):
            if s[i][j] == "#":
                if not any(s[i + di][j + dj] == "#" for di, dj in [[0, 1], [1, 0], [0, -1], [-1, 0]]):
                    print("No")
                    f = True
                    break
    if not f:
        print("Yes")


if __name__ == '__main__':
    main()

