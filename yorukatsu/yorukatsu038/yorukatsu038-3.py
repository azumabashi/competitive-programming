# from ABC133-C


def main():
    l, r = map(int, input().split())
    if l + 2019 <= r:
        print(0)
    else:
        ans = float("inf")
        for i in range(l, r + 1):
            for j in range(i + 1, r + 1):
                ans = min(ans, (i * j) % 2019)
        print(ans)


if __name__ == '__main__':
    main()

