# from ABC057-B


def main():
    n, m = map(int, input().split())
    student = []
    for _ in range(n):
        student.append([int(x) for x in input().split()])
    check_point = []
    for _ in range(m):
        check_point.append([int(x) for x in input().split()])
    answer = ["0" for _ in range(n)]
    for i in range(n):
        now_ans = 0
        now_dist = float("inf")
        for j in range(m):
            dist = abs(student[i][0] - check_point[j][0]) + abs(student[i][1] - check_point[j][1])
            if dist < now_dist:
                now_ans = j + 1
                now_dist = dist
        answer[i] = str(now_ans)
    print("\n".join(answer))


if __name__ == '__main__':
    main()

