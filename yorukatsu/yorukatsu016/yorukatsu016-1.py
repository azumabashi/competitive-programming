# from ABC074-B


def main():
    n = int(input())
    k = int(input())
    x = [int(x) for x in input().split()]
    x.sort()
    answer = 0
    for i in range(n):
        answer += min(2 * x[i], 2 * abs(x[i] - k))
    print(answer)


if __name__ == '__main__':
    main()

