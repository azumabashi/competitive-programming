# from ABC161-D
from collections import deque


def main():
    k = int(input())
    q = deque([i for i in range(1, 10)])
    answer = 0
    for i in range(k):
        now = q.popleft()
        answer = now
        if now % 10 != 0:
            q.append(10 * now + now % 10 - 1)
        q.append(10 * now + now % 10)
        if now % 10 != 9:
            q.append(10 * now + now % 10 + 1)
    print(answer)


if __name__ == '__main__':
    main()

