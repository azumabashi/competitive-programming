from math import ceil


def main():
    l = [int(input()) for _ in range(6)]
    n, a, b, c, d, e = l
    group = ceil(n / min(a, b, c, d, e))
    print(4 + group)


if __name__ == '__main__':
    main()

