from collections import deque


def main():
    h, w = map(int, input().split())
    inf = 10 ** 9 + 7
    grid = [list(input()) for _ in range(h)]
    count = [[inf for _ in range(w)] for _ in range(h)]
    d = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    wall = 0
    count[0][0] = 1
    for g in grid:
        wall += g.count("#")
    s = deque([[0, 0]])
    while s:
        y, x = s.popleft()
        if grid[y][x] == ".":
            grid[y][x] = "#"
            for dy, dx in d:
                if 0 <= y + dy < h and 0 <= x + dx < w and grid[y + dy][x + dx] == ".":
                    count[y + dy][x + dx] = min(count[y + dy][x + dx], count[y][x] + 1)
                    s.append([y + dy, x + dx])
    print(h * w - count[-1][-1] - wall if count[-1][-1] != inf else -1)


if __name__ == '__main__':
    main()

