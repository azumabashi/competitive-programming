from bisect import bisect_right


def main():
    n, m, k = map(int, input().split())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    answer = 0
    is_changed = False
    if a[0] <= k or b[0] <= k:
        answer = 1
    elif a[0] + b[0] <= k:
        answer = 2
    for i in range(1, n):
        a[i] += a[i - 1]
        if k < a[i] and not is_changed:
            answer = i
            is_changed = True
    if a[-1] <= k:
        answer = n
    is_changed = False
    for i in range(1, m):
        b[i] += b[i - 1]
        if k < b[i] and not is_changed:
            answer = max(answer, i)
            is_changed = True
    if b[-1] <= k:
        answer = max(answer, m)
    if a[-1] + b[-1] <= k:
        answer = n + m
    elif all(k < x for x in [a[0], b[0], a[0] + b[0]]):
        answer = 0
    else:
        for i in range(n):
            l = bisect_right(b, k - a[i])
            if l == 0 or (l == m and b[l - 1] + a[i] > k):
                continue
            answer = max(answer, i + 1 + l)
    print(answer)


if __name__ == '__main__':
    main()

