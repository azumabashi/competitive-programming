# pypyでAC


def main():
    n = int(input())
    l = [0 for _ in range(n + 1)]
    for i in range(1, n + 1):
        for j in range(i, n + 1, i):
            l[j] += 1
    answer = 0
    for i in range(n + 1):
        answer += i * l[i]
    print(answer)


if __name__ == '__main__':
    main()

