# 方針は良い，numpyでの高速化を要する


def main():
    height, width, build_cost, upper_limit = map(int, input().split())
    price = []
    each_price = [[0 for _ in range(width + 1)] for _ in range(height)]
    answer = 0
    for _ in range(height):
        price.append(list(map(int, input().split())))
    for i in range(height):
        for j in range(1, width):
            each_price[i][j] = price[i][j]
            price[i][j] += price[i][j - 1]
    for i in range(1, height):
        for j in range(width):
            price[i][j] += price[i - 1][j]
    print(price)
    for i in range(height):
        for j in range(width):
            for k in range(i, height):
                for l in range(j, width):
                    area = (k - i + 1) * (l - j + 1)
                    now_cost = price[k][l] + area * build_cost
                    if i > 0:
                        now_cost -= price[i - 1][l]
                    if j > 0:
                        now_cost -= price[k][j - 1]
                    if i > 0 and j > 0:
                        now_cost += price[i - 1][j - 1]
                    if now_cost <= upper_limit:
                        answer = max(answer, area)
    print(answer)


if __name__ == '__main__':
    main()

