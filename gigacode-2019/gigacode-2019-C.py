def main():
    days = int(input())
    allowance = [int(x) for x in input().split()]
    cost = [int(x) for x in input().split()]
    sum_allowance = 0
    answer = 10 ** 10
    for i in range(days):
        sum_allowance += allowance[i]
        if cost[i] <= sum_allowance:
            answer = min(answer, cost[i])
    print(answer if answer < 10 ** 10 else -1)


if __name__ == '__main__':
    main()

