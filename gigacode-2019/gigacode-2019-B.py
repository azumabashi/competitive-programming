def main():
    n, x, y, z = map(int, input().split())
    answer = 0
    for _ in range(n):
        a, b = map(int, input().split())
        if x <= a and y <= b and z <= a + b:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

