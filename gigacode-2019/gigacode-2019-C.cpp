#include<bits/stdc++.h>
using namespace std;

int main() {
    long long height, width, build_cost, budget;
    cin >> height >> width >> build_cost >> budget;
    vector<vector<long long>> price(height, vector<long long>(width));
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            cin >> price[i][j];
        }
    }
    for (int i = 0; i < height; i++) {
        for (int j = 1; j < width; j++) {
            price[i][j] += price[i][j - 1];
        }
    }
    for (int i = 1; i < height; i++) {
        for (int j = 0; j < width; j++) {
            price[i][j] += price[i - 1][j];
        }
    }
    long long answer = 0;
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            for (int k = i; k < height; k++) {
                for (int l = j; l < width; l++) {
                    long long area = (k - i + 1) * (l - j + 1);
                    long long now_cost = price[k][l] + area * build_cost;
                    if (i > 0) now_cost -= price[i - 1][l];
                    if (j > 0) now_cost -= price[k][j - 1];
                    if (i * j > 0) now_cost += price[i - 1][j - 1];
                    if (now_cost <= budget) answer = max(answer, area);
                }
            }
        }
    }
    cout << answer << endl;
    return 0;
}