from itertools import combinations


def main():
    n, m = map(int, input().split())
    a = [0] + list(map(int, input().split()))
    bonus_score = []
    bonus_cond = []
    for _ in range(m):
        l = list(map(int, input().split()))
        bonus_score.append(l[0])
        bonus_cond.append(set(l[2:]))
    ans = 0
    member = [i for i in range(1, n + 1)]
    for now_member in combinations(member, 9):
        now = 0
        for i in now_member:
            now += a[i]
        for i in range(m):
            if len(list(filter(lambda x: x in bonus_cond[i], now_member))) > 2:
                now += bonus_score[i]
        ans = max(ans, now)
    print(ans)


if __name__ == '__main__':
    main()
