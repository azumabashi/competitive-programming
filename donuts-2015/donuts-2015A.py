from math import pi


def main():
    radius, distance = map(int, input().split())
    print(radius * radius * pi * 2 * distance * pi)


if __name__ == '__main__':
    main()

