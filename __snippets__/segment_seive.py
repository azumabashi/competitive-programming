def segment_sieve(l, r, m):
    interval = r - l + 1
    p = [0] * (m + 1)
    for i in range(2, m + 1):
        for j in range(2 * i, m + 1, i):
            p[j] += i
    prime = []
    for i in range(2, m + 1):
        if not p[i]:
            prime.append(i)
    is_prime = [False] * (m + 1)
    min_prime = [i for i in range(l, r + 1)]
    count_prime_factors = [0] * interval
    for p in prime:
        is_prime[p] = True
    for p in prime:
        for i in range(((l + p - 1) // p) * p - l, interval, p):
            now = min_prime[i]
            cnt = 0
            while not now % p:
                now //= p
                cnt += 1
            count_prime_factors[i] += cnt
            min_prime[i] = now
    for i in range(interval):
        if min_prime[i] > 1:
            count_prime_factors[i] += 1
    return sum([int(is_prime[x]) for x in count_prime_factors])
