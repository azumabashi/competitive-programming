"""
C++でいうmultiset
from: https://tsubo.hatenablog.jp/entry/2020/06/15/124657
"""


import heapq


class HeapDict:
    def __init__(self):
        self.h = []
        self.d = {}

    def insert(self, x):
        heapq.heappush(self.h, x)
        if x not in self.d:
            self.d[x] = 1
        else:
            self.d[x] += 1

    def erase(self, x):
        self.d[x] -= 1
        while len(self.h) != 0:
            val = heapq.heappop(self.h)
            if self.d[val]:
                heapq.heappush(self.h, val)
                break

    def is_exist(self, x):
        return x in self.d and self.d[x] != 0

    def get_min(self):
        return self.h[0]

    def size(self):
        return len(self.h)
