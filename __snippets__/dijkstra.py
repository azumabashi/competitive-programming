def dijkstra(root, graph):
    from heapq import heapify, heappop, heappush
    max_val = 10 ** 10
    # graph[s] := (t, dist(s,t))
    dist = [max_val for _ in range(len(graph))]
    dist[root] = 0
    q = []
    heapify(q)
    heappush(q, (0, root))
    while q:
        now_d, now_v = heappop(q)
        for next_v, length in graph[now_v]:
            now_dist = dist[now_v] + length
            if dist[next_v] > now_dist:
                dist[next_v] = now_dist
                heappush(q, (dist[next_v], next_v))
    return dist