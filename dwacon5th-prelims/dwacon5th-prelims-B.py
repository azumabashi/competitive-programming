from itertools import accumulate


def main():
    n, k = map(int, input().split())
    a = [0] + list(accumulate(map(int, input().split())))
    val = []
    for i in range(n + 1):
        for j in range(i + 1, n + 1):
            val.append(abs(a[i] - a[j]))
    ans = 0
    p = [1]
    for _ in range(45):
        p.append(p[-1] * 2)
    for i in range(40, 0, -1):
        f = p[i - 1]
        cnt = 0
        for v in val:
            if (ans + f) & v == ans + f:
                cnt += 1
        if cnt >= k:
            ans += f
    print(ans)


if __name__ == '__main__':
    main()
