def convert_to_decimal(number, base):
    result = 0
    number = str(number)
    for i in range(-1, -len(number) - 1, -1):
        result += int(number[i]) * pow(base, abs(i) - 1)
    return result


def main():
    a = int(input())
    for i in range(10, 10001):
        if a == convert_to_decimal(i, i):
            print(i)
            break
    else:
        print(-1)


if __name__ == '__main__':
    main()

