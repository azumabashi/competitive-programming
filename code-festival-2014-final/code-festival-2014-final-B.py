def main():
    integers = list(map(int, list(input())))
    print(sum(integers[0::2]) - sum(integers[1::2]))


if __name__ == '__main__':
    main()

