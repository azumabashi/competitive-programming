def main():
    n = int(input())
    raw_rating = list(map(int, input().split()))
    rating = [raw_rating[0]]
    for i in range(1, n):
        if rating[-1] != raw_rating[i]:
            rating.append(raw_rating[i])
    change_diff = 0
    if len(rating) > 1:
        is_increasing = 1 if rating[0] < rating[1] else 0
        # increasing: 1, decreasing: 0.
        for i in range(1, len(rating)):
            if (rating[i] < rating[i - 1] and is_increasing == 1) or (rating[i] > rating[i - 1] and is_increasing == 0):
                is_increasing = (is_increasing + 1) % 2
                change_diff += 1
    print(2 + change_diff if change_diff > 0 else 0)


if __name__ == '__main__':
    main()

