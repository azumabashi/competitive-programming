def main():
    index = int(input())
    fib = [1, 1]
    for _ in range(index - 1):
        fib.append(fib[-1] + fib[-2])
    print(fib[index])


if __name__ == '__main__':
    main()

