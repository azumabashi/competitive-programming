def main():
    string = list(input())
    answer = [""] * len(string)
    answer[0] = string[0]
    for i in range(1, len(string)):
        if string[i] == " " and string[i - 1] != " ":
            answer[i] = ","
        elif string[i] != " ":
            answer[i] = string[i]
    print("".join(answer))


if __name__ == '__main__':
    main()

