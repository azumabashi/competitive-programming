def main():
    cookies = list(map(int, input().split()))
    answer = 0
    if cookies[0] == cookies[1] == cookies[2] and cookies[0] % 2 == 0:
        answer = -1
    elif all(cookies[i] % 2 == 0 for i in range(3)):
        half = [0, 0, 0]
        sum_cookies = sum(cookies)
        while all(cookies[i] % 2 == 0 for i in range(3)):
            for i in range(3):
                half[i] = (sum_cookies - cookies[i]) // 2
            cookies[0] = half[1] + half[2]
            cookies[1] = half[0] + half[2]
            cookies[2] = half[1] + half[0]
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

