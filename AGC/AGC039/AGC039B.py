from collections import deque


def is_bipartite(v, n):
    q = deque([(0, 1)])
    res = [0] * n
    while q:
        now, col = q.pop()
        res[now] = col
        for n_v in v[now]:
            if res[n_v] == col:
                return False
            elif res[n_v] == 0:
                q.append((n_v, -col))
    return True


def main():
    n = int(input())
    g = [[] for _ in range(n)]
    grid = []
    for i in range(n):
        s = input()
        grid.append(s)
        for j in range(n):
            if s[j] == "1":
                g[i].append(j)
    ans = -1
    if is_bipartite(g, n):
        max_val = 1 << 50
        dist = [[max_val for _ in range(n)] for _ in range(n)]
        for i in range(n):
            for j in g[i]:
                dist[i][j] = 1
            dist[i][i] = 0
        for k in range(n):
            for i in range(n):
                for j in range(n):
                    dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])
        for i in range(n):
            for j in range(n):
                if dist[i][j] == max_val:
                    continue
                ans = max(ans, dist[i][j])
        ans += 1
    print(ans)


if __name__ == '__main__':
    main()
