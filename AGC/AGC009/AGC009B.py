def convert_number(number, correspondence):
    result = ""
    for digit_num in number:
        result += correspondence[int(digit_num)]
    return result


def main():
    correspondence = input().split()
    inverse_correspondence = [0 for _ in range(10)]
    for i in range(10):
        inverse_correspondence[int(correspondence[i])] = str(i)
    length = int(input())
    numbers = [int(convert_number(input(), inverse_correspondence)) for _ in range(length)]
    numbers.sort()
    for num in numbers:
        print(convert_number(str(num), correspondence))


if __name__ == '__main__':
    main()

