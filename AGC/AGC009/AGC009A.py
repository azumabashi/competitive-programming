from math import ceil


def main():
    length = int(input())
    sequence = [0 for _ in range(length)]
    target = [0 for _ in range(length)]
    for i in range(length):
        a, b = map(int, input().split())
        sequence[i] = a
        target[i] = b
    answer = 0
    cumulative_push = 0
    for i in range(-1, -length - 1, -1):
        sequence[i] += cumulative_push
        now_push = ceil(sequence[i] / target[i]) * target[i] - sequence[i]
        cumulative_push += now_push
        answer += now_push
    print(answer)


if __name__ == '__main__':
    main()

