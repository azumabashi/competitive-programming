def main():
    n, a, b = map(int, input().split())
    print((b - a) // 2 if a % 2 == b % 2 else min(a, n - b + 1) + (b - a - 1) // 2)


if __name__ == '__main__':
    main()

