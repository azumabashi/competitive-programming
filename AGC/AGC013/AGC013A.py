def main():
    length = int(input())
    sequence = list(map(int, input().split()))
    answer = 0
    index = 0
    while index < length:
        while index + 1 < length and sequence[index] == sequence[index + 1]:
            index += 1
        if index + 1 < length and sequence[index] < sequence[index + 1]:
            while index + 1 < length and sequence[index] <= sequence[index + 1]:
                index += 1
        elif index + 1 < length and sequence[index + 1] < sequence[index]:
            while index + 1 < length and sequence[index + 1] <= sequence[index]:
                index += 1
        answer += 1
        index += 1
    print(answer)


if __name__ == '__main__':
    main()

