def main():
    length = int(input())
    numbers = list(map(int, input().split()))
    accumulate_numbers = [numbers[0] for _ in range(length)]
    for i in range(1, length):
        accumulate_numbers[i] = accumulate_numbers[i - 1] + numbers[i]
    count = {}
    answer = 0
    for a in accumulate_numbers:
        if a in count:
            answer += count[a]
            count[a] += 1
        else:
            count[a] = 1
        if a == 0:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

