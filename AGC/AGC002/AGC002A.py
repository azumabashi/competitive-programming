def main():
    a, b = map(int, input().split())
    if a <= 0 <= b:
        print("Zero")
    elif a < 0 and (a - b + 1) % 2:
        print("Negative")
    else:
        print("Positive")


if __name__ == '__main__':
    main()

