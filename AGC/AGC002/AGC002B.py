def main():
    boxes, operation_num = map(int, input().split())
    operation = []
    for _ in range(operation_num):
        operation.append(list(map(int, input().split())))
    can_exist_red = [False for _ in range(boxes)]
    ball = [1 for _ in range(boxes)]
    can_exist_red[0] = True
    for op_from, op_to in operation:
        ball[op_to - 1] += 1
        ball[op_from - 1] -= 1
        if can_exist_red[op_from - 1]:
            can_exist_red[op_to - 1] = True
            if ball[op_from - 1] == 0:
                can_exist_red[op_from - 1] = False
    answer = 0
    for c in can_exist_red:
        if c:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

