def f(n, a):
    res = 0
    while n % a == 0:
        n //= a
        res += 1
    return res


def main():
    n = int(input())
    a = []
    base = pow(10, 9)
    for _ in range(n):
        r_num = input()
        if "." not in r_num:
            a.append(int(r_num) * base)
        else:
            b, c = r_num.split(".")
            c = c.ljust(9, "0")
            a.append(int(b) * base + int(c))
    size = 100
    cnt = [[0 for _ in range(size)] for _ in range(size)]
    fact_cnt = []
    for aa in a:
        now = aa
        two = f(now, 2)
        five = f(now, 5)
        fact_cnt.append([two, five])
        cnt[two][five] += 1
    cnt_sum = [[0 for _ in range(size + 1)] for _ in range(size + 1)]
    for i in range(1, size + 1):
        for j in range(1, size + 1):
            cnt_sum[i][j] += cnt_sum[i - 1][j] + cnt_sum[i][j - 1] - cnt_sum[i - 1][j - 1] + cnt[i - 1][j - 1]
    ans = 0
    for cnt_t, cnt_f in fact_cnt:
        new_t = max(18 - cnt_t, 0)
        new_f = max(18 - cnt_f, 0)
        ans += cnt_sum[size][size] - cnt_sum[size][new_f] - cnt_sum[new_t][size] + cnt_sum[new_t][new_f]
        if all(y <= x for x, y in zip([cnt_t, cnt_f], [new_t, new_f])):
            ans -= 1
    ans //= 2
    print(ans)


if __name__ == '__main__':
    main()
