def main():
    q, h, s, d = map(int, input().split())
    target = int(input())
    h = min(2 * q, h)
    s = min(2 * h, s)
    d = min(2 * s, d)
    answer = target // 2 * d + (target % 2) * s
    print(answer)


if __name__ == '__main__':
    main()

