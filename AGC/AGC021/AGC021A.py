def get_digit_sum(n):
    result = 0
    for d in n:
        result += int(d)
    return result


def main():
    n = input()
    answer = str(int(n[0]) - 1) + "9" * (len(n) - 1)
    print(max(get_digit_sum(answer), get_digit_sum(n)))


if __name__ == '__main__':
    main()

