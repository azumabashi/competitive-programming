def main():
    length, min_number, max_number = map(int, input().split())
    print(max(0, (length - 2) * (max_number - min_number) + 1) if min_number <= max_number else 0)


if __name__ == '__main__':
    main()

