def main():
    button = input()
    floor = len(button)
    answer = 0
    for i in range(1, floor + 1):
        if button[i - 1] == "U":
            answer += floor - i
            answer += (i - 1) * 2
        else:
            answer += (floor - i) * 2
            answer += i - 1
    print(answer)


if __name__ == '__main__':
    main()

