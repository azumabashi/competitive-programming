from string import ascii_lowercase


def main():
    word = input()
    target_list = list(ascii_lowercase)
    answer = float("inf")
    for target in target_list:
        now_ans = 0
        now_word = word
        while len(set(now_word)) > 1:
            tmp = ""
            for i in range(len(now_word) - 1):
                if now_word[i] == target or now_word[i + 1] == target:
                    tmp += target
                else:
                    tmp += now_word[i]
            now_word = tmp
            now_ans += 1
        answer = min(answer, now_ans)
    print(answer)


if __name__ == '__main__':
    main()

