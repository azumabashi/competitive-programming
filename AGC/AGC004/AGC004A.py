def main():
    length = list(map(int, input().split()))
    length.sort()
    print(length[0] * length[1] * (length[2] - 2 * (length[2] // 2)))


if __name__ == '__main__':
    main()

