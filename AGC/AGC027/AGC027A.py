from itertools import accumulate
from bisect import bisect_right


def main():
    children, cookies = map(int, input().split())
    target = sorted(list(map(int, input().split())))
    sum_target = sum(target)
    if sum_target < cookies:
        print(children - 1)
    elif cookies < target[0]:
        print(0)
    else:
        accumulate_target = list(accumulate(target))
        print(bisect_right(accumulate_target, cookies))


if __name__ == '__main__':
    main()

