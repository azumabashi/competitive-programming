def get_digit_sum(n):
    result = 0
    while n > 0:
        result += n % 10
        n //= 10
    return result


def main():
    ab_sum = int(input())
    answer = float("inf")
    for i in range(1, ab_sum // 2):
        answer = min(answer, get_digit_sum(i) + get_digit_sum(ab_sum - i))
    print(answer if ab_sum > 3 else 2)


if __name__ == '__main__':
    main()

