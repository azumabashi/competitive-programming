from math import ceil


def main():
    passengers, capacity, waiting_limit = map(int, input().split())
    arrive_time = [int(input()) for _ in range(passengers)]
    arrive_time.sort()
    count = 0
    waiting = 0
    waiting_since = -1
    for t in arrive_time:
        if waiting_since > 0 and waiting_since + waiting_limit < t or capacity <= waiting:
            count += ceil(waiting / capacity)
            waiting = 0
            waiting_since = -1
        waiting += 1
        if waiting_since < 0:
            waiting_since = t
    count += ceil(waiting / capacity)
    print(count)


if __name__ == '__main__':
    main()

