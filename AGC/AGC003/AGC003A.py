def main():
    direction = input()
    now = [0, 0]
    dir_count = {"N": 0, "E": 0, "S": 0, "W": 0}
    for d in direction:
        dir_count[d] += 1
        if d == "N":
            now[0] += 1
        elif d == "E":
            now[1] += 1
        elif d == "S":
            now[0] -= 1
        else:
            now[1] -= 1
    answer = False
    if ((now[1] < 0 and 0 < dir_count["E"]) or (0 < now[1] and 0 < dir_count["W"]) or now[1] == 0) and \
            ((now[0] < 0 and 0 < dir_count["N"]) or (0 < now[0] and 0 < dir_count["S"]) or now[0] == 0):
        answer = True
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()

