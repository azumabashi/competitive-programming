def main():
    string = input()
    length = len(string) + 1
    answer = [0] * length
    for i in range(length - 1):
        if string[i] == "<":
            answer[i + 1] = answer[i] + 1
    for i in range(length - 2, -1, -1):
        if string[i] == ">":
            answer[i] = max(answer[i], answer[i + 1] + 1)
    print(sum(answer))


if __name__ == '__main__':
    main()

