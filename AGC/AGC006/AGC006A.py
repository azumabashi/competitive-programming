def main():
    length = int(input())
    s = input()
    t = input()
    overlap = 0
    for i in range(1, length + 1):
        if t[:i] == s[-i:]:
            overlap = i
    print(2 * length - overlap)


if __name__ == '__main__':
    main()

