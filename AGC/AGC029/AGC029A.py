def main():
    reversi = input()
    answer = 0
    count_black = [0] * len(reversi)
    if reversi[0] == "B":
        count_black[0] = 1
    for i in range(1, len(reversi)):
        if reversi[i] == "B":
            count_black[i] = count_black[i - 1] + 1
        else:
            count_black[i] = count_black[i - 1]
    for i in range(len(reversi)):
        if reversi[i] == "W":
            answer += count_black[i]
    print(answer)


if __name__ == '__main__':
    main()

