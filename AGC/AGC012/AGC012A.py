def main():
    groups = int(input())
    strength = list(map(int, input().split()))
    strength.sort()
    answer = 0
    for i in range(-2, -2 * groups - 2, -2):
        answer += strength[i]
    print(answer)


if __name__ == '__main__':
    main()

