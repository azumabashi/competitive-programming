def n_pow(a, n):
    if n <= 0:
        return 0
    return pow(a, n)


def main():
    bags, remainder = map(int, input().split())
    biscuits = list(map(int, input().split()))
    count = [0, 0]
    for b in biscuits:
        count[b % 2] += 1
    if remainder == 1 and count[1] == 0:
        print(0)
    elif count[1] == 0:
        print(pow(2, bags))
    else:
        print(n_pow(2, bags - 1))


if __name__ == '__main__':
    main()

