def main():
    d = int(input())
    s = input()
    n = [int(t) for t in s]
    dp_min = [[0 for _ in range(110)] for _ in range(10010)]
    dp_not = [[0 for _ in range(110)] for _ in range(10010)]
    dp_not[0][0] = 1
    mod = 10 ** 9 + 7
    for i in range(len(s)):
        for j in range(d):
            for k in range(10):
                ni = i + 1
                nj = (j + k) % d
                dp_min[ni][nj] += dp_min[i][j]
                dp_min[ni][nj] %= mod
                if n[i] > k:
                    dp_min[ni][nj] += dp_not[i][j]
                    dp_min[ni][nj] %= mod
                elif n[i] == k:
                    dp_not[ni][nj] += dp_not[i][j]
                    dp_not[ni][nj] %= mod
    print(dp_min[len(s)][0] % mod - 1)


if __name__ == '__main__':
    main()
