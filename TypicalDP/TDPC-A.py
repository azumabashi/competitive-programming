def main():
    n = int(input())
    p = [int(x) for x in input().split()]
    dp = [[set() for _ in range(2)] for _ in range(n)]
    # dp[i][j] := i問目を　j=0: 使う，j=1: 使わない　ときに考えうる総配点
    for i in range(n):
        if i == 0:
            dp[i][0].add(p[i])
            dp[i][1].add(0)
        else:
            for before_score in dp[i - 1][0]:
                dp[i][0].add(before_score + p[i])
                dp[i][1].add(before_score)
            for before_score in dp[i - 1][1]:
                dp[i][0].add(before_score + p[i])
                dp[i][1].add(before_score)
    print(len(dp[-1][0] | dp[-1][1]))


if __name__ == '__main__':
    main()

