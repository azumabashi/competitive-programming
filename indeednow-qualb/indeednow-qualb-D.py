def main():
    n, c = map(int, input().split())
    a = [int(k) for k in input().split()]
    idx = [[] for _ in range(c)]
    for i in range(n):
        idx[a[i] - 1].append(i + 1)
    for i in range(c):
        res = 0
        k = len(idx[i])
        res += max(idx[i][0], 1) * max(n - idx[i][0] + 1, 1)
        for j in range(1, k):
            res += max(idx[i][j] - idx[i][j - 1], 1) * max(n - idx[i][j] + 1, 1)
        print(res)


if __name__ == '__main__':
    main()
