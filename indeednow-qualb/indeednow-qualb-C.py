from heapq import heapify, heappop, heappush


def main():
    n = int(input())
    graph = [[] for _ in range(n)]
    for _ in range(n - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        graph[a].append(b)
        graph[b].append(a)
    ans = [0]
    is_used = {0}
    q = graph[0]
    heapify(q)
    while q:
        now = heappop(q)
        if now not in is_used:
            is_used.add(now)
            ans.append(now)
            for next_v in graph[now]:
                if next_v not in is_used:
                    heappush(q, next_v)
    print(" ".join(map(lambda x: str(x + 1), ans)))


if __name__ == '__main__':
    main()
