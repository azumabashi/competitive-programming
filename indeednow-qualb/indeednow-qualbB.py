def main():
    word = input()
    goal = input()
    answer = -1
    for i in range(len(word) + 1):
        if word == goal:
            answer = i
            break
        else:
            word = word[-1] + word[:-1]
    print(answer)


if __name__ == '__main__':
    main()

