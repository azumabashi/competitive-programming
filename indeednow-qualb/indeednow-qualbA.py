def main():
    start = list(map(int, input().split()))
    goal = list(map(int, input().split()))
    print(abs(start[0] - goal[0]) + abs(start[1] - goal[1]) + 1)


if __name__ == '__main__':
    main()

