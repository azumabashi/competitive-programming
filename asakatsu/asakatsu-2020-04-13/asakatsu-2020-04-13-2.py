# from ABC096-C


def main():
    h, w = map(int, input().split())
    s = [list(input()) for _ in range(h)]
    delta = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    black_count = 0
    for i in range(h):
        for j in range(w):
            if s[i][j] == "#":
                black_count += 1
                tmp = 0
                for dy, dx in delta:
                    new_y = dy + i
                    new_x = dx + j
                    if 0 <= new_y < h and 0 <= new_x < w:
                        if s[new_y][new_x] == "#":
                            tmp += 1
                if tmp > 0:
                    black_count -= 1
    print("Yes" if black_count == 0 else "No")


if __name__ == '__main__':
    main()

