# from ABC107-C


def main():
    n, k = map(int, input().split())
    x = [int(x) for x in input().split()]
    answer = float("inf")
    if 1 < n:
        for i in range(n - k + 1):
            if x[i + k - 1] < 0:
                answer = min(answer, -x[i])
            elif 0 < x[i]:
                answer = min(answer, x[i + k - 1])
            else:
                answer = min(answer, min(abs(x[i]), abs(x[i + k - 1])) + x[i + k - 1] - x[i])
    else:
        answer = abs(x[0])
    print(answer)


if __name__ == '__main__':
    main()

