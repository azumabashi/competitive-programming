# from ABC022-B


def main():
    n = int(input())
    answer = 0
    count = {}
    for _ in range(n):
        a = int(input())
        if a in count:
            answer += 1
            count[a] += 1
        else:
            count[a] = 1
    print(answer)


if __name__ == '__main__':
    main()

