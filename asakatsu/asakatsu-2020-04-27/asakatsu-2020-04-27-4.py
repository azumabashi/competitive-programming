# from ABC163-D


def sum_from_1(n):
    return n * (n + 1) // 2


def main():
    n, k = map(int, input().split())
    answer = 0
    mod = 10 ** 9 + 7
    for i in range(k, n + 2):
        answer += sum_from_1(n) - sum_from_1(n - i) - sum_from_1(i - 1) + 1
        answer %= mod
    print(answer)


if __name__ == '__main__':
    main()

