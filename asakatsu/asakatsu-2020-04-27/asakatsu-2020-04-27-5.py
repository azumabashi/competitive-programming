# from ABC103-D


def main():
    n, m = map(int, input().split())
    requests = [list(map(int, input().split())) for _ in range(m)]
    requests.sort(key=lambda x: x[1])
    bridge = [-1]
    for begin, end in requests:
        if begin <= bridge[-1] < end:
            continue
        else:
            bridge.append(end - 1)
    print(len(bridge) - 1)


if __name__ == '__main__':
    main()

