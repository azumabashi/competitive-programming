# from ARC029-A


def main():
    n = int(input())
    t = [int(input()) for _ in range(n)]
    answer = float("inf")
    for i in range(pow(2, n)):
        time = [0, 0]
        for j in range(n):
            if (i >> j) & 1:
                time[1] += t[j]
            else:
                time[0] += t[j]
        answer = min(answer, max(time[1], time[0]))
    print(answer)


if __name__ == '__main__':
    main()

