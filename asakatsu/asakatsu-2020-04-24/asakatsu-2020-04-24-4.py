# from ARC080-D


def main():
    h, w = map(int, input().split())
    n = int(input())
    a = [-1] + [int(x) for x in input().split()]
    for i in range(1, n + 1):
        a[i] += a[i - 1]
    answer = ["-1" for _ in range(w * h)]
    index = 0
    for i in range(h * w):
        if not (a[index] < i <= a[index + 1]):
            index += 1
        answer[i] = str(index + 1)
    for i in range(h):
        if i % 2 == 0:
            print(" ".join(answer[i * w: (i + 1) * w]))
        else:
            print(" ".join(reversed(answer[i * w: (i + 1) * w])))


if __name__ == '__main__':
    main()

