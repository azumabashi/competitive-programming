# from ABC103-D


def main():
    n, m = map(int, input().split())
    query = [[int(x) for x in input().split()] for _ in range(m)]
    break_bridges = [-1]
    query.sort(key=lambda x: x[1])
    for begin, end in query:
        if not (begin <= break_bridges[-1] < end):
            break_bridges.append(end - 1)
    print(len(break_bridges) - 1)


if __name__ == '__main__':
    main()

