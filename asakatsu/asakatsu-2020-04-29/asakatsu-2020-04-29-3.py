# from ABC123-C
from math import ceil


def main():
    n = int(input())
    required_times = [int(input()) for _ in range(5)]
    groups = ceil(n / min(required_times))
    print(groups + 4)


if __name__ == '__main__':
    main()

