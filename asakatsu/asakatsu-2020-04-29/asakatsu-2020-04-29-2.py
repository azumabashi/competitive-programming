# from ABC096-C


def main():
    h, w = map(int, input().split())
    s = [["." for _ in range(w + 2)]]
    for _ in range(h):
        s.append(["."] + list(input()) + ["."])
    s.append(s[0])
    delta = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    count_black = 0
    count_possible = 0
    for i in range(1, h + 1):
        for j in range(1, w + 1):
            if s[i][j] == "#":
                count_black += 1
                if any(s[dy + i][dx + j] == "#" for dy, dx in delta):
                    count_possible += 1
    print("Yes" if count_possible == count_black else "No")


if __name__ == '__main__':
    main()

