# from ABC094-B


def main():
    n, m, x = map(int, input().split())
    a = [int(x) - 1 for x in input().split()]
    count = [0 for _ in range(n)]
    for aa in a:
        count[aa] += 1
    for i in range(1, n):
        count[i] += count[i - 1]
    print(min(count[x - 1], count[n - 1] - count[x - 1]))


if __name__ == '__main__':
    main()

