# from ABC088-C


def main():
    c = [[int(x) for x in input().split()] for _ in range(3)]
    all_sum = 0
    answer = True
    for i in range(3):
        all_sum += sum(c[i])
    if all_sum % 3 == 0:
        all_sum //= 3
    else:
        answer = False
    now_sum = 0
    for i in range(3):
        now_sum += c[i][i]
    if now_sum != all_sum:
        answer = False
    now_sum = 0
    for i in range(3):
        now_sum += c[i][2 - i]
    if now_sum != all_sum:
        answer = False
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()

