def main():
    num = list(map(int, input().split()))
    max_num = max(num)
    answer = 0
    for i in range(3):
        answer += (max_num - num[i]) // 2
        num[i] += ((max_num - num[i]) // 2) * 2
    num.sort()
    if num[0] == num[1] and num[1] != num[2]:
        answer += 1
    elif num[1] == num[0] + 1:
        answer += 2
    print(answer)


if __name__ == '__main__':
    main()

