def main():
    n = int(input())
    t = [int(input()) for _ in range(n)]
    t.sort()
    answer = float("inf")
    for i in range(pow(2, n)):
        need = [0, 0]
        bin_i = bin(i)[2:].zfill(n)
        for i in range(n):
            if bin_i[i] == "1":
                need[0] += t[i]
            else:
                need[1] += t[i]
        answer = min(answer, max(need))
    print(answer)


if __name__ == '__main__':
    main()

