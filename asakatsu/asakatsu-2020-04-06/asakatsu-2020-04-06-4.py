from collections import deque


def main():
    n = int(input())
    tree = [[] for _ in range(n)]
    dist = [0 for _ in range(n)]
    for _ in range(n - 1):
        a, b, c = map(int, input().split())
        tree[a - 1].append([b - 1, c])
        tree[b - 1].append([a - 1, c])
    q, k = map(int, input().split())
    k -= 1
    s = deque([k])
    dist[k] = -1
    while s:
        now = s.pop()
        for t, d in tree[now]:
            if dist[t] == 0:
                dist[t] = dist[now] + d
                dist[t] += 1 if now == k else 0
                s.append(t)
    dist[k] = 0
    for _ in range(q):
        x, y = map(lambda x: int(x) - 1, input().split())
        print(dist[x] + dist[y])


if __name__ == '__main__':
    main()

