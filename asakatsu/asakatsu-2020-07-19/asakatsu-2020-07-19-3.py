def main():
    n, m = map(int, input().split())
    deg_long = 6 * m
    deg_short = (n % 12) * 30 + 0.5 * m
    deg = abs(deg_long - deg_short)
    print(min(deg, abs(360 - deg)))


if __name__ == '__main__':
    main()

