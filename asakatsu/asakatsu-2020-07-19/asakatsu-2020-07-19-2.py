def main():
    n = int(input())
    target = list("indeednow")
    target.sort()
    for _ in range(n):
        s = list(input())
        s.sort()
        print("YES" if target == s else "NO")


if __name__ == '__main__':
    main()

