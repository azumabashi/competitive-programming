def main():
    k = int(input())
    ans = [1, 1]
    for _ in range(k - 1):
        ans.append(sum(ans[-2:]))
    print(" ".join(map(str, ans[-2:])))


if __name__ == '__main__':
    main()

