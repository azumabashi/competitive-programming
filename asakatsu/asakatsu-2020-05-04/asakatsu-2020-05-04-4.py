# from ABC031-C


def main():
    n = int(input())
    a = list(map(int, input().split()))
    answer = -float("inf")
    for i in range(n):
        now_score = -float("inf")
        now_aoki_index = 0
        for j in range(n):
            now_sum = sum(a[min(i, j) + 1:max(i, j) + 1:2])
            if i == j:
                continue
            if now_score < now_sum:
                now_aoki_index = j
                now_score = now_sum
        answer = max(answer, sum(a[min(i, now_aoki_index):max(i, now_aoki_index) + 1:2]))
    print(answer)


if __name__ == '__main__':
    main()

