# from ARC014-B


def main():
    n = int(input())
    word = []
    answer = "DRAW"
    is_changed = False
    for i in range(n):
        w = input()
        if not is_changed:
            if (i > 0 and word[-1][-1] != w[0]) or w in word:
                if i % 2 == 0:
                    answer = "LOSE"
                    is_changed = True
                else:
                    answer = "WIN"
                    is_changed = True
        word.append(w)
    print(answer)


if __name__ == '__main__':
    main()

