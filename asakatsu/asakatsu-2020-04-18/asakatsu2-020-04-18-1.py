# from ABC112-B


def main():
    n, max_t = map(int, input().split())
    max_val = 10 ** 9 + 7
    answer = max_val
    for _ in range(n):
        c, t = map(int, input().split())
        if t <= max_t:
            answer = min(answer, c)
    print(answer if answer != max_val else "TLE")


if __name__ == '__main__':
    main()

