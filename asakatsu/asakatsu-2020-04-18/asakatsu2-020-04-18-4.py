# from ABC121-D


def find_xor_from_zero(n):
    # XORは周期4で求まることを利用
    return [n, 1, n + 1, 0][n % 4]


def main():
    a, b = map(int, input().split())
    if a != b:
        print(find_xor_from_zero(a - 1) ^ find_xor_from_zero(b))
    else:
        print(a)


if __name__ == '__main__':
    main()

