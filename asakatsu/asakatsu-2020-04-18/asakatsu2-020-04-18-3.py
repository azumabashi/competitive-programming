# from ARC068-C
from math import ceil


def main():
    x = int(input())
    answer = ceil(x / 11)
    if 1 <= x % 11 <= 6:
        answer = answer * 2 - 1
    else:
        answer *= 2
    print(answer)


if __name__ == '__main__':
    main()

