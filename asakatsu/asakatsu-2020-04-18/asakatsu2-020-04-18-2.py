# from ABC100-B


def main():
    d, n = map(int, input().split())
    answer = 100 ** d
    if 0 < d:
        while 0 < n:
            if answer % (100 ** d) == 0 and answer % (100 ** (d + 1)) > 0:
                n -= 1
            answer += 100 ** d
        answer -= 100 ** d
    else:
        answer = n if n < 100 else 101
    print(answer)


if __name__ == '__main__':
    main()

