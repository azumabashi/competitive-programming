# from ABC127-D


def main():
    n, m = map(int, input().split())
    a = [int(x) for x in input().split()]
    cards = {}
    for aa in a:
        if aa in cards:
            cards[aa] += 1
        else:
            cards[aa] = 1
    for _ in range(m):
        b, c = map(int, input().split())
        if c in cards:
            cards[c] += b
        else:
            cards[c] = b
    cards = sorted(cards.items(), key=lambda x: x[0], reverse=True)
    answer = 0
    for val, count in cards:
        if n <= 0:
            break
        else:
            answer += min(n, count) * val
            n -= min(n, count)
    print(answer)


if __name__ == '__main__':
    main()

