# from ARC008-A
from math import ceil


def main():
    n = int(input())
    print(min((n // 10) * 100 + (n % 10) * 15, ceil(n / 10) * 100))


if __name__ == '__main__':
    main()

