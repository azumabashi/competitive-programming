# from ARC024-B


def main():
    n = int(input())
    append_back = 1
    has_checked = False
    first = input()
    color = []
    for _ in range(n - 1):
        c = input()
        if first == c and not has_checked:
            append_back += 1
        else:
            has_checked = True
            color.append(c)
    for _ in range(append_back):
        color.append(first)
    if len(set(color)) == 1:
        print(-1)
    else:
        color.append("-1")
        now = 1
        answer = 0
        for i in range(1, n + 1):
            if color[i] == color[i - 1]:
                now += 1
            else:
                if now % 2 == 1:
                    answer = max(answer, (now - 1) // 2)
                elif now % 2 == 0:
                    answer = max(answer, (now - 2) // 2)
                now = 1
        answer += 1
        print(answer)


if __name__ == '__main__':
    main()

