# from ARC036-B


def main():
    n = int(input())
    h = [int(input()) for _ in range(n)]
    inc = [0 for _ in range(n)]
    dec = [0 for _ in range(n)]
    for i in range(1, n):
        if h[i] > h[i - 1]:
            inc[i] = inc[i - 1] + 1
    for i in range(-2, -n - 1, -1):
        if h[i] > h[i + 1]:
            dec[i] = dec[i + 1] + 1
    answer = 1
    for i in range(n):
        answer = max(answer, inc[i] + dec[i] + 1)
    print(answer)


if __name__ == '__main__':
    main()

