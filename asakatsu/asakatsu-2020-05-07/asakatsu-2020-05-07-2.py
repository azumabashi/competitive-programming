# from ABC087-C
from itertools import accumulate


def main():
    n = int(input())
    a = [list(accumulate(map(int, input().split()))) for _ in range(2)]
    answer = a[0][0] + a[1][0]
    if n > 1:
        answer = max(a[0][0] + a[1][-1], a[0][-1] + a[1][-1] - a[1][-2])
        for i in range(1, n):
            answer = max(answer, a[0][i] + a[1][-1] - a[1][i - 1])
    print(answer)


if __name__ == '__main__':
    main()

