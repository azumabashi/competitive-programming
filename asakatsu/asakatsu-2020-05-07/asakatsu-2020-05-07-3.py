# from ABC126-C


def main():
    n, k = map(int, input().split())
    answer = 0.0
    for i in range(1, n + 1):
        now = i
        count = 0
        while now < k:
            now *= 2
            count += 1
        answer += pow(0.5, count)
    answer /= n
    print(answer)


if __name__ == '__main__':
    main()

