# from ABC093-C


def main():
    num = list(map(int, input().split()))
    num.sort()
    answer = 0
    a = num[2] - num[0]
    b = num[2] - num[1]
    if a % 2 == b % 2:
        answer = a // 2 + b // 2 + a % 2
    elif a % 2 != b % 2:
        answer = a // 2 + b // 2 + 2
    print(answer)


if __name__ == '__main__':
    main()

