# from ARC006-B


def main():
    n, l = map(int, input().split())
    g = [" " + input() + " " for _ in range(l + 1)]
    now = g[-1].index("o")
    for i in range(l, -1, -1):
        if g[i][now - 1] == "-":
            now -= 2
        elif g[i][now + 1] == "-":
            now += 2
    print(now // 2 + 1)


if __name__ == '__main__':
    main()

