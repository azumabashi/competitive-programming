# from ARC042-A


def main():
    n, m = map(int, input().split())
    order = [[i, 0] for i in range(n)]
    for i in range(m):
        order[int(input()) - 1][1] = i + 1
    order.sort(key=lambda x: x[1], reverse=True)
    for i in range(n):
        print(order[i][0] + 1)


if __name__ == '__main__':
    main()

