# from ARC036-A


def main():
    n, k = map(int, input().split())
    t = [0] + [int(input()) for _ in range(n)]
    for i in range(1, n + 1):
        t[i] += t[i - 1]
    answer = -1
    for i in range(3, n + 1):
        if t[i] - t[i - 3] < k:
            answer = i
            break
    print(answer)


if __name__ == '__main__':
    main()

