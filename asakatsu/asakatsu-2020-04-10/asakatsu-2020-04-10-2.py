from itertools import product


def main():
    op = ['+', '-']
    numbers = list(map(int, list(input())))
    for now_op in product(op, repeat=3):
        now = numbers[0]
        for i in range(3):
            if now_op[i] == '+':
                now += numbers[i + 1]
            else:
                now -= numbers[i + 1]
        if now == 7:
            answer = ""
            for i in range(3):
                answer += str(numbers[i]) + now_op[i]
            answer += str(numbers[3]) + "=7"
            print(answer)
            break


if __name__ == '__main__':
    main()

