def main():
    n = int(input())
    task = []
    for _ in range(n):
        task.append(list(map(int, input().split())))
    time = 0
    task.sort(key=lambda x: x[1])
    answer = True
    for i in range(n):
        required_time, deadline = task[i]
        if deadline < time + required_time:
            answer = False
        else:
            time += required_time
    print("Yes" if answer else "No")


if __name__ == '__main__':
    main()

