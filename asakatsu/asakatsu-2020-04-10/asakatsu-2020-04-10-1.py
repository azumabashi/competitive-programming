from math import ceil


def main():
    a, b = map(int, input().split())
    print(ceil((b - 1) / (a - 1)))


if __name__ == '__main__':
    main()

