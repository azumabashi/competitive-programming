# from ABC022-A


def main():
    n, s, t = map(int, input().split())
    w = int(input())
    answer = 0
    if s <= w <= t:
        answer += 1
    for _ in range(n - 1):
        a = int(input())
        w += a
        if s <= w <= t:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

