# from ABC137-C


def main():
    word = {}
    n = int(input())
    for _ in range(n):
        s = "".join(sorted(input()))
        if s in word:
            word[s] += 1
        else:
            word[s] = 1
    answer = 0
    for cnt in word.values():
        answer += cnt * (cnt - 1) // 2
    print(answer)


if __name__ == '__main__':
    main()

