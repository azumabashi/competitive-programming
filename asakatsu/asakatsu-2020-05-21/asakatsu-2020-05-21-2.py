# from ABC121-C


def main():
    n, m = map(int, input().split())
    info = []
    for _ in range(n):
        info.append(list(map(int, input().split())))
    info.sort(key=lambda x: x[0])
    answer = 0
    for a, b in info:
        if m <= 0:
            break
        answer += a * min(m, b)
        m -= min(m, b)
    print(answer)


if __name__ == '__main__':
    main()

