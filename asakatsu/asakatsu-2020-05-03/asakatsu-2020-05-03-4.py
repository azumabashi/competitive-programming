# from ABC094-D
from bisect import bisect_left


def main():
    n = int(input())
    a = [int(x) for x in input().split()]
    a.sort()
    if n > 2:
        index = bisect_left(a, a[-1] // 2)
        if index == n - 1:
            index -= 1
        elif index > 0 and a[-1] - a[index] < a[index - 1]:
            index -= 1
        print(a[-1], a[index])
    else:
        print(a[1], a[0])


if __name__ == '__main__':
    main()

