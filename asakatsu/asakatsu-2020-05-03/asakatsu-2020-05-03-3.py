# from AGC015-B


def main():
    s = input()
    n = len(s)
    answer = 2 * (n - 1)
    for i in range(1, n - 1):
        if s[i] == "U":
            answer += n - i - 1 + i * 2
        else:
            answer += (n - i - 1) * 2 + i
    print(answer)


if __name__ == '__main__':
    main()

