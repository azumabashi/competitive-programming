# from ABC057-B


def main():
    n, m = map(int, input().split())
    student = [[int(x) for x in input().split()] for _ in range(n)]
    point = [[int(x) for x in input().split()] for _ in range(m)]
    answer = [0 for _ in range(n)]
    for i, begin in enumerate(student):
        a, b = begin
        dist = float("inf")
        for j, goal in enumerate(point):
            c, d = goal
            if abs(a - c) + abs(b - d) < dist:
                dist = abs(a - c) + abs(b - d)
                answer[i] = j + 1
    print("\n".join(map(str, answer)))


if __name__ == '__main__':
    main()

