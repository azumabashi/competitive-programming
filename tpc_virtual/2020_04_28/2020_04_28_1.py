# from ARC064-C


def main():
    n, x = map(int, input().split())
    a = [int(x) for x in input().split()]
    answer = 0
    for i in range(1, n):
        if x < a[i - 1] + a[i]:
            answer += a[i - 1] + a[i] - x
            a[i] -= min(a[i], a[i] + a[i - 1] - x)
    print(answer)


if __name__ == '__main__':
    main()

