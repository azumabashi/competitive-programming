def main():
    n = int(input())
    s = input()
    east = [0 for _ in range(n)]
    west = [0 for _ in range(n)]
    if s[0] == "E":
        east[0] = 1
    else:
        west[0] = 1
    for i in range(1, n):
        east[i] = east[i - 1]
        west[i] = west[i - 1]
        if s[i] == "E":
            east[i] += 1
        else:
            west[i] += 1
    answer = east[-1] - east[0]
    for i in range(1, n):
        answer = min(answer, west[i - 1] + east[-1] - east[i])
    print(answer)


if __name__ == '__main__':
    main()

