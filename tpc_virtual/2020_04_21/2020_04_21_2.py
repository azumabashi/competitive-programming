# from ARC060-C 部分点解法


def main():
    n, a = map(int, input().split())
    x = [int(x) for x in input().split()]
    answer = 0
    for i in range(1, pow(2, n)):
        count_1 = 0
        now_sum = 0
        for j in range(n):
            if (i >> j) & 1:
                count_1 += 1
                now_sum += x[j]
        if a * count_1 == now_sum:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

