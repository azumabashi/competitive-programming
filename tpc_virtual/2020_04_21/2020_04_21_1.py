# from ABC113-C


def main():
    n, m = map(int, input().split())
    answer = ["" for _ in range(m)]
    all_pref = {}
    for i in range(m):
        p, y = map(int, input().split())
        if p in all_pref:
            all_pref[p].append([y, i])
        else:
            all_pref[p] = [[y, i]]
    for pref, date in all_pref.items():
        date.sort(key=lambda x: x[0])
        order = 0
        for year, num in date:
            answer[num] = str(pref).zfill(6) + str(order + 1).zfill(6)
            order += 1
    print("\n".join(answer))


if __name__ == '__main__':
    main()

