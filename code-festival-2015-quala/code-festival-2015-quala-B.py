def main():
    length = int(input())
    sequence = list(map(int, input().split()))
    answer = 0
    for i in range(length):
        answer += sequence[i] * pow(2, length - i - 1)
    print(answer)


if __name__ == '__main__':
    main()

