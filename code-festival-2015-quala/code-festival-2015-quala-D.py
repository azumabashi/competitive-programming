def check(time, n, m, x):
    left = 1
    for i in range(m):
        if x[i] - left > time:
            # 1号車のほうにしか行けない場合
            return False
        left = min(x[i + 1], max(time - x[i] + 2 * left, (x[i] + left + time) // 2) + 1)
        # 2回通るのが1号車の方かN号車の方かで場合分け，より遠くに行ける方を選ぶ
    return left > n
    # M人目がN号車に到達できていればtimeで点検できる


def binary_search(n, m, x):
    ng = -1
    ok = 10 ** 10
    while ok - ng > 1:
        mid = (ok + ng) // 2
        if check(mid, n, m, x):
            ok = mid
        else:
            ng = mid
    return ok


def main():
    n, m = map(int, input().split())
    x = [int(input()) for _ in range(m)]
    x.append(2 * n)
    print(binary_search(n, m, x))


if __name__ == '__main__':
    main()

