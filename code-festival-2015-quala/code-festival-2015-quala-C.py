def main():
    homework, time = map(int, input().split())
    required_time = []
    normal_time_sum = 0
    copy_time_sum = 0
    for _ in range(homework):
        a, b = map(int, input().split())
        required_time.append(a - b)
        normal_time_sum += a
        copy_time_sum += b
    required_time.sort()
    if time < copy_time_sum:
        print(-1)
    elif normal_time_sum <= time:
        print(0)
    else:
        for i in range(-1, -homework - 1, -1):
            normal_time_sum -= required_time[i]
            if normal_time_sum <= time:
                print(-i)
                break


if __name__ == '__main__':
    main()

