from bisect import bisect_right


def main():
    loop = int(input())
    n = int(input())
    m = int(input())
    d = [0]
    for _ in range(n - 1):
        d.append(int(input()))
    d.append(loop)
    d.sort()
    k = [int(input()) for _ in range(m)]
    ans = 0
    for kk in k:
        idx = bisect_right(d, kk)
        ans += min(abs(d[idx - 1] - kk), abs(d[idx] - kk))
    print(ans)


if __name__ == '__main__':
    main()
