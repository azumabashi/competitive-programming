def main():
    n, x = input().split()
    n = int(n)
    x = list(map(int, list(x)))
    x_len = len(x)
    max_x = max(x)
    x = x[::-1]
    for i in range(max_x + 1, 11):
        now = 0
        for j in range(x_len):
            now += pow(i, j) * x[j]
        if now == n:
            print(i)
            break


if __name__ == '__main__':
    main()

