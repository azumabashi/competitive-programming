def main():
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    answer = {-1: -1}
    for i in range(n):
        answer[a[i]] = i + 1
    a.sort()
    length = -1
    for i in range(n):
        if k < a[i]:
            break
        else:
            length = a[i]
    print(answer[length])


if __name__ == '__main__':
    main()

