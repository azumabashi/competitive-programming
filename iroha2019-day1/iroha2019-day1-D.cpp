#include<bits/stdc++.h>
using namespace std;

int main(){
    int N, X, Y;
    cin >> N >> X >> Y;
    vector<int> A(N);
    for (int i = 0; i < N; i++) cin >> A[i];
    sort(A.begin(), A.end());
    for (int i = N - 1; -1 < i; i--) {
        if (i % 2 == 1) X += A[i];
        else Y += A[i];
    }
    if (X == Y) cout << "Draw";
    else if (X > Y) cout << "Takahashi";
    else cout << "Aoki";
    cout << endl;
    return 0;
}