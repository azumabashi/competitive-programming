def main():
    word = input()
    operation = int(input()) % len(word)
    for _ in range(operation):
        word = word[1:] + word[0]
    print(word)


if __name__ == '__main__':
    main()

