def main():
    n, k = map(int, input().split())
    f = []
    while n % 2 == 0:
        f.append(2)
        n //= 2
    i = 3
    while i * i <= n:
        if n % i == 0:
            n //= i
            f.append(i)
        else:
            i += 2
    if n != 1:
        f.append(n)
    f.sort()
    if len(f) < k:
        print(-1)
    else:
        ans = []
        tmp = 1
        for i in range(len(f)):
            if i < k - 1:
               ans.append(f[i])
            else:
                tmp *= f[i]
        ans.append(tmp)
        ans.sort()
        print(" ".join(map(str, ans)))


if __name__ == '__main__':
    main()

