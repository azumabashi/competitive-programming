answer = 0


def main():
    global answer
    width = int(input())
    height = int(input())
    grid = [list(map(int, input().split())) for _ in range(height)]
    delta = [[0, 1], [1, 0], [0, -1], [-1, 0]]

    def search(d, y, x):
        global answer
        grid[y][x] = 0
        for dy, dx in delta:
            new_y = y + dy
            new_x = x + dx
            if 0 <= new_y < height and 0 <= new_x < width and grid[new_y][new_x]:
                search(d + 1, new_y, new_x)
        grid[y][x] = 1
        answer = max(answer, d + 1)

    for i in range(height):
        for j in range(width):
            if grid[i][j]:
                # ここのifを忘れると大変
                search(0, i, j)
    print(answer)


if __name__ == '__main__':
    main()

