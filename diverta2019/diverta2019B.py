def main():
    red_box, green_box, blue_box, target = map(int, input().split())
    answer = 0
    for i in range(target // red_box + 1):
        for j in range(target // green_box + 1):
            now_balls = red_box * i + green_box * j
            if (target - now_balls) % blue_box == 0 and target >= now_balls:
                answer += 1
            elif target < now_balls:
                break
    print(answer)


if __name__ == '__main__':
    main()

