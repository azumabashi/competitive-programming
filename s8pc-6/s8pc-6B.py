def main():
    customers = int(input())
    good_1 = []
    good_2 = []
    for _ in range(customers):
        a, b = map(int, input().split())
        good_1.append(min(a, b))
        good_2.append(max(a, b))
    answer = float("inf")
    for i in range(customers):
        for j in range(customers):
            now_score = 0
            entrance = good_1[i]
            way_out = good_2[j]
            for k in range(customers):
                if entrance <= good_1[k] <= good_2[k] <= way_out:
                    now_score += abs(way_out - entrance)
                elif good_1[k] < entrance and good_2[k] <= way_out:
                    now_score += abs(entrance - good_1[k]) + abs(way_out - good_1[k])
                elif entrance <= good_1[k] and way_out < good_2[k]:
                    now_score += abs(good_2[k] - entrance) + abs(good_2[k] - way_out)
                else:
                    now_score += abs(good_2[k] - good_1[k]) + abs(entrance - good_1[k]) + abs(good_2[k] - way_out)
            answer = min(answer, now_score)
    print(answer)


if __name__ == '__main__':
    main()

