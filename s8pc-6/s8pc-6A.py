from math import ceil


def main():
    cities, time_leap = map(int, input().split())
    dist = sum(map(int, input().split()))
    print(ceil(dist / time_leap))


if __name__ == '__main__':
    main()

