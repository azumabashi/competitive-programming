from collections import deque
from copy import deepcopy


def main():
    height, width = map(int, input().split())
    grid = [list(input()) for _ in range(height)]
    search = deepcopy(grid)
    dist = [[-1 for _ in range(width)] for _ in range(height)]
    dist[0][0] = 0
    queue = deque([[0, 0]])
    delta = [[1, 0], [0, 1]]
    while queue:
        y, x = queue.pop()
        for dy, dx in delta:
            new_y = y + dy
            new_x = x + dx
            if new_x < 0 or new_y < 0 or height <= new_y or width <= new_x:
                continue
            if dist[new_y][new_x] < 0 and search[new_y][new_x] == ".":
                dist[new_y][new_x] = dist[y][x] + 1
                search[new_y][new_x] = "#"
                queue.append([new_y, new_x])
    is_exist_no_black = False
    for i in range(height):
        if "#" not in grid[i]:
            is_exist_no_black = True
            break
    print("Yay!" if is_exist_no_black and dist[-1][-1] >= 0 else ":(")


if __name__ == '__main__':
    main()

