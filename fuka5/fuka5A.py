def main():
    while True:
        n, k = map(int, input().split())
        if n == k == 0:
            return
        x = list(map(int, input().split()))
        x.sort()
        print(sum(x[:k]))


if __name__ == '__main__':
    main()
