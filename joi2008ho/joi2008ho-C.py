from bisect import bisect_right


def main():
    targets, max_score = map(int, input().split())
    score = [0] + [int(input()) for _ in range(targets)]
    # [0]: 投げない　を表す
    half_score = set()
    targets += 1
    for i in range(targets):
        for j in range(i, targets):
            half_score.add(score[i] + score[j])
    half_score = sorted(half_score)
    half_score_length = len(half_score)
    answer = 0
    for i in range(targets):
        for j in range(i, targets):
            half_index = bisect_right(half_score, max_score - score[i] - score[j])

            if half_index <= 0 or half_score_length <= half_index:
                continue
            if score[i] + score[j] + half_score[half_index - 1] <= max_score:
                answer = max(answer, score[i] + score[j] + half_score[half_index - 1])
    print(answer)


if __name__ == '__main__':
    main()

