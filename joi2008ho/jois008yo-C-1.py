from bisect import bisect_left


def main():
    n, m = map(int, input().split())
    p = [int(input()) for _ in range(n)] + [0]
    ans = 0
    score = [0]
    for i in range(n + 1):
        for j in range(i, n + 1):
            if p[i] + p[j] <= m:
                score.append(p[i] + p[j])
    score.sort()
    for s in score:
        if m < s:
            continue
        idx = bisect_left(score, m - s)
        if s + score[idx - 1] <= m:
            ans = max(ans, s + score[idx - 1])
    print(ans)


if __name__ == '__main__':
    main()
