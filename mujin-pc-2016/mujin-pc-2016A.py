def main():
    key = input()
    right_side = ["O", "P", "K", "L"]
    print("Right" if key in right_side else "Left")


if __name__ == '__main__':
    main()

