from math import pi


def main():
    length = sorted(list(map(int, input().split())), reverse=True)
    print((sum(length) ** 2 - max(0, length[0] - length[1] - length[2]) ** 2) * pi)


if __name__ == '__main__':
    main()

