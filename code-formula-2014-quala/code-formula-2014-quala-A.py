def main():
    number = int(input())
    answer = 0
    i = 1
    while i ** 3 <= number:
        if i ** 3 == number:
            answer = i
        i += 1
    print("YES" if answer else "NO")


if __name__ == '__main__':
    main()

