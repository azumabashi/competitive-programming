from collections import Counter
from string import ascii_uppercase as u_case


def solve():
    s1 = input()
    s2 = input()
    s3 = input()
    c_s1 = Counter(list(s1))
    c_s2 = Counter(list(s2))
    c_s3 = Counter(list(s3))
    min_char_1 = 0
    min_char_2 = 0
    for x in u_case:
        if c_s1[x] + c_s2[2] < c_s3[3]:
            return "NO"
    for x in u_case:
        min_char_1 += max(c_s3[x] - c_s2[x], 0)
        min_char_2 += max(c_s3[x] - c_s1[x], 0)
    return "NO" if min_char_1 > len(s1) // 2 or min_char_2 > len(s1) // 2 else "YES"


def main():
    print(solve())


if __name__ == '__main__':
    main()
