def main():
    days, target = map(int, input().split())
    steps = [int(input()) for _ in range(days)]
    for i in range(1, days):
        steps[i] += steps[i - 1]
    for i in range(days):
        if target <= steps[i]:
            print(i + 1)
            break


if __name__ == '__main__':
    main()

