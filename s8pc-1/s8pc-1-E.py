def main():
    n, q = map(int, input().split())
    a = [int(x) for x in input().split()]
    c = [0] + [int(x) - 1 for x in input().split()] + [0]
    answer = 0
    mod = 10 ** 9 + 7
    dist = [0 for _ in range(n)]
    for i in range(1, n):
        dist[i] += pow(a[i - 1], a[i], mod) + dist[i - 1]
    for i in range(1, q + 2):
        answer += abs(dist[c[i]] - dist[c[i - 1]])
        answer %= mod
    print(answer)


if __name__ == '__main__':
    main()

