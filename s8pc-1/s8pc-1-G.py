def main():
    n, m = map(int, input().split())
    inf = 1 << 60
    dist = [[inf for _ in range(n)] for _ in range(n)]
    lim = [[inf for _ in range(n)] for _ in range(n)]
    for _ in range(m):
        s, t, d, time = map(int, input().split())
        s -= 1
        t -= 1
        dist[s][t] = d
        dist[t][s] = d
        lim[s][t] = time
        lim[t][s] = time
    dp = [[[inf, 0] for _ in range(n)] for _ in range(1 << n)]
    dp[0][0][0] = 0
    dp[0][0][1] = 1
    for i in range(1 << n):
        for j in range(n):
            for k in range(n):
                if 1 & (i >> k) or (dp[i][j][0] + dist[j][k] > lim[j][k] and lim[j][k] < inf and dist[j][k] < inf):
                    continue
                idx = i + (1 << k)
                next_dist = dp[i][j][0] + dist[j][k]
                if dp[idx][k][0] > next_dist:
                    dp[idx][k][0] = next_dist
                    dp[idx][k][1] = dp[i][j][1]
                elif dp[idx][k][0] == next_dist:
                    dp[idx][k][1] += dp[i][j][1]
    if dp[(1 << n) - 1][0][0] < inf:
        print(*dp[(1 << n) - 1][0])
    else:
        print("IMPOSSIBLE")


if __name__ == '__main__':
    main()
