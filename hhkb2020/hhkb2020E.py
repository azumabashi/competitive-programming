def main():
    h, w = map(int, input().split())
    grid = [input() for _ in range(h)]
    left = [[0 for _ in range(w)] for _ in range(h)]
    right = [[0 for _ in range(w)] for _ in range(h)]
    top = [[0 for _ in range(w)] for _ in range(h)]
    bottom = [[0 for _ in range(w)] for _ in range(h)]
    cnt = 0

    for i in range(h):
        for j in range(w):
            if grid[i][j] == ".":
                cnt += 1
                left[i][j] = left[i][j - 1] + 1 if j > 0 else 1
                top[i][j] = top[i - 1][j] + 1 if i > 0 else 1
            if grid[-i - 1][-j - 1] == ".":
                right[-i - 1][-j - 1] = right[-i - 1][-j] + 1 if j > -w + 1 else 1
                bottom[-i - 1][-j - 1] = bottom[-i][-j - 1] + 1 if i > -h + 1 else 1
    mod = 10 ** 9 + 7
    power_2 = [1 for _ in range(h * w + 10)]
    for i in range(1, h * w + 10):
        power_2[i] = (power_2[i - 1] * 2) % mod
    ans = cnt * power_2[cnt]
    ans %= mod
    for i in range(h):
        for j in range(w):
            if grid[i][j] == "#":
                continue
            ans -= power_2[cnt - left[i][j] - right[i][j] - top[i][j] - bottom[i][j] + 3]
            ans %= mod
    print(ans)


if __name__ == '__main__':
    main()
