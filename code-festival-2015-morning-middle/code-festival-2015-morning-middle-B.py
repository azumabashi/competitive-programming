def lcs(s, t):
    dp = [[0 for _ in range(100)] for _ in range(100)]
    for i in range(len(s)):
        for j in range(len(t)):
            if s[i] == t[j]:
                d = dp[i][j] + 1
            else:
                d = 0
            dp[i + 1][j + 1] = max(d, dp[i + 1][j], dp[i][j + 1])
    return dp[len(s)][len(t)]


def main():
    n = int(input())
    s = input()
    ans = float("inf")
    for i in range(1, n):
        ans = min(ans, n - 2 * lcs(s[:i], s[i:]))
    print(ans if n > 1 else 1)


if __name__ == '__main__':
    main()
