def main():
    exams, evaluated_scores, max_score, pass_limit = map(int, input().split())
    scores = [int(input()) for _ in range(exams - 1)]
    scores.sort()
    answer = -1
    if evaluated_scores > 1:
        evaluated_score_sum = sum(scores[-evaluated_scores + 1:])
        required_score = pass_limit * evaluated_scores
        if evaluated_scores <= exams - 1 and required_score <= evaluated_score_sum + scores[-evaluated_scores]:
            answer = 0
        elif evaluated_scores == exams - 1 and required_score <= evaluated_scores:
            answer = 0
        elif required_score <= evaluated_score_sum + max_score:
            answer = max(required_score - evaluated_score_sum, 0)
    elif exams > 1:
        if pass_limit <= scores[-1]:
            answer = 0
        else:
            answer = pass_limit
    else:
        answer = pass_limit
    print(answer)


if __name__ == '__main__':
    main()

