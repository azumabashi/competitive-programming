def main():
    height, width = map(int, input().split())
    grid = [input().split() for _ in range(height)]
    answer = ""
    for i in range(height):
        if answer != "":
            break
        for j in range(width):
            if grid[i][j] == "snuke":
                answer = str(chr(ord("a") + j)).upper() + str(i + 1)
                break
    print(answer)


if __name__ == '__main__':
    main()

