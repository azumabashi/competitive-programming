def main():
    max_score = int(input())
    answer = []
    i = 1
    while i * i + i - 2 * max_score < 0:
        i += 1
    if i * i + i - 2 * max_score == 0:
        answer = [j for j in range(1, i + 1)]
    else:
        while max_score >= 0:
            answer.append(i)
            max_score -= i
            i -= 1
        answer.pop(-1)
        if i + max_score + 1 > 0:
            answer.append(i + max_score + 1)
    for a in answer:
        print(a)


if __name__ == '__main__':
    main()

