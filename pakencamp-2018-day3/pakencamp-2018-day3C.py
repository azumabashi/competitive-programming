from math import floor


def main():
    now_length = float(input())
    answer = 0
    for i in range(2, int(now_length)):
        now = i
        while now < now_length:
            now = floor(now * 1.5)
        if now == now_length:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

