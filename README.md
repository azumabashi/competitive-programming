# About
競技プログラミングのコンテスト参加時・過去問を解いたときに提出したプログラム．

主にPython3を利用していますが，C++やNimで解いたファイルも同じディレクトに入っている場合があります(言語別に分けるより，同じフォルダに入れておいた方がわかりやすいと考えたためです)．
  
なお，コンテスト参加時に作成したプログラムでは，問題で与えられた変数名をそのまま使っている場合があります． 
 
Since 2019-12-29

# フォルダ名について
基本は[AtCoder](https://atcoder.jp/home "AtCoder")のコンテストページのURL(`https://atcoder.jp/contests/hoge` の`hoge`)を採用しています．

ただし，
+ AtCoderの定期コンテスト(AtCoder *** Contest)は略称で示してある．
+ `yukicoder`は[yukicoder](https://yukicoder.me/ "yukicoder")の過去問を解いたときの提出コード
+ `asakatsu`，`yorukatsu`，`AGRC`，`tpc_virtual`は「バーチャルコンテスト」(主にAtCoderの過去問から数題選び，制限時間を設定して実戦形式で解くこと)参加時の提出コード
+ `AOJ`は，[Aizu Online Judge](http://judge.u-aizu.ac.jp/onlinejudge/index.jsp?lang=ja "Aizu Online Judge")に提出したファイル．
  +  Course内の問題は，`course-name_N_alph`という命名がされているとき，`AOJ/course-name/N_alpha.foo`と命名してあります(ここで`foo`は任意の拡張子です)．

# Author
[Twitter](https://twitter.com/azm_bashi "Twitter")

[AtCoder Userpage](https://atcoder.jp/users/Azumabashi "AtCoder Userpage")