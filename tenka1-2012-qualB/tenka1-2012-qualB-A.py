def main():
    a, b, c = map(int, input().split())
    answer = []
    for i in range(1, 128):
        if i % 3 == a and i % 5 == b and i % 7 == c:
            answer.append(i)
    answer.sort()
    print("\n".join(map(str, answer)))


if __name__ == '__main__':
    main()

