def main():
    target = input()
    number = []
    for t in target:
        number.append(int(t))
    number = list(reversed(number))
    print(sum(number[1::2]), sum(number[0::2]))


if __name__ == '__main__':
    main()

