def main():
    word = input().split()
    filtered_num = int(input())
    filtered_word = [input() for _ in range(filtered_num)]
    for i, w in enumerate(word):
        for f in filtered_word:
            if len(w) != len(f):
                continue
            else:
                count = 0
                length = len(w)
                for j in range(length):
                    if w[j] == f[j] or (f[j] == "*" and w[j] != " "):
                        count += 1
                if length == count:
                    word[i] = "*" * length
    print(" ".join(word))


if __name__ == '__main__':
    main()

