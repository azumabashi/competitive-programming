from string import ascii_lowercase as l_case


def solve(password):
    if len(password) == 1:
        if password == ["a"]:
            return "NO"
        elif password[0] in l_case[1:20]:
            return "a" * (l_case.index(password[0]) + 1)
        else:
            return "a" * 19 + l_case[l_case.index(password[0]) - 19]
    for i in range(len(password) - 1):
        if password[i] != password[i + 1]:
            password[i], password[i + 1] = password[i + 1], password[i]
            return "".join(password)
    else:
        if password[0] == "z":
            password = "".join(password)
            return password[:-1] + "ya" if len(password) < 20 else "NO"
        hash_func = {l_case[i]: i + 1 for i in range(26)}
        now_hash = sum([hash_func[p] for p in password])
        ans = ["z" for _ in range(now_hash // 26)]
        now_hash %= 26
        ans.append(l_case[now_hash - 1])
        return "".join(ans)


def main():
    password = list(input())
    print(solve(password))


if __name__ == '__main__':
    main()
