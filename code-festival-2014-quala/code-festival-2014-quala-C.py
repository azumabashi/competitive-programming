def main():
    arrive, departure = map(int, input().split())
    arrive -= 1
    answer = departure // 4 - arrive // 4
    answer -= departure // 100 - arrive // 100
    answer += departure // 400 - arrive // 400
    print(answer)


if __name__ == '__main__':
    main()

