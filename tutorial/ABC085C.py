def main():
    otoshidama = input("").split(" ")
    number = int(otoshidama[0])
    sum = int(otoshidama[1])
    yukichi = 0
    natsume = 0
    higuchi = 0
    result = ""
    for i in range(0, round(sum / 10000) + 1):
        if not (result == "" or result == "-1 -1 -1"):
            break
        nokori = sum - 10000 * i
        if nokori >= 0:
            for k in range(0, round(nokori / 5000) + 1):
                amari = nokori - 5000 * k
                if amari >= 0 and round(i + k + (amari / 1000)) == number:
                    result = str(i) + " " + str(k) + " " + str(round(amari / 1000))
                    break
                else:
                    result = "-1 -1 -1"
                    continue
        else:
            continue
    return result


if __name__ == '__main__':
    print(main())

