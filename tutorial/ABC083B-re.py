import math

def getDigitSum(number):
    # 解答のgetDigitSum関数の別解．
    digitSum = 0
    while number > 0:
        digitSum += number % 10
        number = math.floor(number/10)
    return digitSum


def main():
    numbers = input("").split(" ")
    result = 0
    for i in range(0, int(numbers[0]) + 1):
        digitSum = getDigitSum(i)
        if int(numbers[1]) <= digitSum <= int(numbers[2]):
            result += i
    print(result)
        


if __name__ == "__main__":
    main()
