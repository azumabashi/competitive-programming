def main():
    mochiNumber = int(input(""))
    dinometer = []
    for i in range(0, mochiNumber):
        dinometer.append(int(input("")))

    height = 0
    maximum = max(dinometer) + 1
    while maximum >= max(dinometer):

        if max(dinometer) == 0:
            break
        elif maximum > max(dinometer):
            maximum = max(dinometer)
            dinometer[dinometer.index(max(dinometer))] = 0
            height += 1
        else:
            dinometer[dinometer.index(max(dinometer))] = 0

    print(height)


if __name__ == '__main__':
    main()

