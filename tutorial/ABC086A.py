def main():
    integer = input("").split(" ")
    if int(integer[0]) * int(integer[1]) % 2 == 0:
        print("Even")
    else:
        print("Odd")


if __name__ == '__main__':
    main()

