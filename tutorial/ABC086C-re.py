def isTravelPlanOK(dt,dx,dy):
    return ((dx + dy) <= dt) and ((dx + dy - dt) % 2 == 0)


def main():
    N = int(input())
    time = []
    x = []
    y = []
    time.append(0)
    x.append(0)
    y.append(0)
    result = True
    for i in range(N):
        coodinate = input().split(" ")
        time.append(int(coodinate[0]))
        x.append(int(coodinate[1]))
        y.append(int(coodinate[2]))
    for j in range(0, max(1,N-1)):
        result = isTravelPlanOK(time[j + 1] - time[j], x[j+1]-x[j],y[j+1]-y[j])
        if result == False:
            print("No")
            break
    if result:
        print("Yes")


if __name__ == "__main__":
    main()
