def main():
    A = int(input(""))
    B = int(input(""))
    C = int(input(""))
    X = int(input("")) # input関数の返り値はStringであることに注意．
    possibleWay = 0
    for i in range(0, A + 1):
        for j in range(0, B + 1):
            rest = X - 500 * i - 100 * j
            if 0 <= rest <= 50 * C:
                # restが50の倍数であることは明らかより，rest % 50 == 0は意味がない．
                # A+B+Cが上から抑えられていればその判定も必要だが，本問では不要．
                # 0 <= restは追加．500 * A - 100 * B > Xでは意味がない．
                possibleWay += 1
    print(possibleWay) # 参考：print関数はデフォルトで\nが入る．


if __name__ == "__main__":
    main()
