def main():
    goal = input("")
    words = ["dream", "dreamer", "erase", "eraser"]
    result = ""
    while result == "":
        for word in words:
            if goal[:len(word)] == word:
                goal = goal[len(word):]
                print(goal)
                continue
            if goal == "":
                result = "YES"
                break
            elif not goal[:2] == "dr" or not goal[:2] == "er":
                result = "NO"
                break
    print(result)


if __name__ == '__main__':
    main()

