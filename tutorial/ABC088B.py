def main():
    allNumber = int(input(""))
    nString = input("").split(" ")
    number = []
    alice = 0
    bob = 0
    for i in nString:
        number.append(int(i))
    for j in range(0, allNumber):
        if j % 2 == 0:
            alice += max(number)
        else:
            bob += max(number)
        number[number.index(max(number))] = 0
    print(alice - bob)


if __name__ == '__main__':
    main()

