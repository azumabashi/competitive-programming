def main():
    integerNum = int(input(""))
    integers = input("").split(" ")
    operation = 0
    flag = 0
    while flag == 0:
        for i in range(0, integerNum):
            if int(integers[i]) % 2 == 1:
                flag = 1
                break
            else:
                integers[i] = str(round(int(integers[i]) / 2))
        if flag == 0:
            operation += 1
    return operation


if __name__ == '__main__':
    print(main())

