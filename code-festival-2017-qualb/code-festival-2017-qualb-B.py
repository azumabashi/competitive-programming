def main():
    questions = int(input())
    difficulty = sorted(list(map(int, input().split())))
    problems = int(input())
    problem_set = sorted(list(map(int, input().split())))
    d_index = 0
    count = 0
    for i in range(problems):
        while d_index < questions and difficulty[d_index] < problem_set[i]:
            d_index += 1
        if difficulty[d_index] == problem_set[i]:
            count += 1
            d_index += 1
        else:
            break
    print("YES" if count == problems else "NO")


if __name__ == '__main__':
    main()

