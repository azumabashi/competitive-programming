def main():
    n = int(input())
    s = input()
    t = [input() for _ in range(n)]
    dp = [0 for _ in range(1010)]
    # dp[i] := S[:i]をTで埋める方法(mod 1e9+7)
    dp[0] = 1
    mod = 10 ** 9 + 7
    for i in range(len(s) + 1):
        for j in range(n):
            if i < len(t[j]):
                continue
            if s[i - len(t[j]):i] == t[j]:
                dp[i] += dp[i - len(t[j])]
                dp[i] %= mod
    dp[len(s)] %= mod
    print(dp[len(s)])


if __name__ == '__main__':
    main()
