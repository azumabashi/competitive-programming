def main():
    winning_num = input().split()
    bonus_num = input()
    bought_num = input().split()
    answer = 0
    count_same = 0
    can_get_bonus = False
    for i in range(6):
        if bought_num[i] in winning_num:
            count_same += 1
        elif bought_num[i] == bonus_num:
            can_get_bonus = True
    if count_same == 6:
        answer = 1
    elif count_same == 5 and can_get_bonus:
        answer = 2
    elif count_same == 5:
        answer = 3
    elif count_same == 4:
        answer = 4
    elif count_same == 3:
        answer = 5
    print(answer)


if __name__ == '__main__':
    main()

