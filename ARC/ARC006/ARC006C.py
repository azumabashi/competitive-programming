def main():
    boxes = int(input())
    top_weight = []
    for _ in range(boxes):
        weight = int(input())
        for i in range(len(top_weight)):
            if weight <= top_weight[i]:
                top_weight[i] = weight
                break
        else:
            top_weight.append(weight)
        top_weight.sort()
    print(len(top_weight))


if __name__ == '__main__':
    main()

