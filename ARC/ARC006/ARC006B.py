def main():
    width, length = map(int, input().split())
    lot = [("| " * width)[:-1]] + [input() for _ in range(length + 1)]
    line = lot[-1].index("o")
    for i in range(-2, -length - 2, -1):
        if line > 1 and lot[i][line - 1] == "-":
            line -= 2
        elif line < 2 * width - 2 and lot[i][line + 1] == "-":
            line += 2
    print(line // 2 + 1)


if __name__ == '__main__':
    main()

