def main():
    n = int(input())
    number = n * (n + 1) // 2
    answer = True
    if n == 1:
        answer = False
    i = 2
    while i * i <= number:
        if not number % i:
            answer = False
            break
        i += 1
    print("WANWAN" if answer else "BOWWOW")


if __name__ == '__main__':
    main()

