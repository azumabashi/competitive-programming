def main():
    id_str = input()
    num_list = [str(i) for i in range(10)]
    for i in range(len(id_str)):
        if id_str[i] in num_list:
            answer = id_str[i]
            if i < len(id_str) - 1:
                if id_str[i + 1] in num_list:
                    answer += id_str[i + 1]
            break
    print(answer)


if __name__ == '__main__':
    main()

