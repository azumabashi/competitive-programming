from math import pi


def main():
    n, q = map(int, input().split())
    volume = [0 for _ in range(2 * 10 ** 4 + 1)]
    # volume[i] := 区間[0, i]にある円錐の体積．

    def find_vol(x, d, h):
        return x ** 2 * pow(d, 3) / pow(h, 2)

    for _ in range(n):
        max_x = 0
        x, r, h = map(int, input().split())
        max_val = find_vol(r, h, h)
        for d in range(1, h + 1):
            volume[x + h - d] += max_val - find_vol(r, d, h)
            max_x = max(max_x, x + h - d)
        for i in range(max_x + 1, 2 * 10 ** 4 + 1):
            volume[i] += max_val
    for _ in range(q):
        a, b = map(int, input().split())
        ans = volume[b]
        ans -= volume[a]
        print(ans * pi / 3 if a != b else 0)


if __name__ == '__main__':
    main()
