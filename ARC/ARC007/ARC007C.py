def main():
    c = input()
    n = len(c)
    ans = 100
    for i in range(1 << n):
        res = [0] * n
        cnt = 0
        for j in range(n):
            if (i >> j) & 1:
                cnt += 1
                for k in range(n):
                    if c[k] == "o":
                        res[(j + k) % n] = 1
        if all(r > 0 for r in res):
            ans = min(ans, cnt)
    print(ans)


if __name__ == '__main__':
    main()
