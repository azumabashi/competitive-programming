def main():
    case, listened_num = map(int, input().split())
    listened_cd = [int(input()) for _ in range(listened_num)]
    cd_in_case = [i for i in range(case + 1)]
    for l in listened_cd:
        tmp = cd_in_case[0]
        cd_in_case[cd_in_case.index(l)] = tmp
        cd_in_case[0] = l
    for cd in cd_in_case[1:]:
        print(cd)


if __name__ == '__main__':
    main()

