def solve(n, m):
    if n == 1 and m == 0:
        return [[1, 2]]
    if m < 0 or n < m + 2:
        return [[-1]]
    res = []
    for i in range(1, m + 2):
        res.append([2 * i, 2 * i + 1])
    res.append([1, 2 * m + 4])
    for i in range(n - m - 2):
        res.append([2 * (m + i + 3), 2 * (m + i + 3) + 1])
    return res


def main():
    n, m = map(int, input().split())
    ans = solve(n, m)
    for a in ans:
        print(*a)


if __name__ == '__main__':
    main()
