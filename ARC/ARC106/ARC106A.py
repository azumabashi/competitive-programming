def solve(n):
    three = [1]
    five = [1]
    while three[-1] * 3 <= n:
        three.append(three[-1] * 3)
    while five[-1] * 5 <= n:
        five.append(five[-1] * 5)
    for i in range(1, len(three)):
        for j in range(1, len(five)):
            if three[i] + five[j] == n:
                return i, j
    return -1, -1


def main():
    n = int(input())
    x, y = solve(n)
    if x == -1:
        print(-1)
    else:
        print(x, y)


if __name__ == '__main__':
    main()
