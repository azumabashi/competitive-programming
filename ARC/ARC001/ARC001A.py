def main():
    question_num = int(input())
    exam_answer = input()
    count_answer = {"1": 0, "2": 0, "3": 0, "4": 0}
    for a in exam_answer:
        count_answer[a] += 1
    answer = sorted(count_answer.values())
    print(answer[3], answer[0])


if __name__ == '__main__':
    main()

