def main():
    dates = int(input())
    height = [int(input()) for _ in range(dates)]
    answer = []
    is_increasing = True
    before_height = 0
    before_index = 1
    for i in range(dates):
        if is_increasing and height[i] < before_height:
            is_increasing = False
        if not is_increasing and height[i] > before_height:
            is_increasing = True
            answer.append(i - before_index + 1)
            before_index = i
        before_height = height[i]
    answer.append(dates - before_index + 1)
    print(max(answer))


if __name__ == '__main__':
    main()

