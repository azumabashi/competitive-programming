def main():
    days, limit = map(int, input().split())
    sleep = [int(input()) for _ in range(days)]
    answer = -1
    for i in range(days - 2):
        if sum(sleep[i:i + 3]) < limit:
            answer = i + 3
            break
    print(answer)


if __name__ == '__main__':
    main()

