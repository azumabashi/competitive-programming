def main():
    move, takahashi_speed, turtle_speed, distance = map(int, input().split())
    for _ in range(move):
        distance = turtle_speed * (distance / takahashi_speed)
    print(distance)


if __name__ == '__main__':
    main()

