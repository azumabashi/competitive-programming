def main():
    score_num, target_average, target_range = map(int, input().split())
    score_max = -float("inf")
    score_min = float("inf")
    score_sum = 0
    for _ in range(score_num):
        s = int(input())
        score_sum += s
        score_max = max(score_max, s)
        score_min = min(score_min, s)
    if score_max == score_min:
        print(-1)
    else:
        coefficient = target_range / (score_max - score_min)
        constant = target_average - score_sum * coefficient / score_num
        print(coefficient, constant)


if __name__ == '__main__':
    main()

