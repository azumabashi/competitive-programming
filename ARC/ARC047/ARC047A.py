def main():
    operation_num, tab_limit = map(int, input().split())
    operation = input()
    tabs = 1
    answer = 0
    for op in operation:
        if op == "+":
            tabs += 1
        else:
            tabs -= 1
        if tabs > tab_limit:
            tabs = 1
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

