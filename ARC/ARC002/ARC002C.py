from itertools import product


def main():
    length = int(input())
    command = input()
    button_list = ["A", "B", "X", "Y"]
    answer = float("inf")
    for short_cut in product(button_list, repeat=4):
        new_command = ["".join(short_cut[:2]), "".join(short_cut[2:])]
        now_cmd = command
        answer = min(answer, len(now_cmd.replace(new_command[0], "Z").replace(new_command[1], "Z")))
        now_cmd = command
        answer = min(answer, len(now_cmd.replace(new_command[1], "Z").replace(new_command[0], "Z")))
    print(answer)


if __name__ == '__main__':
    main()

