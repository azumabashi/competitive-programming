from datetime import timedelta, datetime


def main():
    date = input()
    now = list(map(int, date.split("/")))
    if not now[0] % (now[1] * now[2]) == 0:
        day = datetime.strptime(date, "%Y/%m/%d")
        while now[0] % (now[1] * now[2]):
            day += timedelta(days=1)
            now = list(map(int, day.strftime("%Y %m %d").split()))
    print("/".join(map(lambda x: str(x).zfill(2), now)))


if __name__ == '__main__':
    main()

