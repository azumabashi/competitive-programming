def main():
    year = int(input())
    print("YES" if year % 400 == 0 or (not year % 100 == 0 and year % 4 == 0) else "NO")


if __name__ == '__main__':
    main()

