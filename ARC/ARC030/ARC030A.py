def main():
    edge = int(input())
    target = int(input())
    print("YES" if edge >= target * 2 else "NO")


if __name__ == '__main__':
    main()

