from collections import deque


def main():
    vertex, start = map(int, input().split())
    jewel = list(map(int, input().split()))
    graph = [[] for _ in range(vertex)]
    start -= 1
    for _ in range(vertex - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        graph[a].append(b)
        graph[b].append(a)
    answer = 0

    def bfs(s):
        dist = [-1 for _ in range(vertex)]
        q = deque([start])
        dist[s] = 0
        while q:
            now = q.popleft()
            for next in graph[now]:
                if next == s:
                    continue
                elif dist[next] < 0:
                    dist[next] = dist[now] + 1
                    q.append(next)
        return any(dist[now_i] < 0 and jewel[now_i] == 1 for now_i in range(vertex))

    for i in range(vertex):
        if i == start:
            continue
        elif jewel[i] or bfs(i):
            answer += 2
    print(answer)


if __name__ == '__main__':
    main()

