def main():
    desert_gold = list(map(int, input().split()))
    jungle_gold = list(map(int, input().split()))
    answer = 0
    for i in range(7):
        answer += max(desert_gold[i], jungle_gold[i])
    print(answer)


if __name__ == '__main__':
    main()

