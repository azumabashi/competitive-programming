def two_dim_accumulate(val, h, w, arr=None):
    if arr is None:
        arr = [[0 for _ in range(w + 1)] for _ in range(h + 1)]
    for i in range(h):
        for j in range(w):
            arr[i + 1][j + 1] = arr[i][j + 1] + arr[i + 1][j] - arr[i][j] + val[i][j]
    return arr


def two_dim_acc_range_sum(px, py, qx, qy, two_dim_acc):
    return two_dim_acc[qx][qy] - two_dim_acc[px][qy] - two_dim_acc[qx][py] + two_dim_acc[px][py]


def main():
    h, w = map(int, input().split())
    grid = [list(map(int, input().split())) for _ in range(h)]
    white = [[0 for _ in range(w)] for _ in range(h)]
    black = [[0 for _ in range(w)] for _ in range(h)]
    for i in range(h):
        for j in range(w):
            if i % 2 == j % 2:
                black[i][j] += grid[i][j]
            else:
                white[i][j] += grid[i][j]
    white_sum = two_dim_accumulate(white, h, w)
    black_sum = two_dim_accumulate(black, h, w)
    ans = 0
    for px in range(h + 1):
        for py in range(w + 1):
            for qx in range(px + 1, h + 1):
                for qy in range(py + 1, w + 1):
                    if two_dim_acc_range_sum(px, py, qx, qy, white_sum) == two_dim_acc_range_sum(px, py, qx, qy, black_sum):
                        ans = max(ans, (qx - px) * (qy - py))
    print(ans)


if __name__ == '__main__':
    main()
