def main():
    input_pencil, output_pencil, initial_pencil = map(int, input().split())
    answer = initial_pencil
    recycle_num = initial_pencil
    while recycle_num >= input_pencil:
        new_pencil = (recycle_num // input_pencil) * output_pencil
        answer += new_pencil
        recycle_num = recycle_num % input_pencil + new_pencil
    print(answer)


if __name__ == '__main__':
    main()

