def main():
    word_num = int(input())
    sentence = input().split()
    convert_result = ["" for _ in range(word_num)]
    correspondence = {"b": "1", "c": "1", "d": "2", "w": "2", "t": "3", "j": "3", "f": "4", "q": "4",
                      "l": "5", "v": "5", "s": "6", "x": "6", "p": "7", "m": "7", "h": "8", "k": "8",
                      "n": "9", "g": "9", "z": "0", "r": "0"}
    for i in range(word_num):
        for w in sentence[i].lower():
            if w in correspondence:
                convert_result[i] += correspondence[w]
    answer = []
    for c in convert_result:
        if c != "":
            answer.append(c)
    print(" ".join(answer))


if __name__ == '__main__':
    main()

