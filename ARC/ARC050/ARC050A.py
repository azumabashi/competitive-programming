def main():
    upper_case, lower_case = input().split()
    print("Yes" if upper_case.lower() == lower_case else "No")


if __name__ == '__main__':
    main()

