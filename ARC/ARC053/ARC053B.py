def main():
    string = input()
    str_count = {}
    for s in string:
        if s in str_count:
            str_count[s] += 1
        else:
            str_count[s] = 1
    odd_count = 0
    for c in list(str_count.values()):
        if c % 2:
            odd_count += 1
    answer = len(string)
    if odd_count > 0:
        answer = 2 * ((len(string) - odd_count) // (2 * odd_count)) + 1
    return answer


if __name__ == '__main__':
    main()

