def main():
    height, width = map(int, input().split())
    print((width - 1) * height + (height - 1) * width)


if __name__ == '__main__':
    main()

