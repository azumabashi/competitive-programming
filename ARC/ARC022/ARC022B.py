def main():
    n = int(input())
    a = list(map(int, input().split()))
    ans = 0
    right = 0
    cnt = set()
    for left in range(n):
        while right < n and a[right] not in cnt:
            cnt.add(a[right])
            right += 1
        ans = max(ans, right - left)
        if left == right:
            right += 1
        else:
            cnt.remove(a[left])
    print(ans)


if __name__ == '__main__':
    main()
