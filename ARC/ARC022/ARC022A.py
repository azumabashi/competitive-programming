def main():
    string = input().upper()
    answer = 0
    ok = {"I": 0, "C": 1, "T": 2}
    for s in string:
        if s in ok and ok[s] == answer:
            answer += 1
    print("YES" if answer == 3 else "NO")


if __name__ == '__main__':
    main()

