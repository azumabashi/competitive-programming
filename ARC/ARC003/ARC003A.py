def main():
    course_num = int(input())
    grade = input()
    score = {"A": 4, "B": 3, "C": 2, "D": 1, "F": 0}
    answer = 0
    for g in grade:
        answer += score[g]
    print(answer / course_num)


if __name__ == '__main__':
    main()

