def main():
    word_num = int(input())
    words = [input()[::-1] for _ in range(word_num)]
    words.sort()
    for w in words:
        print(w[::-1])


if __name__ == '__main__':
    main()

