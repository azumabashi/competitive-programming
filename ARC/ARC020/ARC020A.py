def main():
    ant_stone, bug_stone = map(lambda x: abs(int(x)), input().split())
    answer = "Draw"
    if ant_stone > bug_stone:
        answer = "Bug"
    elif ant_stone < bug_stone:
        answer = "Ant"
    print(answer)


if __name__ == '__main__':
    main()

