from itertools import permutations


def main():
    paper, cost = map(int, input().split())
    colors = [int(input()) for _ in range(paper)]
    left = [i for i in range(1, 11)]
    answer = float("inf")
    for now_left in permutations(left, 2):
        change_time = 0
        for i in range(paper):
            if colors[i] != now_left[i % 2]:
                change_time += 1
        answer = min(answer, change_time * cost)
    print(answer)


if __name__ == '__main__':
    main()

