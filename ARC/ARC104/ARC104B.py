def main():
    n, s = input().split()
    n = int(n)
    a = [0] * n
    t = [0] * n
    c = [0] * n
    g = [0] * n
    for i in range(n):
        if s[i] == "A":
            a[i] += 1
        elif s[i] == "T":
            t[i] += 1
        elif s[i] == "C":
            c[i] += 1
        elif s[i] == "G":
            g[i] += 1
        if i > 0:
            a[i] += a[i - 1]
            t[i] += t[i - 1]
            c[i] += c[i - 1]
            g[i] += g[i - 1]
    a = [0] + a
    t = [0] + t
    c = [0] + c
    g = [0] + g
    n += 1
    ans = 0
    for i in range(n):
        for j in range(i + 1, n):
            if a[j] - a[i] == t[j] - t[i] and c[j] - c[i] == g[j] - g[i]:
                ans += 1
    print(ans)


if __name__ == '__main__':
    main()
