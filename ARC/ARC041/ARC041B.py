def main():
    height, width = map(int, input().split())
    now_amoeba = [list(map(int, list(input()))) for _ in range(height)]
    answer = [[0 for _ in range(width)] for _ in range(height)]
    for i in range(height - 1):
        for j in range(width - 1):
            if now_amoeba[i][j] > 0:
                now_amoeba[i + 1][j + 1] -= now_amoeba[i][j]
                now_amoeba[i + 1][j - 1] -= now_amoeba[i][j]
                now_amoeba[i + 2][j] -= now_amoeba[i][j]
                answer[i + 1][j] += now_amoeba[i][j]
                now_amoeba[i][j] = 0
    for i in range(height):
        print("".join(map(str, answer[i])))


if __name__ == '__main__':
    main()

