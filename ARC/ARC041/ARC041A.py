def main():
    face, back, = map(int, input().split())
    reverse = int(input())
    answer = face + reverse
    if face + back == reverse:
        answer = back
    elif back < reverse:
        reverse -= back
        answer = back + face - reverse
    print(answer)


if __name__ == '__main__':
    main()

