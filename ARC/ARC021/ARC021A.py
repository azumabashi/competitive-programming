def main():
    grids = [list(input().split()) for _ in range(4)]
    answer = False
    for i in range(4):
        if answer:
            break
        for j in range(3):
            if grids[i][j] == grids[i][j + 1]:
                answer = True
    for i in range(3):
        if answer:
            break
        for j in range(4):
            if grids[i + 1][j] == grids[i][j]:
                answer = True
    print("CONTINUE" if answer else "GAMEOVER")


if __name__ == '__main__':
    main()

