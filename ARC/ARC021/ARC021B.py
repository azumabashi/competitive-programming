def solve():
    n = int(input())
    ans = [0]
    for i in range(n - 1):
        b = int(input())
        ans.append(ans[-1] ^ b)
    b = int(input())
    if ans[0] ^ ans[-1] == b:
        return "\n".join(map(str, ans))
    else:
        return "-1"


def main():
    print(solve())


if __name__ == '__main__':
    main()
