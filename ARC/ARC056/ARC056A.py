def main():
    cost, set_cost, target, set_orange = map(int, input().split())
    answer = 0
    if cost <= set_cost / set_orange:
        answer = target * cost
    else:
        set_num = target // set_orange
        answer = set_cost * set_num
        if cost * (target - set_num * set_orange) <= set_cost:
            answer += cost * (target - set_num * set_orange)
        else:
            answer += set_cost
    print(answer)


if __name__ == '__main__':
    main()

