def main():
    n = int(input())
    i = 1
    divisor = 0
    while i * i <= n:
        if n % i == 0:
            divisor += i
            if i * i != n:
                divisor += n // i
        i += 1
    if divisor == 2 * n:
        print("Perfect")
    elif divisor > 2 * n:
        print("Abundant")
    else:
        print("Deficient")


if __name__ == '__main__':
    main()

