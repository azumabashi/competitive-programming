def main():
    questions, normal, dynamic = map(int, input().split())
    print(normal * max(0, questions - 5) + dynamic * min(questions, 5))


if __name__ == '__main__':
    main()

