def main():
    height = int(input())
    music = []
    answer = 0
    for _ in range(height):
        m = input()
        answer += m.count("x")
        music.append(list(m))
    for j in range(9):
        former = ""
        for i in range(height):
            if not former == music[i][j]:
                if not former == "o" and music[i][j] == "o":
                    answer += 1
            former = music[i][j]
    print(answer)


if __name__ == '__main__':
    main()

