def main():
    choice, answer = map(int, input().split())
    print(2 if answer == 1 else 1)


if __name__ == '__main__':
    main()

