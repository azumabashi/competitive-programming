def is_area_integer(a, b, c):
    """find whether the area of an triangle formed by 3 points a, b, c is integer or not."""
    area = abs((b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0]))
    return area % 2 == 0 and area > 0


def main():
    n = int(input())
    points = [[int(x) for x in input().split()] for _ in range(n)]
    answer = 0
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                if is_area_integer(points[i], points[j], points[k]):
                    answer += 1
    print(answer)


if __name__ == '__main__':
    main()

