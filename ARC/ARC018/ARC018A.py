def main():
    height, bmi = map(float, input().split())
    height /= 100
    print(bmi * height * height)


if __name__ == '__main__':
    main()

