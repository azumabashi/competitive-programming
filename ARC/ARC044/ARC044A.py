def main():
    number = int(input())
    i = 2
    is_prime = True
    while i * i <= number:
        if not number % i:
            is_prime = False
            break
        else:
            i += 1
    if not is_prime:
        if (number % 10) % 2 and not (number % 10) == 5 and number % 3:
            is_prime = True
    print("Prime" if is_prime and not number == 1 else "Not Prime")


if __name__ == '__main__':
    main()

