def main():
    length, road_speed, walk_speed, start, goal = map(int, input().split())
    answer = 0
    if goal > start:
        answer = (goal - start) / (road_speed + walk_speed)
        if walk_speed > road_speed:
            answer = min(answer, (length - goal + start) / (walk_speed - road_speed))
    else:
        answer = (goal + length - start) / (road_speed + walk_speed)
        if road_speed < walk_speed:
            answer = min(answer, (start - goal) / (walk_speed - road_speed))
    print(answer)


if __name__ == '__main__':
    main()

