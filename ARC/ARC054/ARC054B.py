from math import log, exp


def required_time(p, x):
    return x + p * pow(2, -x / 1.5)


def derivative_required_time(p, x):
    return 1 - (log(2) / 1.5) * p * exp(x * -log(2) / 1.5)


def main():
    initial_time = float(input())
    left = -100
    right = 100
    eps = 0.000000001
    answer = 0
    derivative = 10 ** 18
    while abs(derivative) > eps:
        mid = (left + right) / 2
        answer = mid
        derivative = derivative_required_time(initial_time, mid)
        if derivative > 0:
            right = mid
        else:
            left = mid
    print(required_time(initial_time, answer) if answer >= 0 else initial_time)


if __name__ == '__main__':
    main()

