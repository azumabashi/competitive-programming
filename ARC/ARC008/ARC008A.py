from math import ceil


def main():
    target = int(input())
    print(min(100 * (target // 10) + 15 * (target % 10), 100 * ceil(target / 10)))


if __name__ == '__main__':
    main()

