from string import ascii_uppercase
from copy import deepcopy
from math import ceil


def main():
    name_length, brock_length = map(int, input().split())
    name = input()
    kit_string = input()
    count_name = {}
    for s in ascii_uppercase:
        count_name[s] = 0
    count_kit = deepcopy(count_name)
    for n in name:
        count_name[n] += 1
    for k in kit_string:
        count_kit[k] += 1
    answer = 0
    for s in ascii_uppercase:
        if count_name[s] > 0 and count_kit[s] > 0:
            answer = max(answer, ceil(count_name[s] / count_kit[s]))
        elif count_name[s] > 0 and count_kit[s] == 0:
            answer = -1
            break
    print(answer)


if __name__ == '__main__':
    main()

