def main():
    a = list(input())
    length = len(a)
    answer = 0
    diff = 0
    for i in range(length // 2):
        if a[i] != a[-i - 1]:
            diff += 1
    if diff == 0:
        answer = 25 * (length - length % 2)
    elif diff == 1:
        answer = 25 * (length - 2) + 24 * 2
    else:
        answer = 25 * length
    print(answer)


if __name__ == '__main__':
    main()

