def main():
    meat = int(input())
    times = [int(input()) for _ in range(meat)]
    answer = float("inf")
    for i in range(2 ** meat):
        grill_time = [0, 0]
        i = bin(i)[2:].zfill(meat)
        for j in range(len(i)):
            grill_time[int(i[j])] += times[j]
        answer = min(answer, max(grill_time))
    print(answer)


if __name__ == '__main__':
    main()


