def main():
    target = int(input())
    answer = 1
    index = 0
    while True:
        if len(set(list(str(answer)))) == 1:
            index += 1
        if index == target:
            print(answer)
            break
        else:
            answer += 1


if __name__ == '__main__':
    main()

