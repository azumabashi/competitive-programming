def main():
    n = int(input())
    a, b = map(int, input().split())
    if n <= a:
        print("Takahashi")
    elif a == b:
        print("Takahashi" if n % (a + 1) else "Aoki")
    elif a > b:
        print("Takahashi")
    else:
        print("Aoki")


if __name__ == '__main__':
    main()
