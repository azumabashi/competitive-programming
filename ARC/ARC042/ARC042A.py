def main():
    threads, contributions = map(int, input().split())
    contribution = [input() for _ in range(contributions)]
    is_contributed = [False] * threads
    for i in range(-1, -contributions - 1, -1):
        if not is_contributed[int(contribution[i]) - 1]:
            print(contribution[i])
            is_contributed[int(contribution[i]) - 1] = True
    for i in range(threads):
        if not is_contributed[i]:
            print(i + 1)


if __name__ == '__main__':
    main()

