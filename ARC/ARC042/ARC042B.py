from math import sqrt


def distance(x1, y1, x2, y2, from_x, from_y):
    return abs((y2 - y1) * from_x - (x2 - x1) * from_y + x2 * y1 - y2 * x1) / sqrt((y2 - y1) ** 2 + (x2 - x1) ** 2)


def main():
    start_x, start_y = map(int, input().split())
    vertices = int(input())
    x_coordinate = [0 for _ in range(vertices)]
    y_coordinate = [0 for _ in range(vertices)]
    for i in range(vertices):
        x_coordinate[i], y_coordinate[i] = map(int, input().split())
    answer = float("inf")
    x_coordinate.append(x_coordinate[0])
    y_coordinate.append(y_coordinate[0])
    for i in range(vertices):
        dist = distance(x_coordinate[i], y_coordinate[i], x_coordinate[i + 1], y_coordinate[i + 1], start_x, start_y)
        answer = min(answer, dist)
    print(answer)


if __name__ == '__main__':
    main()

