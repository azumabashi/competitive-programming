def main():
    test_takers = int(input())
    answer = -float("inf")
    for _ in range(test_takers):
        score = list(map(int, input().split()))
        answer = max(answer, sum(score[:-1]) + score[-1] * 110 / 900)
    print(answer)


if __name__ == '__main__':
    main()

