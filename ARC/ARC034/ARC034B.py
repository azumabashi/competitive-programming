def get_digit_sum(n):
    result = 0
    while n > 0:
        result += n % 10
        n //= 10
    return result


def main():
    n = int(input())
    digit = 1
    answer = set()
    while pow(10, digit - 1) + 1 <= n:  # <= pow(10, digit) - 1 + 9 * digit:
        for d in range(1, 9 * digit + 1):
            now = n - d
            if get_digit_sum(now) == d:
                answer.add(now)
        digit += 1
    print(len(answer))
    answer = list(answer)
    answer.sort()
    for ans in answer:
        print(ans)


if __name__ == '__main__':
    main()

