def main():
    money_has, coefficient = map(int, input().split())
    target = 2 * 10 ** 12
    answer = 0
    if coefficient == 0:
        answer = target - money_has
    else:
        while money_has < target:
            answer += 1
            money_has += 1 + money_has * coefficient
    print(answer)


if __name__ == '__main__':
    main()

