def main():
    recursion_time = int(input())
    answer = [0] * (recursion_time + 2)
    answer[1] = 1
    answer[2] = 1
    for i in range(3, recursion_time + 2):
        answer[i] = answer[i - 1] + answer[i - 2]
    print(answer[recursion_time], answer[recursion_time + 1])


if __name__ == '__main__':
    main()

