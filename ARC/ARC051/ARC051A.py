def main():
    center_x, center_y, radius = map(int, input().split())
    x_lower, y_lower, x_upper, y_upper = map(int, input().split())

    def dist_from_center(x, y):
        return ((center_x - x) ** 2 + (center_y - y) ** 2) ** 0.5 < radius

    can_exist_red = True
    can_exist_blue = True
    if (dist_from_center(x_lower, y_lower) and dist_from_center(x_lower, y_upper) and
            dist_from_center(x_upper, y_lower) and dist_from_center(x_upper, y_upper)):
        can_exist_blue = False
    if x_lower <= center_x - radius and center_x + radius <= x_upper and y_lower <= center_y - radius \
            and center_y + radius <= y_upper:
        can_exist_red = False
    print("YES" if can_exist_red else "NO")
    print("YES" if can_exist_blue else "NO")


if __name__ == '__main__':
    main()

