from itertools import accumulate


def main():
    days = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    accumulate_days = list(accumulate(days))
    is_holiday = [0 for _ in range(367)]
    for i in range(1, 367):
        if i % 7 in [0, 1]:
            is_holiday[i] = 1
    holidays = int(input())
    for _ in range(holidays):
        m, d = map(int, input().split("/"))
        is_holiday[accumulate_days[m - 1] + d] += 1
    continuing_holidays = 0
    substitute_holiday = 0
    answer = 0
    for i in range(1, 367):
        if is_holiday[i] == 1:
            continuing_holidays += 1
        elif is_holiday[i] == 2:
            continuing_holidays += 1
            substitute_holiday += is_holiday[i] - 1
        elif is_holiday[i] == 0 and substitute_holiday > 0:
            continuing_holidays += 1
            substitute_holiday -= 1
        elif is_holiday[i] == 0:
            answer = max(answer, continuing_holidays)
            continuing_holidays = 0
    answer = max(answer, continuing_holidays)
    print(answer)


if __name__ == '__main__':
    main()

