def main():
    name_card, days, lower_limit, supply_num = map(int, input().split())
    answer = "complete"
    for i in range(1, days + 1):
        if name_card <= lower_limit:
            name_card += supply_num
        name_card -= int(input())
        if name_card < 0:
            answer = str(i)
            break
    print(answer)


if __name__ == '__main__':
    main()

