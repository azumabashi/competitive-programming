from copy import deepcopy


def main():
    left_shoe, right_shoe = map(int, input().split())
    left_size = {}
    for i in range(10, 41):
        left_size[i] = 0
    right_size = deepcopy(left_size)
    for l in list(map(int, input().split())):
        left_size[l] += 1
    for r in list(map(int, input().split())):
        right_size[r] += 1
    answer = 0
    for l, r in zip(left_size.values(), right_size.values()):
        answer += min(l, r)
    print(answer)


if __name__ == '__main__':
    main()

