def main():
    a, b, c = map(int, input().split())
    mod = 998244353

    def f(n):
        return (n * (n + 1) // 2) % mod

    print((f(a) * f(b) * f(c)) % mod)


if __name__ == '__main__':
    main()
