class UnionFind:
    def __init__(self, node):
        self.parent = [-1 for _ in range(node)]

    def find(self, target):
        if self.parent[target] < 0:
            return target
        else:
            self.parent[target] = self.find(self.parent[target])
            return self.parent[target]

    def is_same(self, x, y):
        return self.find(x) == self.find(y)

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x == root_y:
            return
        if self.parent[root_x] > self.parent[root_y]:
            root_x, root_y = root_y, root_x
        self.parent[root_x] += self.parent[root_y]
        self.parent[root_y] = root_x

    def get_size(self, x):
        return -self.parent[self.find(x)]

    def get_root(self):
        return [i for i, root in enumerate(self.parent) if root < 0]


def main():
    n, k = map(int, input().split())
    a = [[int(k) for k in input().split()] for _ in range(n)]
    mod = 998244353
    fact = [1] * 60
    for i in range(1, 60):
        fact[i] *= fact[i - 1] * i
        fact[i] %= mod
    uf_tate = UnionFind(n)
    uf_yoko = UnionFind(n)
    for i in range(n):
        for j in range(i + 1, n):
            if all(a[i][l] + a[j][l] <= k for l in range(n)):
                uf_tate.union(i, j)
    for i in range(n):
        for j in range(i + 1, n):
            if all(a[l][i] + a[l][j] <= k for l in range(n)):
                uf_yoko.union(i, j)
    root_tate = uf_tate.get_root()
    root_yoko = uf_yoko.get_root()
    if root_tate == root_yoko == []:
        print(0)
    else:
        ans = 1
        for t in root_tate:
            ans *= fact[uf_tate.get_size(t)]
            ans %= mod
        for y in root_yoko:
            ans *= fact[uf_yoko.get_size(y)]
            ans %= mod
        print(ans)


if __name__ == '__main__':
    main()
