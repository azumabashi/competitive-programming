def main():
    n, k = map(int, input().split())
    ans = 0
    for i in range(2, 2 * n + 1):
        if 2 <= i - k <= 2 * n:
            now = 1
            now *= i - 1 if i - 1 <= n else 2 * n - i + 1
            now *= (i - k - 1) if i - k - 1 <= n else 2 * n - i + k + 1
            ans += now
    print(ans)


if __name__ == '__main__':
    main()
