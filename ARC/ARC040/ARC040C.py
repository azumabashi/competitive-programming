def main():
    n = int(input())
    grid = [list(input()) for _ in range(n)]
    grid.append(["o" for _ in range(n)])
    ans = 0
    while len(list(filter(lambda x: "." in x, grid))) > 0:
        is_painted = False
        for i in range(n):
            if is_painted:
                break
            for j in range(n - 1, -1, -1):
                if grid[i][j] == ".":
                    is_painted = True
                    ans += 1
                    for k in range(n):
                        grid[i if k < j else i + 1][k] = "o"
                    grid[i][j] = "o"
    print(ans)


if __name__ == '__main__':
    main()
