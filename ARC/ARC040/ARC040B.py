def main():
    length, paint_range = map(int, input().split())
    grid = list(input())
    answer = 0
    for i in range(length - 1, -1, -1):
        if grid[i] == ".":
            answer = max(0, i - paint_range + 1)
            break
    for i in range(length):
        if grid[i] == ".":
            answer += 1
            grid[i:min(i + paint_range, length)] = ["o" for _ in range(min(i + paint_range, length) - i)]
    print(answer)


if __name__ == '__main__':
    main()

