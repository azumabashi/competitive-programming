def main():
    grids = int(input())
    blue_grid = 0
    red_grid = 0
    for _ in range(grids):
        color = input()
        blue_grid += color.count("B")
        red_grid += color.count("R")
    answer = "AOKI"
    if blue_grid == red_grid:
        answer = "DRAW"
    elif blue_grid < red_grid:
        answer = "TAKAHASHI"
    print(answer)


if __name__ == '__main__':
    main()

