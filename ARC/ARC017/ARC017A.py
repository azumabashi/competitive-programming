def main():
    number = int(input())
    i = 2
    answer = True
    if number == 1:
        answer = False
    while i * i <= number:
        if number % i == 0:
            answer = False
            break
        else:
            i += 1
    print("YES" if answer else "NO")


if __name__ == '__main__':
    main()

