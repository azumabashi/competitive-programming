def main():
    periods, show = map(int, input().split())
    track_record = [int(input()) for _ in range(periods)]
    answer = 0
    increase_section = [1 for _ in range(periods)]
    for i in range(periods - 1):
        if track_record[i] < track_record[i + 1]:
            increase_section[i + 1] = increase_section[i] + 1
    for i in range(periods):
        if show <= increase_section[i]:
            answer += 1
    print(answer)


if __name__ == '__main__':
    main()

