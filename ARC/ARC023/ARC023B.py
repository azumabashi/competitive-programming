def main():
    row, col, move = map(int, input().split())
    count = {i: [] for i in range(1, 1000)}
    for i in range(row):
        l = list(map(int, input().split()))
        for j in range(col):
            count[l[j]].append([i, j])
    answer = 0
    for i in range(999, 0, -1):
        if answer != 0:
            break
        for y, x in count[i]:
            distance = x + y
            if distance <= move and (move - distance) % 2 == 0:
                answer = i
                break
    print(answer)


if __name__ == '__main__':
    main()

