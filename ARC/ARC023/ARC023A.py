from math import floor


def calculate_days(y, m, d):
    if m < 3:
        m += 12
        y -= 1
    return 365 * y + floor(y / 4) - floor(y / 100) + floor(y / 400) + floor(306 * (m + 1) / 10) + d - 429


def main():
    date = [int(input()) for _ in range(3)]
    print(calculate_days(2014, 5, 17) - calculate_days(date[0], date[1], date[2]))


if __name__ == '__main__':
    main()

