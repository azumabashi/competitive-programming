from itertools import permutations


def main():
    box_size = list(map(int, input().split()))
    package_size = list(map(int, input().split()))

    def find_boxes(inner_width, inner_height, inner_depth):
        return (box_size[0] // inner_width) * (box_size[1] // inner_height) * (box_size[2] // inner_depth)

    answer = 0
    for comb in permutations([0, 1, 2], 3):
        answer = max(answer, find_boxes(package_size[comb[0]], package_size[comb[1]], package_size[comb[2]]))
    print(answer)


if __name__ == '__main__':
    main()

