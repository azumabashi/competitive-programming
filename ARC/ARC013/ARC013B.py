def main():
    box_num = int(input())
    answer = [0, 0, 0]
    for _ in range(box_num):
        size = sorted(list(map(int, input().split())))
        for i in range(3):
            answer[i] = max(answer[i], size[i])
    print(answer[0] * answer[1] * answer[2])


if __name__ == '__main__':
    main()

