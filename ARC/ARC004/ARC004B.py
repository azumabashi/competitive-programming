def main():
    edges = int(input())
    length = []
    length_sum = 0
    for _ in range(edges):
        l = int(input())
        length.append(l)
        length_sum += l
    min_dist = 0
    if not all(length_sum >= 2 * length[i] for i in range(edges)):
        for l in length:
            if length_sum >= 2 * l:
                min_dist += l
            else:
                min_dist -= l
    print(length_sum)
    print(abs(min_dist))


if __name__ == '__main__':
    main()

