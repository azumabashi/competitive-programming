def main():
    points = int(input())
    coordinate = [tuple(map(int, input().split())) for _ in range(points)]
    answer = 0
    for i in range(points):
        for j in range(i + 1, points):
            answer = max(answer, ((coordinate[i][0] - coordinate[j][0]) ** 2 + (coordinate[i][1] - coordinate[j][1]) ** 2) ** 0.5)
    print(answer)


if __name__ == '__main__':
    main()

