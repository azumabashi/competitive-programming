def main():
    word_num = int(input())
    words = [input() for _ in range(word_num)]
    has_said = []
    answer = "DRAW"
    for i in range(word_num - 1):
        if words[i] in has_said:
            if i % 2 == 0:
                answer = "LOSE"
            else:
                answer = "WIN"
            break
        elif not words[i][-1] == words[i + 1][0]:
            if i % 2 == 0:
                answer = "WIN"
            else:
                answer = "LOSE"
            break
        else:
            has_said.append(words[i])
    print(answer)


if __name__ == '__main__':
    main()

