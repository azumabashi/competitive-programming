def main():
    balls = int(input())
    ball = input()
    print(sum(ball.count(c) % 2 for c in ["R", "G", "B"]))


if __name__ == '__main__':
    main()

