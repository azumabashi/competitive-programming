def main():
    print("Red" if int(input()) % 2 else "Blue")


if __name__ == '__main__':
    main()

