def main():
    name = input()
    answer = True
    for i in range(1, len(name) // 2 + 1):
        if not name[i - 1] == name[-i]:
            answer = False
            break
    print("YES" if answer else "NO")


if __name__ == '__main__':
    main()

