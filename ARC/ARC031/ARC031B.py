from copy import deepcopy
from collections import deque


def main():
    country_map = [list(input()) for _ in range(10)]
    count_island = 0
    d = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    inf = 200
    is_possible = False
    for m in country_map:
        count_island += m.count("o")
    for i in range(10):
        if is_possible:
            break
        for j in range(10):
            if country_map[i][j] == "x":
                now_map = deepcopy(country_map)
                now_map[i][j] = "o"
                count = [[inf for _ in range(10)] for _ in range(10)]
                count[i][j] = 0
                q = deque([[i, j]])
                while q:
                    y, x = q.pop()
                    if now_map[y][x] == "o":
                        now_map[y][x] = "x"
                        for dy, dx in d:
                            if 0 <= y + dy < 10 and 0 <= x + dx < 10 and now_map[y + dy][x + dx] == "o":
                                count[y + dy][x + dx] = min(count[y + dy][x + dx], count[y][x] + 1)
                                q.append([y + dy, x + dx])
                count_not_inf = 0
                for k in range(10):
                    for l in range(10):
                        if (country_map[k][l] == "o" or (k == i and l == j)) and count[k][l] != inf:
                            count_not_inf += 1
                if count_not_inf == count_island + 1:
                    is_possible = True
                    break
    print("YES" if is_possible else "NO")


if __name__ == '__main__':
    main()

