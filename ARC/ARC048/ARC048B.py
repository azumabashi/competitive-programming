def main():
    people = int(input())
    max_rating = 100000
    group_by_rating = [{1: 0, 2: 0, 3: 0} for _ in range(max_rating + 1)]
    participants = []
    sum_people = [0 for _ in range(max_rating + 1)]
    for _ in range(people):
        rating, hand = map(int, input().split())
        participants.append([rating, hand])
        group_by_rating[rating][hand] += 1
        sum_people[rating] += 1
    for i in range(1, max_rating + 1):
        sum_people[i] += sum_people[i - 1]
    win_pattern = {1: 2, 2: 3, 3: 1}
    for rating, hand in participants:
        win = sum_people[rating - 1] + group_by_rating[rating][win_pattern[hand]]
        even = group_by_rating[rating][hand] - 1
        lose = people - win - even - 1
        print(win, lose, even)


if __name__ == '__main__':
    main()

