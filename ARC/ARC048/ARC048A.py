def main():
    start, goal = map(int, input().split())
    print(goal - start if start * goal > 0 else goal - start - 1)


if __name__ == '__main__':
    main()

