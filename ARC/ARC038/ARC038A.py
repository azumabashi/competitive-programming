def main():
    cards = int(input())
    card = sorted(list(map(int, input().split())), reverse=True)
    print(sum(card[::2]))


if __name__ == '__main__':
    main()

