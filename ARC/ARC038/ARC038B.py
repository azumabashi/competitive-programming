H, W = map(int, input().split())
dp = [[None for _ in range(1000)] for _ in range(1000)]
grid = [list(input()) for _ in range(H)]


def solve(x, y):
    if H <= x or W <= y or grid[x][y] == "#":
        return True
    if dp[x][y] is not None:
        return dp[x][y]
    res = False
    if any(not solve(nx, ny) for nx, ny in [[x, y + 1], [x + 1, y], [x + 1, y + 1]]):
        res = True
    dp[x][y] = res
    return res


def main():
    print("First" if solve(0, 0) else "Second")


if __name__ == '__main__':
    main()
