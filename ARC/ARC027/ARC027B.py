class UnionFind:
    def __init__(self, node):
        self.parent = [-1 for _ in range(node)]

    def find(self, target):
        if self.parent[target] < 0:
            return target
        else:
            self.parent[target] = self.find(self.parent[target])
            return self.parent[target]

    def is_same(self, x, y):
        return self.find(x) == self.find(y)

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x == root_y:
            return
        if self.parent[root_x] > self.parent[root_y]:
            root_x, root_y = root_y, root_x
        self.parent[root_x] += self.parent[root_y]
        self.parent[root_y] = root_x

    def get_size(self, x):
        return -self.parent[self.find(x)]

    def get_root(self):
        return [i for i, root in enumerate(self.parent) if root < 0]


def main():
    n = int(input())
    x = input()
    y = input()
    num = {str(i) for i in range(10)}
    uf = UnionFind(n + 1)

    def unite(arr1, arr2=None):
        if arr2 is None:
            arr2 = arr1
        for i in range(n):
            for j in range(n):
                if arr1[i] == arr2[j]:
                    if arr1[i] in num:
                        uf.union(n, i)
                        uf.union(n, j)
                    else:
                        uf.union(i, j)

    unite(x)
    unite(y)
    unite(x, y)
    root = set(uf.get_root())
    if root == {n}:
        print(1)
    elif uf.is_same(0, n):
        print(pow(10, len(root) - 1))
    else:
        print(9 * pow(10, len(root) - 2))


if __name__ == '__main__':
    main()
