def main():
    hour, minute = map(int, input().split())
    print((18 - hour) * 60 - minute)


if __name__ == '__main__':
    main()

