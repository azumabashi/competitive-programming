from math import floor


def main():
    goods_num = int(input())
    answer = 0
    for _ in range(goods_num):
        num, cost = map(int, input().split())
        answer += num * cost
    print(floor(answer * 1.05))


if __name__ == '__main__':
    main()

