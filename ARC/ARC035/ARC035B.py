from itertools import accumulate
from math import factorial


def main():
    questions = int(input())
    solve_time = [int(input()) for _ in range(questions)]
    solve_time.sort()
    mod = 10 ** 9 + 7
    group_by_required_time = {}
    for t in solve_time:
        if t in group_by_required_time:
            group_by_required_time[t] += 1
        else:
            group_by_required_time[t] = 1
    all_required_time = sum(list(accumulate(solve_time)))
    solve_order = 1
    for c in group_by_required_time.values():
        solve_order *= factorial(c)
        solve_order %= mod
    print(all_required_time)
    print(solve_order)


if __name__ == '__main__':
    main()

