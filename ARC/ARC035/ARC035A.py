def main():
    string = input()
    answer = True
    for i in range(1, len(string) // 2 + 1):
        if string[i - 1] == string[-i] or string[i - 1] == "*" or string[-i] == "*":
            continue
        else:
            answer = False
    print("YES" if answer else "NO")


if __name__ == '__main__':
    main()

