def main():
    stone, ant, bug = map(int, input().split())
    answer = "Ant"
    while True:
        if answer == "Ant":
            stone -= min(stone, ant)
        else:
            stone -= min(stone, bug)
        if stone == 0:
            print(answer)
            break
        else:
            if answer == "Ant":
                answer = "Bug"
            else:
                answer = "Ant"


if __name__ == '__main__':
    main()

