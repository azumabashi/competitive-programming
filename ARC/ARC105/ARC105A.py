def main():
    c = list(map(int, input().split()))
    max_c = sum(c)
    for i in range(1 << 4):
        res = 0
        for j in range(4):
            if i & (1 << j):
                res += c[j]
        if 2 * res == max_c:
            return True
    return False


if __name__ == '__main__':
    print("Yes" if main() else "No")
