from functools import reduce
from math import gcd


def main():
    n = int(input())
    a = [int(t) for t in input().split()]
    print(reduce(gcd, a))


if __name__ == '__main__':
    main()
