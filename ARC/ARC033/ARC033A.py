def main():
    length = int(input())
    answer = 0
    for i in range(1, length + 1):
        answer += length - i + 1
    print(answer)


if __name__ == '__main__':
    main()

