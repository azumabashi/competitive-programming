def main():
    ant, bug = map(int, input().split())
    ant_variable = set(map(int, input().split()))
    bug_variable = set(map(int, input().split()))
    print(len(ant_variable & bug_variable) / len(ant_variable | bug_variable))


if __name__ == '__main__':
    main()

