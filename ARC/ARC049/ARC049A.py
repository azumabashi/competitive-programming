def main():
    string = input()
    insert_index = list(map(int, input().split()))
    answer = ''
    for i in range(len(string)):
        if i in insert_index:
            answer += '"'
        answer += string[i]
    if len(string) in insert_index:
        answer += '"'
    print(answer)


if __name__ == '__main__':
    main()

