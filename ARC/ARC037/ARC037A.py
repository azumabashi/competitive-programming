def main():
    lectures = int(input())
    score = list(map(int, input().split()))
    answer = 0
    for s in score:
        if s < 80:
            answer += 80 - s
    print(answer)


if __name__ == '__main__':
    main()

