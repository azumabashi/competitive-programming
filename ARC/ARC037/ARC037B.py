from collections import deque


def main():
    vertices, edges = map(int, input().split())
    graph = [[] for _ in range(vertices)]
    is_checked = [False for _ in range(vertices)]
    for _ in range(edges):
        u, v = map(lambda x: int(x) - 1, input().split())
        graph[u].append(v)
        graph[v].append(u)
    answer = 0
    for i in range(vertices):
        is_possible = True
        if not is_checked[i]:
            stack = deque([(i, -1)])
            is_checked[i] = True
            while stack:
                now, last_visited = stack.pop()
                if not is_possible:
                    break
                for next_v in graph[now]:
                    if next_v == last_visited:
                        continue
                    elif not is_checked[next_v]:
                        is_checked[next_v] = True
                        stack.append((next_v, now))
                    else:
                        is_possible = False
                        break
            if is_possible:
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

