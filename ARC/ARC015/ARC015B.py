def main():
    days = int(input())
    answer = [0] * 6
    temperature = [list(map(float, input().split())) for _ in range(days)]
    for i in range(days):
        if temperature[i][0] >= 35:
            answer[0] += 1
        elif temperature[i][0] >= 30:
            answer[1] += 1
        elif temperature[i][0] >= 25:
            answer[2] += 1
        if temperature[i][1] >= 25:
            answer[3] += 1
        if temperature[i][0] >= 0 and 0 > temperature[i][1]:
            answer[4] += 1
        elif 0 > temperature[i][1]:
            answer[5] += 1
    print(" ".join(map(str, answer)))


if __name__ == '__main__':
    main()

