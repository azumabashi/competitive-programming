from copy import deepcopy


def main():
    a, b = map(list, input().split())
    answer = -float("inf")
    for i in range(3):
        new_a = deepcopy(a)
        new_a[i] = "9"
        answer = max(answer, int("".join(new_a)) - int("".join(b)))
    for i in range(3):
        new_b = deepcopy(b)
        change_to = "0"
        if i == 0:
            change_to = "1"
        new_b[i] = change_to
        answer = max(answer, int("".join(a)) - int("".join(new_b)))
    print(answer)


if __name__ == '__main__':
    main()

