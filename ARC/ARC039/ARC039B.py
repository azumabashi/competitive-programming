def comb(n, k, mod=10 ** 9 + 7):
    denominator = 1
    numerator = 1
    for i in range(1, k + 1):
        denominator = denominator * i % mod
    for i in range(n - k + 1, n + 1):
        numerator = numerator * i % mod
    return numerator * pow(denominator, mod - 2, mod) % mod


def solve(n, k):
    if k < n:
        return comb(n + k - 1, k)
    elif k == n:
        return 1
    return comb(n, k % n)


def main():
    n, k = map(int, input().split())
    print(solve(n, k))


if __name__ == '__main__':
    main()
