from collections import deque


def main():
    height, width = map(int, input().split())
    grid = []
    start = [0, 0]
    goal = [0, 0]
    for i in range(height):
        row = list(input())
        for j in range(width):
            if row[j] == 's':
                start = [i, j]
            elif row[j] == 'g':
                goal = [i, j]
        grid.append(row)
    move_cost = {'.': 0, 's': 0, 'g': 0, '#': 1}
    cost = [[float('inf') for _ in range(width)] for _ in range(height)]
    cost[start[0]][start[1]] = 0
    q = deque([start])
    delta = [[0, 1], [1, 0], [0, -1], [-1, 0]]
    while q:
        y, x = q.popleft()
        for dy, dx in delta:
            new_y = y + dy
            new_x = x + dx
            if 0 <= new_y < height and 0 <= new_x < width:
                new_cost = cost[y][x] + move_cost[grid[new_y][new_x]]
                if new_cost < cost[new_y][new_x]:
                    cost[new_y][new_x] = new_cost
                    if grid[new_y][new_x] == '#':
                        q.append([new_y, new_x])
                    else:
                        q.appendleft([new_y, new_x])
    print('YES' if cost[goal[0]][goal[1]] <= 2 else 'NO')


if __name__ == '__main__':
    main()

