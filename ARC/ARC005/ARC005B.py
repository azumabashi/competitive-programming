def reverse_direction(coordinate, direction):
    if coordinate + direction == 9 or coordinate + direction == -1:
        direction *= -1
    return direction


def main():
    x_coordinate, y_coordinate, direction = input().split()
    x_coordinate = int(x_coordinate) - 1
    y_coordinate = int(y_coordinate) - 1
    grid = [list(input()) for _ in range(9)]
    answer = ["0" for _ in range(4)]
    dx = 0
    dy = 0
    if direction[0] == "R":
        dx = 1
    elif direction[0] == "L":
        dx = -1
    if direction[-1] == "U":
        dy = -1
    elif direction[-1] == "D":
        dy = 1
    answer[0] = grid[y_coordinate][x_coordinate]
    for i in range(1, 4):
        dx = reverse_direction(x_coordinate, dx)
        dy = reverse_direction(y_coordinate, dy)
        x_coordinate += dx
        y_coordinate += dy
        answer[i] = grid[y_coordinate][x_coordinate]
    print("".join(answer))


if __name__ == '__main__':
    main()

