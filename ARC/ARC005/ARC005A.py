def main():
    words = int(input())
    sentence = input()[:-1].split()
    target = ["TAKAHASHIKUN", "Takahashikun", "takahashikun"]
    answer = 0
    for s in sentence:
        for t in target:
            if s == t:
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

