def main():
    x, y, z = map(int, input().split())
    ans = 0
    for i in range(z, (10 ** 9 + 8) * 108, 10 ** 9 + 7):
        if i % 107 == y:
            ans = i
            break
    mod = 17
    while True:
        if ans % mod == x:
            print(ans)
            break
        else:
            ans += (10 ** 9 + 7) * 107


if __name__ == '__main__':
    main()
