def main():
    h1, m1, h2, m2, k = map(int, input().split())
    time = (h2 - h1) * 60 - m1 + m2
    print(max(time - k, 0))


if __name__ == '__main__':
    main()

