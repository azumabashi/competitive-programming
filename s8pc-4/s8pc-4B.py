def main():
    n, k = map(int, input().split())
    a = list(map(int, input().split()))
    ans = 1 << 60
    for i in range(1 << n):
        now_lower = 0
        see = 0
        cost = 0
        can_change_min = True
        for j in range(n):
            if (i >> j) & 1:
                cost += max(1 + now_lower - a[j], 0)
                now_lower = max(now_lower + 1, a[j])
                see += 1
            else:
                if now_lower < a[j]:
                    see += 1
                now_lower = max(now_lower, a[j])
        if see >= k and can_change_min:
            ans = min(ans, cost)
    print(ans)


if __name__ == '__main__':
    main()
