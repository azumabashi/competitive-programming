from string import ascii_lowercase


def solve():
    s = list(input())[:-1]
    let = set(list(ascii_lowercase))
    win = {}
    for _ in range(len(list(filter(lambda x: x in let, s)))):
        a, v = input().split()
        win[a] = int(v)
    while True:
        if len(s) == 1:
            return not win[s[0]]
        i = 0
        while i < len(s) - 2:
            if s[i] in let and s[i + 1] == "-" and s[i + 2] in let:
                x, y = win[s[i]], win[s[i + 2]]
                if min(x, y) > 0 or max(x, y) < 1:
                    return False
                idx = i if win[s[i]] else i + 2
                win[s[idx]] -= 1
                s[i - 1] = s[idx]
                s = s[:i] + s[i + 4:]
            else:
                i += 1


def main():
    print("Yes" if solve() else "No")


if __name__ == '__main__':
    main()
