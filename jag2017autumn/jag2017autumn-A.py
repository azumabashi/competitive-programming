def main():
    n, h, w = map(int, input().split())
    x = [(-1) ** i * int(k) for i, k in enumerate(input().split())]
    windows = [1] * (n * w + 1)
    for i in range(n):
        for j in range(w):
            windows[w * i + j] -= 1
            windows[w * i + j + x[i]] += 1
    print(h * len(list(filter(lambda x: x == 0, windows[:-1]))))


if __name__ == '__main__':
    main()
