import numpy as np


def main():
    a, b, m = map(int, input().split())
    a_c = np.array(list(map(int, input().split())), dtype=int)
    b_c = np.array(list(map(int, input().split())), dtype=int)
    discount = [tuple(map(int, input().split())) for _ in range(m)]
    answer = min(a_c) + min(b_c)
    for d in discount:
        answer = min(answer, a_c[d[0] - 1] + b_c[d[1] - 1] - d[2])
    print(answer)


if __name__ == '__main__':
    main()

