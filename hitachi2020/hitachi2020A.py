def main():
    s = input()
    print("Yes" if s.replace("hi", "") == "" else "No")


if __name__ == '__main__':
    main()

