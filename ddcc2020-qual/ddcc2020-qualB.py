from itertools import accumulate


def main():
    section_num = int(input())
    length = list(map(int, input().split()))
    accumulate_length = list(accumulate(length))
    answer = float("inf")
    for i in range(0, section_num):
        answer = min(answer, abs(accumulate_length[-1] - 2 * accumulate_length[i]))
    print(answer)


if __name__ == '__main__':
    main()

