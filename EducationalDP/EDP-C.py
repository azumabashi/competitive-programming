def main():
    days = int(input())
    value = [list(map(int, input().split())) for _ in range(days)]
    dp = [[0, 0, 0] for _ in range(days)]
    for i in range(3):
        dp[0][i] = value[0][i]
    for i in range(1, days):
        dp[i][0] = max(dp[i - 1][1], dp[i - 1][2]) + value[i][0]
        dp[i][1] = max(dp[i - 1][0], dp[i - 1][2]) + value[i][1]
        dp[i][2] = max(dp[i - 1][0], dp[i - 1][1]) + value[i][2]
    print(max(dp[-1][0], dp[-1][1], dp[-1][2]))


if __name__ == '__main__':
    main()

