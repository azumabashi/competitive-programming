def main():
    goods, weight_limit = map(int, input().split())
    good_info = [tuple(map(int, input().split())) for _ in range(goods)]
    dp = [[0 for _ in range(weight_limit + 1)] for _ in range(goods + 1)]
    for i in range(goods):
        for j in range(weight_limit + 1):
            if j >= good_info[i][0]:
                dp[i + 1][j] = max(dp[i][j], dp[i][j - good_info[i][0]] + good_info[i][1])
            else:
                dp[i + 1][j] = dp[i][j]
    print(dp[goods][weight_limit])


if __name__ == '__main__':
    main()

