def main():
    footing, next_range = map(int, input().split())
    height = list(map(int, input().split()))
    result = abs(height[-1] - height[0])
    if next_range < footing:
        answer = [float("inf")] * footing
        answer[0] = 0
        answer[1] = abs(height[1] - height[0])
        for i in range(footing):
            for j in range(1, next_range + 1):
                answer[i] = min(answer[i], answer[i - j] + abs(height[i] - height[i - j]))
        result = answer[-1]
    print(result)


if __name__ == '__main__':
    main()

