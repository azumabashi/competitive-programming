def main():
    goods, weight_limit = map(int, input().split())
    weight = []
    value = []
    for _ in range(goods):
        w, v = map(int, input().split())
        weight.append(w)
        value.append(v)
    max_value = 10 ** 5 + 10
    dp = [[float("inf") for _ in range(max_value + 1)] for _ in range(goods + 1)]
    dp[0][0] = 0
    for i in range(goods):
        for v in range(max_value + 1):
            if v >= value[i]:
                dp[i + 1][v] = min(dp[i][v], dp[i][v - value[i]] + weight[i])
            else:
                dp[i + 1][v] = dp[i][v]
    answer = 0
    for i in range(max_value):
        if dp[-1][i] <= weight_limit:
            answer = max(answer, i)
    print(answer)


if __name__ == '__main__':
    main()

