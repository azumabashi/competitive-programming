def main():
    footing = int(input())
    height = list(map(int, input().split()))
    answer = [0] * footing
    answer[1] = abs(height[1] - height[0])
    for i in range(2, footing):
        answer[i] = min(answer[i - 2] + abs(height[i] - height[i - 2]), answer[i - 1] + abs(height[i] - height[i - 1]))
    print(answer[-1])


if __name__ == '__main__':
    main()

