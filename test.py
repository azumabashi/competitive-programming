import numpy as np
from scipy.sparse.csgraph import shortest_path, dijkstra
from scipy.sparse import csr_matrix


def main():
    n, m = map(int, input().split())
    grid = [[0 for _ in range(n)] for _ in range(n)]
    for _ in range(m):
        a, b, l = map(int, input().split())
        grid[a - 1][b - 1] = l
        grid[b - 1][a - 1] = l
    grid = np.array(grid)
    print(shortest_path(grid, method="D"))


if __name__ == '__main__':
    main()

"""
5 10
1 2 240
1 3 410
1 4 610
1 5 970
2 3 220
2 4 390
2 5 640
3 4 190
3 5 480
4 5 310
[[  0. 240. 410. 600. 880.]
 [240.   0. 220. 390. 640.]
 [410. 220.   0. 190. 480.]
 [600. 390. 190.   0. 310.]
 [880. 640. 480. 310.   0.]]
"""