from collections import deque
"""
    貪欲ではいけない，反例：
    8
    1 2
    1 3
    2 4
    2 5
    3 6
    3 7
    5 8
    3 1 4 1 5 9 2 6
    
    output:
    22
    6 9 2 5 4 1 1 3
"""


def main():
    n = int(input())
    ans = [0 for _ in range(n)]
    graph = [[] for _ in range(n)]
    for _ in range(n - 1):
        a, b = map(lambda x: int(x) - 1, input().split())
        graph[a].append(b)
        graph[b].append(a)
    c = list(map(int, input().split()))
    c.sort(reverse=True)
    root = 0
    max_deg = 0
    for i in range(n):
        if max_deg < len(graph[i]):
            root = i
            max_deg = len(graph[i])
    q = deque([root])
    ans[root] = c[0]
    idx = 1
    while q:
        now_v = q.pop()
        for next_v in graph[now_v]:
            if ans[next_v] == 0:
                ans[next_v] = c[idx]
                idx += 1
                q.append(next_v)
    print(sum(c[1:]))
    print(" ".join(map(str, ans)))


if __name__ == '__main__':
    main()
