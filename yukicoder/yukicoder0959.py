def main():
    height, width = map(int, input().split())
    tree_probability = float(input())
    if 1 < min(height, width):
        print((height - 2) * (width - 2) * pow(tree_probability, 5) +
              2 * (height + width - 4) * pow(tree_probability, 4) +
              4 * pow(tree_probability, 3))
    elif 1 < max(height, width):
        print(2 * pow(tree_probability, 2) + (max(height, width) - 2) * pow(tree_probability, 3))
    else:
        print(tree_probability)


if __name__ == '__main__':
    main()

