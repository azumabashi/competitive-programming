def solve(n, z):
    target = pow(z, n)
    for i in range(1, 1000):
        for j in range(1, 1000):
            if pow(i, n) + pow(j, n) == target:
                return True
    return False


def main():
    n, z = map(int, input().split())
    if z == 1:
        print("No")
    elif n == 1:
        print("Yes")
    else:
        print("Yes" if solve(n, z) else "No")


if __name__ == '__main__':
    main()
