def main():
    digit = int(input())
    answer = []
    for i in range(digit + 1):
        for j in range(digit + 1):
            answer.append(pow(2, i) * pow(5, j))
    answer.sort()
    print("\n".join(map(str, answer)))


if __name__ == '__main__':
    main()

