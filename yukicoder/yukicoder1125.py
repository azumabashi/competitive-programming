from collections import Counter


def main():
    n = int(input())
    s = Counter(list(map(int, input().split())))
    t = Counter(list(map(int, input().split())))
    if s[0] == t[0] == n:
        print(0)
    elif s[2] > 0 and t[2] > 0:
        print(n * (s[2] + t[2]) - s[2] * t[2])
    elif s[2] > 0 or t[2] > 0:
        print(n * max(s[2], t[2]) + (s[1] if s[2] > 0 else t[1]))
    else:
        print(max(s[1], t[1]))


if __name__ == '__main__':
    main()
