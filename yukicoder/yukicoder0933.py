import sys
sys.setrecursionlimit(10**6)


def get_digit_sum(n):
    if n < 10:
        return n
    else:
        return get_digit_sum(n % 10 + n // 10)


def main():
    length = int(input())
    sequence = [get_digit_sum(int(x)) for x in input().split()]
    product = 1
    for seq in sequence:
        product = get_digit_sum(product * seq)
    print(get_digit_sum(product))


if __name__ == '__main__':
    main()

