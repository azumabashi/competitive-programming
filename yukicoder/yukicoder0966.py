def make_b_min(a, b, c, diff):
    new_a = min(b - 2, a)
    new_c = min(b - 1, c)
    if new_a <= 0:
        return 10 ** 10
    else:
        return a - new_a + c - new_c + diff


def make_b_max(a, b, diff):
    new_b = min(a - 1, b)
    if new_b <= 0:
        return 10 ** 10
    else:
        return b - new_b + diff


def main():
    queries = int(input())
    answer = [0 for _ in range(queries)]
    for i in range(queries):
        diff = 0
        a, b, c = map(int, input().split())
        if c < a:
            a, c = c, a
        if a == c:
            a -= 1
            diff += 1
        result = min(make_b_min(a, b, c, diff), make_b_max(a, b, diff))
        answer[i] = result if result != 10 ** 10 else -1
    print("\n".join(map(str, answer)))


if __name__ == '__main__':
    main()

