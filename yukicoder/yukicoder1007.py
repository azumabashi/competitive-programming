def main():
    length, start = map(int, input().split())
    start -= 1
    coins = [int(x) for x in input().split()]
    small_coin_index = [0]
    for i in range(length):
        if i == start:
            continue
        if coins[i] < 2:
            small_coin_index.append(i)
    answer = coins[start]
    if small_coin_index == [0]:
        if coins[start] == 1:
            answer = max(sum(coins[:start + 1]), sum(coins[start:]))
        elif coins[start] > 1:
            answer = sum(coins)
    elif coins[start] > 0:
        move_range = [0, 0]
        small_coin_index.sort()
        if small_coin_index[-1] < start:
            answer = sum(coins[small_coin_index[-1]:])
        elif start < small_coin_index[1]:
            answer = sum(coins[:small_coin_index[1] + 1])
        else:
            for i in range(len(small_coin_index) - 1):
                if small_coin_index[i] < start < small_coin_index[i + 1]:
                    move_range[0] = small_coin_index[i]
                    move_range[1] = small_coin_index[i + 1]
                    break
            if coins[start] == 1:
                answer = max(sum(coins[move_range[0]:start + 1]), sum(coins[start:move_range[1] + 1]))
            else:
                answer = sum(coins[move_range[0]:move_range[1] + 1])
    print(answer)


if __name__ == '__main__':
    main()

