from collections import deque


def main():
    n, m = map(int, input().split())
    a = [int(x) for x in input().split()]
    s = list(input())
    a = deque(a)
    for now_op in s:
        if now_op == "L":
            left = a.popleft()
            left += a.popleft()
            a.appendleft(left)
            a.append(0)
        else:
            right = a.pop()
            right += a.pop()
            a.append(right)
            a.appendleft(0)
    print(" ".join(map(str, list(a))))


if __name__ == '__main__':
    main()

