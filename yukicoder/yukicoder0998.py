def main():
    numbers = [int(x) for x in input().split()]
    numbers.sort()
    print("Yes" if numbers == [numbers[0] + i for i in range(4)] else "No")


if __name__ == '__main__':
    main()

