class UnionFind:
    def __init__(self, node):
        self.parent = [-1 for _ in range(node)]
        self.node = node

    def find(self, target):
        if self.parent[target] < 0:
            return target
        else:
            self.parent[target] = self.find(self.parent[target])
            return self.parent[target]

    def is_same(self, x, y):
        return self.find(x) == self.find(y)

    def union(self, x, y):
        root_x = self.find(x)
        root_y = self.find(y)
        if root_x == root_y:
            return
        if self.parent[root_x] > self.parent[root_y]:
            root_x, root_y = root_y, root_x
        self.parent[root_x] += self.parent[root_y]
        self.parent[root_y] = root_x

    def get_size(self, x):
        return -self.parent[self.find(x)]

    def get_roots(self):
        return [i for i, val in enumerate(self.parent) if val < 0]

    def members(self, x):
        root = self.find(x)
        return [i for i in range(self.node) if self.find(i) == root]

    def get_group_members(self):
        return {root: self.members(root) for root in self.get_roots()}


def main():
    islands = int(input())
    uf = UnionFind(islands)
    graph = [[] for _ in range(islands)]
    for _ in range(islands - 1):
        u, v = map(int, input().split())
        uf.union(u, v)
        graph[u].append(v)
        graph[v].append(u)
    can_bob_win = False
    all_roots = uf.get_roots()
    if len(all_roots) == 1:
        can_bob_win = True
    elif len(all_roots) == 2:
        group_member = uf.get_group_members()
        ok = 0
        for root in all_roots:
            if len(group_member[root]) < 3:
                ok += 1
            elif all(len(graph[v]) > 1 for v in group_member[root]):
                ok += 1
        if ok == 2:
            can_bob_win = True
    print("Bob" if can_bob_win else "Alice")


if __name__ == '__main__':
    main()

