def main():
    boxes, colors = map(int, input().split())
    factorials = [1 for _ in range(boxes + 1)]
    mod = 10 ** 9 + 7
    for i in range(2, boxes + 1):
        factorials[i] = (factorials[i - 1] * i) % mod
    answer = 0

    def comb(n, k):
        return factorials[n] * pow(factorials[k], mod - 2, mod) * pow(factorials[n - k], mod - 2, mod)

    for i in range(colors + 1):
        sign = 1
        if i % 2 == 1:
            sign = -1
        answer += sign * comb(colors, i) * pow(colors - i, boxes, mod)
        answer %= mod
    print(answer)


if __name__ == '__main__':
    main()

