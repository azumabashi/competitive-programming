def main():
    beans = int(input())
    value = list(map(int, input().split()))
    for i in range(1, beans):
        value[i] += value[i - 1]
    val_set = set(value)
    answer = 1
    for i in range(beans):
        if value[-1] % value[i] == 0:
            j = 1
            while value[i] * j in val_set:
                j += 1
            if value[i] * (j - 1) == value[-1]:
                answer = max(answer, j - 1)
    print(answer)


if __name__ == '__main__':
    main()

