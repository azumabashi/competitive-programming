from collections import defaultdict, Counter


def main():
    n, x = map(int, input().split())
    cnt = defaultdict(int)
    a = []
    for _ in range(n):
        now_a = int(input())
        cnt[x ^ now_a] += 1
        a.append(now_a)
    ans = 0
    if x > 0:
        for aa in a:
            ans += cnt[aa]
        ans //= 2
    else:
        all_a = Counter(a)
        for _, c in all_a.items():
            ans += c * (c - 1) // 2
    print(ans)


if __name__ == '__main__':
    main()
