from bisect import bisect_left


def get_min_value(val, seq, op, target):
    if op == "+":
        now_target = target - val
    else:
        now_target = target / val
    return bisect_left(seq, now_target)


def main():
    col, row, target = map(int, input().split())
    op, *row_numbers = input().split()
    row_numbers = list(map(int, row_numbers))
    row_numbers.sort()
    answer = 0
    for _ in range(col):
        col_num = int(input())
        answer += row - get_min_value(col_num, row_numbers, op, target)
    print(answer)


if __name__ == '__main__':
    main()

