def main():
    n = int(input())
    cheating = []
    cheated = []
    checked = {}
    for _ in range(n):
        a, b = input().split()
        cheating.append(a)
        cheated.append(b)
        checked[b] = False
    cheating_set = set(cheating)
    for c in cheated:
        if c not in cheating_set and not checked[c]:
            print(c)
            checked[c] = True


if __name__ == '__main__':
    main()

