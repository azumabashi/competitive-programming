from math import gcd
from decimal import Decimal


def main():
    a, b = map(int, input().split())
    ab_gcd = gcd(a, b)
    print("Odd" if float(Decimal(str(ab_gcd)) ** Decimal("0.5")).is_integer() else "Even")


if __name__ == '__main__':
    main()
