def main():
    a = list(reversed([int(input()) for _ in range(5)]))
    fib = [1, 1]
    i = 3
    cnt = 0
    while fib[-1] < 10 ** 15:
        fib.append(fib[-1] + fib[-2])
        i += 1
    answer = 0
    for _ in range(10):
        fib.append(-1)
    for index, val in enumerate(fib):
        cnt = 0
        for i in range(5):
            if a[i] != fib[index + i]:
                break
            cnt += 1
        answer = max(answer, cnt)
    answer = max(answer, cnt)
    print(answer)


if __name__ == '__main__':
    main()

