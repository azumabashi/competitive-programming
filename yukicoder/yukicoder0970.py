def main():
    length = int(input())
    output = [int(x) for x in input().split()]
    input_sum = sum(output)
    answer = [0 for _ in range(length)]
    for i in range(length):
        answer[i] = str(input_sum - (length - 1) * output[i])
    print(" ".join(answer))


if __name__ == '__main__':
    main()

