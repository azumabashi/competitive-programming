def main():
    queries = int(input())
    answer = [0 for _ in range(queries)]
    for i in range(queries):
        length, y, x = map(int, input().split())
        outer = min(x, y, length - x - 1, length - y - 1)
        answer[i] = length ** 2 - (length - 2 * outer) ** 2
        length -= outer * 2
        x -= outer
        y -= outer
        begin = [0, 0]
        if x - y < 0:
            begin = [length - 1, length - 1]
            answer[i] += 2 * (length - 1)
        answer[i] += abs(begin[0] - y) + abs(begin[1] - x)
    print("\n".join(map(str, answer)))


if __name__ == '__main__':
    main()

