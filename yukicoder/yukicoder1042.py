from math import log2


def main():
    p, q = map(int, input().split())
    upper = 10 ** 13
    lower = 0.0000000000001

    def solve_time_diff(t):
        return q * t * log2(t) - t ** 2 + p

    answer = 0
    for _ in range(10 ** 6):
        target = (upper + lower) / 2
        result = solve_time_diff(target)
        answer = target
        if result > 0:
            lower = target
        elif result < 0:
            upper = target
        else:
            answer = target
            break
    print(answer)


if __name__ == '__main__':
    main()

