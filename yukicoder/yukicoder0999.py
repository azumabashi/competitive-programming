def main():
    # 貪欲ではダメ，累積和を活用して全探索する．
    length = int(input())
    numbers = [int(x) for x in input().split()]
    diff = [0 for _ in range(length)]
    for i in range(length):
        diff[i] = numbers[2 * i + 1] - numbers[2 * i]
        if i > 0:
            diff[i] += diff[i - 1]
    answer = diff[-1]
    for i in range(length):
        answer = max(answer, diff[-1] - 2 * diff[i])
    print(answer)


if __name__ == '__main__':
    main()

