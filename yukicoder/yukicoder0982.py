from math import gcd


def main():
    a, b = map(int, input().split())
    good_numbers = set()
    gcd_of_ab = gcd(a, b)
    answer = -1
    if gcd_of_ab == 1:
        answer = 0
        lcm = a * b // gcd_of_ab
        for i in range(b + 1):
            for j in range(a + 1):
                now = i * a + j * b
                if lcm < now:
                    break
                good_numbers.add(now)
        for i in range(lcm + 1):
            if i not in good_numbers:
                answer += 1
    print(answer)


if __name__ == '__main__':
    main()

