def how_many_times_fold(length, thickness):
    result = 0
    while thickness < length:
        result += 1
        thickness *= 2
        length /= 2
    return result, thickness


def main():
    width, height, thickness = map(int, input().split())
    thickness /= 1000
    answer, thickness = how_many_times_fold(min(width, height), thickness)
    add, thickness = how_many_times_fold(max(width, height), thickness)
    answer += add
    print(answer)


if __name__ == '__main__':
    main()

