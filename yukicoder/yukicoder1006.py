def main():
    sum_of_a_and_b = int(input())
    answer = []
    min_value_diff = float("inf")
    value = [i for i in range(sum_of_a_and_b + 1)]
    for i in range(2, sum_of_a_and_b + 1):
        j = 1
        while i * j < sum_of_a_and_b + 1:
            value[i * j] -= 1
            j += 1
    for i in range(sum_of_a_and_b):
        now_value_diff = abs(value[i] - value[sum_of_a_and_b - i])
        if now_value_diff == min_value_diff:
            answer.append([i, sum_of_a_and_b - i])
        elif now_value_diff < min_value_diff:
            min_value_diff = now_value_diff
            answer = [[i, sum_of_a_and_b - i]]
    answer.sort(key=lambda x: x[0])
    for ans in answer:
        print(" ".join(map(str, ans)))


if __name__ == '__main__':
    main()

