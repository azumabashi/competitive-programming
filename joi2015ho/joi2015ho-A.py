def main():
    town, journey = map(int, input().split())
    visit = [0 for _ in range(town)]
    order = list(map(lambda x: int(x) - 1, input().split()))
    fare = [list(map(int, input().split())) for _ in range(town - 1)]
    for i in range(journey - 1):
        visit[min(order[i], order[i + 1])] += 1
        visit[max(order[i], order[i + 1])] -= 1
    for i in range(1, town):
        visit[i] += visit[i - 1]
    answer = 0
    for i in range(town - 1):
        answer += min(visit[i] * fare[i][0], visit[i] * fare[i][1] + fare[i][2])
    print(answer)


if __name__ == '__main__':
    main()

