from collections import deque


def solve(s):
    m = deque()
    k = deque()
    n = len(s)
    for i in range(n):
        if s[i] == "(":
            m.append(i)
        elif s[i] == "[":
            k.append(i)
        elif s[i] == ")":
            if not len(m):
                return False
            elif not len(k):
                _ = m.pop()
            else:
                l_m = m.pop()
                l_k = k.pop()
                if l_k > l_m:
                    return False
                k.append(l_k)
        elif s[i] == "]":
            if not len(k):
                return False
            elif not len(m):
                _ = k.pop()
            else:
                l_m = m.pop()
                l_k = k.pop()
                if l_k < l_m:
                    return False
                m.append(l_m)
    return len(k) == len(m) == 0


def main():
    while True:
        now = input()
        if now == ".":
            break
        print("yes" if solve(now) else "no")


if __name__ == '__main__':
    main()

