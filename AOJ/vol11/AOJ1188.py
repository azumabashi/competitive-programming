def election(x):
    q = []
    # print(x)
    left = 0
    f = False
    for i in range(len(x)):
        if x[i] not in {"[", "]"} and x[i - 1] in {"[", "]"}:
            left = i
        elif x[i] == "]" and left < i:
            tmp = x[left:i]
            if tmp[:2] == "##":
                f = True
                tmp = tmp[2:]
            q.append(int(tmp))
            left = len(x)
    q.sort()
    return "##" + str(sum([m // (1 if f else 2) + (1 if not f else 0) for m in q[:len(q) // 2 + 1]]))


def solve(x):
    while any(y in {"[", "]"} for y in x[1:-1]):
        idx = 0
        while True:
            if x[idx:idx + 2] == "[[" and x[idx + 2] != "[":
                left = idx
            elif x[idx:idx + 2] == "]]":
                x = x[:left] + "[" + election(x[left:idx + 2]) + "]" + x[idx + 2:]
                idx = 0
                break
            idx += 1
    print(x[3:-1])


def main():
    q = int(input())
    for _ in range(q):
        solve(input())


if __name__ == '__main__':
    main()
