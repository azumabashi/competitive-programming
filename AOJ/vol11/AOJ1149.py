def solve(n, w, h):
    cake = [[w, h]]
    for _ in range(n):
        p, s = map(int, input().split())
        p -= 1
        x, y = cake.pop(p)
        s %= (x + y)
        if 0 < s < x:
            cake.append([min(s, x - s), y])
            cake.append([max(s, x - s), y])
        else:
            cake.append([x, min(s - x, y - s + x)])
            cake.append([x, max(s - x, y - s + x)])
    res = [x * y for x, y in cake]
    res.sort()
    return res


def main():
    while True:
        n, w, h = map(int, input().split())
        if n + w + h == 0:
            break
        print(*solve(n, w, h))


if __name__ == '__main__':
    main()
