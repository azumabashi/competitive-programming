def main():
    n = 123456
    ans = [0] * (2 * n + 10)
    for i in range(2, 2 * n + 10):
        for j in range(2 * i, 2 * n + 10, i):
            ans[j] += 1
    cnt = [0] * (2 * n + 10)
    for i in range(1, 2 * n + 10):
        if ans[i] == 0:
            cnt[i] += 1
        cnt[i] += cnt[i - 1]
    while True:
        now = int(input())
        if now == 0:
            break
        print(cnt[2 * now] - cnt[now])


if __name__ == '__main__':
    main()
