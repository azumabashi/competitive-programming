def main():
    inf = 1 << 30
    max_val = 10 ** 6 + 100
    dp1 = [inf] * max_val
    dp2 = [inf] * max_val
    dp1[0] = 0
    dp2[0] = 0
    for i in range(1, 201):
        num = i * (i + 1) * (i + 2) // 6
        if num > max_val:
            break
        for j in range(num, max_val):
            if dp1[j] > dp1[j - num] + 1:
                dp1[j] = dp1[j - num] + 1
            if num % 2 == 1 and dp2[j] > dp2[j - num] + 1:
                dp2[j] = dp2[j - num] + 1
    while True:
        n = int(input())
        if not n:
            break
        print(dp1[n], dp2[n])


if __name__ == '__main__':
    main()
