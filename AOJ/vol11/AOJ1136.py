def make(is_require_rev=False):
    m = int(input())
    raw_target = [[int(k) for k in input().split()] for _ in range(m)]
    res = [[0, 0]]
    rev_res = [[0, 0]]
    for i in range(1, len(raw_target)):
        res.append([raw_target[i][k] - raw_target[0][k] for k in range(2)])
    if is_require_rev:
        raw_target = raw_target[::-1]
        for i in range(1, len(raw_target)):
            rev_res.append([raw_target[i][k] - raw_target[0][k] for k in range(2)])
    return m, res, rev_res


def solve(n):
    x, tar, rev_tar = make(is_require_rev=True)
    res = []
    for i in range(1, n + 1):
        m, com, _ = make()
        if x != m:
            continue
        for _ in range(4):
            if tar == com or rev_tar == com:
                res.append(str(i))
                break
            for j in range(m):
                com[j][0], com[j][1] = -com[j][1], com[j][0]
    return res


def main():
    while True:
        n = int(input())
        if not n:
            break
        res = solve(n)
        if res:
            print("\n".join(res))
        print("+++++")


if __name__ == '__main__':
    main()
