#include<bits/stdc++.h>
using namespace std;
#define rep(i, n) for (int i = 0; i < (int)(n); i++)

int lcs(string s, string t) {
  vector<vector<int>> dp(t.size() + 10, vector<int>(s.size() + 10));
  int delta = 0;
  rep(i, s.size()) {
    rep(j, t.size()) {
      delta = 0;
      if (s[i] == t[j]) delta = dp[i][j] + 1;
      dp[i + 1][j + 1] = max(delta, max(dp[i + 1][j], dp[i][j + 1]));
    }
  }
  return dp[s.size()][t.size()];
}

int main() {
  int q;
  cin >> q;
  string X, Y;
  rep(i, q) {
    cin >> X >> Y;
    cout << lcs(X, Y) << endl;
  }
  return 0;
}
