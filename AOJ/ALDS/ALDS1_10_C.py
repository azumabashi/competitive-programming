# AOJではTLE.


def lcs(s, t):
    dp = [[0 for _ in range(len(s) + 10)] for _ in range(len(t) + 10)]
    for i in range(len(s)):
        for j in range(len(t)):
            delta = dp[i][j] + 1 if s[i] == t[j] else 0
            dp[i + 1][j + 1] = max(delta, dp[i][j + 1], dp[i + 1][j])
    return dp[len(s)][len(t)]


def main():
    q = int(input())
    for _ in range(q):
        x = input()
        y = input()
        print(lcs(x, y))


if __name__ == '__main__':
    main()
