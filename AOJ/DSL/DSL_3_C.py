# いわば削っていくしゃくとりでないとACできない？


def main():
    n, q = map(int, input().split())
    a = list(map(int, input().split()))
    x = list(map(int, input().split()))
    for sum_upper in x:
        now_sum = 0
        left = 0
        ans = 0
        for right in range(n):
            now_sum += a[right]
            while now_sum > sum_upper:
                now_sum -= a[left]
                left += 1
            ans += right - left + 1
        print(ans)


if __name__ == '__main__':
    main()
