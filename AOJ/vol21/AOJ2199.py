"""
dp[i][j] := x_iをつくるためにc_jを選んだときの誤差総和の最小値，ではなく，
dp[i][j] := x_iがjになったときの誤差総和の最小値，とする必要がある

accepted by c++
"""


def solve(n, m):
    c = [int(input()) for _ in range(m)]
    x = [int(input()) for _ in range(n)]
    dp = [[1 << 63 for _ in range(260)] for _ in range(n + 1)]
    for i in range(260):
        dp[0][i] = 0
    for i in range(n):
        for j in range(260):
            for k in range(m):
                ny = max(0, min(255, j + c[k]))
                dp[i + 1][ny] = min(dp[i + 1][ny], dp[i][j] + (ny - x[i]) ** 2)
    return min(dp[-1])


def main():
    while True:
        n, m = map(int, input().split())
        if n == m == 0:
            break
        print(solve(n, m))


if __name__ == '__main__':
    main()
