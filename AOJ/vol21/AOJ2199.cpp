#include<bits/stdc++.h>
using namespace std;
#define rep(i, n) for (int i = 0; i < (int)(n); i++)
int inf = 1 << 30;

int solve(int n, int m){
  vector<int> c(m), x(n);
  rep(i, m) cin >> c[i];
  rep(i, n) cin >> x[i];
  vector<vector<int>> dp(n + 1, vector<int>(260, inf));
  dp[0][128] = 0;
  rep(i, n){
    rep(j, 260){
      rep(k, m){
	int ny = max(0, min(255, j + c[k]));
	dp[i + 1][ny] = min(dp[i + 1][ny], dp[i][j] + (ny - x[i]) * (ny - x[i]));
      }
    }
  }
  int res = inf;
  rep(i, 260) res = min(res, dp[n][i]);
  return res;
}

int main(){
  int n, m;
  while (true) {
    cin >> n >> m;
    if (n + m == 0) break;
    else cout << solve(n, m) << endl;
  }
  return 0;
}
