# ダイクストラ法の練習に．
from heapq import heapify, heappop, heappush


def main():
    vertex, edge, root = map(int, input().split())
    max_val = 10 ** 10
    graph = [[] for _ in range(vertex)]
    for _ in range(edge):
        s, t, d = map(int, input().split())
        graph[s].append((t, d))
    dist = [max_val for _ in range(vertex)]
    dist[root] = 0
    q = []
    heapify(q)
    heappush(q, (0, root))
    while q:
        now_d, now_v = heappop(q)
        for next_v, length in graph[now_v]:
            now_dist = dist[now_v] + length
            if dist[next_v] > now_dist:
                dist[next_v] = now_dist
                heappush(q, (dist[next_v], next_v))

    for d in dist:
        print("INF" if d == max_val else d)


if __name__ == '__main__':
    main()

