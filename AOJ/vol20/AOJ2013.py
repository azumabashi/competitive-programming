def time_to_sec(t):
    return 3600 * int(t[0]) + 60 * int(t[1]) + int(t[2])


def solve(n):
    length = 87000
    cnt = [0] * length
    for _ in range(n):
        begin, end = [x.split(":") for x in input().split()]
        cnt[time_to_sec(begin)] += 1
        cnt[time_to_sec(end)] -= 1
    for i in range(1, length):
        cnt[i] += cnt[i - 1]
    print(max(cnt))


def main():
    while True:
        n = int(input())
        if not n:
            break
        solve(n)


if __name__ == '__main__':
    main()
