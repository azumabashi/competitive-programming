def main():
    preliminaries, selected_people = map(int, input().split())
    print(preliminaries * selected_people)


if __name__ == '__main__':
    main()

