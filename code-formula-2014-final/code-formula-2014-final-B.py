def main():
    days = int(input())
    even_days = days // 2
    odd_days = days - even_days
    print(odd_days * 3 - 2 * even_days)


if __name__ == '__main__':
    main()

