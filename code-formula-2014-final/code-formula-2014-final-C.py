def main():
    message = input()
    message_to = set()
    begin_index = -1
    for i in range(len(message) - 1):
        if message[i] == "@" and message[i + 1] != "@" and message[i + 1] != " ":
            if begin_index == -1:
                begin_index = i
            else:
                message_to.add(message[begin_index + 1:i])
                begin_index = i
        elif message[i] == "@" and 0 <= begin_index:
            message_to.add(message[begin_index + 1:i])
            begin_index = i
        elif message[i] == " " and 0 <= begin_index:
            message_to.add(message[begin_index + 1:i])
            begin_index = -1
    if 0 <= begin_index:
        if message[-1] == "@":
            message_to.add(message[begin_index + 1:-1])
        else:
            message_to.add(message[begin_index + 1:])
    message_to = list(sorted(message_to))
    for t in message_to:
        if t != "":
            print(t)


if __name__ == '__main__':
    main()

