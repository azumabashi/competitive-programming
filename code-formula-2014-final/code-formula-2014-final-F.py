def main():
    ans = [[0, 0] for _ in range(100)]
    x = 0
    y = 0
    for r in range(1, 101):
        if y + 2 * r >= 1500:
            x += 200
            y = 2 * r
        else:
            y += 2 * r
        ans[r - 1] = [x + r, y - r]
    for a in ans:
        print(*a)


if __name__ == '__main__':
    main()
